<?php

namespace Csifo\Client\Minify;

class Css extends Minify implements Interfaces\ICss {
	
	protected $minifiedCSS = '';

	protected $intyTags;
	protected $intyInt = 0;
	
	public function handleCss($file){

		$css = file_get_contents($file);
		if(!$this->config->app('DEV_MODE')){
			//get Views
			$viewFilesPattern = preg_replace('#\/client[\/\w.]+#','/views/*view.php',$file);
			$views = glob($viewFilesPattern);
			foreach($views as $view){
				$viewHtml = file_get_contents($view);
				$css = $this->Inty($css,$viewHtml,$view);
			}
		}

		//REAL Minify
		$this->minifiedCSS .= $this->minify($css);
	}
	
	public function minifyModules(){
		
		$paths = [
			$this->config->app('MODULES_DIR') . '/*/client/*.css',
			$this->config->app('TEMPLATE_DIR') . '/html/*.css',
		];
		
		foreach($paths as $path){
			$files = glob($path);
			foreach($files as $file){
				if(stripos($file,'min.css') === false)
					$this->handleCss($file);
			}
		}
		
		file_put_contents($this->config->app('PUBLIC_DIR') . '/assets/operett.css',$this->minifiedCSS);
	}
	
	protected function fixWhiteSpaceSensitive($css){
		$patterns = [
			'(:[ ]*\d(?:px)?([ ]*[\w]+?){1,5}?;)',//Margins,padding,color words,etc...
			'(content:[\S\s]*?;)' //Content
		];
		
		foreach($patterns as $pattern){
			if(preg_match_all('#' . $pattern . '#i',$css,$matches)){
				if(!empty($matches[1])){
					foreach($matches[1] as $match){
						$replaced = str_replace([': ',' ',"\t"],[':',$this->spaceMark,'--[TAB]--'],$match);
						$css = str_replace($match,$replaced,$css);
					}
				}
			}
		}
		
		return $css;
	}
	
	protected function minify($css){
		//(?:^[\S\s]*?|}[\S\s]*?)((?:\#|\.|body|ul|li|div|a|button|input|nav|img|p|span|select|option|submit|footer|h.*?)[\S\s]+?{)
		preg_match_all('#(?:^[\S\s]*?|}[\S\s]*?)((?:\#|\.|[\w])[\S\s]+?{)#i',$css,$matches);

		$css = $this->fixWhiteSpaceSensitive($css);
		
		if(!empty($matches[1])){
			foreach($matches[1] as $v){
				$temp = str_replace(' ',$this->spaceMark,$v);
				$css = str_replace($v,$temp,$css);
			}
		}

		$css = preg_replace([
			'#(\#[\w]+)[ ]*([\d]+%)#i',
			//'#(border[\w-]*?:[ ]*[\d](?:px|%))[ ]*([\w]+;)#i',
			'#([\d]+(?:%|px))[ ](?!;)#i',
		],[
			'$1' . $this->spaceMark . '$2',
			//'$1' . $this->spaceMark . '$2',
			'$1' . $this->spaceMark . '$2',
		],
			$css
		);

		$css = preg_replace('#(\/\*((.*?)|[\S\s]+?)\*\/)|([\n\r]+)|([ ]+(?:(?![0-9]%)))|([\t])#i','',$css);
		$css = preg_replace('#(' . $this->spaceMarkRegex . ')#i',' ',$css);
		$css = preg_replace('#(--\[TAB\]--)#i',chr(9),$css);
		$css = preg_replace('#[ ]*({)#i','$1',$css);
		
		
		//Ha maradt �res tag le�r�s,azt is kiszedj�k
		$css = preg_replace('#([\#\.]([a-zA-Z0-9_-]+){})#i','',$css);
		
		return $css;
	}
	
	protected function Inty($css,$viewHtml,$originalFilePath){
		if(preg_match_all('#id=[\'"]([^\'"]*?)[\'"]|class=[\'"]([^\'"]*?)[\'"]#i',$viewHtml,$selectors) != 0){
			//ID-k
			$ids = array_unique($selectors[1]);
			foreach($ids as $id){
				if($id != "" && preg_match('#\#' . $id . '#i',$css) != 0){
					$css = preg_replace('#\#' . $id . '(\b)#i','#i' . $this->intyInt . '$1',$css);
					$viewHtml = preg_replace('#id=[\'"](.*?\b)' . $id . '(\b.*?)[\'"]#i','id="$1 i' . $this->intyInt . ' $2"',$viewHtml);
					$this->intyTags[$id] = $this->intyInt;
				}
				$this->intyInt++;
			}

			//Class-ok
			$classes = array_unique($selectors[2]);
			foreach($classes as $class){
				$real_classes = explode(' ',$class);
				foreach($real_classes as $c){
					if($c != "" && preg_match('#\.' . $c . '#i',$css) != 0){
						$css = preg_replace('#\.' . $c . '(\b)#i','.c' . $this->intyInt . '$1',$css);
						$viewHtml = preg_replace('#class=[\'"](.*?\b)' . $c . '(\b.*?)[\'"]#i','class="$1c' . $this->intyInt . '$2"',$viewHtml);
					}
					$this->intyInt++;
				}
			}
		}

		$viewHtml = preg_replace('#id=" ([\S\s]+?) "#i','id="$1"',$viewHtml);
		$viewHtml = preg_replace('#class=" ([\S\s]+?) "#i','class="$1"',$viewHtml);
		
		$cachePath = $this->config->app('CACHE_DIR');
		file_put_contents($cachePath . '/IntyView_' . hash("crc32b", $originalFilePath),$viewHtml);
		
		return $css;
	}
	
}