<?php

namespace Csifo\Client\Minify\Interfaces;

interface IJavascript {
	
	public function minifyModules();
}