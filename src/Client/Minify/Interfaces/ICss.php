<?php

namespace Csifo\Client\Minify\Interfaces;

interface ICss {
	
	public function minifyModules();
	
}