<?php

namespace Csifo\Client\Minify;

use Csifo\Client\Minify\Interfaces\IJavascript as IJavascript;

class Javascript extends Minify implements IJavascript {
	
	protected $minifiedJs = '';
	
	protected function handleJs($file){
		
		$js = file_get_contents($file);
					
		preg_match_all('#(RegExp\([\S\s]*?\);)#i',$js,$pm);
		if(empty($pm[1]) === false){
			foreach($pm[1] as $val){
				$rtemp = str_replace(' ',$this->spaceMark,$val);
				$js = str_replace($val,$rtemp,$js);
			}
		}
		
		$js = preg_replace([
			'#((?<!:)\/\/.*)|(\/\*((.*?)|[\S\s]+?)\*\/)|([\n\r]+)|([\t])#i',
			'#\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s+#i', //<--- https://gist.github.com/tovic/d7b310dea3b33e4732c0
		],[
			'',
			'$1'
		],$js);
		
		preg_match_all('#=[ ]*([\'\"][\S\s]+?[\'\"]);#i',$js,$jsm);
		if(empty($jsm[1]) === false){
			foreach($jsm[1] as $val){
				$temp = str_replace(' ',$this->spaceMark,$val);
				$js = str_replace($val,$temp,$js);
			}
		}

		$js = preg_replace([
			'#if\((.*)\)[{]?return[\s]+true;[}]?else[{]?return[\s]+false;[}]?#i',
			/*'#true\b#i','#false\bi#',*/'#return\b#i', //<--- https://gist.github.com/tovic/d7b310dea3b33e4732c0
			'#\s(\))#i'
		],[
			'$1?!1:!0',
			/*'!1','!0',*/'return',
			'$1'
		],$js);

		$js = str_replace(array(' + ',' = ',' == ',' === ',': ','elsereturn'),array('+','=','==','===',':','else return'),$js);
		$js = preg_replace('#(' . $this->spaceMarkRegex . ')#i',' ',$js);

		$this->minifiedJs .= $js;
	}
	
	public function minifyModules(){
		$paths = [
			$this->config->app('MODULES_DIR') . '/*/client/*.js',
			$this->config->app('TEMPLATE_DIR') . '/html/*.js',
		];
		
		foreach($paths as $path){
			$files = glob($path);
			foreach($files as $file){
				if(stripos($file,'min.js') === false)
					$this->handleJs($file);
			}
		}
		
		file_put_contents($this->config->app('PUBLIC_DIR') . '/assets/operett.js',$this->minifiedJs);
	}
}