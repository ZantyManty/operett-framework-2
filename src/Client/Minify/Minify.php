<?php

namespace Csifo\Client\Minify;

use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Helpers\Interfaces\IFileHelper as IFileHelper;

class Minify {
	
	protected $config;
	protected $fileHelper;
	
	/* --Special tags-- */
	protected $spaceMark = '--[SPC]--';
	protected $spaceMarkRegex = '--\[SPC\]--';
	
	public function __construct(IFileHelper $fileHelper,IConfig $config){
		$this->config	  = $config;
		$this->fileHelper = $fileHelper;
	}
}