<?php

namespace Csifo\File\Interfaces\Ext;

use Csifo\File\Interfaces\IFile as IFile;

interface IMp3 extends IFile {
	
	public function getID3Tags();
	
}