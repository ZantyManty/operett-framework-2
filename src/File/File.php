<?php

namespace Csifo\File;

use Csifo\File\Interfaces\IFile as IFile;
use \zipArchive as zipArchive;

class File implements IFile {
	
	protected $path = '';
	
	public function __construct($path){
		$this->path = $path;
	}
	
	public function size(){
		return filesize($this->path);
	}
	
	public function lastModif($format = ''){
		$format = ($format === '') ? 'Y-m-d H:i:s' : $format;
		return date($format,filemtime($this->path));
	}
	
	public function zip(){
		$zip = new zipArchive;
		if($zip->open($this->path . '_' . date('Ymd_his') . '_' . time() . '.zip', ZipArchive::CREATE) === TRUE) {
			$zip->addFile($this->path);
			$zip->close();
			return true;
		}
		else{
			return false;
		}
	}
	
	public function gZip(){
		$gzip = gzopen($this->path . '.gz','wb');
		if($gzip){
			$file_stream = fopen($this->path,'r');
			while(!feof($file_stream)){
				$row = fgets($file_stream);
				gzwrite($gzip,$row);
			}
			fclose($file_stream);
			gzclose($gzip);
		} else {
			return false;
		}
	}
	
}