<?php

namespace Csifo\File\Ext;


use Csifo\File\Interfaces\Ext\IMp3 as IMp3;
use Csifo\File\Interfaces\IFile as IFile;
use Csifo\File\File as File;
use Csifo\Tools\LanguageCodes as LanguageCodes;

class Mp3 extends File implements IMp3 {
	
	protected $id3Tags = [
		'TIT2' => 'Title',
		'TALB' => 'Album',
		'TPE1' => 'Artist',
		'TYER' => 'Year',
		'TLEN' => 'Length',
		'COMM' => 'Comment',
		'APIC' => 'AlbumArt',
		'TCON' => 'GenreCode'
	];
	
	protected $blackList = ['%'];
	
	public function __construct($path){
		/*
		* Path to the mp3 file
		*/
		$this->path = $path;
	}
	
	protected function fetchAlbumCover($data,$mp3File){
		$coverFile = explode('/',$mp3File);
		$ext = (preg_match('#image\/([\w]+?)[ \x00]#i',$data,$extMatch) === 1) ? $extMatch[1] : 'jpg';
		$coverFile = str_replace(['.',' '],['','_'],end($coverFile)) . '_cover.' . $ext;
		$path = preg_replace('#/[\w_���������-]+?\.[\w]+?$#i','',$this->path);
		$data = str_replace('APIC','',$data);
		$dataLength = strlen($data);
		$JFIFpos = strpos($data,'JFIF') - 6;

		/*
		* We don't need the first 19 byte...
		*/
		$newData = '';
		$JFIFreached = false;
		for($i = 0; $i < $dataLength;$i++){
			if($i == $JFIFpos || $JFIFreached){
				$newData .= $data[$i];
				$JFIFreached = true;
			}
		}
		
		file_put_contents($path .  '/' .$coverFile,$newData);
		
		return resolveWithParams('Csifo\File\Interfaces\Ext\IImage',['path' => $path . '/' . $coverFile]);
	}
	
	public function getID3Tags(){

		$size = $this->size($this->path);
		/*
		* Open the mp3 file
		*/
		$h = fopen($this->path,'r');
		/*
		* Read file content
		*/
		$mp3string = fread($h,$size);
		$count = count($this->id3Tags);
		$meta = [];

		foreach($this->id3Tags as $key => $value){
			/*
			* If we found the current tag in the file content
			*/
			if(strpos($mp3string,$key) !== false){
				/*
				* Set the needle before
				* E.g.: TALB\x00 (A NULL character follows EVERY tag!)
				*/
				$pos = strpos($mp3string,$key . chr(0));
				/*
				* Puts needle after "TALB\x00",and gets the 3 bits after it.
				* The 3 bit holds the tags length
				*/
				$tag_length = hexdec(bin2hex(substr($mp3string,($pos + 5),3))) + 1;
				/*
				* Puts the needle after the length data (5 + 3) + 1
				* E.g.: TALB\x00YYY (YYY represents the 3 bit)
				*/
				$data = substr($mp3string, $pos, 9 + $tag_length);
				$data_length = strlen($data);
				
				/*
				* If it's not the Album cover image
				*/
				if($key != 'APIC'){
					$s = '';
					for ($a = 0; $a < $data_length; $a++) {
						$char = $data[$a];
						/*
						* Range of valid characters,what we accept
						*/
						if($char >= chr(32) && $char <= chr(126) && !in_array($char,$this->blackList))
							$s .= $char;
					}
					/*
					* Strip the tag itself
					*/
					$s = str_replace($key,'',$s);
				} else {
					/*
					* Album cover pic
					*/
					$s = $this->fetchAlbumCover($data,$this->path);//$coverFile;
				}
				
				/** Genre **/
				if($key == 'TCON'){
					$code = str_replace(['(',')'],'',$s);
					if(preg_match('#[\d]{2,4}#i',$code)){
						$meta[$value] = Mp3\Genres::getGenreByCode($code);
					} else {
						$meta[$value] = $s;
					}
				}
				/** Comment **/
				else if($key == 'COMM'){
					$langCode = substr($s,0,3);
					if($language = LanguageCodes::getLangByCode($langCode) != 'Unknown'){
						$meta['Comment_language'] = $language;
						$s = str_replace($langCode,'',$s);
					}
					$meta[$value] = $s;
				} else {
					$meta[$value] = $s;
				}
			}
		} //Cycle end
		
		return $meta;
	}
	
}