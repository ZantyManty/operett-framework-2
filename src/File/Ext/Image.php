<?php

namespace Csifo\File\Ext;

use Csifo\File\File as File;
use Csifo\File\Interfaces\Ext\IImage as IImage;
use Csifo\Tools\ImageFilters as Filter;

class Image extends File implements IImage {
	
	protected $image = null;
	protected $type;
	
	public function __construct($path){
		$this->path = $path;
		$this->type = exif_imagetype($path);
		switch($this->type){
			case IMAGETYPE_JPEG:
				$this->image = imagecreatefromjpeg($path);
				break;
			case IMAGETYPE_PNG:
				$this->image = imagecreatefrompng($path);
				break;
			case IMAGETYPE_GIF:
				$this->image = imagecreatefromgif($path);
				break;
		}
	}
	
	public function filter($type){
		$type = strtolower($type);
		$this->image = Filter::$type($this->image);
		return $this;
	}
	
	public function resize($size_arr){
		$height = imagesy($this->image);
		$width = imagesx($this->image);
		
		//Custom width and height
		if(isset($size_arr['width']) && isset($size_arr['height'])){
			$n_width  = $size_arr['width'];
			$n_height = $size_arr['height'];
		}else if(isset($size_arr['width'])){ //Width
			$n_width = $size_arr['width'];
			$n_height = ($height * $size_arr['width']) / $width;
		} else if(isset($size_arr['height'])){ //Height
			$n_width = ($width * $size_arr['height']) / $height;
			$n_height = $size_arr['height'];
		} else if(isset($size_arr['percent'])){ //Percent
			$n_width = ($width / 100) * $size_arr['percent'];
			$n_height = ($height / 100) * $size_arr['percent'];
		}
		
		$temp = imagecreatetruecolor($n_width,$n_height);
		
		imagecopyresampled($temp,$this->image,0,0,0,0,$n_width,$n_height,$width,$height);
		$this->image = $temp;
		return $this;
	}
	
	public function save($file = ''){

		if(stripos($file,'.') !== false) $file = preg_replace('#\.[\w_���������-]+?$#i','',$file);
		
		$path = preg_replace('#\.[\w_���������-]+?$#','',$this->path);
		$path .= ($file !== '') ? $file : '_modified.';
	
		switch($this->type){
			case IMAGETYPE_JPEG:
				$path = $path . 'jpg';
				 imagejpeg($this->image,$path,100);
				break;
			case IMAGETYPE_PNG:
				$path = $path . 'png';
				imagepng($this->image,$path,9);
				break;
			case IMAGETYPE_GIF:
				$path = $path . 'gif';
				imagegif($this->image,$path,9);
				break;
		}
		
		$this->path = $path;
		
		//imagedestroy($this->image);
		//unset($path);
		//return $file;
		
		return $this;
	}
	
}