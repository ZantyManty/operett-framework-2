<?php

namespace Csifo\Tools\SzinkronJet;

use Csifo\Core\Interfaces\IConfig;
use Csifo\Helpers\Interfaces\IFileHelper;

use Csifo\Tools\SzinkronJet\Creator;

class SzinkronJet {
	
	protected $config = null;
	protected $fileHelper = null;
	
	protected $rootNamespace = 'Csifo';
	
	protected $frameWorkHashes = [];
	
	public function __construct(IConfig $config,IFileHelper $fileHelper){
		$this->config = $config;
		$this->fileHelper = $fileHelper;
	}
	
	public function start(){
		//return;
		$this->loadMuiPackages();
	}
	
	protected function loadMuiPackages(){
		$dir = $this->config->app('MUI_DIR') . '/';
		$lang = ucfirst(strtolower($this->config->app('MUI_LANG')));
		$dir .= $lang;
		
		//$fileList = glob($dir . '/*/*.{php}', GLOB_BRACE);
		$fileList = $this->getFiles($dir,'#.*\.(php)$#iu');
		
		foreach($fileList as $muiFile){
			$this->buildMetaForClass($muiFile);
		}
		

		$creator = (new Creator($this->frameWorkHashes,$this->rootNamespace,$this->fileHelper,$lang))
					->createInterfaceMasks();
					
		$replacer = (new Replacer($this->frameWorkHashes))->start();

		//var_dump($this->frameWorkHashes);
	}
	
	/*
	* Source: 
	*/
	protected function getFiles($folder,$pattern){
		$dir = new \RecursiveDirectoryIterator($folder);
		$ite = new \RecursiveIteratorIterator($dir);
		$files = new \RegexIterator($ite, $pattern, \RegexIterator::GET_MATCH);
		$fileList = array();
		foreach($files as $file) {
			$fileList[] = $file[0];
		}
		return $fileList;
	}
	
	protected function buildMetaForClass($full_file_path){
		$langArray = include $full_file_path;
		if(!empty($langArray) && isset($langArray['class']) && isset($langArray['methods']) && isset($langArray['namespace'])){
			
			/*
			* Class name (And this rule for namespaces,and methods TOO!!!)
			*
			* Key: Foreign lang (E.g.: Hungarian)
			* Value: Original English lang's HASHED name
			*/
			$value_class = reset($langArray['class']);
			$key_class = key($langArray['class']);
			
			/*
			* Namespace
			*/
			$value_ns = reset($langArray['namespace']);
			$key_ns = key($langArray['namespace']);
			
			$foreign_namespace_pieces = explode("\\",$value_ns);
			
			//Creating regex pattern
			$namespace_pattern = "(";
			foreach($foreign_namespace_pieces as $piece){
				$namespace_pattern .= "(?:\\\\?" . trim($piece) . ")?";
			}
			$namespace_pattern = rtrim($namespace_pattern,'?');
			$namespace_pattern .= ")";

			/*
			* Array init
			*/
			$original_full_namesapce = $this->rootNamespace . '\\' . $key_ns;
			$normal_ns = $original_full_namesapce . '\\' . $key_class;
			$this->frameWorkHashes[$normal_ns] = [];
			
			/*
			* After array init,we add the ENGLISH class hash
			*/
			$this->frameWorkHashes[$normal_ns]['class_hash'][$value_class] = 'c_' . $this->makeHash($key_class);
			/*
			* If has a parent class
			*/
			if(isset($langArray['extends']))
				$this->frameWorkHashes[$normal_ns]['extends'] = $langArray['extends'];
			
			/*
			* ..and the namespace REGEX pattern
			*/
			$this->frameWorkHashes[$normal_ns]['namespace']['pattern'] = $namespace_pattern;
			
			/*
			* -----------------------Create hashes from the original ENGLISH namespace pieces
			*/
			
			/*
			* Some edit on the exploded foreign language namespace
			*/
			/*unset($foreign_namespace_pieces[1]); // Mui
			$foreign_namespace_pieces[0] = $this->rootNamespace; // Foreign lang code (e.g.: Hu) -> Csifo
			$foreign_namespace_pieces = array_values($foreign_namespace_pieces);
			
			$full_ns_pieces = explode('\\',$original_full_namesapce);
			foreach($full_ns_pieces as $index => $full_ns_piece){
				var_Dump($full_ns_piece);
				$this->frameWorkHashes[$normal_ns]['namespace']['hash'][$foreign_namespace_pieces[$index]] = 'n_' . $this->makeHash($full_ns_piece);
			}*/
			
			$this->createNamespaceHashes($normal_ns,$foreign_namespace_pieces);
		
			foreach($langArray['methods'] as $eng => $foreign){
				$this->frameWorkHashes[$normal_ns]['methods']['hashes'][$foreign] = 'm_' . $this->makeHash($eng);
			}
			
			$this->frameWorkHashes[$normal_ns]['methods']['dictionary'] = $langArray['methods'];
		}
	}
	
	protected function createNamespaceHashes($normal_ns,$foreign_namespace_pieces){
		foreach($foreign_namespace_pieces as $piece){
			$this->frameWorkHashes[$normal_ns]['namespace']['hash'][$piece] = 'n_' . $this->makeHash($piece);
		}
	}
	
	protected function makeHash($string){
		return md5($string);
	}
	
}