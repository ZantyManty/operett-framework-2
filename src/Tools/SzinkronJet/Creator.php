<?php

namespace Csifo\Tools\SzinkronJet;

class Creator {
	
	protected $metaData;
	protected $rootNamespace;
	protected $fileHelper;
	protected $coreNamespace = 'Csifo\\';
	/*
	* Cache namespace
	*/
	protected $cacheNamespace = 'Cache\\Szinkron\\Framework\\';
	
	public function __construct($metaData,$rootNamespace,$fileHelper,$lang){
		$this->metaData = $metaData;
		$this->rootNamespace = $rootNamespace;
		$this->fileHelper = $fileHelper;
		//$this->cacheNamespace .= ucfirst($lang) . '\\';
		//dd($this->metaData);
	}
	
	protected function getParametersAsString($fullNamespace,$method){
		$content = $this->openPhpFile($fullNamespace);
		if(preg_match('#public function ' . trim($method) . '\(([\S\s]*?)\);(?:[\s]*(?:(?:public|protected|private)|}[\s]*$))#iu',$content,$match)){
			return trim($match[1]);
		}
		
	}
	
	protected function openPhpFile($full_namespace){
		$path = str_replace('\\','/',$full_namespace);
		$path = str_replace($this->rootNamespace,'src',$path);
		return file_get_contents(ROOT . '/' . $path . '.php');
	}
	
	protected function collectUsedNamespaces($full_namespace){
		$file_content = $this->openPhpFile($full_namespace);
		
		if(preg_match_all('#(use \\\\?[^;]+;)#iu',$file_content,$m))
			return implode("\r\n",$m[1]) . PHP_EOL . PHP_EOL;
		else
			return '';
	}
	
	protected function cleanArgs($args){
		if(preg_match_all('#(\$[\w_-]+)#iu',$args,$arguments)){
			return implode(',',$arguments[1]);
		} else {
			return $args;
		}
	}
	
	protected function createMethods($methods,$full_namespace,$class_hash){
		$container = '';
		$foreign_to_eng_dict = array_flip($methods['dictionary']);

		foreach($methods['hashes'] as $foreign_name => $hash){
			$original_method_name = $foreign_to_eng_dict[$foreign_name];
			
			$container .= chr(9) . 'public function ' . $hash . '(';
				$args = $this->getParametersAsString($full_namespace,$foreign_to_eng_dict[$foreign_name]);
				$container .= $args;
			$container .= '){' . PHP_EOL;
				$cleanedArgs = $this->cleanArgs($args);
				$container .= chr(9) . chr(9) . 'return $this->' . $class_hash . '_obj->' . $original_method_name . '(' . $cleanedArgs . ');' . PHP_EOL;
			$container .= chr(9) . '}' . PHP_EOL;
			
			/*
			* ---------------------------
			*	Mivel "önmagát" is implementálnunk kell,hogy működjenek a hash-elt maszkok,ezért kvázi fordítva is létre kell hozni a dolgokat.
			*	Szóval,ha a Request osztály ból van hash-elt maszkod,aminek ugye "m_{MD5HASH}" a neve,ezért nem felel meg az önmaga interface-ének,ergo
			*	a rendes rendszer nem fogja bevenni. Ezért kell létrehozni kamuból a rendes metódusokat,amik csak visszatérnek a hash-elt metódus eredményekkel
			* ---------------------------
			*/
			
			$container .= chr(9) . 'public function ' . $original_method_name . '(';
				$args = $this->getParametersAsString($full_namespace,$foreign_to_eng_dict[$foreign_name]);
				$container .= $args;
			$container .= '){' . PHP_EOL;
				$cleanedArgs = $this->cleanArgs($args);
				$container .= chr(9) . chr(9) . 'return $this->' . $hash . '(' . $cleanedArgs . ');' . PHP_EOL;
			$container .= chr(9) . '}' . PHP_EOL;
		}
		
		return $container;
	}
	
	public function createInterfaceMasks(){
		foreach($this->metaData as $full_namespace => $meta){
			$fileContent = '<?php ' . PHP_EOL;

			$full_cache_namespace = $this->cacheNamespace . implode('\\' , $meta['namespace']['hash']);
			$fileContent .= 'namespace ' . $full_cache_namespace . ';' . PHP_EOL . PHP_EOL;
			
			/**
			* Check used namespaces...
			*/
			$fileContent .= $this->collectUsedNamespaces($full_namespace);
			
			/**
			* ...for the extended interface TOO!
			*/
			$extends = false;
			if(isset($meta['extends']) && !empty($meta['extends'])){
				$extend_class = reset($meta['extends']);
				$extend_interface = $this->rootNamespace . '\\' .key($meta['extends']);
				$extends = true;
				$fileContent .= $this->collectUsedNamespaces($extend_interface);
				//$fileContent .=  'use ' . $this->coreNamespace . $extend_class . ';';
			}
			
			$class_hash = reset($meta['class_hash']);
			
			$fileContent .= 'class ' . $class_hash . ' ';
			
			/*
			* Ha van extendelt osztály,belerakjuk
			*/
			if($extends){
				
				$fileContent .= 'extends \\' . $this->coreNamespace . $extend_class . ' ';
			}

			$fileContent .= 'implements \\' . $full_namespace . ' {' . PHP_EOL . PHP_EOL;
			$fileContent .= chr(9) .'protected $' . $class_hash . '_obj = null;' . PHP_EOL . PHP_EOL;
			
			/**
			* Create Contruct
			*/
			$fileContent .= chr(9). 'public function __construct(\\' . $full_namespace . ' $obj){' . PHP_EOL;
				$fileContent .= chr(9) . chr(9) . '$this->' . $class_hash . '_obj = $obj;' . PHP_EOL;
			$fileContent .= chr(9) . '}' . PHP_EOL . PHP_EOL;
			
			/*
			* Create Methods
			*/
			$fileContent .= $this->createMethods($meta['methods'],$full_namespace,$class_hash);
			
			/*
			* Az extendelt osztály metódusait is bedobjuk,
			* mivel ha a sima gazda osztály extend-eljük,annak nincsenek hash-elt osztályai,
			* és uyge a rendes osztályt nem szeretnénk csesztetni :D
			*/
			if($extends){
				$fileContent .= '/*------------------------- Extended class\' methods -------------------------------*/' . PHP_EOL . PHP_EOL;
				$extended_class_methods = $this->metaData[$extend_interface]['methods'];
				$fileContent .= $this->createMethods($extended_class_methods,$extend_interface,$class_hash);
			}
			
			$fileContent .= '}';
			
			/*
			* Create the PHP file
			*/
			$this->createPhpFile($fileContent,$full_cache_namespace,$class_hash);
			
		}
	}
	
	protected function createPhpFile($fileContent,$full_cache_namespace,$class_hash){
		$cachePath = str_replace('\\','/',$full_cache_namespace);
		$this->fileHelper->createDir($cachePath);
		file_put_contents(ROOT . '/' . $cachePath . '/' . $class_hash . '.php',$fileContent);
	}
	
}