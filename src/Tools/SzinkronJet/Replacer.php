<?php 

namespace Csifo\Tools\SzinkronJet;

class Replacer {
	
	protected $metaData = [];
	
	protected $cacheNamespace = 'Cache\\Szinkron\\Framework\\';
	
	public function __construct($metaData){
		$this->metaData = $metaData;
	}
	
	public function start(){
		$files = [
			ROOT . '/App/Modules/Login/Login.controller_§1.php'
		];
		
		foreach($files as $file){
			$this->modifyWithFramework($file);
		}
	}
	
	protected function createHash($string){
		return md5($string);
	}
	
	protected function replaceNamespaces($content,$namespace_meta,$foreign_class_name,$class_hash){
		if(preg_match_all('#' . $namespace_meta['pattern'] . '\\\\?(' . $foreign_class_name . ') as ([^;]+?);#iu',$content,$namespaces)){

			foreach($namespaces[1] as $index => $namespace){
				
				$full_ns = str_replace('\\','\\\\',$namespaces[0][$index]);
				
				$namespace_pieces = explode('\\',$namespace);
				
				$namespace_hash_string = $this->cacheNamespace . '\\';
				foreach($namespace_pieces as $piece){
					$namespace_hash_string .= $namespace_meta['hash'][$piece] . '\\';
				}
				$namespace_hash_string .=  '\\' . $class_hash;
				
				/*
				* ha van "as",akkor azzal dolgozunk
				*/
				if(isset($namespaces[3][$index]) && !empty($namespaces[3][$index])){
					$replace_with = 'ns_as_' . $this->createHash($namespaces[3][$index]);
					$content = preg_replace('#(as )?' . $namespaces[3][$index] . ' *?(;|\$|->|::|\(|;)#u','$1'.$replace_with . ' $2',$content);
					$full_ns = str_replace(' as ' . $namespaces[3][$index] . ';','',$full_ns);
				}
				
				$content = preg_replace('#use +' . $full_ns . '#iu','use ' . $namespace_hash_string,$content);
			}
		}
		
		return $content;
	}
	
	protected function checkMethods($content,$methods_meta){
		foreach($methods_meta['hashes'] as $foreign_method_name => $hash){
			$content = preg_replace('#(->|::)' . $foreign_method_name . '\(#u','$1' . $hash . '(',$content);
		}
		return $content;
	}
	
	public function modifyWithFramework($full_file_path){
		$content = file_get_contents($full_file_path);
		
		foreach($this->metaData as $meta){
			$class_hash = reset($meta['class_hash']);
			$foreign_class_name = key($meta['class_hash']);
			
			/*
			* Check namespaces
			*/
			$content = $this->replaceNamespaces($content,$meta['namespace'],$foreign_class_name,$class_hash);
			
			/*
			* Check methods
			*/
			$content = $this->checkMethods($content,$meta['methods']);
		}
		$path = str_replace('controller','controller_rep',$full_file_path);
		file_put_contents($path,$content);
		
	}
	
}














