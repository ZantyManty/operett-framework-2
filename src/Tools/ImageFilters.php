<?php

namespace Csifo\Tools;

class ImageFilters {
	
	public static function sepia($image){
		imagefilter($image,IMG_FILTER_GRAYSCALE);
		imagefilter($image,IMG_FILTER_COLORIZE, 112, 66, 20);
		imagefilter($image,IMG_FILTER_BRIGHTNESS,-60);
		return $image;
	}
	
	public static function greyscale($image){
		return imagefilter($image,IMG_FILTER_GRAYSCALE);
	}
	
}