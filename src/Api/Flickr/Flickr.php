<?php

namespace Csifo\Api\Flickr;

use Csifo\Api\Flickr\Interfaces\IFlickr as IFlickr;

use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Http\Curl\Interfaces\ICurl as ICurl;

class Flickr implements IFlickr {
	
	protected $config;
	protected $secret = NULL;
	protected $domain = 'https://api.flickr.com';
	
	public function __construct(IConfig $config,ICurl $curl){
		$this->config = $config->get('api')['Flickr'];
		$this->curl	  = $curl;
		$this->setPermissions();
	}
	
	public function setPermissions($perm = 'read'){
		$this->permission = $perm;
	}
	
	public function auth(){
		$api_signature = md5($this->secret . "api_key" . $this->config['key'] . "perms" . $this->permission);
		$authUrl = $this->domain . '/services/auth/?api_key=' . $this->config['key'] . '&perms=' . $this->permission . '&api_sig=' . $api_signature;
		$this->curl->setUrl($authUrl)->handleCookies()->execute();
	}
	
	public function runCommand($command,$args){
		$args = array_merge(['api_key' => $this->config['key'],'format' => 'json'],$args);
		$args['method'] = $command;
		$commandUrl = $this->domain . '/services/rest/?' . http_build_query($args);
		$result = $this->curl->setUrl($commandUrl)->followLocation()->execute();
		$result = preg_replace('#^[\s]*jsonFlickrApi\(|\)$#i','',$result);
		return json_decode($result,true);
	}
	
	public function searchPhotos($text){
		return $this->runCommand('flickr.photos.search',['text' => urlencode($text)]);
	}
	
}