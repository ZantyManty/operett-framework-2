<?php

/*
* Helper functions
*/

function cdir(){
	chdir(dirname(__DIR__));
}

function inc($file){
	//http://stackoverflow.com/questions/186338/why-is-require-once-so-bad-to-use
	if (!defined($file)) {
		define($file, 1);
		return require($file);
	}
}

function incm($file){
	return require($file);
}

function dd(){
	foreach(func_get_args() as $arg){
		echo '<pre>';
		var_dump($arg);
		echo '</pre>';
	}
	exit;
}

function resolve($interface){
	return Csifo\Core\App::getResolver()->get($interface);
}

function resolveWithParams($interface,$params){
	return Csifo\Core\App::getResolver()->resolveWithParams($interface,$params);
}

function resolver(){
	return Csifo\Core\App::getResolver();
}

function add_service($name,$call){
	container()->add($name,$call);
}

function f_exists($file){
	//return stream_resolve_include_path($file);
	return file_exists($file);
}

function ve($var,$file){
	/*$ve = var_export($var);*/
	ob_start();
	var_dump($var);
	$ve = ob_get_clean();
	file_put_contents($file,$ve);
}

function redirect($url){
	header('Location: ' . $url);
	exit;
}