<?php

namespace Csifo\Helpers;

use Csifo\Helpers\Interfaces\IClosure;

class Closure implements IClosure {
	
	public function extractClosure($closure){
		$ref  = new \ReflectionFunction($closure);
		$file = new \SplFileObject($ref->getFileName());
		$file->seek($ref->getStartLine()-1);
		$content = '';
		$numOfLines = ($ref->getEndLine() - $ref->getStartLine()) + 1;
		
		$content = '';
		
		while ($file->key() < $ref->getEndLine()) {
			$content .= $file->current();
			$file->next();
		}
		
		return $this->tokenize($content);
	}
	
	public function tokenize($content){
		if(preg_match('#(function *\([\S\s]+?\))#ui',$content,$funcHead)){
			$closureHead = $funcHead[1];
			$content = trim(preg_replace('#[\S\s]*?function *\([\S\s]+?\) *?{#ui','{',$content));
		
			$length = strlen($content);
			$brace = 0;
			
			$closureBody = '';
			for($i = 0;$i <= $length; ++$i){
				$char = $content[$i];
				
				if($char == '{'){
					$brace++;
				} else if ($char == '}') {
					$brace--;
				}
				
				$closureBody .= $char;
				
				if($brace === 0){
					break;
				}
			}
			
			return $closureHead . $closureBody . ';';
		} else {
			return $content;
		}
	}
}