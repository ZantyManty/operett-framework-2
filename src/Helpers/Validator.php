<?php

namespace Csifo\Helpers;

use Csifo\Helpers\Interfaces\IValidator as IValidator;

class Validator implements IValidator {
	
	public function email($email){
		if(preg_match('#^[\w.-_]+?@[\w-_.]+?\.[\w]{1,3}$#i',$email)){
			if(filter_var($email, FILTER_VALIDATE_EMAIL) !== false){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}