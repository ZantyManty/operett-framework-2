<?php

namespace Csifo\Helpers;

class StringHelper implements Interfaces\IStringHelper {
	
	/**
	* Replace characters with accent
	* @todo Include all chars with accents!!!!!
	*/
	public function generateSlug($string){
		$replace = [
			'á' => 'a',
			'é' => 'e',
			'ú' => 'u',
			'ü' => 'u',
			'ű' => 'u',
			'ó' => 'o',
			'ö' => 'o',
			'ő' => 'o',
			'í' => 'i'
		];
		return str_replace(array_keys($replace),$replace,$string);
	}
	
}