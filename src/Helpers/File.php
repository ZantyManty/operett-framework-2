<?php

namespace Csifo\Helpers;

use Csifo\Core\Interfaces\IConfig as IConfig;

class File implements Interfaces\IFileHelper {
	
	protected $config;
	
	public function __construct(IConfig $config){
		$this->config = $config;
	}
	
	public function dirScan($dir){
		$dir = (substr($dir,-1)!='/') ? $dir.='/' : $dir;
		$dirs = scandir($dir);
		unset($dirs[0]);
		unset($dirs[1]);
		return $dirs;
	}
	
	public function getModuleList(){
		$dir = $this->config->app('MODULES_DIR');
		$modules = $this->dirScan($dir);
		foreach($modules as $key => $module){
			if(stripos($module,'.') !== false){
				unset($modules[$key]);
			} else {
				$modules[$key] = $dir . '/' . $modules[$key];
			}
		}
		
		return array_values($modules);
	}
	
	public function createDir($path){
		//$path = ROOT . '/' . $path;
		if(strpos($path,'/') !== false){
			/*
			* If first character not the "/" AND We have more than 1 "/"
			*/
			if(substr_count($path,'/') > 0 && substr($path,0,1) != '/'){
				
				if(substr($path,0,1) == '/'){
					$path = substr($path,1);
				}
				
				$fileNameArr = explode('/',$path);
				
				/*
				* The already made path dirs' buffer
				* (Everytime the loop make a dir,it adds to this buffer,so we can go deeper and deeper)
				*/
				$pathMade = '';
				
				foreach($fileNameArr as $elem){
					$pathMade .= $elem . '/';
					if(!is_dir($pathMade) && !is_int(strpos($elem,'.'))){
						if(!empty($pathMade))
						{ mkdir($pathMade); }
					}
					
				}

				unset($pathMade);
				unset($fileNameArr);
			} 
			/*
			* Else we have only a filename have a "/" char in the begining,so we cut it down
			*/
			else if(substr($path,0,1) == '/'){
				$path = substr($path,1);
			}
		}
		/*
		* Returns with the path
		*/
		return $path;
	}
	
	public function delete($path){
		if(is_dir($path)){
			$files = $this->dirScan($path);
			if(!empty($files) && $files){
				foreach($files as $file){
					if(strpos($file,'.') !== false){
						unlink($path . '/' . $file);
					} else {
						$this->delete($path . '/' . $file);
					}
				}
				rmdir($path);
			} else {
				rmdir($path);
			}
		} else {
			if(file_exists($path))
			unlink($path);
		}
	}
}