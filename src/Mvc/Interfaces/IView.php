<?php

namespace Csifo\Mvc\Interfaces;

interface IView {
	
	public function render($file,$data);
	
}