<?php

namespace Csifo\Mvc\Interfaces;

interface ILang {
	
	public function getLabel($label);
	
	public function searchForLangFiles($dir = null);
	
	public function fillWithLangTags($content = null);
}