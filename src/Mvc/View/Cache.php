<?php

namespace Csifo\Mvc\View;

class Cache {
	
	protected $cacheDir = '';
	protected $moduleDir = '';
	
	protected $cacheKey = [];
	
	public function __construct($config){
		$this->cacheDir = $config->app('CACHE_DIR');
		$this->moduleDir = $config->app('MODULES_DIR');
	}
	
	protected function makeKey($path){
		if(!isset($this->cacheKey[$path]))
			$this->cacheKey[$path] = 'View_' . hash("crc32b",$path);

		return $this->cacheKey[$path];
	}
	
	public function create($content,$basicFile){
		$cacheKey = $this->makeKey($this->moduleDir . '/' . $basicFile);
		file_put_contents($this->cacheDir . '/' . $cacheKey,$content);
	}
	
	public function getFileName($basicFile){
		return $this->cacheDir . '/' . $this->makeKey($this->moduleDir . '/' . $basicFile);
	}
	
	public function cacheExists($basicFile){
		$cacheKey = $this->makeKey($this->moduleDir . '/' . $basicFile);
		return file_exists($this->cacheDir . '/' . $cacheKey);
	}
}