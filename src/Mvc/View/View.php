<?php

namespace Csifo\Mvc\View;

use Csifo\Mvc\Interfaces\IView as IView;

use Csifo\Mvc\Interfaces\ILang as ILang;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Mvc\View\Template\Interfaces\ITemplate;
use Csifo\Http\Router\Path\Interfaces\IGenerator;

class View implements IView {
	
	protected $config		= null;
	
	protected $lang			= null;
	protected $template		= null;
	protected $urlGenerator	= null;
	protected $cache		= null;
	
	protected $module;
	protected $action;
	protected $headView = 'head.view';
	
	protected $variables = [];
	
	public function __construct(IConfig $config,ILang $lang,ITemplate $template,IGenerator $urlGenerator,$module,$action = 'index'){
		$this->config	= $config;
		$this->lang		= $lang;
		$this->module 	= $module;
		$this->action 	= $action;
		$this->template = $template;
		$this->urlGenerator = $urlGenerator;
		$this->cache	= new Cache($this->config);
	}
	
	public function assign($key,$value){
		$this->variables[$key] = $value;
	}
	
	protected function uri($name,$params){
		return $this->urlGenerator->generatePath($name,$params);
	}
	
	public function load($file = 'header.php'){
		$dir = $this->config->app('TEMPLATE_DIR') . '/html/';
		return $this->includeView(null,$dir . $file . '.php',true);
	}
	
	public function render($file = null,$data = null){
		$HTML = $this->includeView($data,$file);
		return $HTML;
	}
	
	protected function renderLang($content,$module,$view){
		/*
		* Lang
		*/
		$content = $this->lang->fillWithLangTags($content,$module,$view);
		//<(title|p|textarea|span|li|<a[\S\s]*?)>([\S\s]*?)<\/(\1)>
		/*$content = preg_replace('#(<\/[\w]+?>)[\s]*?(<[\w]+)#i','$1$2',$content);*/
		return $content;
	}
	
	public function makeViewFromTemplate($file,$view,$cacheKeyRaw,$cache = true){
		$content = file_get_contents($file);
		/*
		* Parse the template
		*/
		$content = $this->template->tokenize($content);
		/*
		* Parse the language tags
		*/
		$content = $this->renderLang($content,$this->module,$view);
		
		/*
		* If we need the cache (Like,DEV mode is off!)
		*/
		if($cache === true){
			if($this->cache->cacheExists($cacheKeyRaw) === false)
				$this->cache->create($content,$cacheKeyRaw);
			
			return $this->fill($this->cache->getFileName($cacheKeyRaw));
			
		} else {
			file_put_contents('vierw_content.txt',$content . '---[...]---',FILE_APPEND);
			return $this->fill($content,false);
		}
	}
	
	protected function includeView($data = null,$iFile = '',$isFromTemplate = false){
		
		if($isFromTemplate === false)
			$basicFile = $this->config->app('MODULES_DIR') . '/' . ucfirst(strtolower($this->module)) . '/views/' . (empty($iFile) ? $this->action : $iFile) . '.view.php';
		else
			$basicFile = $iFile;
		
		/*
		* Prepare the template
		*/
		$cacheKeyRaw = $this->lang->getLang() . '_' . $basicFile;
		$cache = ($this->config->app('DEV_MODE') === false) ? true : false;
		
		return $this->makeViewFromTemplate($basicFile,$iFile,$cacheKeyRaw,$cache);
	}
	
	protected function fill($_____content,$____isFile = true){
		/*
		* Make the assigned variables accessable
		*/
		extract($this->variables,EXTR_SKIP);
				
		/*
		* Render
		*/
		ob_start();
			if($____isFile === true)
				include $_____content;
			else
				eval('?>' . $_____content);
		return ob_get_clean();
	}
	
	public function __get($key){
		if(isset($this->variables[$key])){
			return $this->variables[$key];
		}
		return false;
	}
	
}