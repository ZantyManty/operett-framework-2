<?php

namespace Csifo\Mvc\View\Form\Interfaces;

interface IFormFields {
	
	public function select();
	
	public function field();
	
	public function checkbox();
	
}