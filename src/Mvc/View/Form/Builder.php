<?php

namespace Csifo\Mvc\View\Form;

use Csifo\Mvc\View\Form\Interfaces\IBuilder;
use Csifo\Factory\Interfaces\IForm as Factory;
use Csifo\Mvc\View\Form\Interfaces\IFormFields;

class Builder implements IBuilder {
	
	protected $factory	= null;
	protected $fields	= null;
	protected $formHtml	= '';
	
	public function __construct(Factory $factory,IFormFields $fields){
		$this->factory = $factory;
		$this->fields = $fields;
	}
	
	public function build($name){
		$moduleForm = $this->factory->get($name);
		
		if($moduleForm !== null){
			if(method_exists($moduleForm,$name)){
				$formData = $moduleForm->$name($this);
				$this->formHtml .= '</form>';
				return $this->formHtml;
			} else {
				throw new \Exception('There is no form with name [ ' . $name . ' ] !');
			}
		} else {
			throw new \Exception('There is no form markup class in the module!');
		}
	}
	
	public function form($method,$action = '',$args = []){
		$this->formHtml = '<form method="' . $method . '" action="' . $action . '" ';
		
		foreach($args as $arg => $value){
			$this->formHtml .= $arg . '="' . $value . '" ';
		}
		
		$this->formHtml .= '>';
		
		return $this;
	}
	
	public function select($values,$args = []){
		
		$selectTemplate = $this->fields->select();
		/*
		* Options
		*/
		$options = '';
		foreach($values as $value => $name){
			$options .= '<option value="' . $value . '" ' . 
				(
					isset($args['SELECTED']) && $args['SELECTED'] == $value ? 
					'selected = "selected"' :
					''
				) . '>' . $name . '</option>';
		}
		$selectTemplate = str_replace('{options}',$options,$selectTemplate);
		
		$this->addToFormHtml($selectTemplate,$args);
		
		return $this;
	}
	
	public function field($args = []){
		$fieldTemplate = $this->fields->field();
		
		$this->addToFormHtml($fieldTemplate,$args);
		
		return $this;
	}
	
	public function checkbox($args = []){
		$checkboxTemplate = $this->fields->checkbox();
		
		if(isset($args['CHECKED']) && $args['CHECKED'] === true){
			$checkboxTemplate = str_ireplace('{isChecked}','checked',$checkboxTemplate);
			unset($args['CHECKED']);
		}
		
		$this->addToFormHtml($checkboxTemplate,$args);
		
		return $this;
	}
	
	public function submit($args = []){
		$fieldTemplate = $this->fields->submit();
		$this->addToFormHtml($fieldTemplate,$args);
		
		return $this;
	}
	
	public function number($args = []){
		$fieldTemplate = $this->fields->submit();
		$fieldTemplate = str_replace('{type}','number',$fieldTemplate);
		$this->addToFormHtml($fieldTemplate,$args);
		return $this;
	}
	
	public function textarea($args = []){
		$fieldTemplate = $this->fields->textarea();
		$this->addToFormHtml($fieldTemplate,$args);
		return $this;
	}
	
	public function addToFormHtml($template = '',$args = []) : void {
		/*
		* Args
		*/
		foreach($args as $arg => $value){
			$template = str_ireplace('{' . $arg . '}',$value,$template);
		}
		
		/*
		* If theres empty arg left in the template
		*/
		$template = preg_replace('#{[\w_-]+}#i','',$template);
		
		$this->formHtml .= $template;
	}
}