<?php

namespace Csifo\Mvc\View\Form;

use Csifo\Mvc\View\Form\Interfaces\IFormFields;

class FormFields implements IFormFields {
	
	public function select(){
		return '<div class="{group-class}">
			<label class="{label-class}" for="{id}">{label}</label>
			<select class="{class}" id="{id}" name="{name}">
				{options}
			</select>
		</div>';
	}
	
	public function field(){
		return '<div class="{group-class}">
			<label class="{label-class}" for="{id}">{label}</label>
			<input name="{name}" type="{type}" class="{class}" id="{id}" placeholder="{placeholder}" value="{value}" >
		</div>';
	}
	
	public function textarea(){
		return '<div class="{group-class}">
			<textarea id="{id}" class="{class}" placeholder="{placeholder}" name="{name}" >{value}</textarea>
		</div>';
	}
	
	public function checkbox(){
		return '<div class="{group-class}">
			<input type="checkbox" class="{class}" id="{id}" name="{name}" {isChecked}>
			<label class="{label-class}" for="{id}">{label}</label>
		</div>';
	}
	
	public function submit(){
		return '<div class="{group-class}">
			<input name="{name}" type="submit" class="{class}" id="{id}" placeholder="{placeholder}" value="{value}" >
		</div>';
	}
}