<?php

namespace Csifo\Mvc\View\Template;

use Csifo\Mvc\View\Template\Interfaces\ITemplate;
use Csifo\Http\Router\Path\Interfaces\IGenerator;
use Csifo\Mvc\View\Form\Interfaces\IBuilder;

class Template implements ITemplate {
	
	protected $urlGenerator	= null;
	protected $formBuilder	= null;
	
	public function __construct(IGenerator $urlGenerator,IBuilder $formBuilder){
		$this->urlGenerator = $urlGenerator;
		$this->formBuilder = $formBuilder;
	}
	
	protected function parse(string $content) : string {
		
		$methods = [
			'parseLoops',
			'parseObjectCalls',
			'parseArithmeticEcho',
			'parseIfElse',
			'parseCommonVars',
			'parseRawCode',
			'parseFunctionCall',
			'parseForm',
			'parseRoutes'
		];
		
		foreach($methods as $method){
			$replacedContent = $this->$method($content);
			if($content !== $replacedContent) return $replacedContent;
		}
		
		return $content;
	}
	
	protected function parseCommonVars(string $content) : string {
		/*
		* |$var| OR |$var['key']| OR |$var('xd')|
		*/
		return \preg_replace('#\|\s*(\$*[\w_]+?(?:[\[\(\S\s\)\]]+?)*?)\s*\|#i','<?php echo $1; ?>',$content);
	}
	
	protected function parseObjectCalls(string $content) : string {
		return \preg_replace('#^\|(\s*\$[\w_-]+?\s*->(?:[\w$-]+?)*(?:[({]+[\S\s]*?[})]+)*\s*)\|$#ui','<?php echo $1; ?>',$content);
	}
	
	protected function parseIfElse(string $content) : string {
		$content = \preg_replace('#^\|\s*((?:else|(?:else\s*)*if)\s*(?:\([\S\s]+\))*)\s*\|$#ui','<?php $1: ?>',$content);
		return \preg_replace('#^\|\s*\/if\s*\|$#','<?php endif; ?>',$content);
	}
	
	protected function parseArithmeticEcho(string $content) : string {
		return \preg_replace('#^\|((\$*[\w_-]+)\s*[\/*+^<>=-]{1,2}\s*(?2))\|$#ui','<?php echo (string)($1); ?>',$content);
	}
	
	protected function parseRawCode(string $content) : string {
		return \preg_replace('#\|\s*>\s*([\S\s]+?)\s*<\s*\|#ui','<?php $1 ?>',$content);
	}
	
	protected function parseFunctionCall(string $content) : string {
		return \preg_replace('#^\|\s*((?!for(?:each)*|fe|w|while|if|else(?:if)*)[\w-]+?\([\S\s]*?\))\s*\|$#ui','<?php echo $1; ?>',$content);
	}
	
	protected function parseLoops(string $content) : string {
		if(\preg_match('#\|\s*((?:fe|for|w(?:hile)*))(\(\s*[\S\s]+?\))\|#i',$content,$loopData)){
			$prefix = '';
			
			switch($loopData[1]){
				case 'w':
				case 'while':
					$prefix = 'while';
				case 'for':
					$prefix = 'for';
				case 'fe':
					$prefix = 'foreach';
			}
			
			return \str_replace($loopData[0],'<?php ' . $prefix . $loopData[2] . ' : ?>',$content);
		}
		
		/*
		* Closing tags
		*/
		if(preg_match('#\|\s*\/\s*(fe|for|w|while)\s*\|#ui',$content,$endLoopData)){
			$tags = [
				'w' => 'endwhile;',
				'while' => 'endwhile;',
				'for' => 'endfor;',
				'fe' => 'endforeach;',
			];
			
			return \str_replace($endLoopData[0],'<?php ' . $tags[$endLoopData[1]] . ' ?>',$content);
		}
		
		return $content;
	}
	
	protected function parseForm(string $content) : string {
		if(\preg_match('#\|\s*form:\s*([\w-]+?)\s*\|#ui',$content,$formName)){
			$formHtml = $this->formBuilder->build($formName[1]);
			$content = str_replace($formName[0],$formHtml,$content);
		}
		return $content;
	}
	
	protected function parseRoutes(string $content) : string {
		/*
		* E.g.: |/login:[id=1,lol=2]|
		*/
		if(\preg_match('#\|\/\s*([\w]+?)\s*:*\s*(?>\[\s*([\S\s]+?)\s*\])*\s*\|#i',$content,$match)){
			$routeName = $match[1];
			$normalisedPathParams = [];
			
			/*
			* If there are parameters...
			*/
			if(isset($match[2]) === true && empty($match[2]) === false){
				$params = preg_split('#,(?=[\w-]+?\s*=)#',$match[2]);
				
				foreach($params as $param){
					if(preg_match('#([\w-]+)\s*=\s*[\'\"]*([\w,@&\#~.-]+)[\'\"]*#ui',$param,$paramData)){
						$normalisedPathParams[$paramData[1]] = trim($paramData[2]);
					}
				}
			}
			
			$generatedUrl = $this->urlGenerator->generatePath($routeName,$normalisedPathParams);
			return str_replace($match[0],"<?php echo '" . $generatedUrl . "'; ?>",$content);
		}
		
		return $content;
	}
	
	public function tokenize(string $content) : string {
		
		if($content === '') return '';
		
		/*
		* Bitwise OR
		*/
		//$content = \preg_replace('#(\$*[\w]+\s*=\s*(\$*[\w]+)\s*)\|(\s*(?2)\s*;*)#ui','$1<BW_OR>$2',$content);
		
		$length = \strlen($content);
		$deep = 0;
		$inParentheses = 0;
		$inCodeBlock = 0;
		
		$parseAccu = '';
		$lastChar = '';
		
		$toReplace = [];
		$replaceWith = [];
		
		$i = 0;
		do {
			$character = $content[$i];
			
			if($character === '('){
				++$inParentheses;
			} else if($character === ')') {
				--$inParentheses;
			}
			
			if($character === '>' && $lastChar === '|'){
				++$inCodeBlock;
			} elseif($character === '|' && $lastChar === '<'){
				--$inCodeBlock;
			}
			
			if($character === '|' && $deep === 0 && $inParentheses === 0 && $inCodeBlock === 0){
				++$deep;
			} else if($character === '|' && $deep > 0 && $inParentheses === 0 && $inCodeBlock === 0){
				--$deep;
			}
			
			if($deep !== 0){
				$parseAccu .= $character;
			} else if($deep === 0 && $parseAccu !== '') {
				$parseAccu .= '|';
				
				$toReplace[] = $parseAccu;
				$replaceWith[] = $this->parse($parseAccu);
				
				$parseAccu = '';
			}
			
			$lastChar = $character;
			
			++$i;
		} while($i < $length);
		
		/*
		* Bitwise OR
		*/
		/*$toReplace[] = '<BW_OR>';
		$replaceWith[] = ' | ';*/
		
		return \str_replace($toReplace,$replaceWith,$content);
	}
}