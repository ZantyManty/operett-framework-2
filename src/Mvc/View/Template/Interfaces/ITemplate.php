<?php

namespace Csifo\Mvc\View\Template\Interfaces;

interface ITemplate {
	
	public function tokenize(string $content);
	
}