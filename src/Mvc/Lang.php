<?php

namespace Csifo\Mvc;

use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Helpers\Interfaces\IFileHelper as IFileHelper;
use Csifo\Cache\SimpleCache\Interfaces\ICache;
use Csifo\Psr\Http\Message\IRequest;

class Lang implements Interfaces\ILang {
	
	protected $config;
	protected $fileHelper;
	
	protected $default;
	protected $prefered;
	
	protected $langs = [];
	protected $labels = [];
	protected $buffer;
	protected $cache;
	
	public function __construct(IConfig $config,IFileHelper $fileHelper,ICache $cache,IRequest $request){
		$this->config = $config;
		$this->cache = $cache;
		$this->fileHelper = $fileHelper;
		
		$this->init();
		$this->prefered = $this->parsePrefferedLangs($request->getHeader('Accept-Language'));
	}
	
	protected function parsePrefferedLangs($header) : string{
		/*if(preg_match_all('#([\w]+?)(?:-([\w]+?)|;q=([\d.]+?))(?:,|$)#i',$header,$langs)){
			$i = 0;
			$first = '';
			foreach($langs[0] as $lang){
				if(isset($this->labels[$langs[1][$i]]))
					return $langs[1][$i];
				
				++$i;
			}
		}*/
		
		$array = explode(',',$header);
		
		foreach($array as $lang){
			if(isset($this->labels[$lang]))
					return $lang;
		}
		
		return $this->config->app('default_lang');
	}
	
	protected function init(){
		/*
		* Init
		*/
		$key = 'ViewLangArray';
		if($this->cache->has($key) === false)
			$this->searchForLangFiles();
		else
			$this->labels = $this->cache->get($key);
	}
	
	protected function cleanWord($val){
		$arr = array("'");
		return str_replace($arr, '', $val);
	}
	
	public function searchForLangFiles($dir = null){
		$mappa = (is_null($dir) ? $this->config->app('MODULES_DIR') . '/' : $dir);
		$dirs = $this->fileHelper->dirScan($mappa);
		
		foreach($dirs as $v){
			if(strpos($v,'.') !== false){ //If it's a file
				
				//If it's a lang file
				if(stripos($v,'lang') !== false){
					/*
					* UPDATED:
					* $langMatch[1] -> The language's 2 letter code. E.g.: en
					*/
					if(preg_match('#Modules\/[\w_-]+?\/lang\/([\w]{2}(?:-[\w]{2})?)(?:$|\/)#ui',$mappa,$langMatch)){
						if(!isset($this->labels[$langMatch[1]]))
							$this->labels[$langMatch[1]] = [];
						$this->labels[$langMatch[1]] += inc($mappa . $v);
					}
				}
			} else {
				$this->searchForLangFiles($mappa . $v . '/');
			}
		}
		$this->cache->set('ViewLangArray',$this->labels);
	}
	
	public function getLabel($label) : string {
		if(isset($this->labels[$this->prefered][$label]) === true)
			return $this->labels[$this->prefered][$label];
		else
			return '';
	}
	
	public function getLang(){
		return $this->prefered;
	}
	
	public function fillWithLangTags($content = null,$module = '',$view = 'index'){
		foreach($this->labels[$this->prefered] as $k => $label){
			$content = str_replace("{#{$k}#}", $label, $content);
		}
		
		return $content;
	}
}