<?php

namespace Csifo\Factory;

use Csifo\Factory\Interfaces\IForm;
use Csifo\Psr\Container\IContainer;
use Csifo\Core\Interfaces\IConfig;

class Form implements IForm {
	
	protected $container	= null;
	protected $config		= null;
	protected $module		= '';
	
	/*
	* @param string $module Module's name. Injected from Container (global parameters)
	* @param Container $container DI container
	*/
	public function __construct($module,IContainer $container,IConfig $config){
		$this->module = $module;
		$this->container = $container;
		$this->config = $config;
	}
	
	public function get(){
		$namespace = $this->config->get('namespaces')['Modules'];
		$interface = $namespace . '\\' . $this->module  . '\Interfaces\IForm';
		if($this->container->has($interface)){
			return $this->container->get($interface);
		} else {
			return null;
		}
	}
	
}