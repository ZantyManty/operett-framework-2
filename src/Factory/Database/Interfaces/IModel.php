<?php

namespace Csifo\Factory\Database\Interfaces;

interface IModel {
	public function make($table);
}