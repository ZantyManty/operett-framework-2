<?php

namespace Csifo\Factory\Database\MySql;

use Csifo\Factory\Database\Interfaces\IModel;
use Csifo\Database\MySql\Interfaces\IBridge;
use Csifo\Database\MySql\Orm\Model as OrmModel;
use Csifo\Helpers\Interfaces\IStringHelper;
use Csifo\Database\MySql\Resolver\Interfaces\IResolver;

class Model implements IModel {
	
	protected $namespace = '\App\Models\\';
	protected $iBridge = null;
	protected $stringHelper = null;
	protected $resolver = null;
	
	public function __construct(IBridge $iBridge,IStringHelper $stringHelper,IResolver $resolver){
		$this->iBridge = $iBridge;
		$this->stringHelper = $stringHelper;
		$this->resolver = $resolver;
	}
	
	public function make($class){
		$table_class = $this->stringHelper->generateSlug($class);
		$table_class = ucfirst(mb_strtolower($table_class));
		$table = $this->resolver->getTableByClass($table_class);
		
		if(class_exists($this->namespace . $table_class)){
			$class = $this->namespace . $table_class;
			return new $class($this->iBridge);
		} else {
			if($table === '')
				throw new \Exception('Table name not found by class [ ' . $class . ' ]!');
			else
				return new OrmModel($table,$this->iBridge);
		}
	}
}