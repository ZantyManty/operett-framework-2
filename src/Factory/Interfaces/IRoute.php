<?php

namespace Csifo\Factory\Interfaces;

interface IRoute {
	
	public function get($url);
	public function post($url);
	public function patch($url);
	public function delete($url);
	public function all($url);
}