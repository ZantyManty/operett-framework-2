<?php

namespace Csifo\Factory;

use Csifo\Psr\Container\IContainer;
use Csifo\Http\Router\Route\Interfaces\ICollection;

class Route implements Interfaces\IRoute {
	
	protected $collection	= null;
	protected $routeClass	= '';
	public $routeCache		= null;
	
	protected $groupMiddlewares = [];

	public function __construct(IContainer $container,ICollection $collection){
		$this->collection = $collection;
		$this->routeClass = $container->getClassById('Csifo\Http\Router\Interfaces\IRoute');
		$this->routeCache = $container->get('Csifo\Http\Router\Interfaces\IRouteCache');
	}
	
	/*
	* Add paths to the router
	* @param $url string	| The url path
	* @param $method string | The request's expected method
	*/
	public function addPath($method,$url){
		$routeObject = new $this->routeClass($url,$method,$this->routeCache);
		if(!empty($this->groupMiddlewares))
			$routeObject->through($this->groupMiddlewares);
		
		$routeObject->createPattern();
		$this->collection->addRouteObject($routeObject,$method);
		
		return $routeObject;
	}

	/*
	* GET method
	*/
	public function get($url){
		return $this->addPath('GET',$url);
	}
	
	/*
	* POST method
	*/
	public function post($url){
		return $this->addPath('POST',$url);
	}
	
	/*
	* PATCH method
	*/
	public function patch($url){
		return $this->addPath('PATCH',$url);
	}
	
	/*
	* DELETE method
	*/
	public function delete($url){
		return $this->addPath('DELETE',$url);
	}
	
	/*
	* All method
	*/
	public function all($url){
		return $this->addPath('ALL',$url);
	}
	
	/*
	* Group
	*/
	public function group($middlewares,$callable){
		$this->groupMiddlewares = $middlewares;
		$callable($this);
		$this->groupMiddlewares = [];
	}
	
	public function getCollection() {
		return $this->collection;
	}
	
	public function exportCache(){
		$this->routeCache->export();
	}
}