<?php
/*
* PSR-15
*
* https://www.php-fig.org/psr/psr-15/
*/

namespace Csifo\Psr\Http\Server;

/*
* Based on PSR-7
*/
use Csifo\Psr\Http\Message\IResponse;
use Csifo\Psr\Http\Message\IServerRequest;

interface IMiddleware
{
	public function process(IServerRequest $request, IRequestHandler $handler);
}