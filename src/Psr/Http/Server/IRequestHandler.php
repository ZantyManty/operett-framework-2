<?php
/*
* PSR-15
*
* https://www.php-fig.org/psr/psr-15/
*
* Middleware handler
*/

namespace Csifo\Psr\Http\Server;

/*
* Based on PSR-7
*/
use Csifo\Psr\Http\Message\IServerRequest;

interface IRequestHandler {
	
	public function handle(IServerRequest $request);
	
}