<?php
/*
* PSR-7
*
* https://www.php-fig.org/psr/psr-7/
*/

namespace Csifo\Psr\Http\Message;

interface IUri {
	
	public function getUri();
	
	public function getScheme();

    public function getAuthority();

    public function getUserInfo();

    public function getHost();

    public function getPort();

    public function getPath();

    public function getQuery();

    public function getFragment();

    public function withScheme($scheme);

    public function withUserInfo($user, $password = null);

    public function withHost($host);

    public function withPort($port);

    public function withPath($path);

    public function withQuery($query);

    public function withFragment($fragment);

    public function __toString();
	
}