<?php
/*
* PSR-7
*
* https://www.php-fig.org/psr/psr-7/
*/

namespace Csifo\Psr\Http\Message;

interface IMessage {
	
	public function getProtocolVersion();

    public function withProtocolVersion($version);

    public function getHeaders();

    public function hasHeader($name);

    public function getHeader($name);

    public function getHeaderLine($name);

    public function withHeader($name, $value);

    public function withAddedHeader($name, $value);

    public function withoutHeader($name);

    public function getBody();

    public function withBody(IStream $body);
}