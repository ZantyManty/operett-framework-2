<?php
/*
* PSR-7
*
* https://www.php-fig.org/psr/psr-7/
*/

namespace Csifo\Psr\Http\Message;

use Csifo\Psr\Http\Message\IUri;

interface IRequest extends IMessage {
	
	public function getRequestTarget();

    public function withRequestTarget($requestTarget);

    public function getMethod();
 
    public function withMethod($method);

    public function getUri();

    public function withUri(IUri $uri, $preserveHost = false);
}