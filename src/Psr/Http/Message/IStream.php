<?php
/*
* PSR-7
*
* https://www.php-fig.org/psr/psr-7/
*/

namespace Csifo\Psr\Http\Message;

interface IStream {
	
	public function read($length);
	
	public function write($string);
	
	public function getContents();
	
	public function getMetadata($key = null);
	
	public function rewind();
	
	public function seek($offset, $whence = SEEK_SET);
	
	public function isReadable();
	
	public function isSeekable();
	
	public function isWritable();
	
	public function eof();
	
	public function tell();
	
	public function getSize();
	
	public function detach();
	
	public function close();
	
	public function __toString();
	
}