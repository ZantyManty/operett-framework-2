<?php
/*
* PSR-7
*
* https://www.php-fig.org/psr/psr-7/
*/

namespace Csifo\Psr\Http\Message;

interface IServerRequest extends IRequest {
	
	public function getServerParams();
	
	public function getCookieParams();
	
	public function withCookieParams($cookies);
	
	public function getQueryParams();
	
	public function withQueryParams($query);
	
	public function getUploadedFiles();
	
	public function withUploadedFiles($uploadedFiles);
	
	public function getParsedBody();
	
	public function withParsedBody($data);
	
	public function getAttributes();
	
	public function getAttribute($name, $default = null);
	
	public function withAttribute($name, $value);
	
	public function withoutAttribute($name);
	
}