<?php
/*
* PSR-7
*
* https://www.php-fig.org/psr/psr-7/
*/

namespace Csifo\Psr\Http\Message;

interface IUploadedFile {
	
    public function getStream();

    public function moveTo($targetPath);

    public function getSize();

    public function getError();
    
    public function getClientFilename();

    public function getClientMediaType();
}