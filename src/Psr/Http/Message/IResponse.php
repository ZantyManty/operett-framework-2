<?php
/*
* PSR-7
*
* https://www.php-fig.org/psr/psr-7/
*/

namespace Csifo\Psr\Http\Message;


interface IResponse extends IMessage {
	
	public function getStatusCode();

    public function withStatus($code, $reasonPhrase = '');

    public function getReasonPhrase();
}