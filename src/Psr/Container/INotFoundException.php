<?php
namespace Csifo\Psr\Container;

/**
 * No entry was found in the container.
 */
interface INotFoundException extends IContainerException
{
}