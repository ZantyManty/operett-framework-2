<?php

namespace Csifo\Psr\Container;

interface IContainer {
	
	public function get($id);
	
	public function has($id);
	
}