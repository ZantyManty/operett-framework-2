<?php

namespace Csifo\Grammar\Interfaces;

interface IPlural {
	
	public function getNonPluralExtended($name);
	
	public function getNonPlural($name);
	
	public function isPlural($name);
}