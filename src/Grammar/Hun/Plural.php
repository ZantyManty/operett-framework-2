<?php

namespace Csifo\Grammar\Hun;

use Csifo\Grammar\Interfaces\IPlural as IPlural;

class Plural implements IPlural {

	protected $mely = ['a','á','o','ó','u','ú'];
	protected $magas = ['ö','ő','ü','ű','e','é','i','í'];
	
	protected $kivetelek = [
		'ló'		=> 'lovak',
		'köcsög'	=> 'köcsögök',
		'farok'		=>	'farkak',
		'fájl'		=>	'fájlok',
		'log'		=>	'logok',
		'poszt'		=>	'posztok',
		//'kéz'		=>	'kezek',
		'könyv'		=>	'könyvek',
		'toll'		=>	'tollak',
		'szatyor'	=>	'szatyrok',
		'jármű'		=>	'járművek',
	];
	
	protected $szimpla = [
		'fél$'	=>	'felek$',
		'szám$'	=>	'számok$',
		'irat$'	=>	'iratok$',
		'ház$'	=>	'házak$',
	];
	
	protected function krkPlural($word){
		/**
		* ökör  -> ökrök
		* bokor -> bokrok
		* szobor -> szobrok
		* marok -> markak
		*/
		if(preg_match('#^((?:[^öüóúőűáéieaíuo]+)?([aöo]{1})[bkr])#iu',$word,$match)){
			return $match[1] . 'r' . $match[2] . 'k';
		} else {
			return false;
		}
	}
	
	protected function ak($word){
		/**
		* tó -> tavak
		* szó -> szavak
		*
		* úr -> urak
		* kút -> kutak
		* út -> utak
		* nyúl -> nyulak
		* kéz -> kezek
		* név -> nevek
		*/
		$replace = [
			'ó' => 'a',
			'ú' => 'u',
			'é' => 'e',
		];
		if(preg_match('#^([^ó]+?)([ó]{1})$#iu',$word,$match)){
			return $match[1] . $replace[$match[2]] . 'vak';
		} else if(preg_match('#^[^ú]*([ú]{1})#iu',$word,$match_u)){
			//$csere = ['ú' => 'u','é' => 'e'];
			return str_replace($match_u[1],$replace[$match_u[1]],$word) . 'ak';
		} else if(preg_match('#^[^é]+é[vz]{1}#iu',$word,$match)){
			return preg_replace('#([^é]+)é([vz]{1})#ui','$1e$2ek',$word);
		} else {
			return false;
		}
	}
	
	public function getPlural($word){
		
		if(isset($this->kivetelek[$word])){
			return $this->kivetelek[$word];
		}
		
		$lowered = mb_strtolower($word);
		//Origin: https://hashmode.com/php-str_split-for-multibyte-string/31
		$letters = preg_split('//u', $lowered, -1, PREG_SPLIT_NO_EMPTY);
		$maganhangzok = array_merge($this->mely,$this->magas);

		$szotagszam = 0;
		$count = count($letters);
		$csakMaganhangzok = [];
		

		for($i = 0;$i < $count;$i++){
			if(in_array($letters[$i],$maganhangzok)){
				$szotagszam++;
				$csakMaganhangzok[] = $letters[$i];
			}
		}
		
		$utolsoBetu = end($letters);
		$utolsoBetuKey = $count - 1;
		$countMgn = count($csakMaganhangzok); //Szótagszám
		
		/**
		* Ha utolsó betű magánhangzó
		*/
		if(in_array($utolsoBetu,$maganhangzok)){
			/**
			* A vagy E esetén ékezetes az utolsó betű!
			*/
			if($utolsoBetu == 'a'){
				$letters[$utolsoBetuKey] = 'á';
				$letters[] = 'k';
				return implode('',$letters);
			} else if($utolsoBetu == 'e'){
				$letters[$utolsoBetuKey] = 'é';
				$letters[] = 'k';
				return implode('',$letters);
			}
			/*
			* Speciális,egy szótagos esetek pl.: Tó
			*/
			if($countMgn == 1){
				$result = $this->ak($word);
				if($result){
					return $result;
				}
			} else {
				if(end($letters) != 'k'){
					$letters[] = 'k';
				}
				
			}
			return implode('',$letters);
		} else {
			
			/**
			* Az első magánhangzó alapján nagyjából 
			* meg lehet állapítani a ragban szereplőt
			*
			* 'Első magánhangzó' => 'Rag magánhangzó'
			*/
			$ragMgnHangz = [
				'ö' => 'e',
				'ő'	=> 'ö',
				'ü'	=> 'e',
				'ű'	=> 'o',
				'é' => 'e',
				'a'	=> 'o',
				'e'	=> 'e',
				'o'	=> 'o',
				'ó'	=> 'o',
				'á'	=> 'o',
			];
			
			/**
			* Speciális egy szótagosok vizsgálata Pl.: Út,kút
			*/
			if($countMgn == 1){
				$result = $this->ak($word);
				if($result){
					return $result;
				} else {
					$ragMgnHangz['a'] = 'a'; //Fal -> falak,Haj -> hajak
					$ragMgnHangz['ö'] = 'ö'; //Tök -> tökök
					$ragMgnHangz['ü'] = 'ö'; //Trükk -> trükkök
					$ragMgnHangz['á'] = 'a'; //Tál -> tála,sál -> sálak
					//$ragMgnHangz['o'] = 'a'; //Toll -> tollak
					$letters[] = $ragMgnHangz[$csakMaganhangzok[0]] . 'k';
					return implode('',$letters);
				}
			}
			/**
			* Speciális két szótagosok
			*/
			else if($countMgn == 2){
				/**
				*Pl: izom,szirom,halom,malom
				*/
				if(preg_match('#[rlz]{1}(([o]{1})[m]{1}$)#iu',$word,$match)){
					return preg_replace('#[o]{1}[m]{1}$#iu','mok',$word);
				}
				/**
				* Pl: féreg,méreg,kéreg
				*/
				if(preg_match('#([^é]*[é]{1}([rt]))e[gk]#ui',$word,$match)){
					$csere = ['r' => 'gek','t' => 'kek'];
					return $match[1] . $csere[$match[2]];
				}
				
				$result = $this->krkPlural($word);
				if($result){
					return $result;
				} else {
					$letters[] = $ragMgnHangz[$csakMaganhangzok[0]] . 'k';
					return implode('',$letters);
				}
				
			} else {
				$letters[] = $ragMgnHangz[$csakMaganhangzok[0]] . 'k';
				return implode('',$letters);
			}
		}
	}
	
	public function getNonPluralExtended($word){
		
		return $this->getNonPlural($word);
	}
	
	public function getNonPlural($word){
		$word = mb_strtolower($word);
		
		$kivetelekFlip = array_flip($this->kivetelek);
		/**
		* Kivételek
		*/
		if(isset($kivetelekFlip[$word])){
			return $kivetelekFlip[$word];
		}
		
		if(preg_match('#[eöoaéá]{1}k$#iu',$word)){
			$szoTo = preg_replace('#[eöoa]{1}k$#iu','',$word);
			
			/*
			* "krk" Visszafele
			*
			* Pl: ökrök -> ökör
			*/
			if(preg_match('#^((?:[^öüóúőűáéíaieu]*?)([oöa]+)){1}([brk]{2})$#iu',$szoTo,$match)){
				/*
				* Pl -> szobor -> szo . b . o . r;
				*/
				return $match[1] . $match[3][0] . $match[2] . $match[3][1];
			}
			/**
			* Pl: izmok -> izom,barmok -> barom,halmok -> halom
			*/
			else if(preg_match('#([rzl]+)(m)$#iu',$szoTo,$match)){
				return preg_replace('#([rzl]+)m#iu','$1om',$szoTo);
			}
			/**
			* Pl: férgek -> féreg,méreg -> mérgek,
			* kérgek -> kéreg,étkek -> étek
			* vétkek -> vétek
			*/
			else if(preg_match('#[é]+[rt]{1}([gk]{1})$#iu',$szoTo,$match)){
				$csere = ['g' => 'eg','k' => 'ek'];
				return preg_replace('#' . $match[1] . '$#iu',$csere[$match[1]],$szoTo);
			}
			
			/**
			* Magánhangzó Á és É
			*/
			else if(preg_match('#([áé]{1})k$#iu',$word,$match)){
				$csere = ['á' => 'a','é' => 'e'];
				return preg_replace('#([áé]{1})k$#iu',$csere[$match[1]],$szoTo);
			}
			/**
			* Egyéb esetek. Pl halak -> hal
			*/
			else {
				/**
				* urak -> úr,kezek -> kéz,stb...
				*/
				if(preg_match('#^[^öüóúőéáűauie]*([ue]){1}[^öüóúőéáűauie]+$#iu',$szoTo,$match)){
					$csere = ['u' => 'ú','e' => 'é'];
					$szoTo = preg_replace('#^([^öüóúőéáűauie]*)[ue]{1}([^öüóúőéáűauie]+)$#iu','$1' . $csere[$match[1]] . '$2',$szoTo);
				}
				return $szoTo;
			}
		} else if(preg_match('#[üőó]+k$#iu',$word)) {
			return preg_replace('#k$#iu','',$word);
		}
	}
	
	public function isPlural($word){
		return preg_match('#[eöoaéá]{1}k$#iu',$word);
	}
}
