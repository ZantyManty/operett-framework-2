<?php

namespace Csifo\Grammar\Enu;

use Csifo\Grammar\Interfaces\IPlural as IPlural;

class Plural implements IPlural {
	
	protected $exceptions = [
		'files'	=>	'file',
		'teeth'	=>	'tooth'
	];
	
	public function getNonPlural($name){
		if(isset($this->exceptions[$name])){
			return $this->exceptions[$name];
		} else {
			if(preg_match('#^(\S+?)(?:s|es|ies|oes(?:\b))$#i',$name,$nonPlural)){
				return $nonPlural[1];
			} else {
				return $name;
			}
		}
	}
	
	public function getNonPluralExtended($name){
		$flipped = array_flip($this->exceptions);
		foreach($flipped as $flip){
			if(preg_match('#\S+?' . $flip . '\b#i',$name)){
				return preg_replace('#' . $flip . '\b#i',$this->exceptions[$flip],$name);
			}
		}
		
		return $this->getNonPlural($name);
	}
	
	public function isPlural($name){
		return (isset($this->exceptions[$name]) || preg_match('#^(\S+?)(?:s|es|ies|oes(?:\b))$#i',$name));
	}
}