<?php

namespace Csifo\Encryption\Interfaces;


interface IPassword {
	
	public function hash(string $password);
	
	public function check(string $password,string $password2);
	
}