<?php

declare(strict_types = 1);

namespace Csifo\Encryption;

use Csifo\Encryption\Interfaces\IPassword;
use Csifo\Core\Interfaces\IConfig;

class Password implements IPassword {
	
	protected $config = [];
	
	public function __construct(IConfig $config){
		$this->config = $config->app('Password');
	}
	
	public function hash(string $password){
		$peppered = $this->pepperPassword($password);
		return password_hash($peppered,$this->config['Algo'],$this->config['Options']);
	}
	
	public function check(string $password,string $hashedPassword){
		$peppered = $this->pepperPassword($password);
		return password_verify($peppered, $hashedPassword);
	}
	
	protected function pepperPassword(string $password){
		return hash_hmac($this->config['PepperAlgo'], $password, $this->config['Pepper']);
	}
	
}