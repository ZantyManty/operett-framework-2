<?php

namespace Csifo\Cache\Cache\File;

use Csifo\Cache\Cache\Interfaces\ICacheItem as ICacheItem;

class CacheItem implements ICacheItem {
	
	private $key;
	
	private $value;
	
	private $expiresAt = null;
	
	/*
	* Is NOT expired
	*/
	private $isHit = false;
	
	public function __construct($key){
		$this->key = $key;
	}
	
	public function get(){
		
        if ($this->isHit()) {
            return $this->value;
        }

        return null;
    }
	
	public function getKey(){
        return $this->key;
    }
	
	public function isHit(){
		
		if (!$this->isHit) {
            return false;
        }
		
        if (null === $this->expiresAt) {
            return true;
        }
		
        $now = new \DateTime();
        return $now < $this->expiresAt;
	}
	
	public function set($value){
        $this->isHit = true;
        $this->value = $value;
        return $this;
    }
	
	public function expiresAt($expiration){
		if($expiration instanceof \DateTime){
			$this->expiresAt = $expiration;
		} else if(is_string($time)){
			$this->expiresAt = new \DateTime($time);
		}
        
        return $this;
    }
	
	public function expiresAfter($time){
		if(is_int($time)){
			$now = new \DateTime();
			$future = (new \DateTime())->add(\DateInterval('PT' . $time . 'S'));
			
			$diff = $future->diff($now);
			$this->expiresAt = $diff;
			
		} else if($time instanceof \DateTime){
			$this->expiresAt = $time;
		} else {
			$this->expiresAt = null;
		}
	}
}