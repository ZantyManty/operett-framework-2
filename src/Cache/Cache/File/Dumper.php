<?php

namespace Csifo\Cache\Cache\File;

use Csifo\Cache\Cache\Interfaces\IDumper as IDumper;

class Dumper implements IDumper {
	
	protected $closure;
	protected $delimiter = "\t";
	protected $fileString = '';
	
	public function __construct(){
		$this->closure = new Closure();
	}
	
	public function dumpArray($array,$delimiterNumber = 1){
		if(!empty($array)){
			$this->fileString .= '[';
			foreach($array as $key => $elem){
				if(is_array($elem) && !empty($elem)){
					$this->fileString .= (is_int($key) ? "{$key}" : "'{$key}'") . "=>";
					$this->dumpArray($elem,($delimiterNumber + 1));
				} else if(is_callable($elem)){
					$this->fileString .= (is_int($key) ? "{$key}" : "'{$key}'") . "=>" . $this->closure->extractClosure($elem);
				} else {
					$this->fileString .= (is_int($key) ? "{$key}" : "'{$key}'") . "=>" . $this->getValidValue($elem) . ",";
				}
			}
			$this->fileString .= ']' . ($delimiterNumber > 1 ? ',' : ';');
		}
	}
	
	protected function getValidValue($element){
		if(is_array($element) && empty($element))
			return "[]";
		else if(is_bool($element) && $element == true)
			return "true";
		else if(is_bool($element) && $element == false)
			return "false";
		else if(is_null($element))
			return "null";
		else if(is_int($element))
			return intval($element);
		else
			return "'" . $element . "'";
	}
	
	protected function startFile(){
		$this->fileString = '<?php ';
	}
	
	protected function endFile(){
		$this->fileString .= '?>';
	}
	
	public function cacheArray($file,$array,$return = true){
		$this->startFile();
		if($return) $this->fileString .= 'return ';
		$this->fileString .= $this->dumpArray($array) . ';';
		$this->endFile();
		$this->putFile($file);
	}
	
	public function getArray($file){
		if(file_exists($file)){
			return include $file;
		}else {
			return false;
		}
	}
	
	protected function putFile($file){
		file_put_contents($file,$this->fileString);
	}
	
}