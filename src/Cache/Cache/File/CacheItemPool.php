<?php

namespace Csifo\Cache\Cache\File;

use Csifo\Cache\Cache\Interfaces\ICacheItemPool as ICacheItemPool;
use Csifo\Cache\Cache\Interfaces\ICacheItem as ICacheItem;
use Csifo\Core\Interfaces\IConfig as IConfig;

class CacheItemPool implements ICacheItemPool {
	
	protected $items = [];
	protected $cachePath = '';
	
	public function __construct(IConfig $config){
		$this->cachePath = $config->app('CACHE_DIR');
	}
	
	public function getItem($key){
		
		if(isset($this->items[$key])) {
			return $this->items[$key];
		}
		
		if(file_exists($this->cachePath . '/' . $key)){
			return unserialize(gzdecode(file_get_contents($this->cachePath . '/' . $key)));
		}
		
		return new CacheItem($key);
	}
	
	public function getItems($keys = []){
		$trunk = [];
		
		foreach($keys as $key){
			$trunk[$key] = $this->items[$key];
		}
		
		return $trunk;
	}
	
	public function hasItem($key){
		return isset($this->items[$key]);
	}
	
	public function clear(){
		$this->items = [];
	}
	
	public function deleteItem($key){
		unset($this->items[$key]);
		if(file_exists($this->cachePath . '/' . $key))
			unlink($this->cachePath . '/' . $key);
	}
	
	public function deleteItems($keys = []){
		foreach($keys as $key){
			unset($this->items[$key]);
		}
	}
	
	public function save(ICacheItem $item){
		file_put_contents($this->cachePath . '/' . $item->getKey(),gzencode(serialize($item)));
	}
	
	public function saveDeferred(ICacheItem $item){
		foreach($this->items as $key => $item){
			$this->save($item);
			unset($this->items[$key]);
		}
	}
	
}