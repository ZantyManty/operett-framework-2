<?php

namespace Csifo\Cache\Cache\File;

/*
* Closure cache class
*/

class Closure {
	
	public function extractClosure($closure){
		$ref  = new \ReflectionFunction($closure);
		$file = new \SplFileObject($ref->getFileName());
		$file->seek($ref->getStartLine()-1);
		$content = '';
		$currentLine = 0;
		$numOfLines = ($ref->getEndLine() - $ref->getStartLine()) + 1;
		
		while ($file->key() < $ref->getEndLine()) {
			if($currentLine == 0){
				$content .= preg_replace('#^[\S\s]*?function#i','function',$file->current());
			} else if($numOfLines == $file->key()){
				$content .= preg_replace('#[,;](?:function\([\S\s]*?\){[\S\s]*)|(?:\[|[\"\'])$#i','',$file->current());
			} else {
				$content .= $file->current();
			}
			
			$currentLine++;
			$file->next();
		}
		
		return preg_replace('#\);$#i','',$content);
		
	}
}