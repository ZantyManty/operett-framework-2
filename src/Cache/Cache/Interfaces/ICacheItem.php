<?php

namespace Csifo\Cache\Cache\Interfaces;

interface ICacheItem {
	
    public function getKey();

    public function get();

    public function isHit();

    public function set($value);

    public function expiresAt($expiration);

    public function expiresAfter($time);
}