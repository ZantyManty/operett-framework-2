<?php

namespace Csifo\Cache\Cache\Interfaces;

interface ICacheItemPool {
	
	public function getItem($key);
	
	public function getItems($keys = []);
	
	public function hasItem($key);
	
	public function clear();
	
	public function deleteItem($key);
	
	public function deleteItems($keys = []);
	
	public function save(ICacheItem $item);
	
	public function saveDeferred(ICacheItem $item);
}