<?php

namespace Csifo\Cache\Cache\Interfaces;

interface IInvalidArgumentException extends CacheException {
	
}