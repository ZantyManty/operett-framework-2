<?php

namespace Csifo\Cache\SimpleCache;

use Csifo\Core\Interfaces\IConfig;
use Csifo\Core\Interfaces\FailSafe;
use Csifo\Cache\SimpleCache\Interfaces\ICache;

class File implements ICache,FailSafe {
	
	protected $cachePath = '';
	protected $ttl = 0;
	
	public function __construct(IConfig $config,$ttl = 31449600){ //Default TTL -> 1 year
		$this->cachePath = $config->app('CACHE_DIR');
		$this->ttl = $ttl;
	}
	
	public function checkObjectStatus() : bool {
		if(file_exists($this->cachePath) === false){
			return mkdir($this->cachePath,0775);
		} else {
			return true;
		}
	}
	
	public function get($key, $default = null){
		if(file_exists($this->cachePath . '/' . $key) === true){
			
			if(@filemtime($this->cachePath . '/' . $key) <= time()){
				unlink($this->cachePath . '/' . $key);
				return $default;
			}
			return unserialize(gzdecode(file_get_contents($this->cachePath . '/' . $key)));
		}
		return $default;
	}
	
	public function set($key, $value, $ttl = null){
		$ttl = ($ttl != null) ? $ttl : $this->ttl;
		$expires = time() + $ttl;

		file_put_contents($this->cachePath . '/' . $key,gzencode(serialize($value)));
		touch($this->cachePath . '/' . $key,$expires);
	}
	
	public function delete($key){
		unlink($this->cachePath . '/' . $key);
	}
	
	public function clear() {
		$files = scandir($this->cachePath);
		foreach($files as $file){
			if($file !== '.' && $file !== '..')
				unlink($this->cachePath . '/' . $file);
		}
	}
	
	public function getMultiple($keys, $default = null){
		if(is_array($keys) === true){
			$data = [];
			foreach($keys as $key){
				$data[$key] = $this->get($key);
			}
			return $data;
		} else {
			return $default;
		}
	}
	
	public function setMultiple($values, $ttl = null){
		if(is_array($values) === true){
			foreach($values as $key => $value){
				$this->set($key,$value,$ttl);
			}
		}
	}
	
	public function deleteMultiple($keys){
		if(is_array($keys) === true){
			foreach($keys as $key){
				unlink($this->cachePath . '/' . $key);
			}
		}
	}
	
	public function has($key){
		return file_exists($this->cachePath . '/' . $key);
	}
}