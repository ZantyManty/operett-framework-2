<?php

namespace Csifo\Cache\SimpleCache;

use Csifo\Core\Interfaces\IConfig;
use Csifo\Core\Interfaces\FailSafe;
use Csifo\Cache\SimpleCache\Interfaces\ICache;

class Memcache implements ICache,FailSafe {
	
	protected $memcache = null;
	
	public function __construct(IConfig $config){
		if(class_exists('\Memcache')){
			$this->memcache = new \Memcache;
			$host = $config->cache('Memcache')['server'];
			$this->memcache->addServer($host);
		}
	}
	
	public function checkObjectStatus() : bool{
		return ($this->memcache != null) ? true : false;
	}
	
	public function get($key, $default = null){
		if($this->has($key)){
			return $this->memcache->get($key);
		}
		return $default;
	}
	
	public function set($key, $value, $ttl = null){
		$this->memcache->set($key,$value,0,$ttl);
	}
	
	public function delete($key){
		$this->memcache->delete($key);
	}
	
	public function clear(){
		$this->memcache->flush();
	}
	
	public function getMultiple($keys, $default = null){
		$data = [];
		foreach($keys as $key){
			$data[] = $this->memcache->get($key);
		}
		return $data;
	}
	
	public function setMultiple($values, $ttl = null){
		if(is_array($values) === true){
			foreach($values as $key => $value){
				$this->memcache->set($key,$value,0,$ttl);
			}
		}
	}
	
	public function deleteMultiple($keys){
		foreach($keys as $key){
			$this->memcache->delete($key);
		}
	}
	
	public function has($key){
		return ($this->memcache->get($key) !== false) ? true : false;
	}
	
}