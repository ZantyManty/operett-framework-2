<?php

namespace Csifo\Core;

use Csifo\Factory\Controller 		as ControllerFactory;
use Csifo\Http\Interfaces\IRoutes 	as IRoutes;

class App {
	
	protected static $container = null;
	protected static $config = null;
	
	public function __construct(){
		self::$config = new Config();
		
		/*
		* Container
		*/
		$containerConfig = new \Config\Container;
		$containerDrivers = new \Config\Drivers;
		$containerModules = new \Config\Modules;
		self::$container = new Container\Container($containerConfig,$containerDrivers,$containerModules,self::$config);
		
		/*
		* Set error reporting
		*/
		$dev = self::$config->app('DEV_MODE');
		$this->setErrorReporting($dev);
		
		/*
		* Run development helper tasks...
		*/
		if($dev === true){
			$this->runDevHooks($dev);
		}
	}
	
	protected function setErrorReporting($dev = true){
		if($dev === true){
			error_reporting( E_ALL );
			ini_set('display_errors', 1);
		} else {
			error_reporting(0);
			ini_set('display_errors', 0);
			ini_set("error_log", "/tmp/php-error.log");
		}
	}
	
	public function runDevHooks($dev){
		if(class_exists('App\DevHooks') === true){
			/*
			* Dev jobs runs immediately in the constructor,
			* so we can delete the object right after resolving!
			*/
			$hooks = self::$container->resolveClass('App\DevHooks');
			unset($hooks);
		}
	}
	
	public static function getResolver(){
		return self::$container;
	}
	
	/*
	* Is app running from command line,or web server
	*
	* @return boolean
	*/
	public static function isCli(){
		return (php_sapi_name() === 'cli') ? true : false;
	}
	
	/*
	* App's start point
	*
	* @param $cmdArgs array
	*/
	public function run($cmdArgs = []){
		
		$cli = self::isCli();
		
		if($cli === false){
			
			$routes = self::$container->get(IRoutes::class);
			/*
			* Router return with the controller's,and the action's name [Or a Closure]
			*/
			$respone = $routes->handle();
			/*
			* End of the story...
			*/
			$this->response($respone);
			
		} else if($cli === true){
			
			$params = [
				[
					'id'		=> \Csifo\Console\Interfaces\IConsole::class,
					'method'	=> '__construct',
					'args'		=> ['cmdArgs' => $cmdArgs]
				]
			];
			
			self::$container->addCustomParameter($params);
			$cmd = self::$container->get('Csifo\Console\Interfaces\IConsole');
			echo $cmd->parse();
		} else {
			echo 'OOPS...';
		}
	}
	
	protected function response($response){
		if($response->hasHeader('Content-Type') == false)
			$response->withHeader('Content-Type','text/html');
		
		$response->respond();
	}
}