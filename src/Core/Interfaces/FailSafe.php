<?php

namespace Csifo\Core\Interfaces;

interface FailSafe {
	public function checkObjectStatus() : bool;
}