<?php

namespace Csifo\Core\Interfaces;

interface IConfig {
	
	public function get($name);
	
}