<?php

declare(strict_types=1);

namespace Csifo\Core\Scalar;

use Csifo\Core\Scalar\Interfaces\IScalar;

class Number implements IScalar {
	
	use Traits\Factory;
	
	protected $value = 0;
	
	public function __construct(int $value = 0){
		$this->value = $value;
	}
	
	public function val(){
		return $this->value;
	}
	
}