<?php

declare(strict_types=1);

namespace Csifo\Core\Scalar;

use Csifo\Core\Scalar\Interfaces\IScalar;

class Text implements IScalar {
	
	use Traits\Factory;
	
	protected $value = '';
	
	public function __construct(string $value = ''){
		$this->value = $value;
	}
	
	public function replace($search,$replace){
		return new $this(str_replace($search,$replace,$this->value));
	}
	
	public function preplace($search,$repalce){
		return new $this(preg_replace($pattern,$replace,$this->value));
	}
	
	public function capital(){
		return new $this(ucfirst($this->value));
	}
	
	public function length(){
		return strlen($this->value);
	}
	
	public function substr($start,$length = 0){
		if($length === 0){
			return new $this(substr($this->value,$start));
		} else {
			return new $this(substr($this->value,$start,$length));
		}
	}
	
	public function split($delimiter){
		return explode($delimiter,$this->value);
	}
	
	public function match($pattern){
		return (preg_match($pattern,$this->value,$match)) ? $match : [];
	}
	
	public function matchAll($pattern){
		return (preg_match_all($pattern,$this->value,$match)) ? $match : [];
	}
	
	public function val(){
		return $this->value;
	}
	
}