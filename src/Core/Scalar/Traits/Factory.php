<?php

namespace Csifo\Core\Scalar\Traits;

trait Factory {
	
	protected $scalarTypes = [
		'int' => 'Number',
		'string' => 'Text'
	];
	
	protected function getScalar($value,$type){
		if(isset($this->scalarTypes[$type])){
			$className = '\\' . $this->scalarTypes[$type];
			return new $className($value);
		} else {
			throw new \Exception('There is no type class for ' . $type . '!');
		}
	}
	
}