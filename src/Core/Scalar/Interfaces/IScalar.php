<?php

namespace Csifo\Core\Scalar\Interfaces;

interface IScalar {
	
	public function val();
}