<?php

namespace Core;

class Autoload {
	
	public static function load($class){
		if(\defined($class) === false){
			$file = str_replace(['\\','Csifo'],['/','src'],$class);
			if(file_exists(ROOT . '/' . $file . '.php')){
				include ROOT . '/' . $file . '.php';
				\define($class,1);
			} else {
				return false;
			}
		}
	}
}