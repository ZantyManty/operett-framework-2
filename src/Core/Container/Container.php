<?php

namespace Csifo\Core\Container;

use Csifo\Psr\Container\IContainer;
use Csifo\Config\Interfaces\IContainer	as Config;
use Csifo\Config\Interfaces\IDrivers	as Drivers;
use Csifo\Config\Interfaces\IModules	as Modules;
use Csifo\Core\Interfaces\IConfig		as CommonConfig;

class Container implements IContainer {
	
	protected $ids			= [];
	protected $classes		= [];
	protected $drivers		= [];
	protected $globalParams	= [];
	
	protected $resolved_singletons = [];
	
	protected $primitives = [
		'int'		=> 1,
		'string'	=> 1,
		'float'		=> 1,
		'bool'		=> 1,
		'callable'	=> 1,
		'array'		=> 1
	];
	
	protected $scalarClasses = [];
	
	public function __construct(Config $config,Drivers $drivers,Modules $modules,CommonConfig $commonConfig){
		/*
		* Services
		*/
		$classes = $config->classes;
		/*
		* Add modules
		*/
		$classes += $modules->modules;
		
		/*
		* Drivers
		*/
		$this->drivers = $drivers->drivers;
		$configDrivers = $commonConfig->get('drivers');
		
		foreach($configDrivers as $name => $entry){
			$classes += $this->drivers[$name][$entry];
		}
		
		/*
		* Common classes
		*/
		foreach($classes as $id => $array){
			$class = $array['class'];
			unset($array['class']);
			$this->ids[$id] = $class;
			$this->classes[$class] = $array;
		}
		
		/*
		* Itself
		*/
		$containerClass = __CLASS__;
		$this->ids['Csifo\Psr\Container\IContainer'] = $containerClass;
		$this->resolved_singletons[$containerClass] = $this;
		
		/*
		* Callables,etc...
		*/
		$config->extra($this);
	}
	
	public function addClosure($id,callable $callable){
		$this->ids[$id] = $callable;
	}
	
	public function addGlobalParameters(array $params){
		$this->globalParams += $params;
	}
	
	public function addCustomParameter(array $parameters){
		foreach($parameters as $param){
			$method = (isset($param['method']) === true && empty($param['method']) === false) ? $param['method'] : '__construct';
			if(isset($param['class']) === true){
				$this->classes[$param['class']]['args'][$method] = $param['args'];
			} else if(isset($param['id']) === true){
				$class = $this->ids[$param['id']];
				$this->classes[$class]['args'][$method] = $param['args'];
			} else {
				throw new \Exception('There\'s no ID or CLASS at this custom parameter!');
			}
		}
	}
	
	public function addInstance($class,$instance){
		$this->resolved_singletons[$class] = $instance;
	}
	
	public function swapDriver($name,$entry){
		if(isset($this->drivers[$name][$entry]) === true){
			foreach($this->drivers[$name][$entry] as $id => $classData){
				$class = $classData['class'];
				unset($classData['class']);
				$this->ids[$id] = $class;
				$this->classes[$class] = $classData;
			}
		} else {
			throw new \Exception("There's no driver entry [ $name -> $entry ]!");
		}
	}
	
	public function getClassById($id) : string {
		if(isset($this->ids[$id]) === true){
			return $this->ids[$id];
		}
		
		return '';
	}
	
	/*
	* Van-e ilyen id-vel osztály (PSR 11)
	*/
	public function has($id){
		return isset($this->ids[$id]);
	}
	
	public function get($id){
		/*
		* Megnézzük,van-e egyáltalán ilyen id
		* Ha nincs,akkor feltételezzük,hogy egy osztály
		*/
		if($this->has($id) === false){
			throw new NotFoundException($id);
			//return $this->resolveClass($id);
		}
		
		/*
		* Ha Closure alapú
		*/
		if(is_callable($this->ids[$id]) === true){
			return $this->ids[$id]($this);
		}
		
		/*
		* Feloldás
		*/
		$className = $this->ids[$id];
		$object = $this->resolveClass($className);
		/*
		* Fail safe mechanism
		*/
		if(isset($this->classes[$className]['backups']) === true){
			if($object->checkObjectStatus() === false){
				/*
				* Check for the backups
				*/
				foreach($this->classes[$className]['backups'] as $class){
					if(class_exists($class) === true){
						$object = $this->resolveClass($class);
						if($object->checkObjectStatus() === true){
							return $object;
						}
					}
				}
				
				throw new \Exception('There are no toher backup classes for [ ' . $className . ' ]!');
			}
		}
		return $object;
	}
	
	public function resolveArguments(array $reflectionArguments,$class = '',$method = ''){
		/*
		* Temporary array for the resolved parameters
		*/
		$params_for_Resolve = [];
		$i = 0;
		foreach($reflectionArguments as $param){
			/*
			* Parameter's name
			*/
			$param_name = $param->getName();
			
			/**
			* And has a type
			*/
			$paramHasType = $param->hasType();
			
			/*
			* If we have preloaded class specified parameter value given for loading,we use it,
			* and push the loop to the next param to resolve.
			*/
			if(isset($this->classes[$class]['args'][$method][$param_name])){
				
				$paramValue = $this->classes[$class]['args'][$method][$param_name];
				
				if($paramHasType === false){
					$params_for_Resolve[$i] = $paramValue;
				} else {
					$paramTypeName = $param->getType()->getName();
					if(isset($this->primitives[$paramTypeName]) === true){
						$params_for_Resolve[$i] = $paramValue;
					} else {
						$params_for_Resolve[$i] = $this->resolveScalar($paramTypeName,$paramValue,false);
					}
				}
				
				++$i;
				continue;
			}
			/*
			* If there is a global parameter given,we use it!
			*/
			else if(isset($this->globalParams[$param_name])) {
				$params_for_Resolve[$i] = $this->globalParams[$param_name];
				++$i;
				continue;
			}
			
			/*
			* If the parameter have a default value,we use it!
			*
			* Example:
			* public function view($number = 10)...
			* In that case,10 is a default value
			*/
			if($param->isDefaultValueAvailable()){
				$params_for_Resolve[$i] = $param->getDefaultValue();
				++$i;
				continue;
			} 
			/*
			* If not... Then we are continue searching...
			*/
			else if($paramHasType === true){
				
				/*
				* Get the type's name
				*/
				$param_type_name = $param->getType()->getName();
				$typeNameLow = strtolower($param_type_name);
				
				/*
				* If it is not a primitive (int,string,etc...) we try to resolve it
				* For that,we call this function recursive,because if it's a class,
				* it has other params,and so and so....
				*/
				if(!isset($this->primitives[$typeNameLow])){
					
					if(isset($this->scalarClasses[$param_type_name]) === false){
						/*
						* If the container has the specified class,we get it
						*/
						if($this->has($param_type_name)){
							$params_for_Resolve[$i] = $this->get($param_type_name);
						} 
						/*
						* If not,we resolve it as a classic class...
						*/
						else {
							$params_for_Resolve[$i] = $this->resolveClass($param_type_name);
						}
					} else {
						$params_for_Resolve[$i] = $this->resolveScalar($param_type_name,null,true,$param_name,$class,$method);
					}
					
				} else {
					/*
					* If we can't resolve it,throw an Exception
					*/
					throw new \Exception('Couldn\'t resolve parameter: [' . $param_name . '] In class: [' . $class . ']');
				}
			} else {
				/*
					* If we can't resolve it,throw an Exception
					*/
				throw new \Exception('Couldn\'t resolve parameter: [' . $param_name . '] In class: [' . $class . '] (There\'s NO TYPE!!)');
			}
			/*
			* Current param's index...
			*/
			++$i;
		}
		
		return $params_for_Resolve;
	}
	
	/*
	* Register the scalar classes
	* @param $scalarData array Scalar classes
	*/
	public function registerScalarClasses($scalarData){
		foreach($scalarData as $class => $sd){
			class_alias($sd['class'],$class);
		}
		$this->scalarClasses = $scalarData;
	}
	
	/*
	* Resolve a scalar
	*/
	protected function resolveScalar($type,$value = null,$lookForDefault = true,$paramName = '',$class = '',$method = ''){
		
		if(isset($this->scalarClasses[$type]) === false) return;
		
		$scalarClass = $this->scalarClasses[$type]['class'];
		
		if($lookForDefault === true){
			$docBlock = (new \ReflectionMethod($class,$method))->getDocComment();
			if($docBlock !== false){
				if(preg_match('#' . $paramName . '\s*\([\'"]*([\w -_]+?)[\'"]*\)#ui',$docBlock,$default)){
					$value = $default[1];
				} else {
					$value = $this->scalarClasses[$type]['default'];
				}
			} else {
				$value = $this->scalarClasses[$type]['default'];
			}
		}
		
		return new $scalarClass($value);
	}
	
	public function resolveClass($class,$method = '__construct',$onlyParams = false){
		
		/*
		* Ha singleton,és már fel van oldva,nem szarjuk el az 
		* időt (meg a memóriát) feleslegesen,
		* és visszatérünk a cache-elt objektummal...
		*/
		if(isset($this->resolved_singletons[$class])){
			return $this->resolved_singletons[$class];
		}
		
		/*
		* Létrehozunk egy metódusra specializált reflection osztály,
		* majd rögtön el is kérjük a paramétereit (Vagy ha úgy tetszik,az argument-eket)
		*
		* A "$method_arguments" egy objektum tömb lesz,alább loop-oljuk
		*/
		$method_arguments = (method_exists($class,$method) === true) ? (new \ReflectionMethod($class,$method))->getParameters() : [];
		
		/*
		* Elkérjük a paramétereket
		*/
		$arguments = $this->resolveArguments($method_arguments,$class,$method);
		
		/**
		* Ha csak a paraméterek kellenek,akkor visszatérünk azokkal.
		*/
		if($onlyParams === true){
			return $arguments;
		}
		/*
		* Ha nem,akkor bedobjuk az előzőleg feloldott paramétereket,
		* és létrehozatunk egy új objektumot.
		*/
		else {
			/*
			* Példányosítunk,DE ha nincs konstruktor,akkor a nélkül
			*/
			
			/*
			* HA VAN konstruktor,de egy másik metódust hívtak,akkor attól még a konstruktort is fel kell oldani
			*/
			$object = 
				(method_exists($class,'__construct') === true) 
					? new $class(...$arguments)
					: new $class();

			if(isset($this->classes[$class]['singleton']) === true && $this->classes[$class]['singleton'] === true){
				/*
				* Ha singleton,akkor el cache-eljük későbbre is,
				* ne kelljen szopni az újbóli feloldással,és időt vesztegetni
				*/
				$this->resolved_singletons[$class] = $object;
			}
			
			return $object;
		}
	}
	
	public function resolveClassWithoutCallMethod($class,$method) : array {
		$object = $this->resolveClass($class);
		if(method_exists($object,$method) === true){
			$args	= $this->resolveArguments((new \ReflectionMethod($class,$method))->getParameters(),$class,$method);
		} else {
			$args = [];
		}
		
		return ['object' => $object,'args' => $args];
	}
	
	/*
	* Closure-ökhöz,névtelen függvényekhet...
	*/
	public function resolveFunction(callable $callable,$call = false){
		$reflection = new \ReflectionFunction($callable);
		$arguments  = $reflection->getParameters();
		$resolved_arguments = $this->resolveArguments($arguments);
		
		return ($call === true ? call_user_func_array($callable,$resolved_arguments) : $resolved_arguments);
	}
}