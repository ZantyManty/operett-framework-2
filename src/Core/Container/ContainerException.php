<?php

namespace Csifo\Core\Container;

use Csifo\Psr\Container\IContainerException;

class ContainerException extends \Exception implements IContainerException {
	
	public function __construct($message = '',$code = 0){
		parent::__construct($message, $code);
	}
}