<?php

namespace Csifo\Core\Container;

use Csifo\Psr\Container\INotFoundException;

class NotFoundException extends ContainerException implements INotFoundException {
	
	public function __construct($message = '',$code = 0){
		$message = 'This service is not found: ' . $message;
		parent::__construct($message, $code);
	}
}