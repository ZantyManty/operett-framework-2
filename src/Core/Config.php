<?php

namespace Csifo\Core;

class Config implements Interfaces\IConfig {
	
	protected $config = [];
	
	public function __construct($fileName = ''){
		$fileName = ($fileName === '') ? ROOT . '/Config/config.php' : $fileName;
		include $fileName;
		$this->config['app'] 		= $application;
		$this->config['drivers'] 	= $drivers;
		$this->config['api'] 		= $api;
		$this->config['websocket'] 	= $websocket;
		$this->config['namespaces'] = $namespaces;
		$this->config['cache'] = $cache;
	}
	
	public function get($name){
		if(isset($this->config[$name])){
			return $this->config[$name];
		}
	}
	
	public function cache($name){
		return $this->config['cache'][$name];
	}
	
	public function app($name){
		return $this->config['app'][$name];
	}
	
	public function namespaces($name){
		return $this->config['namespaces'][$name];
	}
}