<?php

namespace Csifo\Console\Interfaces;

use Csifo\Psr\Container\IContainer;

interface IConsole {
	
	public function __construct(array $cmdArgs = [],IContainer $container);
	
	public function parse() : string;
}