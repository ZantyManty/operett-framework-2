<?php

namespace Csifo\Console\Commands;

use Csifo\Helpers\Interfaces\IFileHelper as IFileHelper;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Database\Interfaces\IConnection as IConnection;

class Generator {
	
	
	protected $config = null;
	protected $fileHelper = null;
	
	protected $moduleDirs = [
		'client',
		'Interfaces',
		'lang',
		'lang/en/index',
		'views'
	];
	
	public function __construct(IConfig $config,IFileHelper $helper){
		$this->config = $config;
		$this->fileHelper = $helper;
	}
	
	protected function prepareModul($moduleName){
		$moduleName = ucfirst(mb_strtolower($moduleName));
		$modulesDir = $this->config->app('MODULES_DIR') . '/' . $moduleName;
		
		/**
		* Creating dirs
		*/
		$this->fileHelper->createDir($modulesDir);
		chmod($modulesDir,0775);
		
		foreach($this->moduleDirs as $dir){
			$this->fileHelper->createDir($modulesDir . '/' . $dir);
			chmod($modulesDir . '/' . $dir,0775);
		}
		
		/**
		* Creating interfaces
		*/
		$this->createModelController($moduleName,$modulesDir . '/Interfaces','IController');
		$this->createModelController($moduleName,$modulesDir . '/Interfaces','IModel');
		
		touch($modulesDir . '/client/' . $moduleName . '.css');
		touch($modulesDir . '/client/' . $moduleName . '.js');
	}
	
	public function module($moduleName){
		
		$this->prepareModul($moduleName);

		/**
		* Sample controller,and model
		*/
		$this->createModelController($moduleName,$modulesDir);
		$this->createModelController($moduleName,$modulesDir,'Model');
		
		/**
		* Views,lang,etc
		*/
		touch($modulesDir . '/views/index.view.php');
		touch($modulesDir . '/lang/en/index/index.lang.php');
	}
	
	public function crud($moduleName){
		/**
		* Create module
		*/
		$this->prepareModul($moduleName);
	}
	
	protected function createModelController($moduleName,$modulesDir,$type = 'Controller',$crud = '',$tableName = '') {
		$template = dirname(__FILE__) . '/Generator/Templates/' . $crud . $type . '.tpl';
		$content = file_get_contents($template);
		$content = str_replace('{#moduleName#}',$moduleName,$content);
		if($tableName !== ''){
			$content = str_replace('{#tableName#}',$tableName,$content);
		}
		file_put_contents($modulesDir . '/' . $type . '.php','<?php ' . PHP_EOL . $content);
	}
	
}