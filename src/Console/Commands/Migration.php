<?php

namespace Csifo\Console\Commands;

use Csifo\Helpers\Interfaces\IFileHelper as IFileHelper;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Database\Interfaces\IConnection as IConnection;

class Migration {
	
	protected $config;
	protected $fileHelper;
	protected $connection;
	
	public function __construct(IFileHelper $helper,IConfig $config,IConnection $connection){
		$this->config		= $config;
		$this->fileHelper	= $helper;
		$this->connection	= $connection;
	}
	
	public function up($file = '*'){
		$path = $this->config->app('MIGRATION_PATH');
		$files = glob($path . "/{$file}.php");
		
		foreach($files as $file){
			
			$migrationClass	 = 'App\Migrations\\' . ucfirst(str_replace('.php','',basename($file)));
			$migrationObject = new $migrationClass;
			
			/*
			* Get the methods with reflection
			*/
			$reflection = new \ReflectionClass($migrationClass);
			$methods = $reflection->getMethods();
			
			/*
			* Get the current migration file's content,
			* because we need the PHPDoc comments
			*/
			$fileContent = file_get_contents($file);
			
			foreach($methods as $method){
				$methodName = $method->name;
				$tableOnject = resolve('Csifo\Database\Migration\Interfaces\ItableSchema');
				$migrationObject->$methodName($tableOnject);
				
				/*
				* If the table has no name,we use the method's name...
				*/
				if($tableOnject->getTableName() == ''){
					$tableOnject->name($methodName);
				}
				
				/*
				* Get back the final name
				*/
				$tableName = $tableOnject->getTableName();
				
				/*
				* Get the comment for the table 
				* (Only if there a PHPDoc commant AND the developer doesn't added comment to the class)
				*/
				if($tableOnject->getComment() == ''){
					if(preg_match('#(?:}[\s]*?|class[\s]*?[\w _-]+?[\s]*?{[\s]*?)\/\*([\S\s]+?)\*\/[\s]*?public[\s]+?(?:[\s]*?staic)?[\s]*?function ' . $methodName . '#i',$fileContent,$comment)){
						$commentString = preg_replace('#^(?:[\s]+\*|[\s]+)#im','',$comment[1]);
						$tableOnject->comment(str_replace(["'"],["\'"],$commentString));
					}
				}
				
				$SQLstring = (string)$tableOnject;

				try{
					$this->connection->GiveUpdate($SQLstring);
					if($this->connection->GiveExists("SHOW TABLES LIKE '{$tableName}'"))
					{ echo "Table [ {$tableName} ] created!" . PHP_EOL; }
				} catch(\PDOException $e){
					echo $e->getMessage() . PHP_EOL;
				}
			}
		}
	}
}