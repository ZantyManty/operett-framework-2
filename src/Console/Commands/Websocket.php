<?php

namespace Csifo\Console\Commands;

class Websocket {
	
	public function run(){
		if(class_exists('\App\\MyWsServer')){
			$server = new \App\MyWsServer;
		} else {
			$server = resolve('Csifo\Http\Websocket\Interfaces\IServer');
		}
		/*
		* Resolve the needed objects
		*/
		$config = resolve('Csifo\Core\Interfaces\IConfig');
		$controllerFactory = resolve('Csifo\Factory\Interfaces\IController');
		
		$server->load($config,$controllerFactory)->connect()->run();
	}
	
}