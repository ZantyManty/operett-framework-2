
namespace App\Modules\{#moduleName#};

use Csifo\Mvc\Interfaces\IView;
use App\Modules\{#moduleName#}\Interfaces\IModel;
use App\Modules\IController as GlobalController;

/*
* {#moduleName#} module
*/
class Controller implements Interfaces\IController {
	
	protected $globalController;
	protected $model;
	protected $view;
	
	public function __construct(GlobalController $controller,IModel $model,IView $view){
		$this->globalController = $controller;
		$this->model = $model;
		$this->view	 = $view;
	}
	
	/**
	* List view
	*/
	public function index(){
		{#moduleName#} = $this->model->get{#moduleName#}();
		$this->view->assign('entities',{#moduleName#});
		return $this->view->render('index');
	}
	
	/**
	* Edit/New
	*/
	public function edit($id = 0,IServerRequest $request){
		$post = $request->getParsedBody();
		$entity = (intval($id) > 0) ? $this->model->getEntity($id) : new \Model\Dto\Entity();
		
		if(!empty($post)){
			
			$id = $this->model->save($entity,$post);
			header('Location: /edit/' . $id);
		}
		
		$this->view->assign('Entity',$entity);
		
		return $this->view->render('edit');
	}
	
	/**
	* Delete
	*/
	public function remove($id){
		$this->model->remove($id);
		header('Location: /users');
	}
	
}