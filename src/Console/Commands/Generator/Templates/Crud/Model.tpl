
namespace App\Modules\{#moduleName#};

use App\Modules\IModel as GlobalModel;
use Csifo\Database\Connection\Interfaces\IResolver as Connections;

class Model implements Interfaces\IModel {
	
	protected $globalModel;
	protected $connection;
	
	public function __construct(GlobalModel $globalModel,Connections $connection){
		$this->globalModel = $globalModel;
		$this->connection = $connection->getConnection();
	}
	
	public function get{#moduleName#}(){
		return $this->connection->select('*')->from({#tableName#})->run();
	}
	
	public function saveEntity($entity,$data){
		$entity->fill($data);
		return $entity->save();
	}
	
	public function getEntity($id){
		return $this->connection->select('*')->from({#tableName#})->where('id','=',$id)->limit(1)->run();
	}
	
	public function remove($id){
		$entity = $this->getEntity($id);
		return $entity->delete();
	}
}