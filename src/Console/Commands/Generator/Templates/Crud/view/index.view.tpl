<div>
	<a href="/edit">{#entityAddNewLink#}</a>
	<table>
	<tr>
		<th>{#entityId#}</th>
		<th>Edit</th>
	</tr>
	|fe($entities as $entity)|
		<tr>
			<td>|$entity->{'id'}|</td>
			<td><a href="/edit/|$entity->{'id'}|" >{#entityEditLink#}</a></td>
		</tr>
	|/fe|
	</table>
</div>