
namespace App\Modules\{#moduleName#};

use App\Modules\IModel as GlobalModel;
use Csifo\Database\Connection\Interfaces\IResolver as Connections;

class Model implements Interfaces\IModel {
	
	protected $globalModel;
	protected $connection;
	
	public function __construct(GlobalModel $globalModel,Connections $connection){
		$this->globalModel = $globalModel;
		$this->connection = $connection->getConnection();
	}
}