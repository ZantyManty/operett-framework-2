
namespace App\Modules\{#moduleName#};

use Csifo\Mvc\Interfaces\IView;
use App\Modules\{#moduleName#}\Interfaces\IModel;
use App\Modules\IController as GlobalController;

/*
* {#moduleName#} module
*/
class Controller implements Interfaces\IController {
	
	protected $globalController;
	protected $model;
	protected $view;
	
	public function __construct(GlobalController $controller,IModel $model,IView $view){
		$this->globalController = $controller;
		$this->model = $model;
		$this->view	 = $view;
	}
	
	/**
	* Index
	*/
	public function index(){

	}
	
}