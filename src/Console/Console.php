<?php

namespace Csifo\Console;

use Csifo\Psr\Container\IContainer;
use Csifo\Console\Interfaces\IConsole as IConsole;
use Csifo\Core\Container\NotFoundException;

class Console implements IConsole {
	
	protected $arguments = [];
	protected $container = null;
	
	public function __construct(array $cmdArgs = [],IContainer $container){
		$this->arguments = $cmdArgs;
		$this->container = $container;
	}
	
	public function parse() : string {
		
		unset($this->arguments[0]); //It's just the "subrett" prefix
		
		if(empty($this->arguments) === false){
			
			if($this->arguments[1] === '?'){
				return $this->loadHelps();
			}
			
			/*
			* Get the class and method
			*/
			$temp = explode('.',$this->arguments[1]);
			
			$class = $temp[0];
			$method = (isset($temp[1]) === false || $temp[1] === '') ? '__construct' : $temp[1];
			
			if($method == '?'){
				$this->loadHelps();
			} else {
				unset($this->arguments[1]); //Drop the "class.method" part,we don't need it anymore
				
				/*
				* Parse the parameters
				*/
				$args = [];
				foreach($this->arguments as $param){
					$temp = explode('=',$param);
					if(isset($temp[1]) && $temp[1] != ''){
						$varName = preg_replace('#^-*#i','',$temp[0]); //Cut of the "-" prefix (If exists...)
						$args[$varName] = trim($temp[1]);
					}
				}
				
				/*
				* If the command defined in the system
				*/
				try {
					$classFullName = (class_exists('Csifo\Console\Commands\\' . $class) == true) ? 'Csifo\Console\Commands\\' . $class : 'App\Console\Commands\\' . $class;
					$object = $this->container->resolveClass($classFullName);
					
					$params = [
						[
							'class'		=> $classFullName,
							'method'	=> $method,
							'args'		=> $args
						]
					];
					
					$this->container->addCustomParameter($params);
					
					$parameters = $this->container->resolveClass($classFullName,$method,true);
					return call_user_func_array(array($object,$method),$parameters);
				} catch(NotFoundException $e){
					return $e->getMessage();
				}
			}
		} else {
			return 'Nothing to run...';
		}
	}
	
	protected function loadHelps(){
		/*
		* @todo
		* I'll do it later... ;)
		*/
		return 'Syntact: Class.Method param1=Something param2=10';
	}
}