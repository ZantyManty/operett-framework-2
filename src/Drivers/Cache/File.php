<?php

namespace Csifo\Drivers\Cache;

use Csifo\Helpers\Interfaces\IFileHelper as IFileHelper;
use Csifo\Core\Interfaces\IConfig;

class File implements Interfaces\IFile {
	
	protected $path = '';
	protected $fileName = '';
	protected $fileHelper;
	
	public function __construct(IConfig $config,IFileHelper $fileHelper){
		$this->fileHelper = $fileHelper;
		
		$this->path = $config['paths']['cache'];
		unset($config);
		/*
		* If the given path is not valid (dir not exists)
		* then we create it
		*/
		if(!is_dir($this->path)) $this->fileHelper->createDir($this->path);
	}
	
	public function setFileName($name){
		$this->path .= '/' . $name;
	}
	
	public function putArray($array){
		$this->put(serialize($array));
	}
	
	public function getArray(){
		return unserialize($this->get());
	}
	
	public function put($data){
		file_put_contents($this->path,gzencode($data));
	}
	
	public function get(){
		return gzdecode(file_get_contents($this->path));
	}
	
}