<?php

namespace Csifo\Drivers\Cache\Interfaces;

interface IFile {
	
	public function get();
	public function put($data);
	
}