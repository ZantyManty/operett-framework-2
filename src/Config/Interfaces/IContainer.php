<?php

namespace Csifo\Config\Interfaces;

interface IContainer {
	public function extra($container);
}