<?php

namespace Csifo\Config\Interfaces;

interface IRoutes {
	
	public function routes($route);
}