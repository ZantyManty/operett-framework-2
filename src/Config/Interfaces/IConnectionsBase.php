<?php

namespace Csifo\Config\Interfaces;

interface IConnectionsBase {
	
	public function getConnection(string $name = '');
	
}