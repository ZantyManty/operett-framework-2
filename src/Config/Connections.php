<?php

namespace Csifo\Config;

use Csifo\Config\Interfaces\IConnectionsBase;

class Connections implements IConnectionsBase {
	
	protected $dsns = [
		'Mysql'	=> 'mysql:host=%s;dbname=%s',
		'Pgsql'	=> 'pgsql:dbname=%s;host=%s'
	];
	
	public function getConnection(string $name = '') : array {
		
		if($name === '' || isset($this->connections[$name])){
			$config = ($name === '') ? reset($this->connections) : $this->connections[$name];
			$type = ucfirst(strtolower($config['type']));
			$config['dsn'] = sprintf($this->dsns[$type],$config['host'],$config['db']);
			
			if($name === ''){
				$config['name'] = key($this->connections);
			}
			
			return $config;
		} else {
			throw new \Exception('There\'s no conenction with name ' . $name);
		}
	}
}