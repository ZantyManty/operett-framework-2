<?php

namespace Csifo\Database\Connection\Sql\Interfaces;

interface IConnection {
	
	public function Connect();
}