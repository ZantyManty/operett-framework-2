<?php

namespace Csifo\Database\Connection\Sql;

use \PDO;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Database\Connection\Sql\Interfaces\IConnection as IConnection;

use Csifo\Config\Interfaces\IConnections as Config;

class Connection implements IConnection
{    
	protected $pdo = null;
	
	protected static $user;
	protected static $pass;
	protected static $dsn;
	protected static $db;
	protected static $model;
	
	public function __construct(Config $configObject)
	{
		$config = $configObject->getConnection();
		static::$user = $config['user'];
		static::$pass = $config['password'];
		static::$dsn = $config['dsn'];
		static::$db = $config['db'];
		static::$model = $config['model'];
		
		unset($config);
		$this->Connect();
	}
	
	public function getConfig(){
		return [
			'db' => static::$db,
			'model' => static::$model
		];
	}
	
	public function Connect(){
		if($this->pdo === null){
			try {
				if(static::$dsn !== ''){
					$this->pdo = new \PDO(
						static::$dsn,
						static::$user,
						static::$pass,
						[
							PDO::ATTR_ERRMODE 			 => PDO::ERRMODE_EXCEPTION,
							PDO::ATTR_EMULATE_PREPARES	 => false,
							PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
							//PDO::ATTR_PERSISTENT		 => true,
							PDO::ATTR_AUTOCOMMIT 		 => false
						]
					);
				}
			} catch (PDOException $e) {
				throw new PDOException('Message: ' . $e->getMessage());
			}
		}
		
		return $this->pdo;
	}
	
	public function getPdo(){
		return $this->pdo;
	}
	
	/*
	* Close the connection
	*/
	public function __destruct()
	{
		$this->pdo = null;
		unset($this->pdo);
	}
}