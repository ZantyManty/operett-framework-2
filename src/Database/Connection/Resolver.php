<?php

namespace Csifo\Database\Connection;

use Csifo\Database\Connection\Interfaces\IResolver;
use Csifo\Psr\Container\IContainer;
use Csifo\Config\Interfaces\IConnections as Config;

class Resolver implements IResolver {
	
	protected $interfaces = [
		'Mysql' => [
			'query'		=> 'Csifo\Database\MySql\Orm\Interfaces\IQuery',
			'commands'		=> 'Csifo\Database\MySql\Orm\Interfaces\ICommands',
			'connection'	=> 'Csifo\Database\Connection\Sql\Interfaces\IConnection'
		]
	];
	protected $connections = [];
	
	protected $config = null;
	protected $container = null;
	
	public function __construct(IContainer $container,Config $config){
		$this->config = $config;
		$this->container = $container;
	}
	
	public function getConnection(string $name = ''){
		if(isset($this->connections[$name])){
			return $this->connections[$name];
		} else if($name === '' && empty($this->connections) === false){
			return reset($this->connections);
		} else {
			/* Resolve the connection */
			$connectionConfig = $this->config->getConnection($name);
			$type = ucfirst(strtolower($connectionConfig['type']));
			
			$this->container->swapDriver('Database',$type);
			
			$this->container->addCustomParameter([
				[
					'id'		=> $this->interfaces[$type]['connection'],
					'args'		=> [
						'config' =>	$connectionConfig
					]
				]
			]);
			
			$name = ($name === '') ? $connectionConfig['name'] : $name;
			
			$connection = $this->container->get($this->interfaces[$type]['query']);
			$this->connections[$name] = $connection;
			
			return $connection;
		}
	}
	
}