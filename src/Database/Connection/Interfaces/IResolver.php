<?php

namespace Csifo\Database\Connection\Interfaces;

interface IResolver {
	
	public function getConnection(string $name);
	
}