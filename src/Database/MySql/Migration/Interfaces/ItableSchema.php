<?php

namespace Csifo\Database\MySql\Migration\Interfaces;

interface ItableSchema {
	
	public function getTableName();
}