<?php

namespace Csifo\Database\MySql\Migration;

use Csifo\Database\MySql\Migration\Interfaces\ISchema as ISchema;

class Schema implements ISchema {
	
	protected $fields;
	protected $unallowedKeys = ['PRIMARY'];
	protected $specialTypes = [
		'TIMESTAMP',
		'DATETIME'
	];
	
	public function __construct(){}
	
	protected function addField($type,$name,$length,$default){
		$this->fields[$name] = [
			'type'	 =>	$type,
			'length' => $length,
			'default' => $default
		];
	}
	
	public function int($name,$length = 11,$default = 0){
		$this->addField('INT',$name,$length,$default);
		return $this;
	}
	
	public function varchar($name,$length = 255,$default = ''){
		$this->addField('VARCHAR',$name,$length,$default);
		return $this;
	}
	
	public function text($name,$length = 65535,$default = ''){
		$this->addField('TEXT',$name,$length,$default);
		return $this;
	}
	
	public function tinyInt($name,$length = 4,$default = 0){
		$this->addField('INT',$name,$length,$default);
		return $this;
	}
	
	public function date($name,$default = '0000-00-00'){
		$this->addField('DATE',$name,'',$default);
		return $this;
	}
	
	public function dateTime($name,$default = '0000-00-00 00:00:00'){
		$this->addField('DATETIME',$name,'',$default);
		return $this;
	}
	
	public function withComment($comment){
		$key = $this->getLastElementKey();
		$this->fields[$key]['comment'] = $comment;
		return $this;
	}
	
	public function withCollate($collate){
		$key = $this->getLastElementKey();
		$this->fields[$key]['collate'] = $comment;
		return $this;
	}
	
	protected function getLastElementKey(){
		$keys = array_keys($this->fields);
		return end($keys);
	}
	
	public function primaryKey(){
		$key = $this->getLastElementKey();
		$this->fields[$key]['PRIMARY'] = true;
		return $this;
	}
	
	public function timestamp($name,$type = ' DEFAULT'){
		$this->addField('TIMESTAMP',$name,'10','0000-00-00 00:00:00');
		$key = $this->getLastElementKey();
		$this->fields[$key]['TIMESTAMP_PARAMS'] = ($type != 'UPDATE' ? $type : ' ON UPDATE') . ' CURRENT_TIMESTAMP';
		return $this;
	}
	
	public function __toString(){
		$output = "";
		
		$primaryKey = '';
		foreach($this->fields as $name => $data){
			$type = $data['type'];
			unset($data['type']);
			
			if(isset($data['PRIMARY']) && $primaryKey == ''){ 
				$primaryKey = $name;
				unset($data['default']);
			}
			
			$output .= $name . " ". $type;
			
			/*
			* Set the length
			*/
			if(!in_array($type,$this->specialTypes)){
				$output .= "(". $data['length'] . ")";
				unset($data['length']);
			} else {
				$output .= " ";
			}
			
			$output .= (isset($data['notNull']) && $data['notNull']) ? "" : " NOT NULL";

			/*
			* Other properties...
			*/
			if(!in_array($type,$this->specialTypes)){
				foreach($data as $key => $property){
					/*
					* unallowedKeys: The key itself doesn't needed!
					*/
					$output .= " " . (!in_array($key,$this->unallowedKeys) ? strtoupper($key) . " '{$property}'" : "");
				}
			} else {
				if($type == 'TIMESTAMP' && isset($data['TIMESTAMP_PARAMS'])){
					$output .= $data['TIMESTAMP_PARAMS'];
					unset($data['TIMESTAMP_PARAMS']);
				}
			}
			
			/*
			* If it's a primary key
			*/
			$output .= (isset($data['PRIMARY']) ? " AUTO_INCREMENT" : "");
			
			
			$output .= "," . PHP_EOL;
		}
		
		if($primaryKey != ''){
			$output .= "PRIMARY KEY (`$primaryKey`)," . PHP_EOL;
		}
		
		return rtrim($output,PHP_EOL . ',') . PHP_EOL;
	}
}