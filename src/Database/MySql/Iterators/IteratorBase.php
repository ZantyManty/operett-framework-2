<?php

namespace Csifo\Database\MySql\Iterators;

abstract class IteratorBase {
	
	protected $callbacks = [];
	
	/*
	* Add a modifier callback to the stack
	*/
	public function modify($callback){
		$this->callbacks[] = $callback;
		return $this;
	}
	
	/*
	* Execute the stacked modifiers
	*
	* $item @Csifo\Database\Orm\Model\Model
	*/
	public function executeModifiers($item){
		foreach($this->callbacks as $callback){
			$item = $callback($item);
		}
		
		return $item;
	}
}