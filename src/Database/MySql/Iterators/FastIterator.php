<?php

namespace Csifo\Database\MySql\Iterators;

use Csifo\Database\MySql\Bridge;
use \PDO;

class FastIterator extends IteratorBase implements \Iterator {
	
	protected $stmt;
	protected $cursor = 0;
	protected $item;
	
	public function __construct($statement,$class,$table = null,Bridge $bridge){
		$this->stmt = $statement;
		$this->stmt->setFetchMode(\PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $class,[$table,$bridge]);
		$this->next();
	}
	
	public function next(){
		$this->cursor++;
		/*
		* Every record gets it's own,new object
		*/
		$this->item = $this->stmt->fetch(
            \PDO::FETCH_ORI_NEXT,
			$this->cursor
		);
	}
	
	public function current(){
		$this->item = $this->executeModifiers($this->item);
		return $this->item;
	}
	
	public function rewind() {
		$this->cursor = 0;
	}
	
	public function key() {
		return $this->cursor;
	}
	
	public function valid(){
		return ($this->item === false) ? false : true;
	}
	
	public function toJson(){
		$json = '[';
		while($this->valid() === true){
			$json .= $this->current()->toJson() . ',';
			$this->next();
		}
		$json = rtrim($json,',');
		return $json . ']';
	}
}