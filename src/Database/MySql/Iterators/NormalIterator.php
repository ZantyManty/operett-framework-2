<?php

namespace Csifo\Database\MySql\Iterators;

class NormalIterator extends IteratorBase implements \Iterator { 
	
	protected $data;
	protected $cursor = 0;
	protected $item;
	
	public function __construct($data,$bridge,$table){
		$this->data = $data;
		$relations = $bridge->getRelations()->setRelations($table);
		
	}
	
	public function next(){
		++$this->cursor;
	}
	
	public function current(){
		$this->data[$this->cursor] = $this->executeModifiers($this->data[$this->cursor]);
		return $this->data[$this->cursor];
	}
	
	public function rewind() {
		$this->cursor = 0;
	}
	
	public function key() {
		return $this->cursor;
	}
	
	public function valid(){
		return isset($this->data[$this->cursor]);
	}
	
	public function getCount(){
		return \count($this->data);
	}
	
	public function toJson(){
		$json = '[';
		while($this->valid() === true){
			$json .= $this->current()->toJson() . ',';
			$this->next();
		}
		$json = rtrim($json,',');
		return $json . ']';
	}
}