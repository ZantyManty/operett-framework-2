<?php

namespace Csifo\Database\MySql\Base;

use \PDO;
use Csifo\Database\Connection\Sql\Interfaces\IConnection as IConnection;
use Csifo\Database\MySql\Bridge;
use Csifo\Database\MySql\Iterators\NormalIterator;

class Basic implements Interfaces\IBasic
{
	/*
	* PDO object
	*/
	protected static $pdo = null;
	protected static $model;
	protected static $db;
	/*
	* Db bridge (For the connection and the resolver)
	*/
	protected static $bridge;
	
	protected static $primaryTable = '--default---';

	
	public function __construct(IConnection $connection,Bridge $bridge){
		if(self::$pdo === null) { self::$pdo = $connection->getPdo(); }
		
		$dbData 		= $connection->getConfig();
		static::$db 	= $dbData['db'];
		static::$model 	= $dbData['model'];
		static::$bridge = $bridge;
	}
	
	public function getDatabase(){
		return static::$db;
	}
	
	/*
	* Checks if the searched row is exists or no
	*
	* @return boolean
	*/
	public function GiveExists($query,$executeArray = null){
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		return ($q->rowCount() === 0) ? false : true;
	}
	
	public function GiveCount($query,$executeArray = null){
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		return $q->rowCount();
	}
	
	/*
	* Return only one row
	*
	* @return mixed (Csifo\Database\Orm\Model\Model OR Associative array (Depends on "$modeAssoc"))
	*/
	public function GiveRow($query,$executeArray = null,$primaryTable = '',$class = '',$modeAssoc = false)
	{
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		$count = $q->rowCount();
		
		if($modeAssoc == false){
			
			$primaryTable = ($primaryTable == '') ? self::$primaryTable : $primaryTable;
			$class = ($class !== '') ? $class : static::$model;
			$primaryKey = static::$bridge->getResolver()->getPrimaryKey($primaryTable);
			$isRelated = static::$bridge->getRelations()->relationsNeeded($primaryTable);
			
			switch($count){
					case 0:
						return false;
					default:
						$q->setFetchMode(PDO::FETCH_INTO,new $class(static::$bridge,$primaryTable,$primaryKey,$isRelated));
						$eredm = $q->fetch();
						return $eredm;
				}
			
		} else {
			switch($count)
			{
				case 0:
					return false;
				default:
					$eredm = $q->fetch(PDO::FETCH_ASSOC);
					return $eredm;
			}
		}
	}
	
	/*
	* Give back only a value
	*
	* @return mixed
	*/
	public function GiveVal($query,$executeArray = null){
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		$count = $q->rowCount();
		switch($count)
		{
			case 0:
				return false;
			default:
				$eredm = $q->fetch(PDO::FETCH_ASSOC);
				if(count($eredm) == 1 && $eredm != false && reset($eredm) != NULL){
					$flipped = array_flip($eredm);
					$key = end($flipped);
					unset($flipped);
					return $eredm[$key];
				} else {return false; }
		}
	}
	
	/*
	* Give back the searched rows
	*
	* @return mixed (Csifo\Database\Iterators\NormalIterator OR Associative array (Depends on "$modeAssoc"))
	*/
	public function GiveAll($query,$executeArray = null,$primaryTable = '',$class = '',$modeAssoc = false)
	{
		if($modeAssoc === false){
			/*
			* Preparing...
			*/
			$class = ($class !== '') ? $class : static::$model;
			$primaryTable = ($primaryTable == '') ? self::$primaryTable : $primaryTable;
			$primaryKey = static::$bridge->getResolver()->getPrimaryKey($primaryTable);
			$isRelated = static::$bridge->getRelations()->relationsNeeded($primaryTable);
			
			$q = self::$pdo->prepare($query);
			$q->execute($executeArray);
			
			/*
			* We need FETCH_PROPS_LATE because we need the __construct() run first!
			*/
			$data = $q->fetchAll(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE,$class,[static::$bridge,$primaryTable,$primaryKey,$isRelated]);

			return (is_array($data) === true) ? new NormalIterator($data,static::$bridge,$primaryTable) : false;
			
		} else {
			$q = self::$pdo->prepare($query);
			$q->execute($executeArray);
			$count = $q->rowCount();
			switch($count)
			{
				case 0:
					return false;
				default:
					return $q->fetchAll(PDO::FETCH_ASSOC);
			}
		}
	}
	
	/*
	* Updates the table with PDO transaction
	*
	* @return bool
	*/
	public function GiveUpdate($query,$executeArray = null) : bool //Beillesztés VAGY Frissítés VAGY Törlés táblába
	{
		self::$pdo->beginTransaction();
		try {
			$q = self::$pdo->prepare($query);
			if(!is_null($executeArray)){
				$q->execute($executeArray);
			}else{
				$q->execute();
			}
			
			//$id = (int)self::$pdo->lastInsertId();
			self::$pdo->commit();
			return true;

		} catch(Expection $e) {
			self::$pdo->rollBack();
			throw new \Exception(' Update error! Text: ' . $e->getMessage());
			return false;
		}
	}
	
	public function GiveColumns($table)
	{
		$query = "SELECT COLUMN_NAME
					FROM   information_schema.columns
					WHERE  table_name = ':table' AND TABLE_SCHEMA = ':db'
					ORDER  BY ordinal_position ";
		$data = [':table' => $table, ':db' => DBNAME];
		return $this->GiveAll($query,$data);
	}
	
	/*
	* Update Or Insert.
	* It checks if the row is already exists. If yes,it only updates it. If no,it inserts the row.
	*/
	public function UorI($table,$executeArray,$where) //Eldönti,hogy van-e már ilyen bejegyzés a táblában. Ha van,akkor UPDATE,ha nincs,akkor INSERT
	{
		$whereString = 'WHERE ';
		
		foreach($where as $k => $v){
			$whereString .= $k . ' = :' . $k . ' AND ';
		}
		$whereString = rtrim($whereString,' AND ');
		
		$queryA = "SELECT * FROM `".$table."` ".$whereString;
		if(!$this->GiveExists($queryA,$where)) //Beillesztés (Ha még nem létezik)
		{
			$query = "INSERT INTO `".$table."` ";
			$rowNames = "(";
			$values = " VALUES (";
			
			foreach($executeArray as $k => $v){
				$rowNames .= $k . ',';
				$values   .= ':' . $k . ',';
			}
			
			$rowNames .= ")";
			$values .= ")";
			$query .= $rowNames.$values;
			$query = str_replace(",)",")",$query); //Kis korrektúra

			$this->GiveUpdate($query,$executeArray);
		}
		else //Frissítés
		{ 
			$query = "UPDATE `".$table."` SET ";
			
			foreach($executeArray as $k => $v){
				$query .= $k . '=:' . $k . ',';
			}
			$query = rtrim($query,',');
			$query .= ' ' . $whereString;
			$arr = array_merge($executeArray,$where);
			
			$this->GiveUpdate($query,$arr);
		}
	}
	
	/*
	* Close the connection and destroy the PDO object
	*/
	public function __destruct()
	{
		$pdo = null;
		unset($pdo);
	}
}