<?php

namespace Csifo\Database\MySql\Orm;

trait Join {
	
	public function rawJoin($table,$select = '*',$method = 'LEFT',$joinData){
		$this->joins[$table]['method'] = $method;
		$this->joins[$table]['raw'] = $joinData;
		if(!empty($select))
			$this->fields($select,$table);
		return $this;
	}
	
	public function join($table,$select = [],$method = 'LEFT',$target = null,$fk_key = null,$pri_key = null){
		
		$target = ($target === null ? $this->table : $target);
		$this->joins[$table]['method'] = $method;
		$fk_keyData = ($fk_key === null) ? $this->resolver->getForeignKey($table,$target) : $table . '.' . $fk_key;
		/*
		* We have the connection only in 1 direction,so we need to switch to the
		* primary table, IF it's joins backward...
		*/
		$realTable = (preg_match('#^' . $table . '\.#i',$fk_keyData) == 1) ? $target : $table;
		
		$this->joins[$table]['fk_key'] = $fk_keyData;
		$this->joins[$table]['pr_key'] = $realTable . '.' . ($pri_key === null ? $this->resolver->getPrimaryKey($table) : $pri_key);
		if(!empty($select))
			$this->fields($select,$table);
		return $this;
	}
}