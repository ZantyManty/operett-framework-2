<?php

namespace Csifo\Database\MySql\Orm;

use Csifo\Database\MySql\Orm\Interfaces\IQuery;
use Csifo\Database\MySql\Orm\Commands;
use Csifo\Database\MySql\Interfaces\IBridge;

use Csifo\Cache\SimpleCache\Interfaces\ICache;

class Query extends Commands implements IQuery {
	
	use Join;
	
	protected $aliasSeparator = '.';

	protected $resolver	= null;
	protected $cache	= null;
	/**
	* The built query itself
	*/
	protected $query = '';
	protected $attributes	= [];
	
	/*
	* Query parts
	*/
	protected $tables		= [];
	protected $selectFields	= [];
	protected $where		= [];
	protected $joins		= [];
	protected $groupBy		= [];
	protected $orderBy		= [];
	protected $limit		= '';
	protected $selectTmp	= [];
	
	public function __construct(IBridge $bridge,ICache $cache){
		$this->bridge		= $bridge;
		$this->cache		= $cache;
		$this->resolver		= $bridge->getResolver();
		$this->relations	= $bridge->getRelations();

		parent::__construct($bridge->getConnection());
	}

	public function emptyThis(){
		$this->tables 		= [];
		$this->query 		= '';
		$this->table		= '';
		$this->selectFields = [];
		$this->where 		= [];
		$this->joins 		= [];
		$this->limit 		= '';
		$this->attributes 	= [];
		$this->groupBy		= [];
		$this->selectTmp	= [];
	}
	
	public function __clone(){
		$this->emptyThis();
	}
	
	public function with($tables){
		$foreignKeys = [];
		foreach($tables as $target_table => $columns){
			$key = $this->resolver->getForeignKey($this->table,$target_table);
			if($key !== false){
				/* One to one,or One to Many */
				//$foreignKeys[$target_table] = $key;
				/*
				* Hotfix! If there's no foreign key backward,then we need the target table's primary key!
				*/
				$foreignKeys[$target_table] = (stripos($key,$target_table . '.') !== false) ? str_replace($target_table . '.','',$key) : $this->bridge->getResolver()->getPrimaryKey($target_table);
			} else {
				/* Many to many */
				$foreignKeys = $this->resolver->getManyToManyMeta($this->table,$target_table);
			}
		}
		
		$this->relations->setForeignKeys($foreignKeys);
		$this->relations->setRelationTables($this->table,$tables);
		
		return $this;
	}
	
	protected function fields($select = [],$table = null){
		/**
		* If array
		*/
		if(is_array($select)){
			$this->selectFields[$table] = $select;
		} else {
			if($select != '*'){
				$fields = explode(',',$select);
				$this->selectFields[$table] = $fields;
			} else {
				$this->selectFields[$table] = $this->resolver->getColumnNames($table);
			}
		}
		$pk = $this->resolver->getPrimaryKey($table);
		if(in_array($pk,$this->selectFields[$table]) === false) {
			$this->selectFields[$table][] = $pk;
		}
	}

	public function select(array $select = [],bool $distinct = false){
		$this->query = 'SELECT ' . ($distinct === true ? 'DISTINCT ' : '');
		$this->selectTmp = $select;
		return $this;
	}
	
	public function from($table){
		$this->table = $table;
		$this->fields($this->selectTmp,$table);
		return $this;
	}
	
	public function andWhere($key, $operator, $value,$dataKey = ''){
		$this->where($key, $operator, $value, 'AND',$dataKey);
		return $this;
	}
	
	public function orWhere($key, $operator, $value,$dataKey = ''){
		$this->where($key, $operator, $value, 'OR',$dataKey);
		return $this;
	}
	
	public function whereIn($key,$value,$type = 'AND',$dataKey = ''){
		$hash = ($dataKey === '') ? ':' . str_replace(['.'],'',$key) : $dataKey;
		
		$this->where[] = (!empty($this->where) ? $type . ' ' : '') . ' FIND_IN_SET (' . $key .',' . $hash . ')';
		
		$this->attributes[$hash] = (is_array($value)) ? implode(',',$value) : $value;
		return $this;
	}
	
	public function where($key, $operator, $value, $type = '',$dataKey = ''){
			
		$hash = ($dataKey === '') ? ':' . str_replace(['.'],'',$key) : $dataKey;
		
		if($operator === 'LIKE')
			$this->where[] = ($type =! '' ? $type : '') . ' ' . $key . ' ' . $operator . ' ' . $hash;
		else
			$this->where[] = ($type =! '' ? $type : '') . ' ' . $key . ' ' . $operator . ' ' . $hash;
		
		$this->attributes[$hash] = $value;
		return $this;
	}
	
	public function whereBetween(string $key,array $value, string $type = '',array $dataKey = []){
			
		$hash = (empty($dataKey[0])) ? ':' . str_replace(['.'],'',$key) : $dataKey[0];
		$hash2 = (empty($dataKey[1])) ? ':' . str_replace(['.'],'',$key) . '1' : $dataKey[1];
		
		//$this->where[] = ($type =! '' ? $type : '') . ' ' . $key . ' ' . $operator . ' ' . $hash;
		$this->where[] = ($type =! '' ? $type : '') . ' (' . $key . ' BETWEEN ' . $hash . ' AND ' . $hash2 . ')';
		//dd($value);
		$this->attributes[$hash] = $value[0];
		$this->attributes[$hash2] = $value[1];
		return $this;
	}
	
	public function limit($limit = '1',$offset = 0){
		
		if($offset === 0){
			$this->limit = ' LIMIT ' . $limit;
		} else {
			$this->limit = ' LIMIT ' . $limit . ',' . $offset;
		}
		
		return $this;
	}

	public function groupBy($value){
		$this->groupBy[] = $value;
		return $this;
	}
	
	protected function fetch($isVal = ''){
		$class = $this->resolver->getClassByTable($this->table);
		$class = (class_exists($class)) ? $class : '';

		if($this->limit == ' LIMIT 1' || preg_match('#LIMIT 1 *$#i',$this->query)){
			return $this->connection->GiveRow($this->query,$this->attributes,$this->table,$class);
		} else if($isVal !== ''){
			return $this->connection->GiveRow($this->query,$this->attributes,$this->table,$class)->$isVal;
		} else {
			return $this->connection->GiveAll($this->query,$this->attributes,$this->table,$class);
		}
	}
	
	public function orderBy(array $orderBy){
		$this->orderBy = $orderBy;
	}
	
	/*
	* Creates a specified alias from a basic alias 
	* (If is not aliased,then return with the given column name)
	*/
	protected function checkAlias($table,$field){
		$columns = $this->resolver->getColumnNames($table);
		$columnsRegex = implode('|',$columns);
		
		if(preg_match('#([\S\s]*?(' . $columnsRegex . ')[\S\s]*?) +?as +?([\S\s]+?)$#i',$field,$realField) === 1){
			
			$accurateTableField = $table . '.' . $realField[2];
			$alias = str_replace($realField[2],$accurateTableField,$realField[1]) . ' AS "' . $table . $this->aliasSeparator . $realField[2] . $this->aliasSeparator . $realField[3] . '"';
			return [true,$alias];
		} else {
			return [false,$field];
		}
	}
	
	protected function buildQuery(){
		$haveJoins = !empty($this->joins);
		
		foreach($this->selectFields as $table => $array){
			if(empty($array)){
				$columns = $this->resolver->getColumns($table);
				foreach($columns as $field){
					$this->query .= ($haveJoins ? $table . '.' . $field . ' AS "' . $table . $this->aliasSeparator . $field . '"' : $field ) . ',';
				}
			} else {
				foreach($array as $field){
					$fieldByAlias = $this->checkAlias($table,$field);
					/*
					* Handle Aliases...
					*/
					if($haveJoins){
						$this->query .= ($fieldByAlias[0] == true) ? $fieldByAlias[1] : $table . '.' . $field . ' AS "' . $table . $this->aliasSeparator . $field . '"';
					} else {
						$this->query .= ($fieldByAlias[0] == true) ? $fieldByAlias[1] : $table . '.' . $field . ' AS "' . $table . $this->aliasSeparator . $field . '"';
					}
					$this->query .= ',';
				}
			}
		}
		$this->query = rtrim($this->query,',');
		$this->query .= ' FROM ' . $this->table;
		
		if($haveJoins){
			foreach($this->joins as $table => $joinData){
				if(isset($joinData['raw']) && !empty($joinData['raw'])){
					$this->query .= $joinData['method'] . ' JOIN ' . $table . ' ON ' . $joinData['raw'];
				} else {
					$this->query .= ' ' . $joinData['method'] . ' JOIN ' . $table . ' ON ' . 
					$joinData['pr_key'] . '=' . 
					$joinData['fk_key'];
				}
			}
		}
		
		$this->query .= (!empty($this->where) ? ' WHERE' . implode(' ',$this->where) : '');
		$this->query .= (!empty($this->groupBy) ? ' GROUP BY ' . implode(',',$this->groupBy) : '');
		$this->query .= (!empty($this->orderBy) ? ' ORDER BY ' . implode(',',$this->orderBy) : '');
		$this->query .= $this->limit;
	}
	
	public function run($isVal = ''){
		$this->buildQuery();
		$result = $this->fetch($isVal);
		$this->emptyThis();
		return $result;
	}
	
	public function chunk($count,$callable){
		
		$this->buildQuery();
		$countQuery = preg_replace('#^SELECT [\S\s]+?FROM ([\S]+)#ui','SELECT COUNT(*) AS count FROM $1',$this->query);
		$allCount = $this->connection->GiveRow($countQuery,null,'','',true)['count'];
		
		$page = 1;
		$resCount = 0;
		$pageCount = intval(ceil(($allCount / $count) * 1));

		$query = $this->query; //Original query without the LIMIT
		
		do {
			$this->query .= ' LIMIT ' . $resCount . ', ' . $count;

			$result = $this->fetch();
			$resCount += $result->getCount();
			
			if($callable($result,$page,$pageCount) === false){
				return false;
			}
			
			unset($result);
			
			/*
			* Get back the original query
			*/
			$this->query = $query;
			
			++$page;
			
		} while ($page <= $pageCount);
		
		return true;
	}
	
	public function arrayQuery(array $query){
		$queryData = $query['QUERY'];
		$queryAttr = $query['DATA'];
		$isVal = (isset($query['IS_VAL']) && $query['IS_VAL'] !== '') ? $query['IS_VAL'] : '';
		
		$cache_key = 'SqlQuery_' . (string)crc32(serialize($queryData));
		
		if($this->cache->has($cache_key) === false){
			
			/*
			* Make the query
			*/
			$this->select($queryData['SELECT']);
			$this->from($queryData['FROM']);
			
			if(isset($queryData['WHERE'])){
				foreach($queryData['WHERE'] as $type => $data){
					
					if(preg_match('#^ *(OR|AND) *\d*?$#i',$type,$inType)){
						$type = (!empty($inType[1])) ? $inType[1] : 'AND';
						$this->where($data['COLUMN'], $data['OPRT'], $queryAttr[$data['VALUE']],$type,$data['VALUE']);
					}
					else if(preg_match('#(OR|AND)* *IN#i',$type,$inType)){
						$type = (!empty($inType[1])) ? $inType[1] : 'AND';
						$this->whereIn($data['COLUMN'],$queryAttr[$data['VALUE']],$type,$data['VALUE']);
					} 
					else if(preg_match('#(OR|AND)* *BETWEEN#i',$type,$inType)){
						$type = (!empty($inType[1])) ? $inType[1] : 'AND';
						$values[0] = $queryAttr[$data['VALUE'][0]];
						$values[1] = $queryAttr[$data['VALUE'][1]];
						$this->whereBetween($data['COLUMN'],$values,$type,$data['VALUE']);
					} 
					else if($type === 'FIRST'){
						$this->where($data['COLUMN'], $data['OPRT'], $queryAttr[$data['VALUE']],'',$data['VALUE']);
					}
				}
			}
			
			if(isset($queryData['JOIN'])){
				foreach($queryData['JOIN'] as $table => $tableData){
					$select = (isset($tableData['FIELDS'])) ? $tableData['FIELDS'] : [];
					$method = (isset($tableData['METHOD'])) ? $tableData['METHOD'] : 'LEFT';
					$target = (isset($tableData['TARGET'])) ? $tableData['TARGET'] : null;
					$fk_key = (isset($tableData['FOREIGN_KEY'])) ? $tableData['FOREIGN_KEY'] : null;
					$pri_key = (isset($tableData['PRIMARY_KEY'])) ? $tableData['PRIMARY_KEY'] : null;
					$this->join($table,$select,$method,$target,$fk_key,$pri_key);
				}
			}
			
			
			if(isset($queryData['GROUP_BY'])){
				foreach($queryData['GROUP_BY'] as $groupBy){
					$this->groupBy($groupBy);
				}
			}
			
			if(isset($queryData['ORDER_BY'])){
				$this->orderBy($queryData['ORDER_BY']);
			}
			
			if(isset($queryData['LIMIT'])){
				$limit = isset($queryData['LIMIT']['LIMIT']) ? $queryData['LIMIT']['LIMIT'] : '1';
				$offset = isset($queryData['LIMIT']['OFFSET']) ? $queryData['LIMIT']['OFFSET'] : 0;
				
				$this->limit($limit,$offset);
			}
			
			$this->buildQuery();
			
			file_put_contents('query.txt',$this->query);

			$this->cache->set($cache_key,$this->query);

			$result = $this->fetch($isVal);
			$this->emptyThis();
			return $result;
			
		} else {
			$this->table = $queryData['FROM'];
			$this->query = $this->cache->get($cache_key);
			$this->attributes = $query['DATA'];
			file_put_contents('query.txt',$this->query);
			$result = $this->fetch($isVal);
			$this->emptyThis();
			return $result;
		}
	}
}