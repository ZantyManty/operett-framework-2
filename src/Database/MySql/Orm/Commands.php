<?php

namespace Csifo\Database\MySql\Orm;

use Csifo\Database\MySql\Base\Interfaces\IBasic as IBasic;
use Csifo\Database\MySql\Orm\Interfaces\ICommands as ICommands;
use Csifo\Database\MySql\Resolver\Resolver as Resolver;

class Commands implements ICommands {
	
	protected $connection;
	
	public function __construct(IBasic $connection){
		$this->connection = $connection;
	}
	
	public function update($table,$params = [],$where = [],$limit = 0){
		$q = 'UPDATE ' . $table . ' SET ';
		$updateParams = [];
		
		foreach($params as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$q .= $column . ' = ' . $key . ',';
		}
		
		$q = rtrim($q,',');
		$q .= ' WHERE ';
		
		foreach($where as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$q .= $column . ' = ' . $key . ' AND ';
		}
		
		$q = rtrim($q,' AND ');
		
		if($limit > 0){
			$q.= ' LIMIT ' . $limit;
		}
		return $this->connection->GiveUpdate($q,$updateParams);
	}
	
	public function delete($table,$where = [],$limit = 0){
		$q = 'DELETE FROM ' . $table . ' WHERE ';
		$updateParams = [];
		
		foreach($where as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$q .= $column . ' = ' . $key . ' AND ';
		}
		
		$q = rtrim($q,' AND ');
		
		if($limit > 0){
			$q.= ' LIMIT ' . $limit;
		}
		$this->connection->Connect();
		return $this->connection->GiveUpdate($q,$updateParams);
	}
	
	public function insert($table,$params){
		$q = 'INSERT INTO ' . $table . ' ';
		$updateParams = [];
		
		$insertColumns	= '(';
		$insertValues	= '(';
		
		foreach($params as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$insertColumns .= $column . ',';
			$insertValues .= $key . ',';
		}
		
		$insertColumns	= rtrim($insertColumns,',') . ') VALUES ';
		$insertValues	= rtrim($insertValues,',') . ') ';
		
		$q .= $insertColumns . $insertValues;
		
		return $this->connection->GiveUpdate($q,$updateParams);
	}
	
	/*
	* Declares a mySQL variable
	*/
	public function declareVal($var,$value){
		$this->connection->GiveUpdate('SET @:var=:val;',[':var' => $var,':val' => $value]);
		return $this;
	}
	
	public function findByPk($table,$id){
		$pk = $this->loader->getPrimaryKey($table);
		return $this->select()->from($table)->where($pk,'=',$id)->run();
	}
	
	public function exists($table,$where = []){
		$whereString = '';
		foreach($where as $key => $val){
			$whereString .= $key . ' = ' . (strpos($key,':') === false ? ':' : '') . $key . ' AND ';
		}
		$whereString = rtrim($whereString,' AND ');
		return $this->connection->GiveExists('SELECT * FROM ' . $table . ' WHERE ' . $whereString,$where);
	}
	
	public function reWrite($table,$data,$where = []){
		$where = (empty($where) === true) ? $data : $where;
		$this->connection->UorI($table,$data,$where);
	}
	
	public function raw($query,$data = null){
		if(preg_match('#^[\S\s]+LIMIT 1$#i',$query) === 1){
			return $this->connection->GiveRow($query,$data,'','',true);
		} else {
			return $this->connection->GiveAll($query,$data,'','',true);
		}
	}
	
	public static function insertUpdateRaw($query,$data = null){
		return $this->connection->GiveUpdate($query,$data);
	}
}