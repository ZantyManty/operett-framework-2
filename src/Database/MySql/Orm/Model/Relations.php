<?php

namespace Csifo\Database\MySql\Orm\Model;

use Csifo\Database\MySql\Orm\Interfaces\IRelations;
//use Csifo\Database\MySql\Base\Interfaces\IBasic;
use Csifo\Database\MySql\Interfaces\IBridge;
use Csifo\Database\MySql\Iterators\NormalIterator;

class Relations implements IRelations {
	
	protected $bridge;
	
	protected $connection;
	/*
	* 'table' => 'foreign_key'
	*/
	protected $foreignKeys;
	
	/*
	* The tables,we need to get
	* 'motherTable' => ['table' => 'column1,column2,etc...']
	*/
	protected $tables = [];
	protected $ids = [];
	protected $relatedModels = [];
	
	/*
	* Many to many connections
	*
	* Only the ids,because the models is in "$relatedModels"
	*/
	protected $manyToMany = [];
	
	public function __construct(IBridge $bridge){
		$this->bridge = $bridge;
		$this->connection = $bridge->getConnection();
	}
	
	public function relationsNeeded($motherTable){
		return !empty($this->tables[$motherTable]);
	}

	public function setForeignKeys($foreignKeys){
		$this->foreignKeys = $foreignKeys;
	}
	
	public function setRelationTables($motherTable,$tables){
		$this->tables[$motherTable] = $tables;
	}
	
	protected function ManyToOne($table,$motherTable,$columns,$foreignKey){
		$query = "SELECT {$columns} FROM {$table} WHERE {$foreignKey} IN (" . str_repeat('?,', count($this->ids) - 1) . "?)";
		$models = $this->connection->GiveAll($query,$this->ids,$table);

		foreach($models as $model){
			$fk = $model->{$foreignKey};
			$this->relatedModels[$motherTable][$table][$fk][] = $model;
		}
	}
	
	protected function ManyToMany($table,$motherTable,$columns,$foreignKey){
		/*
		* MANY TO MANY
		*
		* Get the child IDs from the link table
		*/
		$query = "SELECT {$foreignKey['foreignKeyTarget']} AS 'child',{$foreignKey['foreignKey']} AS 'mother' FROM {$foreignKey['linkTable']}";
		$query .= " WHERE {$foreignKey['foreignKey']} IN (" . str_repeat('?,', count($this->ids) - 1) . "?) ORDER BY {$foreignKey['foreignKeyTarget']} ASC";
		$child_ids_result = $this->connection->GiveAll($query,$this->ids,$foreignKey['linkTable'],true);

		if($child_ids_result !== false){
			$child_ids = [];
			foreach($child_ids_result as $id){
				$this->manyToMany[$id['mother']][] = $id['child'];
				if(!in_array($id['child'],$child_ids)){
					$child_ids[] = $id['child'];
				}
			}
			
			/*
			* Get the data from the child table
			*/
			$query = "SELECT {$columns} FROM {$table} WHERE {$foreignKey['childPrimaryKey']} IN (" . str_repeat('?,', count($child_ids) - 1) . "?) ORDER BY {$foreignKey['childPrimaryKey']} ASC ";
			$models = $this->connection->GiveAll($query,$child_ids,$table);
			
			/*
			* Build the bindings
			*/
			$i = 0;
			foreach($models as $model){
				//$this->relatedModels[$motherTable][$table][$model->{$foreignKey['childPrimaryKey']}] = $model;
				$this->relatedModels[$motherTable][$table][$child_ids[$i]] = $model;
				++$i;
			}
		}
	}
	
	public function addId($id){
		$this->ids[] = $id;
	}
	
	/*
	* Eager loading
	*/
	public function setRelations($motherTable){
		if(isset($this->tables[$motherTable])){
			$tables = $this->tables[$motherTable];
			
			foreach($tables as $table => $columns){
				$foreignKey = $this->foreignKeys[$table];
				/* One to one,Many to one */
				if(!is_array($foreignKey)){
					$this->ManyToOne($table,$motherTable,$columns,$foreignKey);
				} else {
					$this->ManyToMany($table,$motherTable,$columns,$foreignKey);
				}
			}
		}
		$this->ids = [];
	}
	
	/*
	* Lazy loading
	*/
	protected function lazyLoading($motherTable,$table,$id = 0,$columns = '*'){

		$tables = [$table => $columns];
		$this->setRelationTables($motherTable,$tables);
		
		/*
		* Set the keys...
		*/
		$key = $this->bridge->getResolver()->getForeignKey($motherTable,$table);
		if($key !== false){
			/* One to one,or One to Many */
			//$foreignKeys[$table] = $key;
			/*
			* Hotfix! If there's no foreign key backward,then we need the target table's primary key!
			*/
			$foreignKeys[$table] = (stripos($key,$table . '.') !== false) ? str_replace($table . '.','',$key) : $this->bridge->getResolver()->getPrimaryKey($table);
		} else {
			/* Many to many */
			$foreignKeys = $this->bridge->getResolver()->getManyToManyMeta($motherTable,$table);
		}
		
		$this->setForeignKeys($foreignKeys);
		$this->addId($id);
		$this->setRelations($motherTable);
		
		return $this->getRelations($motherTable,$table,$id,$columns);
	}
	
	public function getRelations($motherTable,$table,$id = 0,$columns = '*'){

		 if(isset($this->manyToMany[$id]) && $this->relatedModels[$motherTable]) {
			 /* MANY TO MANY */
			$child_ids = $this->manyToMany[$id];
			$return = [];
			foreach($child_ids as $id){
				$return[] = $this->relatedModels[$motherTable][$table][$id];
			}
			
			return $return;
		} else if(isset($this->relatedModels[$motherTable][$table][$id]) && is_array($this->relatedModels[$motherTable][$table][$id])){
			/* ONE TO ONE,MANY TO ONE*/
			return $this->relatedModels[$motherTable][$table][$id];
		} else {
			return $this->lazyLoading($motherTable,$table,$id,$columns);
		}
	}
}