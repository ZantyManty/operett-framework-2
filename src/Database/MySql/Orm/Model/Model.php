<?php

namespace Csifo\Database\MySql\Orm\Model;

use Csifo\Database\MySql\Interfaces\IBridge;

class Model {
	
	protected $table = '';
	protected $primaryKey;
	/*
	* Connection bridge
	*/
	protected $bridge = null;
	
	protected $modified = [];
	protected $original = [];
	protected $aliases = [];
	
	/*
	* Relationships
	*/
	protected $isRelated = false;
	
	public function __construct(IBridge $bridge,$table = '',$primaryKey = '',$isRelated = false){
		if($this->table === '')
			$this->table = $table;
		
		$this->primaryKey = $primaryKey;
		
		$this->isRelated = $isRelated;
		$this->bridge = $bridge;
	}
	
	public function getOriginal(){
		return $this->original;
	}
	
	public function toDate($name,$format){
		if(preg_match('#[\d]{4}-[\d]{2}-[\d]{2}(?: [\d]{2}:[\d]{2}:[\d]{2})?#',$this->get($name)) === 1){
			if($format === 'elapsed'){
				return \Supply\Dater::elapsed($this->get($name));
			} else {
				return \Supply\Dater::format($this->get($name),$format);
			}
		}
	}
	
	public function toJson(){
		return json_encode($this->original);
	}
	
	protected function handleAlias($name){
		
		return explode('.',$name);
		/*
		* $pure[1] -> Table name
		* $pure[2] -> Column name
		* $pure[3] -> Alias (if exists)
		*/
		if(strpos($name,'_aliased_') !== false){
			preg_match('#([\w]+)\.([\w]+)_aliased_([\w.]+)#iu',$name,$pure);
			$this->aliases[$pure[3]] = ['TABLE' => $pure[1],'COLUMN' => $pure[2]]; //Table name
			return [$pure[1],$pure[2],$pure[3]];
		} else if(preg_match('#^([\w]+?)\.([\w.]+?)$#iu',$name,$data) == 1){
			return [$data[1],$data[2]];
		} else {
			return $name;
		}
	}
	
	public function __get($name){
		if(!isset($this->aliases[$name])){
			//$pureName = $this->handleAlias($name);
			$pureName = explode('.',$name);
			
			/*
			* If theres no second element,then is a pure column name
			*/
			if(isset($pureName[1]) === false){
				$pureName[1] = $pureName[0]; //Column name
				$pureName[0] = $this->table; //Table name
			}
			
			if(isset($this->modified[$pureName[0]][$pureName[1]])){
				return $this->modified[$pureName[0]][$pureName[1]];
			} else {
				return ($this->original[$pureName[0]][$pureName[1]]) ? $this->original[$pureName[0]][$pureName[1]] : null;
			}
		} else {
			$realNameData = $this->aliases[$name];
			if(isset($this->modified[$realNameData['TABLE']][$realNameData['COLUMN']])){
				return $this->modified[$realNameData['TABLE']][$realNameData['COLUMN']];
			} else {
				return $this->original[$realNameData['TABLE']][$realNameData['COLUMN']];
			}
		}
	}
	
	public function __set($name, $val){
		/*
		* Check for alias 
		*/
		if(!isset($this->aliases[$name])){
			//$pureName = $this->handleAlias($name);
			$pureName = explode('.',$name);
			
			if(isset($this->original[$pureName[0]][$pureName[1]])){
				$this->modified[$pureName[0]][$pureName[1]] = $val;
			} else {
				/**
				* If the Model is new!!
				*/
				if($this->isRelated === true && $pureName[0] == $this->table && $this->primaryKey === $pureName[1]){
					$bridge->getRelations()->addId($val);
				}
				$this->original[$pureName[0]][$pureName[1]] = $val;
				
				/*
				* If has alias...
				*/
				if(isset($pureName[2]) === true){
					$this->aliases[$pureName[2]] = ['TABLE' => $pureName[0], 'COLUMN' => $pureName[1]];
				}
			}
		} else {
			$realNameData = $this->aliases[$name];
			$this->modified[$realNameData['TABLE']][$realNameData['COLUMN']] = $val;
		}
	}
	
	/**
	* Fill the model
	*/
	public function fill($data){
		if(is_array($data)){
			if(empty($this->original))
				$this->original[$this->table] = $data;
			else if(isset($this->modified[$this->table]))
				$this->modified[$this->table] = $data + $this->modified[$this->table];
			else
				$this->modified[$this->table] = $data;
		} else {
			throw new \Exception('Model fill() needs an ARRAY,not a ' . gettype($data) . '!');
		}
	}
	
	/*
	* Saves the data into the database
	*/
	public function save(){
		$resolver = $this->bridge->getResolver();
		try {
			if(!empty($this->modified)){
				foreach($this->modified as $table => $columns){
					$primary_key = $resolver->getPrimaryKey($table);
					
					$q = 'UPDATE ' . $table . ' SET ';
					$d = [];
					foreach($columns as $name => $value){
						$q .= $name . ' = ?,';
						$d[] = $value;
					}
					$q = rtrim($q,',');
					
					$q .= ' WHERE ' . $primary_key . ' = ?';

					$d[] = $this->original[$table][$primary_key];
					$this->bridge->getConnection()->GiveUpdate($q,$d);
				}
			} else {
				/**
				* If this is a new object
				*/
				foreach($this->original as $table => $columns){

					$primary_key = $resolver->getPrimaryKey($table);
					
					if(!isset($this->original[$table][$primary_key]) || ($this->original[$table][$primary_key] === 0 || $this->original[$table][$primary_key] === '')){
						$q = 'INSERT INTO ' . $table . ' (';
						$d = [];
						$values = '';
						foreach($columns as $name => $value){
							$q .= $name . ',';
							$values .= '?,';
							$d[] = $value;
						}
						
						$q = rtrim($q,',');
						$values = rtrim($values,',');
						$q .= ') VALUES (' . $values . ')';
						/**
						* After save,we set the table's primary key
						*/
						$this->original[$table][$primary_key] = $this->bridge->getConnection()->GiveUpdate($q,$d);
					}
				}
			}
			
			return $this->original[$this->table][$this->primaryKey];
		} catch(\PDOException $e){
			throw new \Exception('Model save error! ' . $e->getMessage());
		}
	}
	
	public function delete(){
		$resolver = $this->bridge->getResolver();
		
		foreach($this->original as $table => $columns){
			$primary_key = $resolver->getPrimaryKey($table);
			$q = 'DELETE FROM ' . $table . ' WHERE ' . $primary_key . ' = :prim_' .  $primary_key . ' LIMIT 1';
			$d = [];
			$d[':prim_' .  $primary_key] = $this->original[$table][$primary_key];
			$this->bridge->getConnection()->GiveUpdate($q,$d);
		}
	}
	
	public function getRelated($table,$cloumns = '*'){
		$id = $this->original[$this->table][$this->primaryKey];
		return $bridge->getRelations()->getRelations($this->table,$table,$id,$cloumns);
	}
	
	/*public function __set($name,$val){
		$this->set($name, $val);
	}*/
	
	/*public function __get($name){
		return $this->get($name);
	}*/
}