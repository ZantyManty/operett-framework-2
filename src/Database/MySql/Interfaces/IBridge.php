<?php

namespace Csifo\Database\MySql\Interfaces;

interface IBridge {
	
	public function getResolver();
}