<?php
/*
* Hungarian plural resolver module for the Dorinity Framework
*/

namespace Csifo\Database\MySql\Resolver\Grammar;

use Csifo\Database\MySql\Resolver\Interfaces\IGrammar as IGrammar;

class Hu implements IGrammar {

	protected $patterns = [
		'[msz]*a[gdm]{1}á[r]{1}' => ['ár$' => 'arak'], //Szamár,madár,agár
		'á[rt]{1}ok$' => ['(á[rt]{1})ok$' => '$1kok'], //Átok,árok
		'á[l]{1}om$' => ['(á[l]{1})ok$' => '$1mok'] //Álom
	];
	
	protected $simple = [
		'szatyor'	=>	'szatyrok',
		'jármű'		=>	'járművek',
		'ló'		=>	'lovak',
	];
	
	protected $kivetelek = [
		'ló'		=> 'lovak',
		'köcsög'	=> 'köcsögök',
		'farok'		=>	'farkak',
		'fájl'		=>	'fájlok',
		'log'		=>	'logok',
		'poszt'		=>	'posztok',
		//'kéz'		=>	'kezek',
		'könyv'		=>	'könyvek',
		'toll'		=>	'tollak',
		'szatyor'	=>	'szatyrok',
		'jármű'		=>	'járművek',
	];
	
	/*
	* $column -> Az aktuális oszlop neve (Egyes szám)
	* $table -> Az IDEGEN tábla (többes szám),aminek a nevében megnézed az oszlopot ($column)
	*/
	public function handle($column,$table){
		
		/*
		* Magának a kellő szónak a kinyerése (Szóval,pl "_id" utótag levágása...)
		*/
		if(preg_match('#^([\wéáűúőóüöí]+)_(?:id|azon|kulcs|azonosító)$#ui',$column,$match)){
			$word = trim($match[1]);

			/*
			* Egyértelmű kivételek
			*/
			if(isset($this->simple[$word])){
				return (preg_match('#' . $this->simple[$word] . '$#i',$table) === 0) ? false : true;
			}
			/*
			* Ha nincs olyan szerencsénk(?) hogy egy konkrét kivétel,akkor folytatjuk...
			*/
			else {
				/*
				* Ha adott magánhangzók vannak a végén,azokat lecsípjük
				*/
				$word = (preg_match('#([\wúőűáéóüöí]+?)[áaóée]{1}$#iu',$word,$chunk)) ? $chunk[1] : $word;
				
				/*
				* Megnézzük kivételekkel
				*/
				$word_excep = $this->exceptions($word);
				if($word_excep){
					/*
					* Ha van kivétel
					*/
					return (preg_match('#' . $word_excep . '$#iu',$table) === 0) ? false : true;
				} else {
					/*
					* Ha normál ragozású
					*/
					if(preg_match('#' . $word . '[oáőóeaéöóíúű]{1}k$#iu',$table) !== 0){
						return true;
					} else {
						return false;
					}
				}
			}
		} else {
			return false;
		}
	}

	public function getNonPlural($word){
		$word = mb_strtolower($word);
		
		$kivetelekFlip = array_flip($this->kivetelek);
		/**
		* Kivételek
		*/
		if(isset($kivetelekFlip[$word])){
			return $kivetelekFlip[$word];
		}
		
		if(preg_match('#[eöoaéá]{1}k$#iu',$word)){
			$szoTo = preg_replace('#[eöoa]{1}k$#iu','',$word);
			
			/*
			* "krk" Visszafele
			*
			* Pl: ökrök -> ökör
			*/
			if(preg_match('#^((?:[^öüóúőűáéíaieu]*?)([oöa]+)){1}([brk]{2})$#iu',$szoTo,$match)){
				/*
				* Pl -> szobor -> szo . b . o . r;
				*/
				return $match[1] . $match[3][0] . $match[2] . $match[3][1];
			}
			/**
			* Pl: izmok -> izom,barmok -> barom,halmok -> halom
			*/
			else if(preg_match('#([rzl]+)(m)$#iu',$szoTo,$match)){
				return preg_replace('#([rzl]+)m#iu','$1om',$szoTo);
			}
			/**
			* Pl: férgek -> féreg,méreg -> mérgek,
			* kérgek -> kéreg,étkek -> étek
			* vétkek -> vétek
			*/
			else if(preg_match('#[é]+[rt]{1}([gk]{1})$#iu',$szoTo,$match)){
				$csere = ['g' => 'eg','k' => 'ek'];
				return preg_replace('#' . $match[1] . '$#iu',$csere[$match[1]],$szoTo);
			}
			
			/**
			* Magánhangzó Á és É
			*/
			else if(preg_match('#([áé]{1})k$#iu',$word,$match)){
				$csere = ['á' => 'a','é' => 'e'];
				return preg_replace('#([áé]{1})k$#iu',$csere[$match[1]],$szoTo);
			}
			/**
			* Egyéb esetek. Pl halak -> hal
			*/
			else {
				/**
				* urak -> úr,kezek -> kéz,stb...
				*/
				if(preg_match('#^[^öüóúőéáűauie]*([ue]){1}[^öüóúőéáűauie]+$#iu',$szoTo,$match)){
					$csere = ['u' => 'ú','e' => 'é'];
					$szoTo = preg_replace('#^([^öüóúőéáűauie]*)[ue]{1}([^öüóúőéáűauie]+)$#iu','$1' . $csere[$match[1]] . '$2',$szoTo);
				}
				return $szoTo;
			}
		} else if(preg_match('#[üőó]+k$#iu',$word)) {
			return preg_replace('#k$#iu','',$word);
		}
	}
	
	protected function exceptions($word){
		
		$needMiscException = true;
		foreach($this->patterns as $pattern => $replace){
			if(preg_match('#' . $pattern . '#ui',$word)){
				$checkPattern = key($replace);
				$word = preg_replace('#' . $checkPattern . '#ui',$replace[$checkPattern],$word);
				/*
				* Ha megvan,akkor NINCS szükség további kivétel vizsgálatokra
				*/
				$needMiscException = false;
				break;
			}
		}
		
		/*
		* További kivételek
		*/
		if($needMiscException === true){
			$methods = ['krkPlural','ak'];
			
			foreach($methods as $method){
				$word = $this->$method($word);
				if($word !== false) break;
			}
		}
		
		return $word;
	}
	
	protected function krkPlural($word){
		/**
		* ökör  -> ökrök
		* bokor -> bokrok
		* szobor -> szobrok
		* marok -> markak
		*/
		if(preg_match('#^((?:[^öüóúőűáéieaíuo]+)?([aöo]{1})[bkr])$#iu',$word,$match)){
			return $match[1] . 'r' . $match[2] . 'k';
		} else {
			return false;
		}
	}
	
	protected function ak($word){
		/**
		* tó -> tavak
		* szó -> szavak
		*
		* úr -> urak
		* kút -> kutak
		* út -> utak
		* nyúl -> nyulak
		* kéz -> kezek
		* név -> nevek
		*/
		$replace = [
			'ó' => 'a',
			'ú' => 'u',
			'é' => 'e',
		];
		if(preg_match('#^([^ó]+?)([ó]{1})$#iu',$word,$match)){
			return $match[1] . $replace[$match[2]] . 'vak';
		} else if(preg_match('#[^ú]*([ú]{1})$#iu',$word,$match_u)){
			//$csere = ['ú' => 'u','é' => 'e'];
			return str_replace($match_u[1],$replace[$match_u[1]],$word) . 'ak';
		} else if(preg_match('#[^é]+é[vz]{1}$#iu',$word,$match)){
			return preg_replace('#([^é]+)é([vz]{1})$#ui','$1e$2ek',$word);
		} else {
			return false;
		}
	}

}