<?php

namespace Csifo\Database\MySql\Resolver\Grammar;

use Csifo\Database\MySql\Resolver\Interfaces\IGrammar as IGrammar;

class En implements IGrammar {
	
	protected $exceptions = [
		'files'	=>	'file',
		'teeth'	=>	'tooth'
	];
	
	public function handle($column,$table){

		if(preg_match('#^([\wé]+)_(?:id|key)$#ui',$column,$match)){
			$word = trim($match[1]);
			
			/*
			* Exceptions
			*/
			if(isset($this->exceptions[$word]) && preg_match('#' . $this->exceptions[$word] . '$#ui',$table)){
				return true;
			} 
			/*
			* Normal plural
			*/
			else {
				return (preg_match('#^' . $word . '(?:s|es|ies|oes(?:\b))$#ui',$table) == 0) ? false : true;
			}
		} else { 
			return false; 
		}
	}
	
	public function getNonPlural($name){
		if(isset($this->exceptions[$name])){
			return $this->exceptions[$name];
		} else {
			if(preg_match('#^(\S+?)(?:s|es|ies|oes(?:\b))$#i',$name,$nonPlural)){
				return $nonPlural[1];
			} else {
				return $name;
			}
		}
	}
}