<?php

namespace Csifo\Database\MySql\Resolver\Interfaces;

interface IGrammar {
	
	/*
	* $column string	| The column,what you check in the foreign table's name
	* $table string		| The foreign table's name
	*/
	public function handle($column,$table);
	
	public function getNonPlural($word);
	
}