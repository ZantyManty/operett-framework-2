<?php

namespace Csifo\Database\MySql\Resolver\Interfaces;

interface IBuilder {

	public static function buildCache();

}