<?php

namespace Csifo\Database\MySql\Resolver\Interfaces;

interface IResolver {
	
	public function getTableData($table);
	
	public function getPrimaryKey($table);
	
	public function getForeignKey($table,$target);
}