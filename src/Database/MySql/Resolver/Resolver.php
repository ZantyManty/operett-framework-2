<?php

namespace Csifo\Database\MySql\Resolver;

use Csifo\Database\Interfaces\IConnection as IConnection;
use Csifo\Database\MySql\Resolver\Interfaces\IGrammar as IGrammar;
use Csifo\Cache\SimpleCache\Interfaces\ICache;
use Csifo\Database\MySql\Base\Interfaces\IBasic as IBasic;
use Csifo\Helpers\Interfaces\IStringHelper;
use Csifo\Core\Interfaces\IConfig;

class Resolver implements Interfaces\IResolver {
	
	protected $metaData;
	protected $grammar;
	protected $connection;
	protected $stringHelper;
	
	public function __construct(ICache $cache,IBasic $basic,IGrammar $grammar,IStringHelper $stringHelper,IConfig $config){
		$this->connection = $basic;
		$this->grammar = $grammar;
		$this->stringHelper = $stringHelper;
		$this->config = $config;
		$this->prepareCache($cache);
	}
	
	protected function prepareCache($cache){
		/*
		* Prepare cache...
		*/
		$database = $this->connection->getDatabase();
		/*
		* If we have a connection,only then prepare the cache!!
		*/
		if($database !== ''){
			$cacheKey = 'ORM_loader_' . $database;
			$cacheData = $cache->get($cacheKey);

			if($cacheData === null){
				Builder::init($this->connection,$this->grammar,$this->stringHelper,$this->config);
				$this->metaData = Builder::buildCache();
				$cache->set($cacheKey,$this->metaData);
			} else {
				$this->metaData = $cacheData;
			}
		}
	}
	
	public function getConnection(){
		return $this->connection;
	}
	
	public function getTableData($table){
		return $this->metaData['TABLES'][$table];
	}
	
	public function getColumnNames($table){
		return array_keys($this->getTableData($table)['COLUMNS']);
	}
	
	public function getPrimaryKey($table){
		return $this->getTableData($table)['PRIMARY'];
	}
	
	public function getClassByTable($table) : string {
		if(isset($this->metaData['TABLES'][$table]['CLASS'])){
			return 'App\Models\\' . $this->metaData['TABLES'][$table]['CLASS'];
		} else {
			return '';
		}
	}
	
	public function getTableByClass($class) : string {
		if(isset($this->metaData['MODEL_ALIASES'][$class])){
			return $this->metaData['MODEL_ALIASES'][$class]['tableName'];
		} else {
			return '';
		}
	}
	
	public function getManyToManyMeta($table,$target_table){
		$foreignKeys = [];
		$table_for_search = $table . '_' . $target_table;
		$key = $this->getForeignKey($table,$table_for_search,'',true);
		
		if($key !== false){
			//$foreignKeys[$target_table] = [$table_for_search => $key];
			$foreignKeys[$target_table] = [
					'linkTable'			=> $table_for_search,
					'foreignKey'		=> $key,
					'foreignKeyTarget'	=> $this->getForeignKey($target_table,$table_for_search,'',true),
					'childPrimaryKey'	=> $this->getPrimaryKey($target_table)
				];
		} else {
			$table_for_search = $target_table . '_' . $table;
			$key = $this->getForeignKey($table,$table_for_search,'',true);
			
			if($key !== false){
				$foreignKeys[$target_table] = [
					'linkTable'			=> $table_for_search,
					'foreignKey'		=> $key,
					'foreignKeyTarget'	=> $this->getForeignKey($target_table,$table_for_search,'',true),
					'childPrimaryKey'	=> $this->getPrimaryKey($target_table)
				];
			} else {
				throw new \Exception('Many to many link table foreign key not found!');
			}
		}
		return $foreignKeys;
	}
	
	protected function findForeignByPrimary($primary_table,$target_table,$primary_table_prim_key,$onlyKey = false){
		$connections = $this->metaData['CONNECTIONS'];

		/*
		* Direction 1:
		*/
		if(isset($connections[$primary_table][$target_table][$primary_table_prim_key])){
			/*
			* Foreign key is in $connections[$primary_table][$target_table][$primary_table_prim_key]
			*/
			return ($onlyKey == false ? $target_table . '.' : '') . $connections[$primary_table][$target_table][$primary_table_prim_key];
		} 
		/*
		* Direction 2:
		*/
		else {
			/*
			* Foreign table's primary key 
			* (We need it as an array key to find the foreign key in the "connections" array)
			*/
			//$primary_key = $this->metaData['TABLES'][$target_table]['PRIMARY'];
			$primary_key = (isset($this->metaData['TABLES'][$target_table]['PRIMARY'])) ? $this->metaData['TABLES'][$target_table]['PRIMARY'] : '';
			if(isset($connections[$target_table][$primary_table][$primary_key])){
				return ($onlyKey == false ? $primary_table . '.' : '') . $connections[$target_table][$primary_table][$primary_key];
			} else {
				//throw new \Exception('Foreign key for this resolving [ ' . $primary_table . ' | ' . $target_table . ' ] is not found!');
				return false;
			}
		}
	}
	
	public function getForeignKey($table,$target,$key = '',$onlyKey = false){
		$prim_key1 = ($key == '') ? $this->getPrimaryKey($table) : $key;
		return $this->findForeignByPrimary($table,$target,$prim_key1,$onlyKey);
	}
}