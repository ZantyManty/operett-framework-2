<?php

namespace Csifo\Database\MySql\Resolver;

use Csifo\Database\MySql\Base\Interfaces\IBasic as IBasic;
use Csifo\Database\MySql\Resolver\Interfaces\IGrammar as IGrammar;
use Csifo\Helpers\Interfaces\IStringHelper;
use Csifo\Core\Interfaces\IConfig;

class Builder implements Interfaces\IBuilder {
	
	protected static $connection		= null;
	protected static $grammar			= null;
	protected static $stringHelper		= null;
	protected static $config			= null;
	protected static $modelNamespace	= 'App\Models';
	
	public static function init(IBasic $connection,IGrammar $grammar,IStringHelper $stringHelper,IConfig $config){
		if(self::$connection == null){
			self::$connection = $connection;
		}
		
		if(self::$grammar == null){
			self::$grammar = $grammar;
		}
		
		self::$stringHelper = $stringHelper;
		self::$config = $config;
	}
	
	public static function buildCache(){
		$trunk = [];
		
		$tables = self::getTables();
		$modell_class_aliases = [];
		
		if($tables){
			foreach($tables as $table){
				//Table meta data (Columns,primary key,etc...)
				$column_name = key($table);
				$trunk['TABLES'][$table[$column_name]] = self::getTableData($table[$column_name]);
				/**
				* Creating "magic" alliases
				*/
				$nonPluralName = self::$grammar->getNonPlural($table[$column_name]);
				$alias_class_name = ucfirst(self::$stringHelper->generateSlug($nonPluralName));
				
				/**
				* It's name is in snake case
				* E.g.:
				* Delivery_address
				* - RegexGroup 1 has: _a
				* - RegexGroup 2 has: a
				*
				* Then we make RegexGroup 2 char upper because of camelCase
				*/
				if(preg_match_all('#(?<!^)([_-]([\w]{1}))#ui',$alias_class_name,$m)){
					$i = 0;
					foreach($m[2] as $starterChar){
						$starterChar_temp = strtoupper($starterChar);
						$alias_class_name = str_replace($m[1][$i],$starterChar_temp,$alias_class_name);
						++$i;
					}
				}
				
				$trunk['TABLES'][$table[$column_name]]['CLASS'] = $alias_class_name;
				$modell_class_aliases[$alias_class_name] = [
					'class'			=> $alias_class_name,
					'tableName'		=> $table[$column_name]
				];
			}
		}

		/** Get connections **/
		$connections = self::getConnections();
		if($connections){
			foreach($connections as $connection){
				//$trunk['CONNECTIONS'][$connection->table][$connection->column][$connection->t_table][$connection->t_column] = true;
				$trunk['CONNECTIONS'][$connection->t_table][$connection->table][$connection->column] = $connection->t_column;
			}
		}

		/** Build connections by name (If doesn't InnoDB) **/
		$connectionsByName = self::buildConnectionsByName($trunk);
		
		$trunk['CONNECTIONS'] = isset($trunk['CONNECTIONS']) ? array_merge($trunk['CONNECTIONS'],$connectionsByName) : $connectionsByName;
		$trunk['MODEL_ALIASES'] = $modell_class_aliases;
		
		self::buildAliasClasses($modell_class_aliases);
		
		return $trunk;
	}
	
	protected static function buildAliasClasses($aliasData) {
		/**
		* @todo Rendesen megcsinálni...
		*/
		$appDir = self::$config->app('APP_DIR');
		foreach($aliasData as $data){
			if(file_exists($appDir . '/Models/' . $data['class'] . '.php') === false){
				$fileString = '<?php ' . PHP_EOL . PHP_EOL . 'namespace ' . self::$modelNamespace . ';' . str_repeat(PHP_EOL,2);
				$fileString .= 'use Csifo\Database\MySql\Orm\Model\Model;' . str_repeat(PHP_EOL,2);
				$fileString .= 'class ' . $data['class'] . ' extends Model { ' 
				. str_repeat(PHP_EOL,2)
				. chr(9) . 'protected $table = "' . $data['tableName'] . '";' . str_repeat(PHP_EOL,2)
				. ' }' 
				. PHP_EOL;

				file_put_contents($appDir . '/Models/' . $data['class'] . '.php',$fileString);
			}
		}
	}
	
	protected static function buildConnectionsByName($trunk) : array {

		if(isset($trunk['TABLES']) === false) return [];

		$connections = [];
		
		/**
		* In the $trunk['TABLES'] array,we have the columns
		*/
		foreach($trunk['TABLES'] as $table => $columns){

			$columns = array_keys($trunk['TABLES'][$table]['COLUMNS']);
			$count = count($columns);
			/*
			* Loop throuht the columns
			*/
			for($i = 0; $i < $count; ++$i){
				/*
				* Loop throught the other tables
				*/
				foreach($trunk['TABLES'] as $table_foreign => $columns_foreign){
					/*
					* If it's NOT self
					*/
					if($table_foreign !== $table){
						/*
						* If there's a connection BY NAME,then we store it in the
						* "$connections" array.
						*/
						if(self::$grammar->handle($columns[$i],$table_foreign)){
							//[idegen_tabla][alany_tabla][alany_kulcs] = 'idegen_kulcs_rendes_neve'
							
							/*
							* We are checking that the accepted column is REALLY exists among the "foreign"(HERE is primary) table's columns
							*
							* Cut down the word after the LAST "_" e.g.: user_table_id -> id
							*/
							if(preg_match('#_([^_]+?)$#ui',$columns[$i],$m)){
								/*
								* Check if the column name (e.g.: id) is among the "owner" table's column
								*/
								if(isset($trunk['TABLES'][$table_foreign]['COLUMNS'][$m[1]])){
									$connections[$table_foreign][$table][$trunk['TABLES'][$table]['PRIMARY']] = $columns[$i];
								}
							}
						}
					}
				}
			}
		}
		
		return $connections;
	}
	
	protected static function getTableData($table){
		$trunk = [];
		
		/** Get primary key **/
		$query = "SHOW INDEX FROM {$table} WHERE Key_name = 'PRIMARY';";
		$key_row = self::$connection->GiveRow($query,null,'','',true);
		$trunk[$key_row['Key_name']] = $key_row['Column_name'];
		
		/** Get columns **/
		$query = "DESCRIBE {$table};";
		$columns = self::$connection->GiveAll($query,null,'','',true);
		if($columns){
			foreach($columns as $column){
				$trunk['COLUMNS'][$column["Field"]] = $column;
				unset($trunk['COLUMNS'][$column["Field"]]["Field"]);
			}
		}
		return $trunk;
	}
	
	protected static function getTables(){
		return self::$connection->GiveAll('SHOW TABLES;',null,'','',true);
	}
	
	protected static function getConnections(){
		$query = "SELECT
			i.TABLE_NAME AS 'table', 
			k.COLUMN_NAME AS 'column',
			k.REFERENCED_TABLE_NAME AS 't_table',
			k.REFERENCED_COLUMN_NAME AS 't_column' 
		FROM information_schema.TABLE_CONSTRAINTS i 
			LEFT JOIN information_schema.KEY_COLUMN_USAGE k 
			ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME 
		WHERE i.CONSTRAINT_TYPE = 'FOREIGN KEY' AND i.TABLE_SCHEMA = DATABASE();";
		return self::$connection->GiveAll($query,null,'','',true);
	}
}