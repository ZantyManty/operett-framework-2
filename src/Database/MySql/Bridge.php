<?php

namespace Csifo\Database\MySql;

use Csifo\Database\MySql\Interfaces\IBridge;

class Bridge implements IBridge {
	
	protected $resolver = null;
	protected $relations = null;
	
	public function getResolver(){
		if($this->resolver === null){
			$this->resolver = resolve('Csifo\Database\MySql\Resolver\Interfaces\IResolver');
		}
		return $this->resolver;
	}
	
	public function getConnection(){
		return $this->getResolver()->getConnection();
	}
	
	public function getRelations(){
		if($this->relations === null){
			$this->relations = resolve('Csifo\Database\MySql\Orm\Interfaces\IRelations');
		}
		return $this->relations;
	}
}