<?php

namespace Csifo\Http\Interfaces;

interface IRoutes {
	
	public function handle();
}