<?php

namespace Csifo\Http\Interfaces;

interface ICookie {
	
	public function get($key);
	
	public function set($key,$value,$time);
	
	public function destroy($key);
	
}