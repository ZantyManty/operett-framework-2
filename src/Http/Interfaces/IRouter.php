<?php

namespace Csifo\Http\Interfaces;

interface IRouter {
	
	public function dispatch();
	
}