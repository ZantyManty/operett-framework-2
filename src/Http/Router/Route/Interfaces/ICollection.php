<?php

namespace Csifo\Http\Router\Route\Interfaces;

use Csifo\Http\Router\Interfaces\IRoute;

interface ICollection {
	public function addRouteObject(IRoute $route,string $method);
}