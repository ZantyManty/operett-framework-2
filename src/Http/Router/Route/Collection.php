<?php

namespace Csifo\Http\Router\Route;

use Csifo\Http\Router\Route\Interfaces\ICollection;
use Csifo\Http\Router\Interfaces\IRoute;

class Collection implements ICollection {
	
	protected $routes = [];
	
	protected $groups = [];
	
	protected $index = 0;
	
	/*
	* name => pattern ($index)
	*/
	protected $names = [];
	
	public function addRouteObject(IRoute $route,string $method){
		$this->routes[$this->index] = [
			'object' => $route,
			'method' => $method
		];
		
		$this->index++;
	}
	
	public function fillNames(){
		if(empty($this->names) === true){
			foreach($this->routes as $index => $data){
				$name = $data['object']->getName();
				if($name !== '')
					$this->names[$name] = $index;
			}
		}
	}
	
	public function getRouteByName(string $name){
		if(isset($this->names[$name])){
			return $this->routes[$this->names[$name]]['object'];
		}
	}
	
	public function getRoutes(){
		return $this->routes;
	}
	
	public function getGroups(){
		return $this->groups;
	}
	
	public function __sleep(){
		return ['routes','groups','names'];
	}
	
}