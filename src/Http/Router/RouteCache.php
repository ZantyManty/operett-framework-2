<?php

namespace Csifo\Http\Router;

use Csifo\Http\Router\Interfaces\IRouteCache;
use Csifo\Helpers\Interfaces\IClosure;
use Csifo\Core\Interfaces\IConfig;

class RouteCache implements IRouteCache {
	
	protected $helper;
	protected $functions = [];
	protected $path = '';
	
	public function __construct(IClosure $helper,IConfig $config){
		$this->helper = $helper;
		$this->path = $config->app('CACHE_DIR');
	}
	
	public function exportClosure($closure) : string {
		$rawClosure = $this->helper->extractClosure($closure);
		$name = 'f' . time() . rand(0,7000);
		$rawClosure = preg_replace('#^\s*function#ui','function ' . $name,$rawClosure);
		$rawClosure = preg_replace('#;\s*$#ui','',$rawClosure);
		$this->functions[] = (string)$rawClosure;
		return (string)$name;
	}
	
	public function export() {
		/*var_Dump(self::$functions);
		exit;*/
		$buffer = '<?php' . str_repeat(PHP_EOL,2);
		foreach($this->functions as $closure){
			$buffer .= $closure . str_repeat(PHP_EOL,2);
		}
		file_put_contents($this->path . '/RouteClosures.php',$buffer);
	}
}