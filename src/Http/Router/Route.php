<?php

namespace Csifo\Http\Router;

use Csifo\Http\Router\Interfaces\IRoute;

class Route implements IRoute {
	
	protected $url = '';
	
	protected $name = '';
	
	protected $method = '';
	
	protected $middlewares = [];
	
	protected $call = null;
	
	protected $cache = null;
	
	/*
	* Avalaible types
	*/	
	protected $types = [
		'any' => '[\w-]',
		'num' => '\d',
		'str' => '[a-z_-]'
	];
	
	protected $patternArray = [];
	
	protected $patternArrayCount = 0;
	
	public function __construct($url,$method,$cache){
		$this->url = $url;
		$this->method = $method;
		$this->cache = $cache;
	}
	
	/*
	* Serialize
	*/
	public function __sleep(){
		if(\is_callable($this->call) === true){
			$this->call = '->' . $this->cache->exportClosure($this->call);
		}
		return ['url', 'method', 'middlewares', 'call','patternArray','patternArrayCount'];
	}
	
	public function name($name){
		$this->name = $name;
		return $this;
	}
	
	/*
	* Set the controller,or a callable
	* @param string || callable
	*/
	public function to($call){
		
		if(\is_callable($call) === false){
			$exp = explode('@',$call);
			$exp[0] = ucfirst(strtolower($exp[0]));
			$this->call = $exp;
		} else {
			$this->call = $call;
		}
		
		return $this;
	}
	
	/*
	* Through middlewares
	* @param string || array
	*/
	public function through($middlewares){
		if(\is_array($middlewares)){
			$this->middlewares = array_merge($this->middlewares,$middlewares);
		} else {
			$this->middlewares[] = $middlewares;
		}
		return $this;
	}
	
	public function getParamCount(){
		return $this->patternArrayCount;
	}
	
	/*
	* Create the regex pattern
	* @param string $url 		The given path
	* @return string $pattern	The created regex pattern
	*/
	public function createPattern(){
		
		if(empty($this->patternArray) === true){
			$fragments = explode('/',trim($this->url,'/'));
			
			$index = 0;
			
			foreach($fragments as $fragment){
				if(preg_match_all('#\{([\w]+):((?1))?(?::*(\?)*)\}#ui',$fragment,$paramMatch)){
					
					$paramData = [];
					$regexPattern = $fragment;
					$wholeParamNeeded = 0;
					
					foreach($paramMatch[1] as $i => $paramName){
						
						$paramData[$i + 1] = $paramName;
						
						$type = isset($this->types[$paramMatch[2][$i]]) === true ? $this->types[$paramMatch[2][$i]] : '[\w]+';
						$needed = (isset($paramMatch[3][$i]) && !empty($paramMatch[3][$i]) ? '*' : '+');
						
						/*
						* If we have AT LEAST 1 needed
						*/
						if($needed === '+'){
							$wholeParamNeeded++;
						}
						
						$pattern = '(' . $type . $needed . ')';
						
						$regexPattern = str_replace($paramMatch[0][$i],$pattern,$regexPattern);
					}					
					
					/*
					*	0 => is param?
					*	1 => regex pattern
					*	2 => paramData (Whic controller arg match the capture groups inside the url fragment)
					*	3 => Needed?
					*/
					$this->patternArray[$index] = [
						true,
						$regexPattern,
						$paramData,
						($wholeParamNeeded > 0 ? true : false)
						
					];
					
				} else {
					$this->patternArray[$index] = [
						false,
						$fragment,
					];
				}
				++$index;
			}
			
			$this->patternArrayCount = \count($this->patternArray);
		}
		
		//$this->patternArray = array_values($this->patternArray);
		
		return $this->patternArray;
	}
	
	public function getName(){
		return $this->name;
	}
	
	public function getUrl(){
		return $this->url;
	}
	
	/*
	* @return string $this->pattern The created pattern from the given uri path
	*/
	public function getPattern(){
		$this->pattern = $this->createPattern();
		return $this->pattern;
	}
	
	/*
	* @return array $this->middlewares The given middleware class names (Without the full namespace)
	*/
	public function getMiddlewares(){
		return $this->middlewares;
	}
	
	/*
	* If the developer didn't define the controller or the the callable closure,
	* we gather it from the uri itself.
	*
	* Note: I handle the default Controller and method handling later...
	*
	* @return string The gathered or default Controller class name and method.
	*/
	protected function getCallByPath(){
		if(preg_match('#^\/?([\w]+?)\/([\w]+?)(?:\/|$)#i',$this->url,$match)){
			return ucfirst($match[1]) . '@' . (isset($match[2]) ? $match[2] : 'index');
		} else {
			return 'Default@index';
		}
	}
	
	/*
	* @return string|callable $this->call 
	*	The given Controller class name and method (e.g.: ContClass@mehod) or a callable
	*/
	public function getCall(){
		/*
		* If we don't have a defined call string or closure,
		* we get if rom the uri...
		*/
		if($this->call === null){
			$this->call = getCallByPath();
		}
		
		return $this->call;
	}
}