<?php

namespace Csifo\Http\Router\Facade;

//use Csifo\Http\Router\Facade\Route	as Route;

use Csifo\Psr\Container\IContainer;
use Csifo\Config\Interfaces\IRoutes as RoutesConfig;
use Csifo\Http\Interfaces\IRoutes	as IRoutes;
use Csifo\Http\Interfaces\IRouter	as IRouter;
use Csifo\Factory\Interfaces\IRoute as IRouteFactory;
use Csifo\Cache\SimpleCache\Interfaces\ICache;

class RouterShell implements IRoutes {
	
	protected $router 		= null;
	protected $routes		= null;
	protected $factory		= null;
	protected $cache		= null;
	
	public function __construct(IContainer $container,IRouter $router,RoutesConfig $routes,IRouteFactory $factory,ICache $cache){
		$this->router		= $router;
		$this->routes		= $routes;
		$this->factory		= $factory;
		$this->cache		= $cache;
		$this->container	= $container;
	}
	
	public function handle(){
		
		if($this->cache->has('RouteCollection') == false){
			$this->routes->routes($this->factory);
			$collection = $this->factory->getCollection();
			$collection->fillNames();
			$this->cache->set('RouteCollection',$collection);
			$this->factory->exportCache();
		} else {
			$collection = $this->cache->get('RouteCollection');
			$this->container->addInstance(get_class($collection),$collection);
		}

		$this->router->addRouteObjects($collection->getRoutes());

		return $this->router->dispatch();
	}
}