<?php

namespace Csifo\Http\Router;

use Csifo\Core\Interfaces\IConfig;
/*
* PSR-7
*/
use Csifo\Psr\Http\Message\IServerRequest 	as IServerRequest;
use Csifo\Psr\Http\Message\IResponse 		as IResponse;
use Csifo\Psr\Http\Message\IStream 			as IStream;

/*
* PSR-15
*/
use Csifo\Psr\Http\Server\IRequestHandler	as IRequestHandler;

use Csifo\Http\Interfaces\IRouter 			as IRouter;
use Csifo\Core\Container\Container;

class Router implements IRouter {
	
	protected $config;
	
	protected $routes = [];
	
	/*
	* Router objects' array's indexes
	*/
	protected $methods = [];
	
	protected $currentData = [];
	
	/*
	* Group's filters (temporary middleware storage array)
	*/
	//protected $groupFilters = [];
	
	protected $routeCounter = 0;
	
	protected $request;
	
	protected $response;
	
	protected $container;
	
	protected $stream;
	
	protected $middlewareHandler;
	
	public function __construct(IServerRequest $request,IResponse $response,Container $container,IStream $stream,IRequestHandler $middlewareHandler,IConfig $config){
		$this->request	= $request;
		$this->response = $response;
		$this->container	= $container;
		$this->stream = $stream;
		$this->middlewareHandler = $middlewareHandler;
		$this->config = $config;
	}
	
	/*
	* Define a group,and immediately run it's callable
	*
	* Note: "$this->groupFilters" just a temporary var
	*/
	public function group($middlewares,$callable) : void {
		$this->groupFilters = $middlewares;
		$callable();
		$this->groupFilters = [];
	}
	
	/*
	* Adds objects,and adds it's array index 
	* to the right method sub array.
	*
	* Note: We need this "sub array thing" and indexes because it's make Router faster. 
	* E.g. a GET request coming,the router only need the GET sub array from the methods,and
	* get's only the right indexes,then gets the right object from the "routes" array.
	*/
	public function addRouteObjects($routes) : void {
		foreach($routes as $route){
			$object = $route['object'];
			//$object->through($this->groupFilters);
			
			$this->routes[] = $object;
			if($route['method'] === 'ALL'){
				$this->methods['GET'][] = $this->routeCounter;
				$this->methods['POST'][] = $this->routeCounter;
				$this->methods['PATCH'][] = $this->routeCounter;
				$this->methods['DELETE'][] = $this->routeCounter;
			} else {
				$this->methods[$route['method']][] = $this->routeCounter;
			}
			$this->routeCounter++;
		}
	}

	/*
	* Create the controller's resolving callabe for the last middleware
	*
	* @return callable
	*/
	protected function createControllerCallable() : callable {
		$routeData = $this->currentData;
		$container = $this->container;
		$namespace = $this->config->get('namespaces')['Modules'];
		$cachePath = $this->config->app('CACHE_DIR');
		/*
		* Create,and return with the callable
		*/
		return function() use($routeData,$container,$namespace,$cachePath){
			if(\is_string($routeData['call']) === false && \is_array($routeData['call']) === false){
				return \call_user_func_array($routeData['call'],$container->resolveFunction($routeData['call']));
				
			} else if(\is_string($routeData['call']) === true && strpos($routeData['call'],'->') !== false){
				
				require_once $cachePath . '/RouteClosures.php';
				$funcName = str_replace('->','',$routeData['call']);
				return call_user_func_array($funcName,$container->resolveFunction($funcName));
				
			} else {
				$module = $routeData['call'][0];
				$method = $routeData['call'][1];
				
				$controllerClass = $namespace . '\\' . $module  . '\Controller';
				
				$routerParams = [
					[
						'id'		=> 'Csifo\Mvc\Interfaces\IView',
						'args'		=> [
							'module' =>	$module,
							'action' => $method
						]
					],
					[
						'class'		=> $controllerClass,
						'method'	=> $method,
						'args'		=> $routeData['params']
					],
				];
				
				//Add the parameters
				$container->addCustomParameter($routerParams);
				$container->addGlobalParameters(['module' => $module]);
				
				/*
				* Call the beast!!
				*/
				$controller = $container->resolveClassWithoutCallMethod($controllerClass,$method);
				//return call_user_func_array([$controller['object'],$method],$controller['args']);
				return $controller['object']->$method(...$controller['args']);
			}
		};
	}
	
	protected function handle($parameters,$route,$controllerParameters){
		/*
		* "getCall()" returns with an exploded string (array) OR a callable
		*/
		$this->currentData['call']	 = $route->getCall();
		$this->currentData['params'] = $controllerParameters;
		
		/*
		* Init middleware handler
		*/
		$this->middlewareHandler->init($this->response,$this->stream);
		
		/*
		* Give the Middleware list...
		*/
		$this->middlewareHandler->setMiddlewareArray($route->getMiddlewares());
		
		/*
		* Set Controller callable
		*/
		$this->middlewareHandler->setControllerCallable($this->createControllerCallable());
		
		/*
		* Run the middlewares,and get the response
		*/
		return $this->middlewareHandler->handle($this->request);
	}

	public function dispatch() : IResponse {
		$url 	= trim($this->request->getUri()->getPath(),'/');
		$method = $this->request->getMethod();
		
		$urlArray = \explode('/',$url);
		/*
		* The exploded array's first element always empty,so we need a trick :)
		*/
		$urlArrayCount = \count($urlArray);
		
		$found = false;
		foreach($this->methods[$method] as $key){

			$routeObject = $this->routes[$key];
			
			if($urlArrayCount <= $routeObject->getParamCount()){
				//If the element number less,orr equal,it's maybe OK
				$patternArray = $this->routes[$key]->getPattern();
				$controllerParameters = [];
				
				if($url === '' && $this->routes[$key]->getUrl() === '/'){
					/*
					* Főoldal
					*/
					$found = true;
				} else {
					
					foreach($patternArray as $pattern_key => $data){
						
						//Normal segment
						if($data[0] === false && $urlArray[$pattern_key] === $data[1]){
							$found = true;
							
						} else if($data[0] === true) {
														
							//Parameter
							if($data[3] === true || (isset($urlArray[$pattern_key]) && $data[3] === false)){
								
								if(preg_match('#' . $data[1] . '#ui',$urlArray[$pattern_key],$urlParamData)){
									unset($urlParamData[0]);
									foreach($urlParamData as $groupIndex => $group){
										$controllerParameters[$data[2][$groupIndex]] = $group;
									}
									
									$found = true;
									
								} else {
									$found = false;
								}
							} else if(!isset($urlArray[$pattern_key]) && $data[3] === true){
								$found = false;
							}
							
						} else {
							//HA nem paraméter,ÉS nem is egyenlő,akkor már nem fog eggyezni
							$found = false;
							break;
						}
					}
				}
				
				//exit;

				if($found === true){
					return $this->handle($patternArray,$routeObject,$controllerParameters);
				}
			}
		}
		
		/*
		* If path not found
		*/
		$stream = $this->stream->changeStream('php://memory','w+')->write('Route not Found!');
		return $this->response->withStatus(404)->withBody($stream);
	}
}