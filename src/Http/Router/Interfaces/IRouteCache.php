<?php

namespace Csifo\Http\Router\Interfaces;

interface IRouteCache {
	
	public function exportClosure($closure);
	
}