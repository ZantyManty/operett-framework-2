<?php 

namespace Csifo\Http\Router\Interfaces;

interface IRoute {
	
	public function to($call);
	public function through($middlewares);
	
}