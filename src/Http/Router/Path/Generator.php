<?php

namespace Csifo\Http\Router\Path;

use Csifo\Http\Router\Path\Interfaces\IGenerator;
use Csifo\Http\Router\Route\Interfaces\ICollection;

class Generator implements IGenerator {
	
	protected $routeCollection = null;
	
	public function __construct(ICollection $collection){
		$this->collection = $collection;
	}
	
	public function generatePath($name,$params) : string {
		
		$routeObj = $this->collection->getRouteByName($name);

		if($routeObj !== null){
			$path = $routeObj->getUrl();
			$fragments = explode('/',$path);
		
			$generatedPath = '';
			foreach($fragments as $fragment){
				if(strpos($fragment,'{') !== false && strpos($fragment,'}') !== false){
					if(preg_match_all('#({(\w+):*(any|str|num)*:*(\?)*})#i',$fragment,$frag_params)){
						$i = 0;
						foreach($frag_params[2] as $par){
							if(isset($params[$par])){
								$fragment = str_replace($frag_params[1][$i],$params[$par],$fragment);
							} else {
								$fragment = str_replace($frag_params[1][$i],'',$fragment);
							}
							++$i;
						}
						$generatedPath .= $fragment . '/';
					}
					
				} else {
					$generatedPath .= $fragment . '/';
				}
			}
			
			return preg_replace('#\/+$#','/',$generatedPath);
		} else {
			return '/';
		}
	}
}