<?php

namespace Csifo\Http;

class GeoIp {
	
	/*
	* Free IP API providers
	* 'provider url' => required fields
	*/
	protected static $providers = [
		'http://ip-api.com/json/{ip}' => [ //Good accuracy!
			'status' 	  => 'status',
			'city'		  => 'city',
			'country' 	  => 'country',
			'countryCode' => 'countryCode',
			'zip'		  => 'zip',
			'lat'		  => 'lat',
			'lon'		  => 'lon',
			'org'		  => 'isp',
			'timezone'	  => 'timezone',
		], 
		'https://api.ipdata.co/{ip}' => [
			'city'			=> 'city',
			'country_name'	=> 'country',
			'country_code'	=> 'countryCode',
			'postal'		=> 'zip',
			'latitude'		=> 'lat',
			'longitude'		=> 'lon',
			'organisation'	=> 'isp',
			'timezone'		=> 'timezone',
		],
		'http://freegeoip.net/json/{ip}' => [
			'city'			=> 'city',
			'country_name'	=> 'country',
			'region_code'	=> 'countryCode',
			'zip_code'		=> 'zip',
			'latitude'		=> 'lat',
			'longitude'		=> 'lon',
			'time_zone'		=> 'timezone',
		],
	];
	
	/*
	* If we already got the information,we need this array
	*/
	protected static $cache = [];
	
	/*
	* Gets the geo data from a provider,and returns with it.
	*
	* @param 		$ip string	 | The IPv4 address
	* @return self::$cache array | Associative array
	*/
	public static function getData($ip){
		if(empty(self::$cache)){
			foreach(self::$providers as $provider => $fields){
				$json = file_get_contents(str_replace('{ip}',$ip,$provider));
				$status = $http_response_header[0];
				if(stripos($status,'200 OK') !== false){
					/*
					* If the request is successful
					*/
					$decoded = json_decode($json,true);
					
					if(isset($decoded['status']) && $decoded['status'] == 'fail'){
						continue;
					} else {
						if(isset($fields['status'])) unset($fields['status']);
						foreach($fields as $field => $alias){
							self::$cache[$alias] = isset($decoded[$field]) ? $decoded[$field] : '';
						}
						return self::$cache;
					}
				} else {
					continue;
				}
			}
		}
		
		return self::$cache;
	}
}