<?php

namespace Csifo\Http\Curl;

use Csifo\Http\Curl\Interfaces\ICurl as ICurl;
use Csifo\Core\Interfaces\IConfig as IConfig;

class Curl implements ICurl {
	
	protected $ch;
	protected $cookieFile;
	
	public function __construct(IConfig $config){
		$this->cookieFile = $config->app('TRUNK_DIR') . '/curl_cookies.txt';
		$this->ch = curl_init();
		curl_setopt($this->ch,CURLOPT_RETURNTRANSFER,1);
	}
	
	public function setUrl($url){
		curl_setopt($this->ch,CURLOPT_URL,$url);
		return $this;
	}
	
	public function post($fields){
		if(is_array($fields)){
			$queryFields = http_build_query($fields);	
		}
		curl_setopt($this->ch,CURLOPT_POST,1);
		curl_setopt($this->ch,CURLOPT_POSTFIELDS,$queryFields);
		return $this;
	}
	
	public function execute(){
		$result = curl_exec($this->ch);
		curl_close($this->ch);
		$this->ch = curl_init();
		curl_setopt($this->ch,CURLOPT_RETURNTRANSFER,1);
		return $result;
	}
	
	public function handleCookies(){
		curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookieFile);
		curl_setopt($this->ch, CURLOPT_COOKIE, "cookiename=0");
		curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookieFile);
		return $this;
	}
	
	public function setFollowLocation($value = 0){
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $value);
		return $this;
	}
	
	public function sslVerifyOff(){
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
		return $this;
	}
	
	public function dontFollowLoaction(){
		return $this->setFollowLocation(0);
	}
	
	public function followLocation(){
		return $this->setFollowLocation(1);
	}
	
	public function __destruct(){
		$this->curl = null;
	}
	
}