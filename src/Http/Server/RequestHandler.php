<?php
/*
* Middleware handler class
*/
namespace Csifo\Http\Server;

use Csifo\Psr\Http\Server\IRequestHandler;
/*
* PSR-7
*/
use Csifo\Psr\Http\Message\IResponse;
use Csifo\Psr\Http\Message\IStream;
use Csifo\Psr\Http\Message\IServerRequest;

use Csifo\Core\Interfaces\IConfig;

class RequestHandler implements IRequestHandler {
	
	protected $middlewares = [];
	protected $pointer = 0;
	
	/*
	* Callable,previously created in the router
	*/
	protected $controller;
	
	protected $response;
	protected $stream;
	
	protected $middlewareNamespace = '';
	
	public function __construct(IConfig $config){
		$this->middlewareNamespace = $config->namespaces('Middleware');
	}

	public function init(IResponse $response,IStream $stream) {
		$this->response = $response;
		$this->stream = $stream;
	}
	
	public function handle(IServerRequest $request) : IResponse{
		/*
		* Get the actual middleware with the pointer.
		*/
		$next = (isset($this->middlewares[$this->pointer])) ? $this->middlewares[$this->pointer] : false;
		++$this->pointer;
		
		/*
		* If there's no middleware left
		*/
		if($next === false){
			/*
			* Controller...
			* And the response...
			*/
			$controller = $this->controller;
			return $this->createResponse($controller());
			
		} else {
			$next = $this->middlewareNamespace . '\\' . $next;
			$result = (new $next($this->response))->process($request,$this);
		}
		
		return $result;
	}
	
	public function setMiddlewareArray($middlewares){
		$this->middlewares = $middlewares;
	}
	
	public function setControllerCallable($callable){
		$this->controller = $callable;
	}
	
	/*
	* Creates a response object 
	* (If the "$result" parameter is not an "IResponse" implementation)
	*
	* @param string | IResponse $result
	* @return IResponse implementation
	*/
	protected function createResponse($result) : IResponse {
		if($result instanceof IResponse){
			return $result;
		} else{
			$result = (is_callable($result)) ? $result() : $result;
			$stream = $this->stream->changeStream('php://memory','w+')->write($result);
			return $this->response->withBody($stream);
		}
	}
	
}