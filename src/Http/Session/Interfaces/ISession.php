<?php

namespace Csifo\Http\Session\Interfaces;

interface ISession {
	
	public function get($key);
	
	public function set($key,$value);
	
	public function has($key);
	
}