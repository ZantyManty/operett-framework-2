<?php

namespace Csifo\Http\Session\Classic;

use Csifo\Http\Session\Interfaces\ISession as ISession;

class Session implements ISession {
	
	public function __construct(){
		if(session_id() === ''){
			session_start();
		}
	}
	
	public function get($key){
		if($this->has($key)){
			return $_SESSION[$key];
		}
		return false;
	}
	
	public function set($key,$value){
		$_SESSION[$key] = $value;
	}
	
	public function has($key){
		return isset($_SESSION[$key]);
	}
	
	public function destroy(){
		$_SESSION = [];
		session_destroy();
	}
	
}