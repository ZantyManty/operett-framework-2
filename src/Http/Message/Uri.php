<?php

namespace Csifo\Http\Message;

use Csifo\Psr\Http\Message\IUri;

class Uri implements IUri {
	
	protected $uri = ''; //Complete URI
	
	protected $scheme = ''; //http or https,etc...
	
	protected $host = '';
	
	protected $path = '';
	
	protected $query = '';
	
	protected $fragment = '';
	
	protected $port = '';
	
	protected $user = '';
	
	protected $password = '';
	
	public function __construct($url = ''){
		$this->uri = ($url === '') ? (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" : $url;
		if($this->uri !== ''){ $this->setParts(); }
	}
	
	public function getUri(){
		return $this->uri;
	}
	
	protected function setParts(){
		$parts = parse_url($this->uri);
		
		$this->scheme	= (isset($parts['scheme'])) ? $parts['scheme'] : '';
		$this->host		= (isset($parts['host'])) ? $parts['host'] : ''; //www.host.com
		$this->port		= (isset($parts['ports'])) ? intval($parts['ports']) : 0;
		$this->user		= (isset($parts['user'])) ? $parts['user'] : '';
		$this->password = (isset($parts['pass'])) ? $parts['pass'] : '';
		$this->path		= (isset($parts['path'])) ? $parts['path'] : '';	// /path/to/something
		$this->query	= (isset($parts['query'])) ? $parts['query'] : ''; //?key=value
		$this->fragment = (isset($parts['fragment'])) ? $parts['fragment'] : ''; //#fragment
	}
	
	public function getScheme(){
		return $this->scheme;
	}
	
	public function getHost(){
		return $this->host;
	}
	
	public function getPort(){
		return $this->port;
	}
	
	public function getPath(){
		return $this->path;
	}
	
	public function getQuery($asArray = false){
		$get = filter_var(strip_tags($this->query),FILTER_SANITIZE_STRING);
		if($asArray){
			parse_str($get,$query);
			return $query;
		}
		return $get;
		//return filter_var(strip_tags($this->query),FILTER_SANITIZE_STRING);
	}
	
	public function getFragment(){
		return $this->fragment;
	}
	
	public function getUserInfo(){
		return [$this->user,$this->password];
	}
	
	public function getAuthority(){
        $authority = $this->host;
		
        if ($this->userInfo !== '') {
            $authority = $this->userInfo . '@' . $authority;
        }
		
        if ($this->port !== null) {
            $authority .= ':' . $this->port;
        }
		
        return $authority;
    }
	
	public function withScheme($scheme){
		$this->scheme = $scheme;
		return $this;
	}
	
	public function withUserInfo($user, $password = null){
		$this->user		= $user;
		$this->password = ($password !== null) ? $password : '';
		return $this;
	}
	
	public function withHost($host){
		$this->host = $host;
		return $this;
	}
	
	public function withPort($port){
		$this->port = $port;
		return $this;
	}
	
	public function withPath($path){
		$this->path = $path;
		return $this;
	}
	
	public function withQuery($query){
		$this->query = $query;
		return $this;
	}
	
	public function withFragment($fragment){
		$this->fragment = $fragment;
		return $this;
	}
	
	public function __toString(){
		$url = $this->getScheme();
		$url .= $this->getHost();
		$url .= ($this->port !== '') ? ':' . $this->getPort() : '';
		$url .= $this->getPath();
		$url .= ($this->query !== '') ? '?' . $this->getQuery() : '';
		$url .= ($this->fragment !== '') ? '#' . $this->getFragment() : '';
		
		return $url;
	}
}