<?php

namespace Csifo\Http\Message;

use Csifo\Psr\Http\Message\IMessage	as IMessage;
use Csifo\Psr\Http\Message\IStream	as IStream;

class Message implements IMessage {
	
	protected $headers = [];
	
	protected $stream = null;
	
	protected $protocol = '1.1';
	
	public function __construct(IStream $stream){
		$this->stream = $stream;
	}
	
	public function getProtocol(){
		return $this->protocol;
	}
	
	public function getHeaders(){
        return $this->headers;
    }
	
	public function getHeader($header){
		if(isset($this->headers[$header])){
			return $this->headers[$header];
		} else {
			$header = strtoupper(str_replace('-','_',$header));
			return $_SERVER['HTTP_' . $header];
		}
	}
	
	public function getHeaderLine($header){
        return $header . ',' .$this->getHeader($header);
    }
	
	public function getBody(){
		return $this->stream;
	}
	
	public function getProtocolVersion(){
		return $this->protocol;
	}
	
	public function hasHeader($header){
		return isset($this->headers[$header]);
	}
	
	public function withAddedHeader($name,$value){
		if(is_array($value) === true){
			$value = implode(',',$value);
		}

		if(!isset($this->headers[$name])){
			$this->headers[$name] = $value;
		} else {
			$this->headers[$name] .= $value;
		}
	}
	
	public function withHeader($header,$value){
		$this->headers[$header] = $value;
		return $this;
	}
	
	public function withoutHeader($header){
		unset($this->headers[$header]);
		return $this;
	}
	
	public function withProtocolVersion($version){
		$this->protocol = $version;
		return $this;
	}
	
	public function withBody(IStream $body){
		$this->stream = $body;
		return $this;
	}
	
}