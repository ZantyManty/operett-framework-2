<?php

namespace Csifo\Http\Message;

use Csifo\Psr\Http\Message\IUploadedFile as IUploadedFile;
use Csifo\Helpers\MimeTypes as MimeTypes;

class UploadedFile implements IUploadedFile {
	
	
	protected $name;
	
	protected $size;
	
	protected $fullPath;
	
	protected $type;
	
	protected $error;
	
	protected $stream = null;
	
	protected $specialFileInterfaces = [
		'.(?:jpg|png|gif)$'	=>	'Ext\IImage',
		'.mp3$'				=>	'Ext\IMp3',
	];
	
	public function __construct($name,$fullPath,$type,$size,$error = '',$sapi = false){
		$this->name 	= $name;
		$this->fullPath = $fullPath;
		$this->type		= $type;
		$this->size 	= $size;
		$this->error	= $error;
		$this->sapi		= $sapi;
	}
	
	public function getStream(){
		return $this->stream;
	}
	
	public function moveTo($targetPath){
		/*
		* If file havent got an extension,we try to decide it
		*/
		if(preg_match('#\/?[\w]+\.[\w]+$#i',$targetPath) === 0){
			$extClass = MimeTypes::getExtensionByMime($this->type);
			$targetPath .= $extClass;
		}
		
		/*
		* Prepare Dir
		*/
		$targetPath = resolve('Csifo\Helpers\Interfaces\IFileHelper')->createDir($targetPath);
		
		if($this->sapi){
			if(!move_uploaded_file($this->fullPath,$targetPath)){
				$this->error = 'Failed to move the file [ ' . $this->name . ' ] (Sapi!)!';
			} else {
				$this->fullPath = $targetPath;
			}
		} else {
			if(!rename($this->fullPath,$targetPath)){
				$this->error = 'Failed to move file [ ' . $this->name . ' ]!';
			}
		}
		
		/*
		* Try to determinate if we have a "special" file
		*/
		$targetInterface = 'IFile'; //Default interface
		
		foreach($this->specialFileInterfaces as $pattern => $interface){
			if(preg_match('#' . $pattern . '#i',$extClass)){
				$targetInterface = $interface;
				break;
			}
		}
		
		return resolveWithParams('Csifo\File\Interfaces\\' . $targetInterface,['path' => $this->fullPath]);
	}
	
	public function getSize(){
		return $this->size;
	}
	
	public function getError(){
		return $this->error;
	}
	
	public function getClientFilename(){
		return $this->name;
	}
	
	public function getClientMediaType(){
		return $this->type;
	}
}