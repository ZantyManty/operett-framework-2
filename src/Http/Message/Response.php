<?php

namespace Csifo\Http\Message;

use Csifo\Psr\Http\Message\IResponse as IResponse;
use Csifo\Psr\Http\Message\IStream as IStream;

class Response extends Message implements IResponse  {
	
	protected $statusCode;
	
	protected $reasonPhrase = 'OK';
	
	public function __construct($code = 200,$reason = 'OK',IStream $body,$headers = [],$protocol = '1.1'){
		$this->reasonPhrase = $reason;
		$this->statusCode 	= $code;
		$this->headers		= $headers;
		$this->protocol 	= $protocol;
		$this->body 		= $body;
	}
	
	public function getStatusCode(){
        return $this->statusCode;
    }
	
    public function getReasonPhrase(){
        return $this->reasonPhrase;
    }
	
    public function withStatus($code, $reasonPhrase = ''){
		$this->reasonPhrase = $reasonPhrase;
		$this->statusCode 	= $code;
		return $this;
    }
	
	public function respond(){
		//$response = "HTTP/{$this->getProtocol()} {$this->getStatusCode()} {$this->getReasonPhrase()}\r\n";
		header("HTTP/{$this->getProtocol()} {$this->getStatusCode()} {$this->getReasonPhrase()}");
		
		$headers = $this->getHeaders();
		
		/*
		* Security
		*/
		/*header_remove('X-Powered-By');
		header_remove('Server: ');*/
		
		foreach($headers as $name => $value){
			header("{$name}: " . $value);
		}
		
		/*
		* Body
		*/
		$stream = $this->getBody();
		
		if ($stream->isSeekable()) {
			$stream->rewind();
		}
		
		 while (!$stream->eof()) {
			echo $stream->read();
		}
	}
}