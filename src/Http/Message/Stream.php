<?php

namespace Csifo\Http\Message;

use Csifo\Psr\Http\Message\IStream as IStream;

class Stream implements IStream {
	
	/*
	* The stream resource
	*/
	protected $stream = null;
	
	protected $readable = null;
	
	protected $writable = null;
	
	protected $seekable = null;
	
	protected $size = null;
	
	protected $modes = [
		'read'	=>	[
			'r' => true,'r+' => true,'a+' => true,'a' => true,
			'c+b' => true,'r+b' => true,'x+' => true,'c+' => true,
			'c+t' => true,'w+t' => true,'x+t' => true
		],
		'write'	=>	[
			'a' => true,'a+' => true,'w' => true,'w+' => true,
			'w+b' => true,'wb' => true,'rw' => true,'x+' => true,
			'c+' => true,'r+t' => true,'x+t' => true,'c+t' => true
		]
	];
	
	public function __construct($streamString = 'php://input',$mode = 'r'){
		$this->stream = fopen($streamString,$mode);
		$this->getMetadata();
	}
	
	public function changeStream($streamString,$mode){
		$this->close();
		$this->stream = null;
		$this->stream = fopen($streamString,$mode);
		$this->getMetadata();
		return $this;
	}
	
	public function read($length = 1024){
		$string = fread($this->stream,$length);
		
		if($string === false){
			throw new \RuntimeException('Read failed from stream!');
		}
		
		return $string;
	}
	
	public function write($string){
		if(fwrite($this->stream,$string) === false){
			throw new \RuntimeException('Write failed from stream!');
		}
		return $this;
	}
	
	public function rewind(){
		$this->seek(0);
		return $this;
	}
	
	public function seek($offset,$whence = SEEK_SET){
		if($this->seekable === false){
			throw new \RuntimeException('Stream is not seekable!');
		}
		
		if(fseek($this->stream,$offset,$whence) === -1){
			throw new \RuntimeException('Stream seek error at offset [ ' . $offset . ' ][' . $whence . ']!');
		}
	}
	
	public function tell(){
		return ftell($this->stream);
	}
	
	public function isReadable(){
        return $this->readable;
    }
	
    public function isWritable(){
        return $this->writable;
    }
	
    public function isSeekable(){
        return $this->seekable;
    }
	
	
	public function getMetadata($key = null){
		$metaData = stream_get_meta_data($this->stream);
		
		if($key !== null){
			return $metaData[$key];
		}
		
		$this->seekable =	$metaData['seekable'];
		$this->readable	=	isset($this->modes['read'][$metaData['mode']]);
		$this->writable	=	isset($this->modes['write'][$metaData['mode']]);
		return $metaData;
	}
	
	public function eof(){
		return feof($this->stream);
	}
	
	public function getSize(){
		$stats = fstat($this->stream);
        if (isset($stats['size'])) {
            $this->size = $stats['size'];
            return $this->size;
        }
	}
	
	public function getContents(){
		return stream_get_contents($this->stream);
	}
	
	public function close(){
		if(fclose($this->stream) === false){
			throw new \RuntimeException('Can\'t close the stream!');
		}
	}
	
	public function detach(){
        if (!isset($this->stream)) {
            return null;
        }
        $result = $this->stream;
        unset($this->stream);
        $this->size = null;
        $this->readable = $this->writable = $this->seekable = false;
        return $result;
    }
	
	public function __toString(){
		if($this->seekable){
			$this->seek(0);
		}
        return $this->getContents();
	}
}