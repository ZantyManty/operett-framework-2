<?php

namespace Csifo\Http\Message;

use Csifo\Psr\Http\Message\IServerRequest as IServerRequest;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Http\Security\Interfaces\IXss as IXss;

use Csifo\Psr\Http\Message\IUri as IUri;
use Csifo\Http\User\Interfaces\IUser as IUser;

class ServerRequest extends Request implements IServerRequest {
	
	protected $cookies = [];
	
	protected $uploadedfiles = [];
	
	protected $parsedBody = null;
	
	protected $attributes = [];
	
	protected $tempPath;
	
	protected $xss = null;
	
	public function __construct(IConfig $config,IXss $xss,IUri $uri,IUser $user){
		parent::__construct($uri,$user);
		$this->tempPath = $config->app('TMP_DIR');
		$this->xss = $xss;
	}
	
	public function getServerParams(){
		return $_SERVER;
	}
	
	public function getCookieParams(){
		if(empty($this->cookies)){
			return $_COOKIE;
		} else {
			return $this->cookies;
		}
	}
	
	public function withCookieParams($cookies){
		$this->cookies = $cookies;
	}
	
	public function getQueryParams(){
		return $this->uri->getQuery();
	}
	
	public function withQueryParams($query){
		$query = implode('&',$query);
		$this->uri->withQuery($query);
	}
	
	public function getUploadedFiles(){
		if(empty($this->uploadedfiles)){
			if(isset($_FILES) && !empty($_FILES)){
				$this->createFileObjects($_FILES);
			}
		}
		
		return $this->uploadedfiles;
	}
	
	public function withUploadedFiles($uploadedFiles){
		$this->uploadedfiles = $uploadedFiles;
	}
	
	protected function createFileObjectFromRaw($rawData){
		$rawFile = preg_replace('#Content-Type: ([\w/]+)[\n\r]#i','',$val);
		//Put it in the temp folder
		$name = trim($data[2][$key]);
		$path = $this->tempPath . '/' . $name;
		
		file_put_contents($path,trim($rawFile));
		
		$type = mime_content_type($path);
		$size = filesize($path);
		
		return new UploadedFile($name,$path,$type,$size);
	}
	
	/*
	* Parser for RAW formdata (e.g.: Postman)
	*/
	protected function formDataParser($body){
		if(preg_match('#Boundary([\w]+)#i',$body,$boundary) === 1){
			$boundary = $boundary[1];
			
			preg_match_all('#"([\w]+?)"(?:; filename="([\S]*?\.[\w]+?)")?[\s]*?([\S\s]*?)------WebKitFormBoundary' . $boundary . '#i',$body,$data);
			$return = [];
			
			foreach($data[3] as $key => $val){
				/*
				* If that is a file
				*/
				if(!empty($data[2][$key])){
					$this->uploadedfiles[trim($data[1][$key])] = $this->createFileObjectFromRaw(trim($val));
				} else {
					$return[trim($data[1][$key])] = filter_var(trim($val),FILTER_SANITIZE_STRING);
				}
			}
			
			return $return;
		} else {
			return [];
		}
	}
	
	protected function getPostData($body){
		$array = [];
		parse_str($body,$array);
		/*
		* Filtering
		*/
		array_walk_recursive($array, function (&$value) {
			 $value = filter_var(trim($value),FILTER_SANITIZE_STRING);
			 $value = $this->xss->clean($value);
		});
		return $array;
	}
	
	public function getParsedBody(){
		$body = (string)$this->getBody();

		if(!empty($body)){
			$server = $this->getServerParams();
			$contentType = ($server['CONTENT_TYPE']) ? $server['CONTENT_TYPE'] : $server['HTTP_CONTENT_TYPE'];

			if($contentType === 'application/json'){
				$this->parsedBody = json_decode($body,true);
			} else if($contentType === 'form-data') {
				$this->parsedBody = $this->formDataParser($body);
			} else {
				$this->parsedBody = $this->getPostData($body);
			}

			return $this->parsedBody;
		} else {
			/**
			* It's "multipart/form_data"
			*/
			if(isset($_POST) && !empty($_POST)){
				array_walk_recursive($_POST, function (&$value) {
					 $value = filter_var(trim($value),FILTER_SANITIZE_STRING);
				});
				
				/*
				* Files
				*/
				$this->getUploadedFiles();
				
				$this->parsedBody = $_POST;
			}
			
			return $this->parsedBody;
		}
	}
	
	protected function createFileObjects($files){
		foreach($files as $name => $file){
			$this->uploadedfiles[$name] = new UploadedFile($file['name'],$file['tmp_name'],$file['type'],$file['size'],$file['error'],true);
		}
	}
	
	public function withParsedBody($data){
		$this->parsedBody = $data;
	}
	
	public function getAttributes(){
		return $this->attributes;
	}
	
	public function getAttribute($name, $default = null){
		if(isset($this->attributes[$name])){
			return $this->attributes[$name];
		}
		
		return $default;
	}
	
	public function withAttribute($name, $value){
		$this->attributes[$name] = $value;
		return $this;
	}
	
	public function withoutAttribute($name){
		if(isset($this->attributes[$name])){
			unset($this->attributes[$name]);
		}
		return $this;
	}
	
}