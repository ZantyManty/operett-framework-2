<?php

namespace Csifo\Http\Message;

use Csifo\Psr\Http\Message\IUri as IUri;
use Csifo\Psr\Http\Message\IRequest;
use Csifo\Http\User\Interfaces\IUser as IUser;

class Request extends Message implements IRequest {
	
	protected $uri = null;
	
	protected $method;
	
	protected $user;
	
	protected $requestTarget = '';
	
	public function __construct(IUri $uri,IUser $user){
		$this->uri		= $uri;
		$this->method	= $_SERVER['REQUEST_METHOD'];
		$this->user 	= $user;
	}
	
	public function user(){
		return $this->user;
	}
	
	public function getMethod(){
		return $this->method;
	}
	
	public function withMethod($method)
    {
        $new->method = strtoupper($method);
        return $this;
    }
	
	public function getRequestTarget(){
		
		if($this->requestTarget != ''){
			return $this->requestTarget;
		}
		
		$target = $this->uri->getPath();
        if ($target == '') {
            $target = '/';
        }
		
        if ($this->uri->getQuery() != '') {
            $target .= '?' . $this->uri->getQuery();
        }
		
		if($this->uri->getFragment() != ''){
			$target .= '#' . $this->uri->getFragment();
		}
		
		return $target;
	}
	
	public function withRequestTarget($requestTarget){
		$this->requestTarget = $requestTarget;
		return $this;
	}
	
	public function getUri(){
		return $this->uri;
	}
	
	public function withUri(IUri $uri,$preserveHost = false){
		$this->uri = $uri;
		return $this;
	}
}