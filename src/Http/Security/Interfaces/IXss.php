<?php

namespace Csifo\Http\Security\Interfaces;

interface IXss {
	
	public function clean($string);
	
}