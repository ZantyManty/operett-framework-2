<?php

namespace Csifo\Http\Security;

use Csifo\Core\Interfaces\IConfig;

class Xss implements Interfaces\IXss {
	
	protected $allowedTags = '';
	
	protected $specialChars = [
		'\s',
		'\xE2\x80\x8B' //Zero width space
	];
	
	protected $htmlEvents = [
			"onafterprint",
			"onbeforeprint",
			"onbeforeunload",
			"onerror",
			"onhashchange",
			"onload",
			"onmessage",
			"onoffline",
			"ononline",
			"onpagehide",
			"onpageshow",
			"onpopstate",
			"onresize",
			"onstorage",
			"onunload",
			"onblur",
			"onchange",
			"oncontextmenu",
			"onfocus",
			"oninput",
			"oninvalid",
			"onreset",
			"onsearch",
			"onselect",
			"onsubmit",
			"onkeydown",
			"onkeypress",
			"onkeyup",
			"onclick",
			"ondblclick",
			"onmousedown",
			"onmousemove",
			"onmouseout",
			"onmouseover",
			"onmouseup",
			"onmousewheel",
			"onwheel",
			"ondrag",
			"ondragend",
			"ondragenter",
			"ondragleave",
			"ondragover",
			"ondragstart",
			"ondrop",
			"onscroll",
			"oncopy",
			"oncut",
			"onpaste",
			"onabort",
			"oncanplay",
			"oncanplaythrough",
			"oncuechange",
			"ondurationchange",
			"onemptied",
			"onended",
			"onerror",
			"onloadeddata",
			"onloadedmetadata",
			"onloadstart",
			"onpause",
			"onplay",
			"onplaying",
			"onprogress",
			"onratechange",
			"onseeked",
			"onseeking",
			"onstalled",
			"onsuspend",
			"ontimeupdate",
			"onvolumechange",
			"onwaiting",
			"onshow",
			"ontoggle",
		];
		
		protected $stringCache = [];
		
	public function __construct(IConfig $config){
		$this->allowedTags = $config->app('AllowedTags');
	}
	
	public function clean($string){
		
		/*
		* Replace script tags
		*/
		$pattern = str_replace('\s*','[' . implode('',$this->specialChars) . ']*','<{1,}\/{0,}\s*(?:j\s*a\s*v\s*a\s*)?s\s*c\s*r\s*i\s*p\s*t\s*>{1,}');
		$string = preg_replace('#' . $pattern . '#i','[removed]',$string);
		
		/*	
		* Check HTML event strings
		*/
		foreach($this->htmlEvents as $event){
			if(!isset($this->stringCache[$event])){
				$htmlEventString = implode('[' . implode('',$this->specialChars) . ']*',str_split($event));
				$htmlEventString = rtrim($htmlEventString,'|');
				$this->stringCache[$event] = $htmlEventString;
			} else {
				$htmlEventString = $this->stringCache[$event];
			}
			
			$string = preg_replace('#(<{1,}[\sa-z\xE2\x80\x8B]+?' . $htmlEventString . '=)[\'\"][^\'\"]+?[\'\"]#i','$1[removed]$2',$string);
		}
		
		return strip_tags($string,$this->allowedTags);
	}
}