<?php

namespace Csifo\Http;

use Csifo\Http\Interfaces\ICookie as ICookie;

class Cookie implements ICookie {
	
	public function get($key){
		return (isset($_COOKIE[$key]) ? $_COOKIE[$key] : null);
	}
	
	public function set($key,$val,$time){
		$time = strtoupper($type);
		$finalTime = 0;
		
		if(preg_match('#(\D{1})$#i',$time,$amount)){
			switch($amount[1]){
				case 'S': //Second
					$finalTime = $time;
					break;
				case 'I': //Minute
					$finalTime = 60 * $time;
					break;
				case 'H': //Hour
					$finalTime = 3600 * $time;
					break;
				case 'D': //Day
					$finalTime = 3600 * 24 * $time;
					break;
				case 'W': //Week
					$finalTime = 3600 * 24 * 7 * $time;
					break;
				case 'M': //Month
					$finalTime = 3600 * 24 * 30 * $time;
					break;
				case 'Y': //Year
					$finalTime = 3600 * 24 * 7 * 52 * $time;
					break;
			}
			setcookie($name, $value, time() + $finalTime, getenv('HTTP_HOST'));
		} else {
			throw new \Exception('Cookie time is not in a valid format! The right format e.g. is "2h","6w",etc... ');
		}
	}
	
	public function destroy($key){
		setcookie($key, null, time() - 3600, getenv('HTTP_HOST'));
	}
	
}