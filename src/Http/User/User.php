<?php

namespace Csifo\Http\User;

use Csifo\Http\User\Interfaces\IUser as IUser;
use Csifo\Http\User\Interfaces\IUserAgent as IUserAgent;
use Csifo\Http\Session\Interfaces\ISession as ISession;
use Csifo\Http\Interfaces\ICookie as ICookie;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Http\GeoIp\Interfaces\IGeoIp;

class User implements IUser {
	
	protected $session;
	protected $cookie;
	protected $userAgent;
	protected $geoIp;
	protected $ip;
	
	public function __construct(IUserAgent $agent,ISession $session,IConfig $config,ICookie $cookie,IGeoIp $geoIp){
		$this->userAgent = $agent;
		$this->session = $session;
		$this->cookie = $cookie;
		$this->geoIp = $geoIp;
	}
	
	public function ip(){
		if($this->ip == null){
			$client  = (isset($_SERVER['HTTP_CLIENT_IP'])) 			? $_SERVER['HTTP_CLIENT_IP'] 		: getenv('HTTP_CLIENT_IP');
			$forward = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) 	? $_SERVER['HTTP_X_FORWARDED_FOR']  : getenv('HTTP_X_FORWARDED_FOR');;
			$remote  = $_SERVER['REMOTE_ADDR'];

			if(filter_var($client, FILTER_VALIDATE_IP)){
				$ip = $client;
			}elseif(filter_var($forward, FILTER_VALIDATE_IP)){
				$ip = $forward;
			}else{
				$ip = $remote;
			}
			
			$this->ip = $ip;
		}

		return $this->ip;
	}
	
	public function rawAgent(){
		return $this->userAgent->getAgent();
	}
	
	public function info(){
		$data = $this->userAgent->getFullInfo();
		return array_merge($this->ipInfo(),$data);
	}
	
	public function ipInfo(){
		/*
		* Set the IP address
		*/
		$data['ip']	  = $this->ip();
		/*
		* Set the time,when we are get the info
		*/
		$data['time'] = date('Y-m-d H:i:s');
		/*
		* Get the geolocation,merge it 
		* with the time,and ip,and return it
		*/
		return array_merge($this->geoIp->getData($this->ip),$data);
	}
	
	public function isAuthed(){
		return $this->session->has('User');
	}
	
	public function auth($data){
		$this->session->set('User',$data);
	}
	
	public function set($key,$val){
		$data = $this->session->get('User');
		if($data){
			$data[$key] = $val;
			$this->session->set('User',$data);
		} else {
			$this->session->set('User',[$key => $val]);
		}
	}
	
	public function has($key){
		if($this->session->has('User')){
			$data = $this->session->get('User');
			return (isset($data[$key]) ? $data[$key] : false);
		} else {
			return false;
		}
	}
	
	public function __call($method,$args){
		if(preg_match('#get([\w]+)#i',$method,$property)){
			$userData = $this->session->get('User');
			$key = strtolower(trim($property[1]));
			return $userData[$key];
		}
	}
	
}