<?php

namespace Csifo\Http\User\Interfaces;

interface IUserAgent {
	
	public function getOs();
	public function getBrowser();
	
}