<?php

namespace Csifo\Http\User;

use Csifo\Http\User\Interfaces\IUserAgent as IUserAgent;

class UserAgent implements IUserAgent {
	
	protected $userAgent;
	
	protected $windowsVersions = [
		'10.0' =>	'10',
		'6.3'  =>	'8.1',
		'6.2'  =>	'8',
		'6.1'  =>	'7',
		'6.0'  =>	'Vista',
		'5.2'  =>	'Server 2003',
		'5.1'  =>	'XP',
		'5.0'  =>	'2000',
		'98'   =>	'98',
		'95'   =>	'95',
		'3.1'  =>	'3.1'
	];
	
	protected $browsers = [
		'K-Meleon',
		'icecat',
		'Firefox',
		'Chrome',
		'Maxthon',
	];
	
	protected $linuxVersions = [
		'Ubuntu',
		'Mint',
		'NetBSD',
		'OpenBSD',
		'Debian',
		'Fedora',
		'Red Hat',
		'FreeBSD',
	];
	
	/*
	* Phone/tablet regex patterns
	*/
	protected $phones = [
		'Alcatel' => [
			'ALCATEL ([\w ]+?) Build',
		],
		'Asus' => [
			'ASUS_([\w]+?) ',
		],
		'Sony Xperia' => [
			'((?:C|D|SGP)[\d]+)',
		],
		'Sony Ericsson' => [
			'sonyericsson([\w]+?)(?: |\/)',
		],
		'Sony Tablet' => [
			'Sony Tablet ([\w]+)',
		],
		'Huawei' => [
			'HUAWEI[_ ]*?|HONOR([\w]+(?:-[\w]+)?)',
			'HUAWEI ([\w]+?(?: *[\w]+(?: *[\w]+?))) Build',
			'(MediaPad [\w]+(?: *[\w.]+?)) Build\/Huawei',
		],
		'HTC' => [
			'HTC[_ ]*([\w ]+?) Build',
			'HTC[_ ]*([\w]+(?: [\w]+))',
			'HTC[ _]([\w_]+)',
			'(Desire *[\w]+)',
		],
		'Lenovo' => [
			'Z([\d]+)',
			'Lenovo *([\w]+-[\w]+?) Build',
			'Lenovo[ _]*([\w]+[_-]*[\w]+?)(?: Build|\/| )',
			'(YOGA Tablet [\w]+?-[\w]+?) Build',
		],
		'HP' => [
			'Laptop *(HP[\w]+)',
			'(hp-tablet)',
		],
		'LG' => [
			'LG-([\w]+)',
		],
		'Nokia' => [
			'Nokia[\w]+',
		],
		'Nokia Lumia' => [
			'Lumia *([\d]+)',
		],
		'Samsung' => [
			'(?:SAMSUNG[- ])?((?:SM|SCH|GT|SGH|SPH|SEC)-(?:[\w]+))',
			'SAMSUNG[ _]([\w]+)',
		],
		'ZTE' => [
			'ZTE[ -]*([\w_]+?)\/',
		],
		'PSP' => [
			'PSP[\S\s]*?([\d]+(?:\.[\d]+))',
		],
		'Playstation' => [
			'PLAYSTATION ([\d]+)',
		],
		'Sharp' => [
			'SHARP-([\w]+-[\w]+)',
		],
		'Windows Phone' => [
			'ZuneWP(\d+)',
		],
		'Xbox' => [
			'Xbox (One)',
		],
		'Xiaomi' => [
			'(Redmi (?:(?:Note) *[\d]+|[\d]+(?:X)))',
			'MI[ _]([\w ]+?) *Build',
			'(HM[ _]*\d+?\w+)',
		],
	];
	
	protected $cache = [];
	
	public function __construct(){
		$this->userAgent = $_SERVER['HTTP_USER_AGENT'];
	}
	
	public function getAgent(){
		return $this->userAgent;
	}
	
	public function getFullInfo(){
		if(empty($this->cache)){
			$this->getOS();
			$this->getBrowser();
			$this->getDevice();
		}
		
		return $this->cache;
	}
	
	public function getOS(){
		if(preg_match('#(windows|linux|Macintosh|Android)[\S\s]*?([\d]+[._]+(?:[\d]+(?:[._]+[\d]+)?)?)#i',$this->userAgent,$match)){
			if(stripos($match[1],'windows') !== false){
				$this->cache['OS'] = 'Windows';
				if(!empty($match[2])){
					if(isset($this->windowsVersions[$match[2]])){
						$this->cache['OsVersion'] = $this->windowsVersions[$match[2]];
					}
				} 
			} else if(stripos($match[1],'macintosh') !== false){
				$this->cache['OS'] = 'Mac';
				$this->cache['OsVersion'] = (!empty($match[2])) ? str_replace('_','.',$match[2]) : '';
			} else if(stripos($match[1],'linux') !== false){
				$this->cache['OS'] = 'Linux';
				if(preg_match('#(' . implode('|',$this->linuxVersions) . ')#i',$this->userAgent,$ver)){
					$this->cache['OsVersion'] = trim($ver[1]);
				}
			} else if(stripos($match[1],'android') !== false){
				$this->cache['OS'] = 'Android';
				$this->cache['OsVersion'] = (!empty($match[2])) ? $match[2] : '';
			}
			
		} else {
			$this->cache['OS'] = 'Unknown';
			$this->cache['OsVersion'] = 'Unknown';
		}
	}
	
	public function getDevice(){
		
		
		
		/*if(preg_match('#(iPhone|iPad|Android)[\s\S]*?([\d]+[._]+(?:[\d]+(?:[._]+[\d]+)?)?)#i',$this->userAgent,$deviceInfo)){
			$this->cache['Device'] = trim($deviceInfo[1]);
			if(!empty($deviceInfo[2]))
				$this->cache['DeviceVersion'] = trim($deviceInfo[2]);
		} else if(stripos($this->userAgent,'Macintosh') !== false){
			$this->cache['Device'] = 'Mac';
		}*/
	}
	
	public function getBrowser(){
		if(preg_match('#(' . implode('|',$this->browsers) . ')\/([\d]+(?:\.[\d]+(?:\.[\d]+)?)?)#i',$this->userAgent,$browser)){
			if($browser[1] == 'Firefox'){
				$this->cache['Browser'] = (stripos($browser[1],'Iceweasel') !== false) ? 'Iceweasel' : 'Mozilla ' . trim($browser[1]);
			} else if($browser[1] == 'Chrome'){
				$this->cache['Browser'] = 'Google Chrome';
			}
			
			$this->cache['BrowserVersion'] = trim($browser[2]);
		} else if(preg_match('#MSIE[\s]*?([\d]+(?:\.[\d]+))#i',$this->userAgent,$browser)){
			$this->cache['Browser'] = 'Internet Explorer';
			$this->cache['BrowserVersion'] = trim($browser[1]);
		} else if(preg_match('#Safari\/([\d]+(?:\.[\d]+(?:\.[\d]+)?)?)#i',$this->userAgent,$browser)){
			$this->cache['Browser'] = 'Safari';
			$this->cache['BrowserVersion'] = trim($browser[1]);
		}
	}
	
}