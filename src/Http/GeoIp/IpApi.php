<?php

namespace Csifo\Http\GeoIp;

use Csifo\Http\GeoIp\Interfaces\IGeoIp;
use Csifo\Core\Interfaces\FailSafe;

class IpApi implements IGeoIp,FailSafe {
	
	protected $url = 'http://www.ip-api.com';
	
	protected $aliases = [
			'status' 	  => 'status',
			'city'		  => 'city',
			'country' 	  => 'country',
			'countryCode' => 'countryCode',
			'zip'		  => 'zip',
			'lat'		  => 'lat',
			'lon'		  => 'lon',
			'org'		  => 'isp',
			'timezone'	  => 'timezone',
	];
	
	public function getData($ip){
		$json = file_get_contents($this->url . '/json/' . $ip);
		$status = $http_response_header[0];
		if(stripos($status,'200 OK') !== false){
			/*
			* If the request is successful
			*/
			$decoded = json_decode($json,true);
			
			if(isset($decoded['status']) && $decoded['status'] == 'fail'){
				return [];
			} else {
				unset($this->aliases['status']);
				
				$returnArray = [];
				
				foreach($this->aliases as $field => $alias){
					$returnArray[$alias] = isset($this->aliases[$field]) ? $decoded[$field] : '';
				}
				return $returnArray;
			}
		} else {
			return [];
		}
	}
	
	/*
	* FailSafe
	*/
	public function checkObjectStatus() : bool {
		$headers = @get_headers($this->url);
		return (stripos($headers[0], '200 OK') !== false) ? true : false;
	}
	
}