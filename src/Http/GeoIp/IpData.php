<?php

namespace Csifo\Http\GeoIp;

use Csifo\Http\GeoIp\Interfaces\IGeoIp;
use Csifo\Core\Interfaces\OTronic;

class IpData implements IGeoIp,OTronic {
	
	protected $url = 'https://api.ipdata.co/';
	
	protected $aliases = [
		'city'			=> 'city',
		'country_name'	=> 'country',
		'country_code'	=> 'countryCode',
		'postal'		=> 'zip',
		'latitude'		=> 'lat',
		'longitude'		=> 'lon',
		'organisation'	=> 'isp',
		'timezone'		=> 'timezone',
	];
	
	public function getData($ip){
		$json = file_get_contents($this->url . $ip);
		$status = $http_response_header[0];
		if(stripos($status,'200 OK') !== false){
			/*
			* If the request is successful
			*/
			$decoded = json_decode($json,true);
			
			if(isset($decoded['status']) && $decoded['status'] == 'fail'){
				return [];
			} else {
				$returnArray = [];
				
				foreach($this->aliases as $field => $alias){
					$returnArray[$alias] = isset($this->aliases[$field]) ? $decoded[$field] : '';
				}
				return $returnArray;
			}
		} else {
			return [];
		}
	}
	
	/*
	* O-Tronic
	*/
	public function check(){
		$headers = @get_headers($this->url);
		return (stripos($headers[0], '200 OK') !== false) ? true : false;
	}
	
}