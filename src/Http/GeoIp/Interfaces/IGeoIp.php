<?php

namespace Csifo\Http\GeoIp\Interfaces;

interface IGeoIp {
	
	public function getData($ip);
	
}