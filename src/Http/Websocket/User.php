<?php

namespace Csifo\Http\Websocket;

use Csifo\Http\Websocket\Interfaces\IUser as IUser;
use Csifo\Http\Websocket\Interfaces\ISession as ISession;

class User implements IUser {
	
	protected $handshake = false;
	protected $ip = '';
	protected $socketId = 0;
	/*
	* Session object
	*/
	protected $session;
	
	public function __construct(ISession $session){
		$this->session = $session;
	}
	
	public function setHandshake($bool){
		$this->handshake = $bool;
	}
	
	public function hasHandshake(){
		return $this->handshake;
	}
	
	public function setSocketId($id){
		$this->socketId = $id;
	}
	
	public function getSocketId(){
		return $this->socketId;
	}
	
	public function setIp($ip){
		$this->ip = $ip;
	}
	
	public function getIp(){
		return $this->ip;
	}
	
	public function getSession(){
		return $this->session->get('User');
	}
	
	public function setSession($data){
		$this->session->set('User',$data);
	}
	
}