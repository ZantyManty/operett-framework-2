<?php

namespace Csifo\Http\Websocket\Interfaces;

interface IUser {
	
	public function setHandshake($bool);
	public function hasHandshake();
}