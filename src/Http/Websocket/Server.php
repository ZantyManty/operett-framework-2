<?php

namespace Csifo\Http\Websocket;

use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Factory\Interfaces\IController as IController;

class Server {

	protected $host;
	protected $port;
	
	/*
	* Container
	*/
	protected $resolver;
	
	/*
	* Controller factory object
	*/
	protected $factory;
	
	/*
	* The clients ($clients[0] is the master/server socket)
	*/
	protected static $clients = [];
	
	/*
	* User objects
	*/
	protected static $users = [];
	
	/*
	* Controller objects' memory cache
	*/
	protected static $moduleObjects = [];

	/*
	* Our secret key
	*/
	protected $secret = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
	
	/*
	* NULL (Zend Engine nem tud egy az egyben NULL-t átvenni,ezért referencia kell)
	*/
	protected $null = NULL;
	
	protected $actionHandler;
	
	public function load(IConfig $config,IController $factory){
		$this->host = $config->get('websocket')['host'];
		$this->port = $config->get('websocket')['port'];
		$this->factory = $factory;
		$this->resolver = resolver();
		return $this;
	}

	public function connect(){
		set_time_limit(0);
		/*
		*	AF_INET 	-> IPv4
		*	SOCK_STREAM -> TCP/IP-s socket type
		*/
		$socket = socket_create(AF_INET,SOCK_STREAM, getprotobyname('tcp'));
		//socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
		socket_bind($socket,$this->host,$this->port);
		socket_listen($socket);
		self::$clients[] = $socket;
		
		echo 'Socket listening on [' . $this->host . ':' . $this->port . ']' . PHP_EOL;
		return $this;
	}
	
	protected function getIP($socket){
		socket_getpeername($socket ,$ip);
		return $ip;
	}
	
	protected function resolveUser($socketId){
		$user = resolve('Csifo\Http\Websocket\Interfaces\IUser');
		$user->setSocketId($socketId);
		self::$users[$socketId] = $user;
	}
	
	public function run(){
		while(true){
			$clientReference = self::$clients;
			socket_select($clientReference,$this->null,$this->null,0,10);
			
			foreach($clientReference as $socket) {
				
				/*
				* If a client wants to join (So,if we have a new connection)
				*/
				if ($socket == self::$clients[0]){
					
					/*
					* If we can accept it
					*/
					if(($socket = socket_accept(self::$clients[0]))){
						/*
						* Set to "nonblock": Return immediately!
						*/
						socket_set_nonblock($socket);
						
						/*
						* Socket id (Resource id)
						*/
						$socketId = (int)$socket;

						/*
						* Get sockect's IP
						*/
						$ip = $this->getIP($socket);

						/*
						* Create the user object
						*/
						$this->resolveUser($socketId);

						/*
						* Set IP
						*/
						self::$users[$socketId]->setIp($ip);

						self::$clients[$socketId] = $socket;
						echo 'Socket [ ' . $socketId . ' ] accepted!' . PHP_EOL;
						
					} else {
						/*
						* We couldn't accept the connection
						*/
						echo 'couldn\'t accept the connection!' . PHP_EOL;
					}
				} else {
					/*
					* Socket id (Resource id)
					*/
					$socketId = (int)$socket;
					
					/*
					* Incoming data
					*/
					$data = socket_read($socket,1024);
					$ip = $this->getIP($socket);
					
					/*
					* If the socket is dead
					*/
					if ($data === false || $data === '') {
						
						unset(self::$clients[$socketId]);
						unset(self::$users[$socketId]);
						echo 'Disconecting socket [ ' . $socketId . ' ] !' . PHP_EOL;
					} else {
						if(self::$users[$socketId]->hasHandshake()){
							/*
							* Process incoming data
							*/
							$data = json_decode($this->unmask($data),true);
							$this->processData($socketId,$data);
						} else {
							
							/*
							* Handshake
							*/
							$this->handshake($data,$socket,$socketId);
						}
					}
				}
			}
		}
	}
	
	protected function processData($socketId,$data){
		if(isset($data['action'])){
			$action = $data['action'];
			unset($data['action']);
			$this->{$action}(new Request(self::$users[$socketId],$data));
		}
	}
	
	protected function handshake($header,$socket,$id){
		//Get websocket key
		preg_match('#Sec-WebSocket-Key:?[ ]*([a-zA-Z0-9=\$/\+]+)#i',$header,$key);
		/*
		* Convert the hashed(With sha1) key to Hexadecimal string,the encode to base64
		*/
		$secAccept = trim(base64_encode(pack('H*', sha1($key[1] . $this->secret))));
		//hand shaking header
		$host = $this->host;
		$port = $this->port;
		$upgrade  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
		"Upgrade: websocket\r\n" .
		"Connection: Upgrade\r\n" .
		"Sec-WebSocket-Origin: http://$host:$port\r\n" .
		"Sec-WebSocket-Location: ws://$host:$port/\r\n" .
		"Sec-WebSocket-Accept:$secAccept\r\n\r\n";
		$this->socketWrite($socket,$upgrade);
		self::$users[$id]->setHandshake(true);
		echo 'Handshake successful!' . PHP_EOL;
	}
	
	
	/*
	* Source: https://github.com/sanwebe/Chat-Using-WebSocket-and-PHP-Socket/blob/master/server.php#L90
	*/
	protected function unmask($text) {
		/*
		* https://image.slidesharecdn.com/websocketworkshopkaazingoverview07052012v4-120724150428-phpapp02/95/html5-websocket-introduction-27-728.jpg?cb=1343142480
		*
		* 
		* $text[1] -> 2nd byte: It contains the lenght of the data
		*/
		$length = ord($text[1]) & 127;
		
		/*
		* $mask -> The mask is always 4 byte long
		*
		*		//Lengths:
		*		-> If the length is 126,then there is extra length (2 extra byte,if length is 127,then extra is 4bytes) after the "normal" length,so
		*			the $mask is starts at 4th byte (At length 127,it starts 10th byte).
		*
		* $data -> The data is always come right after the masks!
		*		-> At length 126,it starts at the 8th byte. At length 127,it starts at the 14th byte.
		*/
		if($length == 126) {
			$masks = substr($text, 4, 4);
			$data = substr($text, 8);
		}
		elseif($length == 127) {
			$masks = substr($text, 10, 4);
			$data = substr($text, 14);
		}
		else {
			$masks = substr($text, 2, 4);
			$data = substr($text, 6);
		}
		/*
		* Decode the data
		*/
		$text = "";
		for ($i = 0; $i < strlen($data); ++$i) {
			$text .= $data[$i] ^ $masks[$i%4];
		}
		return $text;
	}
	
	/*
	* Source: https://github.com/sanwebe/Chat-Using-WebSocket-and-PHP-Socket/blob/master/server.php#L112
	*/
	protected function mask($text){
		$b1 = 0x80 | (0x1 & 0x0f);
		$length = strlen($text);
		
		if($length <= 125) //9-15 bits
			$header = pack('CC', $b1, $length);
		
		elseif($length > 125 && $length < 65536) //16 bits
			$header = pack('CCn', $b1, 126, $length);
			
		elseif($length >= 65536) //64 bits
			$header = pack('CCNN', $b1, 127, $length);
			
		return $header.$text;
	}
	
	protected function socketWrite($socket,$data){
		socket_write($socket,$data,strlen($data));
	}
	
}