<?php

namespace Csifo\Http\Websocket;

use Csifo\Http\Websocket\Interfaces\IServer as IServer;
use Csifo\Factory\Controller as ControllerFactory;

class BasicServer extends Server implements IServer {
	
	protected $alternateIds = [];
	
	protected function addAlternateId($original,$new){
		$this->alternateIds[$new] = $original;
	}
	
	protected function delete($key){
		unset(self::$users[$key]);
		unset(self::$clients[$key]);
	}
	
	protected function send($data,$keys = []){
		$data = json_encode($data);
		if(!empty($keys)){
			foreach($keys as $key){
				if(isset($this->alternateIds[$key]) && isset(self::$clients[$this->alternateIds[$key]]))
					$this->socketWrite(self::$clients[$this->alternateIds[$key]],$this->mask($data));
			}
		} else {
			foreach(self::$clients as $key => $client){
				if($key > 0){
					$this->socketWrite($client,$this->mask($data));
				}
			}
		}
	}
	
	/*
	* 	$data = [
			'module' => 'someController',
			'method' => 'someAction',
			'payload' => 'mixed -> Other parameters',
		];
	*/
	protected function resolve($data){
		if(isset($data['module']) && !empty($data['module'])){
			/*
			* If the Controller object isn't resolved yet
			*/
			if(!isset(self::$moduleObjects[$data['module']]['BasicObj'])){
				$controllerData = $this->factory->make($data['module'],$data['method']);
				self::$moduleObjects[$data['module']]['BasicObj'] = $controllerData[0];
				self::$moduleObjects[$data['module']]['ClassName'] = $controllerData[1];
			} else {
				echo 'Object already cached!' . PHP_EOL;
			}
			
			$optimalParamteres = [
				'App\\Controllers\\' . $data['module'] => [
					$data['method'] => $data['payload']
				],
			];
			$this->resolver->addOptParams($optimalParamteres);
			$parameters = $this->resolver->resolveOnlyParams(self::$moduleObjects[$data['module']]['ClassName'],$data['method']);
			return call_user_func_array(array(self::$moduleObjects[$data['module']]['BasicObj'],$data['method']),$parameters);
		}
	}
}