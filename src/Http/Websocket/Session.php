<?php

namespace Csifo\Http\Websocket;

use Csifo\Http\Websocket\Interfaces\ISession as ISession;
use Csifo\Cache\Interfaces\ICacheItemPool as ICacheItemPool;

class Session implements ISession {
	
	protected $data = [];
	protected $cache;

	public function __construct(ICacheItemPool $cache){
		$this->cache = $cache;
	}
	
	public function load($key){
		$this->data  = $this->cache->getItem('sess_' . $key)->get();
		$this->cache->deleteItem('sess_' . $key);
	}
	
	public function get($key){
		if($this->has($key)){
			return $this->data[$key];
		}
		
		return false;
	}
	
	public function set($key,$value){
		$this->data[$key] = $value;
	}
	
	public function has($key){
		return isset($this->data[$key]);
	}
	
}