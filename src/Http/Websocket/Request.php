<?php

namespace Csifo\Http\Websocket;

use Csifo\Http\Websocket\Interfaces\IUser as IUser;

class Request {
	
	protected $user;
	protected $data;
	
	public function __construct(IUser $user,$incomingData){
		$this->user = $user;
		$this->data = $incomingData; /*filter_var_array($incomingData,FILTER_SANITIZE_ENCODED);*/
	}
	
	public function getBody(){
		return $this->data;
	}
	
	public function getUser(){
		return $this->user;
	}
	
}