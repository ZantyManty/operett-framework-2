<?php

namespace App;

use Csifo\Client\Minify\Interfaces\IJavascript as IJavascript;
use Csifo\Client\Minify\Interfaces\ICss			as ICss;

/*
* Development hooks.
* Things,we need when we are development (e.g.: Minify CSS and JS)
*/
class DevHooks {
	
	public function __construct(ICss $css,IJavascript $js){
		$js->minifyModules();
		$css->minifyModules();
	}
}