<?php

namespace App;

use Csifo\Http\Websocket\BasicServer as BasicServer;
use Csifo\Http\Websocket\Request as Request;
//use Csifo\Core\Interfaces\IConfig as IConfig;

class MyWsServer extends BasicServer {
	
	/*
	* Just for the sample...
	*/
	protected $tempCounter = 1;
	
	/*
	* Sample websocket action
	*/
	public function someAction(Request $request){
		/*
		* Parameters from the Websocket request
		*/
		$incomingData = $request->getBody();
		
		/*
		* Resolve the controller
		*/
		$data = [
			'module' => 'Lol',
			'method' => 'wsTroll',
			'payload' => [
				'param1' => $incomingData['params']['token']
			],
		];
		$response = $this->resolve($data);
		
		/*
		* Later,we change the "$this->tempCounter" to the user_id from the session
		*/
		$this->addAlternateId($request->getUser()->getSocketId(),$this->tempCounter);
		$this->tempCounter++;
		
		/*
		* Send to everyone
		*/
		$this->send($response);
	}
	
	public function applyConnect(Request $request){
		$config = resolve('Csifo\Core\Interfaces\IConfig');
		$incomingData = $request->getBody();
		$sessionToken = $incomingData['token'];
		
		$path = $config->app('TRUNK_DIR') . '/' . $sessionToken;
		
		$sessionData = unserialize(file_get_contents($path));
		unlink($path);
		
		$request->getUser()->setSession($sessionData);
		
		$this->addAlternateId($request->getUser()->getSocketId(),$sessionData['id']);
	}
	
	public function SetUserStatus(Request $request){
		$data = $request->getBody();
		$status = $data['status'];
		$user_id = $request->getUser()->getSession()['id'];
		
		/*
		* Resolve the controller
		*/
		$data = [
			'module' => 'Lol',
			'method' => 'wsSetStatus',
			'payload' => [
				'status' => $data['status'],
				'user_id' => $user_id
			],
		];
		
		$this->resolve($data);
		
		$resp = [
			'event' => 'UsrStatusChange',
			'params' => [
				'user_id'	=> $user_id,
				'status'	=> $status,
			],
		];
		
		$this->send($resp);
	}
	
	protected function getOnlineGroupMembers($group_id){

		$data = [
			'module' => 'Lol',
			'method' => 'getOnlineUserIds',
			'payload' => [
				'groupId' => $group_id,
			],
		];
		
		$models = $this->resolve($data);
		
		$ids = [];
			
		foreach($models as $model){
			$ids[] = $model->id;
		}
			
		return $ids;
	}
	
	public function sendUserLeftGroupMsg(Request $request){
		$data = $request->getBody();
		$group_id = $data['group_id'];
		$type = $data['type'];
		$user_id = $request->getUser()->getSession()['id'];
		
		$users = $this->getOnlineGroupMembers($group_id);
		
		$data = [
			'module' => 'Lol',
			'method' => 'makeUserLeftGroupData',
			'payload' => [
				'userId'	=>$user_id,
				'groupId'	=> $group_id,
				'type'		=> $type
			],
		];
		
		$payload = $this->resolve($data);

		$data['method'] = 'setGroupFlag';
		$payload = $this->resolve($data);
		
		$response = [
				'event' => 'systemMsgRecev',
				'params' => $payload
			];
		
		$this->send($response,$users);
	}
	
	public function addUserGroup(Request $request){
		$data = $request->getBody();
		$user_id = $request->getUser()->getSession()['id'];
		$group_id = $data['group_id'];

		//addUsersToGroup($users,$userId,$groupId)
		$data = [
			'module' => 'Lol',
			'method' => 'addUsersToGroup',
			'payload' => [
				'users' => $data['users'],
				'userId' => $user_id,
				'groupId' => $group_id,
			],
		];
		
		$result = $this->resolve($data);
		
		if($result){
			$response = [
				'event' => 'MsgSysRecevMulti',
				'params' => [
					'messages'	=> $result['systemMsg']
				],
			];
			$this->send($response,$result['users']);
		}
	}
	
	public function insertMsg(Request $request){
		$data = $request->getBody();
		
		$group_id = $data['group_id'];
		$message = $data['message'];
		$user_id = $request->getUser()->getSession()['id'];
		
		//wsInsertMsg($message,$userId,$groupId)
		
		/*
		* Resolve the controller
		*/
		$data = [
			'module' => 'Lol',
			'method' => 'wsInsertMsg',
			'payload' => [
				'message' => $message,
				'userId' => $user_id,
				'groupId' => $group_id,
			],
		];
		
		$result = $this->resolve($data);
		/*
		* Ha az illető benne van a csoportban,akkor mehet az üzenet
		*/
		if($result){
			$response = [
				'event' => 'MsgRecev',
				'params' => [
					'name'	=> $result['name'],
					'message'	=> $message,
					'date'	=> $result['date'],
				],
			];
			
			$ids = $this->getOnlineGroupMembers($group_id);
			
			$this->send($response,$ids);
		}
	}
}