<html>
	<head>
		<title>Operett Framework</title>
		<!-- 3rd party stuff -->
		<link href="https://fonts.googleapis.com/css?family=Monoton" rel="stylesheet" />
		<script type="text/javascript" src="/public/assets/jquery30.js" ></script>
		<script type="text/javascript" src="/public/assets/jquery-ui.min.js" ></script>
		<script type="text/javascript" src="/public/assets/jquery.cookie.js" ></script>
		<script type="text/javascript" src="/public/assets/eye.js" ></script>
		<script type="text/javascript" src="/public/assets/layout.js" ></script>
		<script type="text/javascript" src="/public/assets/colorpicker.js" ></script>
		<!-- Own stuff -->
		<link rel="stylesheet" href="/public/assets/operett.css" />
		<script type="text/javascript" src="/public/assets/operett.js" ></script>
	</head>
	<body>