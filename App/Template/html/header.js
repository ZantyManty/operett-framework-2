$(document).ready(function(){
		var host = "ws://127.0.0.1:9000";
		$.socket = new WebSocket(host);
		$.socket.onopen    = function(msg) {
							   console.log("Welcome - status "+this.readyState);
							   
							   var xxx = {
									token: sc_user_id,
									action: 'applyConnect'
								};
								$.socket.send(JSON.stringify(xxx));
						   };
		$.socket.onmessage = function(msg) { 
								var dat = $.parseJSON(msg.data);
								console.log(dat);
								window[dat.event](dat.params);
		};
		$.socket.onclose   = function(msg) { 
							   console.log("Disconnected - status "+this.readyState);
		};
		
		$('#header_set_status').on('change',function(e){
			var x = {
				status : $(this).val(),
				action: 'SetUserStatus'
			};
			socketSend(x);
		});
});

function socketSend(data){
	$.socket.send(JSON.stringify(data));
}

/* WEBSOCKET EVENTS */

function UsrStatusChange(params){
	$('#contact_list_user_' + params.user_id).removeClass().addClass('contact_mini_container contact_status_' + params.status);
	$('#contact_list_user_' + params.user_id).find('.contact_side_piece').removeClass().addClass('contact_mini_container contact_side_piece contact_side_piece_' + params.status + ' contact_status_' + params.status);
	console.log(params.status);
}