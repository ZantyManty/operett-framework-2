<?php

/*
* Just a demo migration class...
*/

namespace App\Migrations;

use Csifo\Database\Migration\Interfaces\ItableSchema as ItableSchema;

class Migratest {
	
	/*
		This is the comment!
		First Test table!
	*/
	public function NamedByMethod(ItableSchema $table){
		$table->/*name('testTable')->*/collation('utf8_general_ci');
		
		$table->addField()->Int('id')->primaryKey();
		$table->addField()->varchar('name',50)->withComment('ThisIsAVarChar,Man!');
		$table->addField()->text('post');
		$table->addField()->timestamp('updated');
	}	
}