<?php

namespace App\Middleware;

use Csifo\Psr\Http\Server\IMiddleware;
use Csifo\Psr\Http\Message\IServerRequest;
use Csifo\Psr\Http\Server\IRequestHandler;
use Csifo\Psr\Http\Message\IResponse;

class Auth implements IMiddleware {
	
	protected $response = null;
	
	public function(IResponse $response){
		$tihs->response = $response;
	}
	
	/*
	* Ha be van jelentkezve a felhasználó,beengedjük,ha nincs,akkor nem!
	*/
	public function process(IServerRequest $request,IRequestHandler $handler) : IResponse {
		
		$response = $handler->handle($request);
		
		if($request->user()->isAuthed() === false){
			return $this->response->withStatus(302)->withHeader('Location', '/login');
		}
		
		return $response;
	}
	
}