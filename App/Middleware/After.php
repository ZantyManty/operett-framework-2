<?php

namespace App\Middleware;

use Csifo\Psr\Http\Server\IMiddleware;
use Csifo\Psr\Http\Message\IServerRequest;
use Csifo\Psr\Http\Server\IRequestHandler;

class After implements IMiddleware {
	
	public function process(IServerRequest $request,IRequestHandler $handler){
		
		/*
		* Something
		*/
		$respone = $handler->handle($request);
		
		echo '------------After middle!------------';
		
		return $respone;
	}
	
}