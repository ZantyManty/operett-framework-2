<?php

namespace App\Middleware;

use Csifo\Psr\Http\Server\IMiddleware;
use Csifo\Psr\Http\Message\IServerRequest;
use Csifo\Psr\Http\Server\IRequestHandler;

class Before implements IMiddleware {
	
	public function process(IServerRequest $request,IRequestHandler $handler){
		
		/*
		* Something
		*/
		
		//return $respone->withStatus(403,'Forbidden');
		
		echo 'Before<br />';
		$respone = $handler->handle($request);
		echo 'Before later<br />';
		return $respone;
		//return $next($request,$respone);
	}
	
}