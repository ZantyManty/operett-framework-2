<?php

namespace App\Modules;

use App\Modules\Model;

use Csifo\Mvc\Interfaces\IView;
use Csifo\Http\User\Interfaces\IUser as IUser;
use Csifo\Core\Interfaces\IConfig as IConfig;

class Controller implements IController {
	
	protected $model;
	protected $view;
	
	public function __construct(Model $model,IView $view){
		$this->model	= $model;
		$this->view		= $view;
	}
	
}