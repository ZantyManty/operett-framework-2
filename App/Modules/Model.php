<?php

namespace App\Modules;

use Csifo\Database\MySql\Orm\Interfaces\IQuery as SQL;

class Model implements IModel {
	
	protected $sql;
	
	public function __construct(SQL $sql){
		$this->sql = $sql;
	}
	
}