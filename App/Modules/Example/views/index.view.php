|$this->load('meta')|

<!-- Example value -->
<h1>|$title|</h1>


<!-- Example foreach -->
<div class="middle" >
|fe($array as $value)|
	<span>|$value|<br/></span>
|/fe|
</div>


<!-- This is a value from a lang file -->
<div class="middle" >
	{#thisFromLang#}
</div>

<div class="middle" >
	Text from the uri: |$text|
</div>

<div class="middle" >
	Your IP: |$ip|
</div>

|$this->load('footer')|