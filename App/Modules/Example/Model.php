<?php
namespace App\Modules\Example;

use App\Modules\IModel as GlobalModel;
use Csifo\Database\MySql\Orm\Interfaces\IQuery as SQL;

class Model implements Interfaces\IModel {
	
	protected $globalModel;
	protected $sql;
	
	public function __construct(GlobalModel $globalModel,SQL $sql){
		$this->globalModel = $globalModel;
		$this->sql = $sql;
	}
}