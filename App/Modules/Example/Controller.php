<?php

namespace App\Modules\Example;

use Csifo\Mvc\Interfaces\IView;
use App\Modules\Example\Interfaces\IModel;
use App\Modules\IController as GlobalController;

use Csifo\Http\User\Interfaces\IUser as IUser;

/*
* Example module
*/
class Controller implements Interfaces\IController {
	
	protected $globalController;
	protected $model;
	
	public function __construct(GlobalController $controller,IModel $model,IView $view){
		$this->globalController = $controller;
		$this->model = $model;
		$this->view	 = $view;
	}
	
	/**
	* @param \Text $text ('this is a default text')
	* @param \Number $number (15)
	* Zárójelben a default paraméter a skaláris objektumhoz
	*/
	public function index(IUser $user,\Text $text,\Number $number){
		
		$this->view->assign('title','Welcome to the Operett Framework! (Scalar Inject)');
		$this->view->assign('array',$text->split(' '));
		$this->view->assign('text',$text->replace('default','common')->capital()->val());
		$this->view->assign('ip',$user->Ip());
		
		return $this->view->render('index');
	}
}