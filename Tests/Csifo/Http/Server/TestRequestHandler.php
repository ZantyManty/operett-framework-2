<?php

namespace Test\Csifo\Http\Server;

use PHPUnit\Framework\TestCase as TestCase;

use Test\Mocks\Http\Message\ServerRequest;
use Test\Mocks\Http\Message\Stream;
use Test\Mocks\Http\Message\Response;

use Csifo\Core\Interfaces\IConfig;
use Csifo\Http\Server\RequestHandler;

class TestRequestHandler extends TestCase {
	
	public function testIt_can_manipulate_response(){
		$request = new ServerRequest();
		$response = new Response();
		$stream = new Stream();
		
		$config = new class implements IConfig {
			public function namespaces($ns){
				return 'Tests\_Sandbox\Middleware';
			}
			
			public function get($name){}
		};
		
		$handler = new RequestHandler($config);
		$handler->init($response,$stream);
		$handler->setMiddlewareArray(['First','Second']);
		
		$callable = function() {
			return '<h1>La Boum!</h1>';
		};
		
		$handler->setControllerCallable($callable);
		
		$result = $handler->handle($request);
		
		$this->assertEquals(403,$result->getStatusCode());
		$this->assertEquals('Access denied',$result->getReasonPhrase());
		$this->assertEquals('Sophie Marceau',$result->getHeader('X-Custom-Header'));
		
		$body = (string)$result->getBody();
		$this->assertEquals('<h1>La Boum!</h1>',$body);
	}
	
}