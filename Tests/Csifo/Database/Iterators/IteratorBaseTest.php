<?php

namespace Tests\Csifo\Database\Iterators;

use PHPUnit\Framework\TestCase as TestCase;
use Tests\Mocks\Database\Bridge;
use Csifo\Database\Iterators\NormalIterator;

class IteratorBaseTest extends TestCase {
	
	public function testIt_can_handle_modifiers(){
		/*
		* Prepare test
		*/
		$bridge = new Bridge;
		
		$mock = $this->getMock();
		$mock2 = $this->getMock();
		/*
		* Prepare reference mock
		*/
		$mock2_name = $mock2->name;
		$mock2->name = $mock2->name . ' suff';
		/*
		* Prepare sample array
		*/
		$array = [0 => $mock];
		
		/*
		* IteratorBase itself is an abstract class,so we need a child
		*/
		$iterator = new NormalIterator($array,$bridge,'users');
		//Create a tiny example modifier class...
		$sampleObject = new class { 
			public function giveSuffix(){
				return ' suff';
			} 
		};
		
		$iterator->modify(function($item) use (&$sampleObject){
			$name = $item->name;
			$item->name = $name . $sampleObject->giveSuffix();
			
			return $item;
		});
		
		foreach($iterator as $item){
			$this->assertEquals($mock2,$item);
		}
	}
	
	protected function getMock(){
		return new class { 
			protected $original = [
				'name' => 'John',
			];
			
			public function __get($key){
				return $this->original[$key];
			}
			
			public function __set($key,$val){
				$this->original[$key] = $val;
			}
		};
	}
	
}