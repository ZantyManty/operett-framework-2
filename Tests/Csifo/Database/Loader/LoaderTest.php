<?php

namespace Tests\Csifo\Database\Loader;

use PHPUnit\Framework\TestCase as TestCase;

use Csifo\Core\Interfaces\IConfig as IConfig;
//use Csifo\Cache\Cache\File\CacheItemPool;
//use Csifo\Cache\SimpleCache\File;
use Csifo\Cache\SimpleCache\Interfaces\ICache;
use Csifo\Database\Resolver\Interfaces\IBuilder as IBuilder;
use Csifo\Database\Resolver\Resolver as Resolver;
use Csifo\Database\Resolver\Grammar\En as Plural;
use Csifo\Helpers\Interfaces\IStringHelper;

/* Helpers */
use Tests\Helpers\DatabaseHelper as DatabaseHelper;
use Tests\Csifo\Database\Loader\_HelperDummyCache;
use Tests\Helpers\CommonHelper;

final class LoaderTest extends TestCase {
	
	public function testIt_can_resolve_table_keys(){
		/*
		* Prepare test
		*/
		CommonHelper::defineRoot();
		$config = $this->getConfigMock();
		$cacheItem = new _HelperDummyCache();
		//$cachePool = new CacheItemPool($config);
		$cachePool = $this->getCacheMock();
		$connection = new DatabaseHelper();
		$stringHelper = $this->getStringHelperMock();
		
		/*
		* Emulate cache file
		*/
		file_put_contents($config->app('CACHE_DIR') . '/ORM_loader_guestbook',gzencode(serialize($cacheItem)));
		
		/**
		* Init
		*/
		$resolver = new Resolver($cachePool,$connection,new Plural,$stringHelper,$config);
		
		/*
		* Primary key test
		*/
		$primaryKey = $resolver->getPrimaryKey('users');
		$this->assertEquals('id',$primaryKey);
		
		/*
		* Foreign key test
		*/
		$foreignKey = $resolver->getForeignKey('users','guestbook_posts');
		$this->assertEquals('guestbook_posts.user_id',$foreignKey);
		
		/*
		* Other direction to foreignKey
		*/
		$foreignKey2 = $resolver->getForeignKey('guestbook_posts','users');
		$this->assertEquals('guestbook_posts.user_id',$foreignKey2);
		
		/*
		* Many to many table data
		*/
		$data = $resolver->getManyToManyMeta('users','groups');
		$expected = [
			'groups' => [
				'linkTable'			=> 'groups_users',
				'foreignKey'		=> 'user_id',
				'foreignKeyTarget'	=> 'group_id',
				'childPrimaryKey'	=> 'id'
			]
		];
		$this->assertEquals($expected,$data);
		
	}
	
	protected function getStringHelperMock(){
		return new class implements IStringHelper {
			public function generateSlug($string){

			}
		};
	}
	
	protected function getCacheMock(){
		return new class implements ICache {
			public function get($key, $default = null){
				return (new _HelperDummyCache())->get();
			}
	
			public function set($key, $value, $ttl = null){}
			
			public function delete($key){}
			
			public function clear(){}
			
			public function getMultiple($keys, $default = null){}
			
			public function setMultiple($values, $ttl = null){}
			
			public function deleteMultiple($keys){}
			
			public function has($key){}
		};
	}
	
	protected function getMockDbBuilder(){
		return new class implements IBuilder {
			public static function buildCache(){}
		};
	}
	
	protected function getConfigMock(){

		return new class implements IConfig {
			
			public function get($name){}
			
			public function app($key){
				if($key == 'CACHE_DIR'){
					return ROOT . '/Tests/_Sandbox/Cache';
				}
			}
		};
	}
}