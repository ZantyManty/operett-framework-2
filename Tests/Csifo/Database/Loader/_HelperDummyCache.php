<?php

namespace Tests\Csifo\Database\Loader;

use Csifo\Cache\Cache\Interfaces\ICacheItem as ICacheItem;

class _HelperDummyCache implements ICacheItem {
	
	private $key = 'ORM_loader';
	private $expiresAt = null;
	private $isHit = false;
	private $value = [
				"TABLES" => [
					"users" => [
						"PRIMARY" => "id",
						"COLUMNS" => [
							"id" => ['Type'	=>	'int(11)'],
							"name" => ['Type'	=>	'varchar(300)'],
							"registration_date" => ['Type'	=>	'datetime'],
							"password" => ['Type'	=>	'varchar(300)']
						],
					],
					"guestbook_posts" => [
						"PRIMARY" => "id",
						"COLUMNS" => [
							"id" => ['Type'	=>	'int(11)'],
							"content" => ['Type'	=>	'varchar(5000)'],
							"created" => ['Type'	=>	'datetime'],
							"user_id" => ['Type'	=>	'int(14)']
						]
					],
					"groups_users" => [
						"PRIMARY" => "id",
						"COLUMNS" => [
							"id" => ['Type'	=>	'int(11)'],
							"group_id" => ['Type'	=>	'int(11)'],
							"user_id" => ['Type'	=>	'int(11)'],
						]
					],
					"groups" => [
						"PRIMARY" => "id",
						"COLUMNS" => [
							"id" => ['Type'	=>	'int(11)'],
							"name" => ['Type'	=>	'varchar(100)'],
						]
					],
				],
				"CONNECTIONS" => [
					"users" => [
						"guestbook_posts" => ["id" => "user_id"],
						"groups_users" => ["id" => "user_id"]
					],
					"groups" => [
						"groups_users" => ["id" => "group_id"]
					]
				]
			];
			
			
	public function get(){
		return $this->value;
	}
	
	public function getKey(){
		return $this->key;
	}
	public function isHit(){}
	public function set($value){}
	public function expiresAt($expiration){}
	public function expiresAfter($time){}
	
}