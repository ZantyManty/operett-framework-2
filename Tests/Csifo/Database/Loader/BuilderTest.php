<?php

namespace Tests\Csifo\Database\Loader;

use PHPUnit\Framework\TestCase as TestCase;

use Csifo\Database\Base\Interfaces\IConnection as IConnection;
use Csifo\Database\Base\Interfaces\IBasic as IBasic;
use Csifo\Database\Resolver\Builder as MetaBuilder;
use Csifo\Database\Resolver\Grammar\En as Plural;
use Csifo\Helpers\Interfaces\IStringHelper;
use Csifo\Core\Interfaces\IConfig;

final class BuilderTest extends TestCase {
	
	public function testIt_can_build_the_meta_array(){
		$connection = $this->getConnectionMock();
		$stringHelper = $this->getStringHelperMock();
		$config = $this->getConfigMock();
		
		MetaBuilder::init($connection,new Plural,$stringHelper,$config);
		
		$result = MetaBuilder::buildCache();

		$expected = [
			"TABLES" => [
				"users" => [
					"PRIMARY" => "id",
					"COLUMNS" => [
						"id" => ['Type'	=>	'int(11)'],
						"name" => ['Type'	=>	'varchar(300)'],
						"registration_date" => ['Type'	=>	'datetime'],
						"password" => ['Type'	=>	'varchar(300)']
					],
					'CLASS'	=> 'User',
				],
				"guestbook_posts" => [
					"PRIMARY" => "id",
					"COLUMNS" => [
						"id" => ['Type'	=>	'int(11)'],
						"content" => ['Type'	=>	'varchar(5000)'],
						"created" => ['Type'	=>	'datetime'],
						"user_id" => ['Type'	=>	'int(11)']
					],
					'CLASS'	=> 'GuestbookPost',
				],
				"groups_users" => [
					"PRIMARY" => "id",
					"COLUMNS" => [
						"id" => ['Type'	=>	'int(11)'],
						"group_id" => ['Type'	=>	'int(11)'],
						"user_id" => ['Type'	=>	'int(11)'],
					],
					'CLASS'	=> 'GroupsUser',
				],
				"groups" => [
					"PRIMARY" => "id",
					"COLUMNS" => [
						"id" => ['Type'	=>	'int(11)'],
						"name" => ['Type'	=>	'varchar(100)'],
					],
					'CLASS'	=> 'Group',
				],
			],
			"CONNECTIONS" => [
				"users" => [
					"guestbook_posts" => ["id" => "user_id"],
					"groups_users" => ["id" => "user_id"]
				],
				"groups" => [
					"groups_users" => ["id" => "group_id"]
				]
			],
			"MODEL_ALIASES" => [
				'User' => [
					'class' => 'User',
					'tableName' => 'users'
				],
				'GuestbookPost' => [
					'class' => 'GuestbookPost',
					'tableName' => 'guestbook_posts'
				],
				'Group' => [
					'class' => 'Group',
					'tableName' => 'groups'
				],
				'GroupsUser' => [
					'class' => 'GroupsUser',
					'tableName' => 'groups_users'
				],
			],
		];
		
		$this->assertEquals($expected,$result);
	}
	
	public function testIt_builded_the_models(){
		/*
		* Generated Model files
		*/
		$modelFiles = [
			'Group.php',
			'GroupsUser.php',
			'GuestbookPost.php',
			'User.php',
		];
		
		foreach($modelFiles as $modelFile){
			$file = ROOT . '/Tests/_Sandbox/GeneratedModels/Models/' . $modelFile;
			$this->assertTrue(file_exists($file));
			/*
			* Contents
			*/
			$expect = file_get_contents(ROOT . '/Tests/_Sandbox/GeneratedModels/References/' . $modelFile);
			$current = file_get_contents($file);
			
			$this->assertEquals($expect,$current);
			
			unlink($file);
		}
	}
	
	protected function getStringHelperMock(){
		return new class implements IStringHelper {
			public function generateSlug($string){
				$replace = [
					'á' => 'a',
					'é' => 'e',
					'ú' => 'u',
					'ü' => 'u',
					'ű' => 'u',
					'ó' => 'o',
					'ö' => 'o',
					'ő' => 'o',
					'í' => 'i'
				];
				return str_replace(array_keys($replace),$replace,$string);
			}
		};
	}
	
	
	protected function getConfigMock(){
		return new class implements IConfig {
			public function app($name){
				if($name == 'APP_DIR'){
					return ROOT . '/Tests/_Sandbox/GeneratedModels';
				}
			}
			
			public function get($name){
				return $this->app($name);
			}
		};
	}
	
	protected function getConnectionMock(){
		return new class implements IConnection,IBasic {
			
			public function Connect() {}
			
			protected $queries = [
				'SHOW TABLES;'	=> [
					0 => ['Tables_in_questbook' => 'users'],
					1 => ['Tables_in_questbook' => 'guestbook_posts'],
					2 => ['Tables_in_questbook' => 'groups'],
					3 => ['Tables_in_questbook' => 'groups_users']
				],
				"SHOW INDEX FROM users WHERE Key_name = 'PRIMARY';" => ['Key_name' => 'PRIMARY','Column_name' => 'id'],
				"SHOW INDEX FROM guestbook_posts WHERE Key_name = 'PRIMARY';" => ['Key_name' => 'PRIMARY','Column_name' => 'id'],
				"SHOW INDEX FROM groups WHERE Key_name = 'PRIMARY';" => ['Key_name' => 'PRIMARY','Column_name' => 'id'],
				"SHOW INDEX FROM groups_users WHERE Key_name = 'PRIMARY';" => ['Key_name' => 'PRIMARY','Column_name' => 'id'],
				"DESCRIBE users;" => [
					0 => [
						'Field' => 'id',
						'Type'	=>	'int(11)',
					],
					1 => [
						'Field' => 'name',
						'Type'	=>	'varchar(300)',
					],
					2 => [
						'Field' => 'registration_date',
						'Type'	=>	'datetime',
					],
					3 => [
						'Field' => 'password',
						'Type'	=>	'varchar(300)',
					],
				],
				"DESCRIBE guestbook_posts;" => [
					0 => [
						'Field' => 'id',
						'Type'	=>	'int(11)',
					],
					1 => [
						'Field' => 'content',
						'Type'	=>	'varchar(5000)',
					],
					2 => [
						'Field' => 'created',
						'Type'	=>	'datetime',
					],
					3 => [
						'Field' => 'user_id',
						'Type'	=>	'int(11)',
					],
				],
				"DESCRIBE groups;" => [
					0 => [
						'Field' => 'id',
						'Type'	=>	'int(11)',
					],
					1 => [
						'Field' => 'name',
						'Type'	=>	'varchar(100)',
					]
				],
				"DESCRIBE groups_users;" => [
					0 => [
						'Field' => 'id',
						'Type'	=>	'int(11)',
					],
					1 => [
						'Field' => 'user_id',
						'Type'	=>	'int(11)',
					],
					2 => [
						'Field' => 'group_id',
						'Type'	=>	'int(11)',
					],
				]
			];
			
			public function GiveAll($query,$params,$classic){
				if(isset($this->queries[$query])){
					return $this->queries[$query];
				} else {
					return false;
				}
			}
			
			public function GiveRow($query,$params,$classic){
				if(isset($this->queries[$query])){
					return $this->queries[$query];
				} else {
					return false;
				}
			}
		};
	}
}