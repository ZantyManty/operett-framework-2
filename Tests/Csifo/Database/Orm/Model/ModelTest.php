<?php

namespace Tests\Csifo\Database\Orm\Model;

use PHPUnit\Framework\TestCase as TestCase;
use Csifo\Database\Orm\Model\Model as Model;
use Csifo\Database\Resolver\Interfaces\IResolver as ILoader;
use Tests\Mocks\Database\Bridge as Bridge;

/* Helpers */
use Tests\Helpers\DatabaseHelper as DatabaseHelper;
use Tests\Helpers\ReflectionHelper as ReflectionHelper;

class ModelTest extends TestCase {
	
	/*
	* It tests the "broadcast" saving function of the calculated,joined data model
	*/
	public function testItCanModifyRecord(){
		/*
		* ======================== Prepare ========================
		*/
		$connection = new DatabaseHelper();
		$connection->Connect();
		$connection->setLoader($this->getLoaderMock());

		/*
		* Create tables
		*/
		$connection->buildTables();
		
		/*
		* Seed test data to the sqllite db in memory
		*/
		$connection->seed();

		/*
		* ======================== Test======================== 
		*
		* Create the model object and seed the "original" data
		*/
		$bridge = new Bridge;
		$joinModel = new Model($bridge,'users','id');

		$joinModel->__set('guestbook_posts.id',1);
		$joinModel->__set('guestbook_posts.user_id_aliased_uid',1);
		$joinModel->__set('guestbook_posts.content_aliased_conto','Original');
		
		$joinModel->__set('users.id',1);
		$joinModel->__set('users.name_aliased_haver_nev','Reka');
		
		/**
		* Modify the data
		*/
		$joinModel->conto = 'Trollolo!';
		$joinModel->haver_nev = 'Reka Acel!';
		
		$joinModel->save();
		
		/*
		* Assert 1. table (guestbook_posts)
		*/
		$expected = [
			'id'		=> "1",
			'user_id'	=> "1",
			'content'	=> 'Trollolo!',
		];
		$result = $connection->GiveRowAssoc('SELECT * FROM guestbook_posts');
		
		$this->assertEquals($expected,$result);
		
		/*
		* Assert 2. table (users)
		*/
		$expected = [
			'id'				=> "1",
			'name'				=> "Reka Acel!",
			'registration_date'	=> '1996-03-23 00:00:00',
			'password'			=>	'$1$2SomeDummyShit'
		];
		$result = $connection->GiveRowAssoc('SELECT * FROM users');
		
		$this->assertEquals($expected,$result);
	}
	
	protected function getLoaderMock(){
		return new class implements ILoader {
			public function getPrimaryKey($table){
				$array = [
					'guestbook_posts' => 'id',
					'users'			  => 'id',
				];
				
				return $array[$table];
			}
			
			public function getTableData($table){}
			
			public function getForeignKey($table,$target){}
		};
	}
	
}