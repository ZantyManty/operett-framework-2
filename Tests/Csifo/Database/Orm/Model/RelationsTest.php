<?php

namespace Tests\Csifo\Database\Orm\Model;

use PHPUnit\Framework\TestCase as TestCase;

use Csifo\Database\Orm\Model\Relations;
/*
* Use own helpers
*/
use Tests\Mocks\Database\Bridge;

class RelationsTest extends TestCase {
	
	protected $bridge;
	
	protected function setUp(){
		/*
		* Prepare classes
		*/
		$this->bridge = new Bridge;
		
		return new Relations($this->bridge);
	}
	
	public function test_It_can_use_relations_with_manyToMany_EagerLoading(){
		
		$relations = $this->setUp();
		
		$foreignKeys = $this->bridge->getResolver()->getManyToManyMeta('groups','users');
		
		$relations->setForeignKeys($foreignKeys);
		$relations->setRelationTables('groups',['users' => '*']);
		
		$relations->addId(1);
		$relations->addId(2);
		
		$relations->setRelations('groups');
		
		/*
		* First group 
		*/
		$result = $relations->getRelations('groups','users',1);
		
		$this->assertTrue(is_array($result));
		$this->assertEquals(1,count($result));
		
		
		$this->assertEquals(1,$result[0]->id);
		$this->assertEquals('Reka Acel!',$result[0]->name);
		
		/*
		* Second group
		*/
		$result2 = $relations->getRelations('groups','users',2);
		
		$this->assertTrue(is_array($result2));
		$this->assertEquals(2,count($result2));
		

		$this->assertEquals(1,$result2[0]->id);
		$this->assertEquals('Reka Acel!',$result2[0]->name);
		$this->assertEquals(2,$result2[1]->id);
		$this->assertEquals('Borbi',$result2[1]->name);
	}
	
	public function test_It_can_use_relations_with_manyToMany_EagerLoading_Backward(){
		$relations = $this->setUp();
		
		$foreignKeys = $this->bridge->getResolver()->getManyToManyMeta('users','groups');
		
		$relations->setForeignKeys($foreignKeys);
		$relations->setRelationTables('users',['groups' => '*']);
		
		$relations->addId(1);
		$relations->addId(2);
		
		$relations->setRelations('users');
		
		/*
		* First user's group (Reka)
		*/
		$result2 = $relations->getRelations('users','groups',1);
		
		$this->assertTrue(is_array($result2));
		$this->assertEquals(2,count($result2));
		

		$this->assertEquals(1,$result2[0]->id);
		$this->assertEquals('Group_First',$result2[0]->name);
		$this->assertEquals(2,$result2[1]->id);
		$this->assertEquals('Group_Second',$result2[1]->name);
		
		/*
		* Second user's group (Borbi) 
		*/
		$result = $relations->getRelations('users','groups',2);
		
		$this->assertTrue(is_array($result));
		$this->assertEquals(1,count($result));
		
		
		$this->assertEquals(2,$result[0]->id);
		$this->assertEquals('Group_Second',$result[0]->name);
	}
	
	public function test_It_can_use_relations_with_manyToMany_LazyLoading(){
		$relations = $this->setUp();
		
		/*
		* First group 
		*/
		$result = $relations->getRelations('groups','users',1);
		
		$this->assertTrue(is_array($result));
		$this->assertEquals(1,count($result));
		
		
		$this->assertEquals(1,$result[0]->id);
		$this->assertEquals('Reka Acel!',$result[0]->name);
		
		/*
		* Second group
		*/
		$result2 = $relations->getRelations('groups','users',2);
		
		$this->assertTrue(is_array($result2));
		$this->assertEquals(2,count($result2));
		

		$this->assertEquals(1,$result2[0]->id);
		$this->assertEquals('Reka Acel!',$result2[0]->name);
		$this->assertEquals(2,$result2[1]->id);
		$this->assertEquals('Borbi',$result2[1]->name);
	}
	
	public function test_It_can_use_relations_with_manyToMany_LazyLoading_Backward(){
		$relations = $this->setUp();
		
		/*
		* First user's group (Reka)
		*/
		$result2 = $relations->getRelations('users','groups',1);
		
		$this->assertTrue(is_array($result2));
		$this->assertEquals(2,count($result2));
		

		$this->assertEquals(1,$result2[0]->id);
		$this->assertEquals('Group_First',$result2[0]->name);
		$this->assertEquals(2,$result2[1]->id);
		$this->assertEquals('Group_Second',$result2[1]->name);
		
		/*
		* Second user's group (Borbi) 
		*/
		$result = $relations->getRelations('users','groups',2);
		
		$this->assertTrue(is_array($result));
		$this->assertEquals(1,count($result));
		
		
		$this->assertEquals(2,$result[0]->id);
		$this->assertEquals('Group_Second',$result[0]->name);
	}
	
	public function test_It_can_use_relations_with_oneToMany_EagerLoading(){
		$relations = $this->setUp();
		
		$key = $this->bridge->getResolver()->getForeignKey('users','guestbook_posts','',true);
		$foreignKeys['guestbook_posts'] = $key;
		
		$relations->setForeignKeys($foreignKeys);
		$relations->setRelationTables('users',['guestbook_posts' => '*']);
		
		$relations->addId(1);
		
		$relations->setRelations('users');
		
		$result = $relations->getRelations('users','guestbook_posts',1);
		
		$this->assertTrue(is_array($result));
		
		$this->assertEquals(2,count($result));
		
		$i = 1;
		foreach($result as $model){
			$this->assertEquals($i,$model->id);
			$i++;
		}
		
		$this->assertEquals('Trollolo!',$result[0]->content);
		$this->assertEquals('Second',$result[1]->content);
	}
	
	public function test_It_can_use_relations_with_oneToMany_EagerLoading_Backward(){
		$relations = $this->setUp();
		
		$key = $this->bridge->getResolver()->getForeignKey('guestbook_posts','users');
		/*
		* Hotfix!!
		*/
		$foreignKeys['users'] = (stripos($key,'users.') !== false) ? str_replace('users.','',$key) : $this->bridge->getResolver()->getPrimaryKey('users');
		
		$relations->setForeignKeys($foreignKeys);
		$relations->setRelationTables('guestbook_posts',['users' => '*']);
		
		$relations->addId(1);
		
		$relations->setRelations('guestbook_posts');
		
		$result = $relations->getRelations('guestbook_posts','users',1);
		
		$this->assertTrue(is_array($result));
		
		$this->assertEquals(1,count($result));
		

		$this->assertEquals(1,$result[0]->id);
		$this->assertEquals('Reka Acel!',$result[0]->name);
	}
	
	public function test_It_can_use_relations_with_oneToMany_LazyLoading(){
		$relations = $this->setUp();
		
		$result = $relations->getRelations('users','guestbook_posts',1);

		
		$this->assertTrue(is_array($result));
		
		$this->assertEquals(2,count($result));
		
		
		$this->assertEquals(1,$result[0]->id);
		$this->assertEquals(2,$result[1]->id);
		$this->assertEquals('Trollolo!',$result[0]->content);
		$this->assertEquals('Second',$result[1]->content);
	}
	
	public function test_It_can_use_relations_with_oneToMany_LazyLoading_Backward(){
		$relations = $this->setUp();
		
		$result = $relations->getRelations('guestbook_posts','users',1);
		
		$this->assertTrue(is_array($result));
		
		$this->assertEquals(1,count($result));

		$this->assertEquals(1,$result[0]->id);
		$this->assertEquals('Reka Acel!',$result[0]->name);
	}
}