<?php

namespace Tests\Csifo\Database\Orm;

use PHPUnit\Framework\TestCase as TestCase;
/*
* Use own helpers
*/
use Tests\Helpers\CommonHelper;
use Tests\Helpers\ReflectionHelper;
use Tests\Helpers\DatabaseHelper as Connection;

use Csifo\Database\Orm\Query;
use Csifo\Cache\File\CacheItemPool as FileCache;

use Csifo\Database\Resolver\Interfaces\IResolver as ILoader;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Database\Resolver\Grammar\En as EnGrammar;

use Tests\Mocks\Database\Bridge;


final class BuilderTest extends TestCase {
	

	public function testIt_can_build_query(){
		/*
		* Define ROOT constant
		*/
		CommonHelper::defineRoot();
		/*
		* Prepare classes
		*/
		$bridge = new Bridge;
		
		/*
		* Tests
		*/
		$builder = new Query($bridge);
		$builder->select()->from('users')->where('id','=',1);
		$result = ReflectionHelper::callProtectedThenGetProperty('Csifo\Database\Orm\Query','buildQuery','query',$builder);
		$expected = 'SELECT id,name,registration_date,password FROM users WHERE id = :id';
		
		$this->assertEquals($expected,$result);
		unset($builder);
		
		$builder = new Query($bridge);
		$builder->select()->from('guestbook_posts')->where('id','=',1);
		$result = ReflectionHelper::callProtectedThenGetProperty('Csifo\Database\Orm\Query','buildQuery','query',$builder);
		$expected = 'SELECT id,content,created,user_id FROM guestbook_posts WHERE id = :id';
		
		$this->assertEquals($expected,$result);
		unset($builder);
		
		/*
		* JOIN
		*/
		$builder = new Query($bridge);
		$builder->select('content')
		->from('guestbook_posts')
		->join('users','name','INNER');
		$result = ReflectionHelper::callProtectedThenGetProperty('Csifo\Database\Orm\Query','buildQuery','query',$builder);
		$expected = 'SELECT guestbook_posts.content AS "guestbook_posts.content",guestbook_posts.id AS "guestbook_posts.id",users.name AS "users.name",users.id AS "users.id" FROM guestbook_posts INNER JOIN users ON users.id=guestbook_posts.user_id';
		$this->assertEquals($expected,$result);
		unset($builder);

		/*
		* JOIN [Backward]
		*/
		$builder = new Query($bridge);
		$builder->select('name')
		->from('users')
		->join('guestbook_posts','content','INNER');
		$result = ReflectionHelper::callProtectedThenGetProperty('Csifo\Database\Orm\Query','buildQuery','query',$builder);
		$expected = 'SELECT users.name AS "users.name",users.id AS "users.id",guestbook_posts.content AS "guestbook_posts.content",guestbook_posts.id AS "guestbook_posts.id" FROM users INNER JOIN guestbook_posts ON users.id=guestbook_posts.user_id';
		$this->assertEquals($expected,$result);
		unset($builder);
	}
}