<?php

namespace Tests\Csifo\Database\Grammar;

use PHPUnit\Framework\TestCase as TestCase;
use Csifo\Database\Resolver\Grammar\Hu as Hu;

final class HuTest extends TestCase 
{
	public function testIt_can_identify_foreign_keys(){
		/*
		* Prepare test
		*/
		$hu = new Hu;
		
		$testTrueArray = [
			'felhasználó_id'	=>	'felhasználók',
			'szamár_id'			=>	'szamarak',
			'madár_id'			=>	'madarak',
			'ló_id'				=>	'lovak',
			'poszt_id'			=>	'posztok',
			'labda_id'			=>	'labdák',
			'banya_id'			=>	'banyák',
		];
		
		$testFalseArray = [
			'bejelentkezés_ideje'	=> 'bejelentkezések',
			'belépés_ideje'			=> 'belépések'
		];
		
		/*
		* Do the tests
		*/
		foreach($testTrueArray as $column => $table){
			$result = $hu->handle($column,$table);
			$this->assertTrue($result);
		}
		
		foreach($testFalseArray as $column => $table){
			$result1 = $hu->handle($column,$table);
			$this->assertFalse($result1);
		}
		
	}
}