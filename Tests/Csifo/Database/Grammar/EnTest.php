<?php

namespace Tests\Csifo\Database\Grammar;

use PHPUnit\Framework\TestCase as TestCase;
use Csifo\Database\Resolver\Grammar\En as En;

final class EnTest extends TestCase 
{
	public function testIt_can_identify_foreign_keys(){
		/*
		* Prepare test
		*/
		$en = new En;
		
		$testTrueArray = [
			'ball_id'	=> 'balls',
			'car_id'	=> 'cars',
			'file_id'	=> 'files'
		];
		
		$testFalseArray = [
			'car_time' => 'cars',
			'ball_number'	=> 'balls'
		];
		
		/*
		* Do the tests
		*/
		foreach($testTrueArray as $column => $table){
			$result = $en->handle($column,$table);
			$this->assertTrue($result);
		}
		
		foreach($testFalseArray as $column => $table){
			$result1 = $en->handle($column,$table);
			$this->assertFalse($result1);
		}
		
	}
}