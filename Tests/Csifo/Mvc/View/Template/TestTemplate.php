<?php

namespace Tests\Csifo\Mvc\View\Template;

use PHPUnit\Framework\TestCase as TestCase;
use Tests\Helpers\CommonHelper;
use Csifo\Mvc\View\Template\Template;

class TestTemplate extends TestCase {
	
	public function testIt_can_parse(){
		//Define ROOT constant
		CommonHelper::defineRoot();
		$templateFolder = ROOT . '/Tests/_Sandbox/Template/';
		
		$template = new Template;
		
		$sample = file_get_contents($templateFolder . 'sample.php');
		$reference = file_get_contents($templateFolder . 'reference.php');
		
		$this->assertEquals($template->parse($sample),$reference);
	}
	
}