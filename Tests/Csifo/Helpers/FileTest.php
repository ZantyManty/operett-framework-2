<?php

namespace Tests\Csifo\Helpers;

use PHPUnit\Framework\TestCase as TestCase;
use Csifo\Helpers\File as File;
use Tests\Helpers\CommonHelper;

use Csifo\Core\Interfaces\IConfig as IConfig;

final class FileTest extends TestCase 
{	
	public function setUp(){
		/*
		* Define ROOT constant
		*/
		CommonHelper::defineRoot();
		
		return new class implements IConfig {
			public function app($string = ''){
				return ROOT . '/Tests/_Sandbox/TestModules';
			}
			
			public function get($name){}
		};
	}
	
	public function testIt_can_create_directories_recursive(){
			
		$dir = 'Tests/_Sandbox/DirForFileTest/Dir1/Dir2/Dir3';
		
		/*
		* - Define ROOT constant
		* - Get anonym mock object for config
		*/
		$config = $this->setUp();
		
		$fileHelper = new File($config);
		
		$result = $fileHelper->createDir($dir);
		$excepted = $dir;
		
		$this->assertEquals($excepted,$result);
		
		/*
		* We are check it physical too!
		*/
		$this->assertFileExists($result);
	}
	
	public function testIt_can_delete_directories_recursive(){
		/*
		* - Define ROOT constant
		* - Get anonym mock object for config
		*/
		$config = $this->setUp();
		
		$dir = ROOT . '/Tests/_Sandbox/DirForFileTest';
		
		$fileHelper = new File($config);
		
		$fileHelper->delete($dir);

		$this->assertFalse(file_exists($dir));
	}
	
	public function testIt_can_get_module_list(){
		/*
		* - Define ROOT constant
		* - Get anonym mock object for config
		*/
		$config = $this->setUp();
		$fileHelper = new File($config);
		$modulesDir = $config->app();
		
		$excepted = [
			$modulesDir . '/Module1',
			$modulesDir . '/Module2',
			$modulesDir . '/Module3'
		];
		$result = $fileHelper->getModuleList();
		
		$this->assertEquals($excepted,$result);
	}
}