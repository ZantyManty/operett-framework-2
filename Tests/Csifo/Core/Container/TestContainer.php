<?php

namespace Tests\Csifo\Core\Container;

use PHPUnit\Framework\TestCase as TestCase;
use Tests\Helpers\CommonHelper;
use Csifo\Core\Container\Container as RealContainer;
use Csifo\Core\Container\NotFoundException;
use Csifo\Core\Interfaces\IConfig;
use Tests\_Sandbox\Config\Container	as MockConfig;
use Tests\_Sandbox\Config\Drivers	as MockDrivers;
use Csifo\Config\Interfaces\IModules;

//Sample classes...
use Tests\_Sandbox\Container\Sample\Singleton;
use Tests\_Sandbox\Container\Sample\CommonObject;

//Sample drivers...
use Tests\_Sandbox\Container\Sample\Drivers\Driver1;
use Tests\_Sandbox\Container\Sample\Drivers\Driver2;

class TestContainer extends TestCase{
	
	protected $container;
	
	protected function setUp(){
		//Define ROOT constant
		CommonHelper::defineRoot();
		/*
		* Mock config
		*/
		$commonConfig = new class implements IConfig {
			public function app($name){
				if($name == 'drivers'){
					return include ROOT . '/Tests/_Sandbox/Container/drivers_config.php';
				}
			}
			
			public function get($name){
				return $this->app($name);
			}
		};
		
		/*
		* Services
		*/
		$config = new MockConfig();
		
		/*
		* Drivers
		*/
		$drivers = new MockDrivers();
		
		/*
		* Modules...
		*/
		$modules = new class implements IModules {
			public $modules = [];
		};
		
		/*
		* The container
		*/
		$this->container = new RealContainer($config,$drivers,$modules,$commonConfig);
	}
	
	public function testIt_can_resolve_services(){
		$this->setUp();
		
		$result = $this->container->get('Sample\ICommonObject');
		$this->assertInstanceOf(CommonObject::class,$result);
		
		$result = $this->container->get('Sample\ISingleton');
		$this->assertInstanceOf(Singleton::class,$result);
		
		//If a service not exists
		$this->expectException(NotFoundException::class);
		$result = $this->container->get('Sample\NotExists');
	}
	
	public function testIt_can_resolve_services_by_drivers(){
		$this->setUp();
		
		$result = $this->container->get('Sample\FromDriver');
		$this->assertInstanceOf(Driver2::class,$result);
	}
	
	public function testIt_can_resolve_services_by_OTronic(){
		$this->setUp();
		
		$result = $this->container->get('Sample\FailSafe');
		$this->assertInstanceOf('Tests\_Sandbox\Container\Sample\FailSafe\Sample2',$result);
		
		$this->expectException(\Exception::class);
		$result = $this->container->get('Sample\FailSafe2');
	}
}