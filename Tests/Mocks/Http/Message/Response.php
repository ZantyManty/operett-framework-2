<?php

namespace Test\Mocks\Http\Message;

use Csifo\Psr\Http\Message\IResponse as IResponse;

class Response extends Message implements IResponse  {
	
	protected $statusCode;
	
	protected $reasonPhrase = 'OK';
	
	public function __construct($code = 200,$reason = 'OK',$body = null,$headers = [],$protocol = '1.1'){
		$this->reasonPhrase = $reason;
		$this->statusCode 	= $code;
		$this->headers		= $headers;
		$this->protocol 	= $protocol;
		$this->body 		= $body;
	}
	
	public function getStatusCode(){
        return $this->statusCode;
    }
	
    public function getReasonPhrase(){
        return $this->reasonPhrase;
    }
	
    public function withStatus($code, $reasonPhrase = ''){
		$this->reasonPhrase = $reasonPhrase;
		$this->statusCode 	= $code;
		return $this;
    }
	
	public function respond(){
		header("HTTP/{$this->getProtocol()} {$this->getStatusCode()} {$this->getReasonPhrase()}");
		
		$headers = $this->getHeaders();
		foreach($headers as $name => $value){
			header("{$name}: {$value}");
		}
		
		return (string)$this->getBody();
	}
}