<?php

namespace Tests\Mocks\Database;

class Model {
	
	protected $original;
	
	protected $bridge;
	
	public function __construct($bridge){
		$this->bridge = $bridge;
	}
	
	public function __set($key,$val){
		$this->original[$key] = $val;
	}
	
	public function __get($key){
		return (isset($this->original[$key])) ? $this->original[$key] : false;
	}
	
}