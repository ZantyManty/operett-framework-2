<?php

namespace Tests\Mocks\Database;

use Csifo\Database\Interfaces\IBridge;
use Tests\Mocks\Database\Resolver;
use Tests\Mocks\Database\Relations;

class Bridge implements IBridge {
	
	protected $relations;
	protected $resolver;
	
	public function getResolver(){
		if($this->resolver === null){
			$this->resolver = new Resolver();
		}
		return $this->resolver;
	}
	
	public function getConnection(){
		return $this->getResolver()->getConnection();
	}
	
	public function getRelations(){
		if($this->relations === null){
			$this->relations = new Relations;
		}
		return $this->relations;
	}
}