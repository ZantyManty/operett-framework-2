<?php

namespace Tests\Mocks\Database;

use Csifo\Database\Resolver\Interfaces\IResolver;
use Tests\Helpers\DatabaseHelper;

class Resolver implements IResolver {
	
	protected $metaData = [
		"TABLES" => [
			"users" => [
				"PRIMARY" => "id",
				"COLUMNS" => [
					"id" => ['Type'	=>	'int(11)'],
					"name" => ['Type'	=>	'varchar(300)'],
					"registration_date" => ['Type'	=>	'datetime'],
					"password" => ['Type'	=>	'varchar(300)']
				],
			],
			"guestbook_posts" => [
				"PRIMARY" => "id",
				"COLUMNS" => [
					"id" => ['Type'	=>	'int(11)'],
					"content" => ['Type'	=>	'varchar(5000)'],
					"created" => ['Type'	=>	'datetime'],
					"user_id" => ['Type'	=>	'int(14)']
				]
			],
			"groups_users" => [
				"PRIMARY" => "id",
				"COLUMNS" => [
					"id" => ['Type'	=>	'int(11)'],
					"group_id" => ['Type'	=>	'int(11)'],
					"user_id" => ['Type'	=>	'int(11)'],
				]
			],
			"groups" => [
				"PRIMARY" => "id",
				"COLUMNS" => [
					"id" => ['Type'	=>	'int(11)'],
					"date" => ['Type'	=>	'datetime'],
				]
			],
		],
		"CONNECTIONS" => [
			"users" => [
				"guestbook_posts" => ["id" => "user_id"],
				"groups_users" => ["id" => "user_id"]
			],
			"groups" => [
				"groups_users" => ["id" => "group_id"]
			]
		]
	];
	protected $connection;
	
	public function __construct(){
		$this->connection = new DatabaseHelper;
	}
	
	public function getConnection(){
		return $this->connection;
	}
	
	public function getTableData($table){
		return $this->metaData['TABLES'][$table];
	}
	
	public function getColumnNames($table){
		return array_keys($this->getTableData($table)['COLUMNS']);
	}
	
	public function getPrimaryKey($table){
		return $this->getTableData($table)['PRIMARY'];
	}
	
	public function getManyToManyMeta($table,$target_table){
		$foreignKeys = [];
		$table_for_search = $table . '_' . $target_table;
		$key = $this->getForeignKey($table,$table_for_search,'',true);
		
		if($key !== false){
			//$foreignKeys[$target_table] = [$table_for_search => $key];
			$foreignKeys[$target_table] = [
					'linkTable'			=> $table_for_search,
					'foreignKey'		=> $key,
					'foreignKeyTarget'	=> $this->getForeignKey($target_table,$table_for_search,'',true),
					'childPrimaryKey'	=> $this->getPrimaryKey($target_table)
				];
		} else {
			$table_for_search = $target_table . '_' . $table;
			$key = $this->getForeignKey($table,$table_for_search,'',true);
			
			if($key !== false){
				$foreignKeys[$target_table] = [
					'linkTable'			=> $table_for_search,
					'foreignKey'		=> $key,
					'foreignKeyTarget'	=> $this->getForeignKey($target_table,$table_for_search,'',true),
					'childPrimaryKey'	=> $this->getPrimaryKey($target_table)
				];
			} else {
				throw new \Exception('Many to many link table foreign key not found!');
			}
		}
		return $foreignKeys;
	}
	
	protected function findForeignByPrimary($primary_table,$target_table,$primary_table_prim_key,$onlyKey = false){
		$connections = $this->metaData['CONNECTIONS'];
		/*
		* Direction 1:
		*/
		if(isset($connections[$primary_table][$target_table][$primary_table_prim_key])){
			/*
			* Foreign key is in $connections[$primary_table][$target_table][$primary_table_prim_key]
			*/
			return ($onlyKey == false ? $target_table . '.' : '') . $connections[$primary_table][$target_table][$primary_table_prim_key];
		} 
		/*
		* Direction 2:
		*/
		else {
			/*
			* Foreign table's primary key 
			* (We need it as an array key to find the foreign key in the "connections" array)
			*/
			$primary_key = (isset($this->metaData['TABLES'][$target_table]['PRIMARY'])) ? $this->metaData['TABLES'][$target_table]['PRIMARY'] : '';
			if(isset($connections[$target_table][$primary_table][$primary_key])){
				return ($onlyKey == false ? $primary_table . '.' : '') . $connections[$target_table][$primary_table][$primary_key];
			} else {
				//throw new \Exception('Foreign key for this resolving [ ' . $primary_table . ' | ' . $target_table . ' ] is not found!');
				return false;
			}
		}
	}
	
	public function getForeignKey($table,$target,$key = '',$onlyKey = false){
		$prim_key1 = ($key == '') ? $this->getPrimaryKey($table) : $key;
		return $this->findForeignByPrimary($table,$target,$prim_key1,$onlyKey);
	}
}