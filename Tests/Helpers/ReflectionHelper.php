<?php

namespace Tests\Helpers;

class ReflectionHelper {
	
	public static function callProtectedThenGetProperty($class,$method,$property,$object,$args = []){
		$class = new \ReflectionClass($class);
		$method = $class->getMethod($method);
		$method->setAccessible(true);
		$method->invokeArgs($object,$args);
		$property = $class->getProperty($property);
		$property->setAccessible(true);
		
		return $property->getValue($object);
	}
	
	public static function setObjectProtectedProperty($property,$propertyObject,$class,$args = []){
		$r = new \ReflectionClass($class);
		$object = $r->newInstanceArgs($args);
		$refObject = new \ReflectionObject($object);
		$property = $refObject->getProperty($property);
		$property->setAccessible(true);
		$property->setValue($property,$propertyObject);
		return $object;
	}
	
}