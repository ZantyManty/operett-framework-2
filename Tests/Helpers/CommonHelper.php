<?php

namespace Tests\Helpers;

class CommonHelper {
	
	public static function getRoot(){
		return dirname(dirname(__DIR__));
	}
	
	public static function defineRoot(){
		if(!defined('ROOT')) define('ROOT',self::getRoot());
	}
	
}