<?php

namespace Tests\Helpers;

use \PDO;
use PHPUnit\DbUnit\TestCaseTrait as DbTrait;

use Csifo\Database\Base\Interfaces\IConnection;
use Csifo\Database\Base\Interfaces\IBasic;
use Csifo\Grammar\Enu\Plural as Plural;
use Tests\Mocks\Database\Bridge;

class DatabaseHelper implements IConnection,IBasic {
	
	use DbTrait;
	
	private static $pdo 	= null;
	private static $bridge	= null;
	protected $connection 	= null;
	
	protected $loader;
	
	public function getConnection(){
		return $this->Connect();
	}
	
	public function Connect(){
		if($this->connection === null){
			if(self::$pdo === null){
				self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
			}
			$this->connection = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
			if(self::$bridge === null){
				self::$bridge = new Bridge;
			}
		}
		
		return $this->connection;
	}
	
	public function getDatabase(){
		return $GLOBALS['DB_DBNAME'];
	}
	
	public function getGrammar(){
		return new Plural();
	}
	
	public function getLoader(){
		return $this->loader;
	}
	
	public function setLoader($loader){
		$this->loader = $loader;
	}
	
	public function getDataset(){
		return $this->createFlatXMLDataSet(dirname(dirname(__FILE__)).'/_Datasets/database-seed.xml');
	}

	public function GiveRow($query,$params){
		
	}
	
	public function GiveAll($query,$executeArray,$table,$isAssoc = false){
		//var_dump($query);
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		/*
		* We need FETCH_PROPS_LATE because we need the __construct() run first!
		*/
		return ($isAssoc === false) ? $q->fetchAll(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE,'Tests\Mocks\Database\Model',[static::$bridge]) : $q->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function GiveRowAssoc($query,$params = null){
		$q = self::$pdo->prepare($query);
		$q->execute($params);
		return $q->fetch(PDO::FETCH_ASSOC);
	}
	
	public function GiveUpdate($query,$params = null){
		$q = self::$pdo->prepare($query);
			if(!is_null($params))
				$q->execute($params);
			else
				$q->execute();
	}
	
	/*
	* --------------------------------- Set up enviroment ---------------------------------
	*/
	
	public function buildTables(){
		$this->GiveUpdate("
			CREATE TABLE `guestbook_posts` (
				`id` INT(11) NOT NULL,
				`user_id` INT(11) NOT NULL,
				`content` VARCHAR(3000) NOT NULL,
				PRIMARY KEY (`id`)
			)
		");
		$this->GiveUpdate("
			CREATE TABLE `users` (
				`id` INT(11) NOT NULL,
				`name` VARCHAR(3000) NOT NULL,
				`registration_date` DATETIME NOT NULL,
				`password` VARCHAR(100) NOT NULL,
				PRIMARY KEY (`id`)
			)
		");
		
		$this->GiveUpdate("
			CREATE TABLE `groups_users` (
				`id` INT(11) NOT NULL,
				`user_id` INT(11) NOT NULL,
				`group_id` INT(11) NOT NULL,
				PRIMARY KEY (`id`)
			)
		");
		
		$this->GiveUpdate("
			CREATE TABLE `groups` (
				`id` INT(11) NOT NULL,
				`name` VARCHAR(100) NOT NULL,
				PRIMARY KEY (`id`)
			)
		");
	}
	
	public function seed(){
		/*
		* Test users
		*/
		$this->GiveUpdate('INSERT INTO users (id,name,registration_date,password) VALUES (:id,:name,:reg_date,:pass)',[':id' => 1,':name' => 'Reka',':reg_date' => '1996-03-23 00:00:00',':pass' => '$1$2SomeDummyShit']);
		$this->GiveUpdate('INSERT INTO users (id,name,registration_date,password) VALUES (:id,:name,:reg_date,:pass)',[':id' => 2,':name' => 'Borbi',':reg_date' => '1997-11-17 00:00:00',':pass' => '$1$2SomePassDummyShit']);
		
		/*
		* Test posts
		*/
		$this->GiveUpdate('INSERT INTO guestbook_posts (id,user_id,content) VALUES (:id,:uid,:content)',[':id' => 1,':uid' => 1,':content' => 'Original']);
		$this->GiveUpdate('INSERT INTO guestbook_posts (id,user_id,content) VALUES (:id,:uid,:content)',[':id' => 2,':uid' => 1,':content' => 'Second']);
		
		/*
		* Test groups
		*/
		$this->GiveUpdate('INSERT INTO groups (id,name) VALUES (:id,:name)',[':id' => 1,':name' => 'Group_First']);
		$this->GiveUpdate('INSERT INTO groups (id,name) VALUES (:id,:name)',[':id' => 2,':name' => 'Group_Second']);
		/*
		* Test groups relation
		*/
		$this->GiveUpdate('INSERT INTO groups_users (id,user_id,group_id) VALUES (:id,:uid,:group_id)',[':id' => 1,':uid' => 1,':group_id' => 1]);
		$this->GiveUpdate('INSERT INTO groups_users (id,user_id,group_id) VALUES (:id,:uid,:group_id)',[':id' => 2,':uid' => 1,':group_id' => 2]);
		$this->GiveUpdate('INSERT INTO groups_users (id,user_id,group_id) VALUES (:id,:uid,:group_id)',[':id' => 3,':uid' => 2,':group_id' => 2]);
	}
	
}