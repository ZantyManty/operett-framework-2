<?php

namespace Tests\_Sandbox\Config;

use Csifo\Config\Interfaces\IContainer;

class Container implements IContainer {
	
	public $classes = [
	
		/*
		* ======================= Simple objects =========================
		*/
		
		'Sample\ICommonObject' => [
			'class' => \Tests\_Sandbox\Container\Sample\CommonObject::class,
		],
		
		/*
		* ======================= Singleton =========================
		*/
		
		'Sample\ISingleton' => [
			'class' => \Tests\_Sandbox\Container\Sample\Singleton::class,
			'singleton'	=> true
		],
		
		/* For FailSafe testing... */
		'Sample\FailSafe'	=> [
			'class' => \Tests\_Sandbox\Container\Sample\FailSafe\Sample1::class,
			'singleton'	=> true,
			'backups' => [
				\Tests\_Sandbox\Container\Sample\FailSafe\Sample2::class,
				\Tests\_Sandbox\Container\Sample\FailSafe\Sample3::class
			]
		],
		
		'Sample\FailSafe2'	=> [
			'class' => \Tests\_Sandbox\Container\Sample\FailSafe\Sample3::class,
			'singleton'	=> true,
			'backups' => [
				\Tests\_Sandbox\Container\Sample\FailSafe\Sample1::class
			]
		]
	];
	
	/*
	* Closure based things like factories,etc...
	*/
	public function extra($container){

	}
}