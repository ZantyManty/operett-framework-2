<?php

namespace Tests\_Sandbox\Config;

use Csifo\Config\Interfaces\IDrivers;

class Drivers implements IDrivers {
	
	public $drivers = [
		'SampleDrivers' => [
			'First' => [
				'Sample\FromDriver'		=> [
					'class'		=> \Tests\_Sandbox\Container\Sample\Drivers\Driver1::class,
					'singleton' => true,
				],
			],
			'Second' => [
				'Sample\FromDriver'		=> [
					'class'		=> \Tests\_Sandbox\Container\Sample\Drivers\Driver2::class,
					'singleton' => true,
				],
			],
			
		]
	];
}