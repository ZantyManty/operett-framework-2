<?php

return [
	'container'	=>	[
		'Sample\ICommonObject' => Tests\_Sandbox\Container\Sample\CommonObject::class,
	],
	
	'singleton'	=>	[
		'Sample\ISingleton' => Tests\_Sandbox\Container\Sample\Singleton::class,
		'Sample\IOTronic'	=> [
			Tests\_Sandbox\Container\Sample\OTronic\Sample1::class,
			Tests\_Sandbox\Container\Sample\OTronic\Sample2::class
		],
		'Sample\IOTronic2'	=> [
			Tests\_Sandbox\Container\Sample\OTronic\Sample1::class,
			Tests\_Sandbox\Container\Sample\OTronic\Sample3::class
		]
	]
];