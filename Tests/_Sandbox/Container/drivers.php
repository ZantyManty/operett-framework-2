<?php

return [
	'SampleDrivers' => [
		'First' => [
			'container' => [],
			'singleton' => [
				'Sample\FromDriver'	=>	Tests\_Sandbox\Container\Sample\Drivers\Driver1::class,
			],
		],
		'Second' => [
			'container' => [],
			'singleton' => [
				'Sample\FromDriver'	=>	Tests\_Sandbox\Container\Sample\Drivers\Driver2::class,
			],
		],
	]
];