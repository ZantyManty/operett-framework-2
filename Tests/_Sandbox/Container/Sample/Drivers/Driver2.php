<?php

namespace Tests\_Sandbox\Container\Sample\Drivers;

use Tests\_Sandbox\Container\Sample\ToInject;

class Driver2 {
	
	protected $injected;
	
	public function __construct(ToInject $injected){
		$this->injected = $injected;
	}
}