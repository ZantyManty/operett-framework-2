<?php

namespace Tests\_Sandbox\Container\Sample;

class Singleton {
	
	protected $injected;
	
	public function __construct(ToInject $injected){
		$this->injected = $injected;
	}
}