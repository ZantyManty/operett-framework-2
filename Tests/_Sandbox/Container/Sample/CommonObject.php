<?php

namespace Tests\_Sandbox\Container\Sample;

class CommonObject {
	
	protected $injected;
	
	public function __construct(ToInject $injected){
		$this->injected = $injected;
	}
}