<?php

namespace Tests\_Sandbox\Container\Sample\FailSafe;

use Csifo\Core\Interfaces\FailSafe;

class Sample3 implements FailSafe {
	
	public function checkObjectStatus() : bool {
		return false;
	}
}