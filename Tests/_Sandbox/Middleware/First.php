<?php

namespace Tests\_Sandbox\Middleware;

use Csifo\Psr\Http\Server\IMiddleware;
use Csifo\Psr\Http\Message\IServerRequest;
use Csifo\Psr\Http\Server\IRequestHandler;

class First implements IMiddleware {
	
	public function process(IServerRequest $request,IRequestHandler $handler){
		
		/*
		* Something
		*/
		$respone = $handler->handle($request);
		$respone->withStatus(403,'Access denied');
		
		return $respone;
	}
	
}