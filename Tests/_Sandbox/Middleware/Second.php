<?php

namespace Tests\_Sandbox\Middleware;

use Csifo\Psr\Http\Server\IMiddleware;
use Csifo\Psr\Http\Message\IServerRequest;
use Csifo\Psr\Http\Server\IRequestHandler;

class Second implements IMiddleware {
	
	public function process(IServerRequest $request,IRequestHandler $handler){
		
		/*
		* Something
		*/
		$respone = $handler->handle($request);
		$respone->withHeader('X-Custom-Header','Sophie Marceau');
		return $respone;
	}
	
}