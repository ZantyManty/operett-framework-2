<?php

/*
* Operett framework driver catalog
*/
namespace Config;

use Csifo\Config\Interfaces\IDrivers;

class Drivers implements IDrivers {
	
	public $drivers = [
		'Cache'	=>	[
			/*
			* File cache driver
			*/
			'File'	=>	[
				'Csifo\Cache\Cache\Interfaces\ICacheItem'		=>	[
					'class'		=> \Csifo\Cache\Cache\File\CacheItem::class,
				],
				'Csifo\Cache\Cache\Interfaces\ICacheItemPool'	=> [
					'class'		=> \Csifo\Cache\Cache\File\CacheItemPool::class,
					'singleton' => true,
				],
				'Csifo\Cache\Cache\Interfaces\IDumper'			=> [
					'class' => \Csifo\Cache\Cache\File\Dumper::class,
					'singleton' => true,
				],
			],
		],
		'Session' => [
			/*
			* Classic $_SESSION
			*/
			'Classic' => [
				'Csifo\Http\Session\Interfaces\ISession'		=> [
					'class'		=> \Csifo\Http\Session\Classic\Session::class,
					'singleton' => true,
				],
			],
		],
		'ORM-Resolver' => [
			'Hu' => [
				'Csifo\Database\MySql\Resolver\Interfaces\IGrammar'	=>	[
					'class'		=> \Csifo\Database\MySql\Resolver\Grammar\Hu::class,
					'singleton' => true,
				],
			],
			'En' => [
				'Csifo\Database\MySql\Resolver\Interfaces\IGrammar'	=> [
					'class'		=> \Csifo\Database\MySql\Resolver\Grammar\En::class,
					'singleton' => true,
				],
			],
		],
		'Database' => [
			/* --- MySQL --- */
			'Mysql' =>[
				'Csifo\Database\Connection\Sql\Interfaces\IConnection' => [
					'class'		=> \Csifo\Database\Connection\Sql\Connection::class,
					'singleton'	=> true
				],
				'Csifo\Database\MySql\Base\Interfaces\IBasic' => [
					'class'		=> \Csifo\Database\MySql\Base\Basic::class,
					'singleton'	=> true
				],
				'Csifo\Database\MySql\Orm\Interfaces\IQuery' => [
					'class'		=> \Csifo\Database\MySql\Orm\Query::class,
					'singleton'	=> true
				],
				'Csifo\Database\MySql\Orm\Interfaces\IRelations' => [
					'class'		=> \Csifo\Database\MySql\Orm\Model\Relations::class,
					'singleton'	=> true
				],
				'Csifo\Database\MySql\Interfaces\IBridge' => [
					'class'		=> \Csifo\Database\MySql\Bridge::class,
					'singleton'	=> true
				],
				'Csifo\Database\MySql\Resolver\Interfaces\IResolver' => [
					'class'		=> \Csifo\Database\MySql\Resolver\Resolver::class,
					'singleton'	=> true
				],
				'Csifo\Factory\Database\Interfaces\IModel' => [
					'class'		=> \Csifo\Factory\Database\MySql\Model::class,
					'singleton'	=> true
				],
			],
		],
	];
}