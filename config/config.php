<?php

/*
* Operett framework configuration file
*/

	/*
	* Application config
	*/
	$application	=	[
		/* --- Options --- */
		'DEV_MODE'		=>	false,
		'AllowedTags'	=>	'<div><p><span>',
		
		/* --- Files --- */
		'routesFile'	=>	ROOT . '/config/routes.php',
		'driverFile'	=>	ROOT . '/config/drivers.php',
		'servicesFile'	=>	ROOT . '/config/services.php',
		'appFile'		=>	ROOT . '/config/app.php',
		
		/* --- Dirs --- */
		'MODULES_DIR'	=>	ROOT . '/App/Modules',
		'APP_DIR'		=>	ROOT . '/App',
		'CACHE_DIR'		=>	ROOT . '/Cache',
		'TMP_DIR'		=>	ROOT . '/Temp',
		'PUBLIC_DIR'	=>	ROOT . '/public',
		'MIGRATION_PATH' => ROOT . '/App/Migrations',
		'TRUNK_DIR'		=>	ROOT . '/Trunk',
		'TEMPLATE_DIR'	=>	ROOT . '/App/Template',
		'MUI_DIR'		=>	ROOT . '/Mui',
		
		/*
		* If you need triggers,you can add the here.
		* These methods run everytime,when you reload the page!
		*/
		'ControllerTriggersCli' => false,
		'ControllerTriggers' => [
			//'loadHead',
		],
		'MUI_LANG' => 'hu',
		'default_lang'	=>	'hu',
		
		/* Password hash options */
		'Password' => [
			'Algo'		=> PASSWORD_ARGON2I,
			'Pepper'	=> '',
			'PepperAlgo' => 'sha256',
			'Options' => [
				'cost' => 12
			]
		],
	];
	
	/*
	* Database Config
	*/
	$database	=	[
		'DBHOST'	=>	'',
		'DBNAME'	=>	'',
		'DBUSER'	=>	'root',
		'DBPASS'	=>	'',
		'MODEL'		=> Csifo\Database\Orm\Model\Model::class
	];
	
	/*
	* Driver uses
	*/
	$drivers	=	[
		'Cache'			=>	'File',
		'Session'		=>	'Classic',
		'ORM-Resolver'	=>	'En',
		'Database'		=> 'Mysql',
	];
	
	/*
	* Built in API data
	*/
	$api	=	[
		'Flickr' => [
			'key' => '',
		],
	];
	
	$cache = [
		'Memcache' => [
			'server' => '127.0.0.1',
			'port'	 => 11211,
		],
	];
	
	$websocket = [
		'enabled' 	=> true,
		'host' 		=> '127.0.0.1',
		'port' 		=> 9000,
	];
	
	$namespaces = [
		'Middleware' => 'App\Middleware',
		'Modules'	 => 'App\Modules'
	];
