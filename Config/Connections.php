<?php

namespace Config;

use Csifo\Config\Interfaces\IConnections;
use Csifo\Config\Connections as Config;

class Connections extends Config implements IConnections {
	
	/*
	* For multiple connections
	*/
	protected $connections = [
		'primary' => [
			'type'		=> 'mysql',
			'host'		=> '127.0.0.1',
			'db'		=> 'test',
			'user'		=> 'root',
			'password'	=> '',
			'model'		=> Csifo\Database\Orm\Model\Model::class
		]
	];
}