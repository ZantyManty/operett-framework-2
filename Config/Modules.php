<?php

namespace Config;

use Csifo\Config\Interfaces\IModules;

class Modules implements IModules {

	public $modules = [
			/* Global */
			'App\Modules\IController' => [
				'class'		=> \App\Modules\Controller::class,
				'singleton'	=> true,
			],
			'App\Modules\IModel' => [
				'class'		=> \App\Modules\Model::class,
				'singleton'	=> true,
			],
			
			/* Modules */
			'App\Modules\Example\Interfaces\IController' => [
				'class'		=> \App\Modules\Example\Controller::class,
				'singleton'	=> true,
			],
			'App\Modules\Example\Interfaces\IModel'	 => [
				'class'		=> \App\Modules\Example\Model::class,
				'singleton'	=> true,
			]
	];
}