<?php

namespace Config;

use Csifo\Config\Interfaces\IContainer;

class Container implements IContainer {
	
	public $classes = [
	
		/*
		* ======================= Simple objects =========================
		*/
		
		'Csifo\Http\Websocket\Interfaces\ISession' => [
			'class'		=> \Csifo\Http\Websocket\Session::class
		],
		'Csifo\Http\Websocket\Interfaces\IUser' => [
			'class'		=> \Csifo\Http\Websocket\User::class
		],
		'Csifo\Http\Router\Interfaces\IRoute' => [
			'class'		=> \Csifo\Http\Router\Route::class
		],
		'Csifo\Database\Migration\Interfaces\ISchema' => [
			'class'		=> \Csifo\Database\Migration\Schema::class
		],
		'Csifo\Database\Migration\Interfaces\ItableSchema' => [
			'class'		=> \Csifo\Database\Migration\tableSchema::class
		],
		'Csifo\Http\Curl\Interfaces\ICurl' => [
			'class'		=> \Csifo\Http\Curl\Curl::class
		],
		
		/*
		* ======================= Singleton =========================
		*/
		
		/* Configs*/
		'Csifo\Core\Interfaces\IConfig' => [
			'class'		=> \Csifo\Core\Config::class,
			'singleton'	=> true
		],
		'Csifo\Config\Interfaces\IRoutes' => [
			'class'		=> \Config\Routes::class,
			'singleton'	=> true
		],
		'Csifo\Config\Interfaces\IConnections' => [
			'class'		=> \Config\Connections::class,
			'singleton'	=> true
		],
		
		/* Web socket*/
		'Csifo\Http\Websocket\Interfaces\IServer' => [
			'class'		=> \Csifo\Http\Websocket\BasicWsServer::class,
			'singleton'	=> true
		],
		
		/* Http*/
		'Csifo\Http\Interfaces\ICookie' => [
			'class'		=> \Csifo\Http\Cookie::class,
			'singleton'	=> true
		],
		'Csifo\Http\Interfaces\IRouter' => [
			'class'		=> \Csifo\Http\Router\Router::class,
			'singleton'	=> true
		],
		'Csifo\Http\Router\Route\Interfaces\ICollection' => [
			'class'		=> \Csifo\Http\Router\Route\Collection::class,
			'singleton'	=> true
		],
		'Csifo\Factory\Interfaces\IRoute' => [
			'class'		=> \Csifo\Factory\Route::class,
			'singleton'	=> true
		],
		'Csifo\Http\Interfaces\IRoutes' => [
			'class'		=> \Csifo\Http\Router\Facade\RouterShell::class,
			'singleton'	=> true
		],
		'Csifo\Http\Router\Interfaces\IRouteCache' => [
			'class'		=> \Csifo\Http\Router\RouteCache::class,
			'singleton'	=> true,
		],
		
		/* --- PSR-7 (Request) --- */
		'Csifo\Psr\Http\Message\IRequest' => [
			'class'		=> \Csifo\Http\Message\Request::class,
			'singleton'	=> true
		],
		'Csifo\Psr\Http\Message\IServerRequest' => [
			'class'		=> \Csifo\Http\Message\ServerRequest::class,
			'singleton'	=> true
		],
		'Csifo\Psr\Http\Message\IResponse' => [
			'class'		=> \Csifo\Http\Message\Response::class,
			'singleton'	=> true
		],
		'Csifo\Psr\Http\Message\IUri' => [
			'class'		=> \Csifo\Http\Message\Uri::class,
			'singleton'	=> true
		],
		'Csifo\Psr\Http\Message\IStream' => [
			'class'		=> \Csifo\Http\Message\Stream::class,
			'singleton'	=> true
		],
		
		/* --- PSR-15 (Middlewares) --- */
		'Csifo\Psr\Http\Server\IRequestHandler' => [
			'class'		=> \Csifo\Http\Server\RequestHandler::class,
			'singleton'	=> true
		],
		
		/* --- Frontend --- */
		'Csifo\Client\Minify\Interfaces\ICss' => [
			'class'		=> \Csifo\Client\Minify\Css::class,
			'singleton'	=> true
		],
		'Csifo\Client\Minify\Interfaces\IJavascript' => [
			'class'		=> \Csifo\Client\Minify\Javascript::class,
			'singleton'	=> true
		],
		'Csifo\Mvc\Interfaces\ILang' => [
			'class'		=> \Csifo\Mvc\Lang::class,
			'singleton'	=> true
		],
		'Csifo\Mvc\Interfaces\IView' => [
			'class'		=> \Csifo\Mvc\View\View::class,
			'singleton'	=> true
		],
		'Csifo\Mvc\View\Template\Interfaces\ITemplate' => [
			'class'		=> \Csifo\Mvc\View\Template\Template::class,
			'singleton'	=> true
		],
		'Csifo\Http\Router\Path\Interfaces\IGenerator' =>[
			'class'		=>	\Csifo\Http\Router\Path\Generator::class,
			'singleton'	=> true
		],
		'Csifo\Mvc\View\Form\Interfaces\IBuilder'	=> [
			'class'		=>	\Csifo\Mvc\View\Form\Builder::class,
			'singleton'	=> true
		],
		'Csifo\Factory\Interfaces\IForm'	=> [
			'class'		=>	\Csifo\Factory\Form::class,
			'singleton'	=> true
		],
		'Csifo\Mvc\View\Form\Interfaces\IFormFields' => [
			'class'		=>	\Csifo\Mvc\View\Form\FormFields::class,
			'singleton'	=> true
		],
		
		/* --- Helpers --- */
		'Csifo\Helpers\Interfaces\IFileHelper' => [
			'class'		=> \Csifo\Helpers\File::class,
			'singleton'	=> true
		],
		'Csifo\Helpers\Interfaces\IValidator' => [
			'class'		=> \Csifo\Helpers\Validator::class,
			'singleton'	=> true
		],
		'Csifo\Helpers\Interfaces\IStringHelper' => [
			'class'		=> \Csifo\Helpers\StringHelper::class,
			'singleton'	=> true
		],
		'Csifo\Helpers\Interfaces\IClosure'		=> [
			'class' => \Csifo\Helpers\Closure::class,
			'singleton'	=> true
		],
		
		/* --- Database --- */
		'Csifo\Database\Connection\Interfaces\IResolver' => [
			'class' => \Csifo\Database\Connection\Resolver::class,
			'singleton'	=> true
		],
		
		/* --- Console --- */
		'Csifo\Console\Interfaces\IConsole' => [
			'class'		=> \Csifo\Console\Console::class,
			'singleton'	=> true
		],
		
		/* --- APIs --- */
		'Csifo\Api\Flickr\Interfaces\IFlickr' => [
			'class'		=> \Csifo\Api\Flickr\Flickr::class,
			'singleton'	=> true
		],
		
		/*--- User --*/
		'Csifo\Http\User\Interfaces\IUserAgent' => [
			'class'		=> \Csifo\Http\User\UserAgent::class,
			'singleton'	=> true
		],
		'Csifo\Http\User\Interfaces\IUser' => [
			'class'		=> \Csifo\Http\User\User::class,
			'singleton'	=> true
		],
		'Csifo\Http\GeoIp\Interfaces\IGeoIp' => [
			'class'		=> \Csifo\Http\GeoIp\IpApi::class,
			'singleton'	=> true
		],
		
		/* --- Security --- */
		'Csifo\Http\Security\Interfaces\IXss' => [
			'class'		=> \Csifo\Http\Security\Xss::class,
			'singleton'	=> true
		],
		'Csifo\Encryption\Interfaces\IPassword' => [
			'class'		=> \Csifo\Encryption\Password::class,
			'singleton'	=> true
		],
		
		/* --- PSR-16 (Simple Cache) --- */
		'Csifo\Cache\SimpleCache\Interfaces\ICache' => [
			'class'		=> \Csifo\Cache\SimpleCache\Memcache::class,
			'singleton'	=> true,
			'backups'	=> [
				\Csifo\Cache\SimpleCache\File::class,
			],
		],
	];
	
	/*
	* Closure based things like factories,etc...
	*/
	public function extra($container){
		/*
		* Scalar objects
		*/
		$container->registerScalarClasses([
			'Number'	=> [
				'class'		=> \Csifo\Core\Scalar\Number::class,
				'default'	=> 0
			],
			'Text'	=> [
				'class'		=> \Csifo\Core\Scalar\Text::class,
				'default'	=> ''
			]
		]);
	}
}