<?php

require __DIR__ . '/src/Core/Autoload.php';
//require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '.././src/functions.php';

class Application {
	
	public function main($args = []) : void {
		define('ROOT',__DIR__);
		
		$this->registerAutoload();
		
		/*
		* Start the app
		*/
		$app = new Csifo\Core\App();
		$app->run($args);
	}
	
	protected function registerAutoload() : void {
		/*
		*Registering the autoloader
		*/
		spl_autoload_register('Core\Autoload::load');
	}
}