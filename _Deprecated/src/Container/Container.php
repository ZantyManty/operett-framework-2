<?php

/*
* Csifogriff
* @author Askab 2017
*/

namespace Csifo\Core\Container;

use Csifo\Psr\Container\IContainer;

class Container implements IContainer {

	protected $container = [];
	protected $singleton = [];
	
	protected $drivers = [];

	protected $resolved = [];
	protected $optParams = [];
	
	public function __construct($config){
		
		if(is_object($config)){
			/*
			* Services
			*/
			$services = include $config->app('servicesFile');
			$this->container = $services['container'];
			$this->singleton = $services['singleton'];
			
			/*
			* Drivers
			*/
			$this->drivers = include $config->app('driverFile');
			$driverConfiguration = $config->get('drivers');
			if($driverConfiguration){
				$this->setDrivers($driverConfiguration);
			}
			
			/*
			* App classes
			*/
			$this->singleton += include $config->app('appFile');
			
			$this->resolved['Csifo\Core\Interfaces\IConfig'] = $config;
			
			$this->singleton[__CLASS__] = __CLASS__;
			$this->resolved[__CLASS__] = $this;
			
		} else {
			throw new Exception('Config class not found!');
		}
	}
	
	public static function getInstance(){
		return $this->resolved[__CLASS__];
	}
	
	public function setDrivers($driverConfig){
		if(is_array($driverConfig) === true){
			foreach($driverConfig as $key => $val){
				$array = $this->drivers[ucfirst($key)][ucfirst($val)];
				$this->singleton = array_merge($this->singleton,$array['singleton']);
				$this->container = array_merge($this->container,$array['container']);
			}
		}
	}
	
	protected function OTronic($classArray,$name,$type = 'singleton'){
		if(is_array($classArray)){
			foreach($classArray as $class){
				$object = $this->resolve($class);
				if($object->check() === true){
					if($type === 'singleton'){
						$this->singleton[$name] = $class;
					} else {
						$this->container[$name] = $class;
					}
					return $object;
				}
			}
			throw new \Exception('O-Tronic resolve failed! All classes has throw false!');
		}
		
		throw new \Exception('Class array is not an array!');
	}
	
	public function checkService($name){
		return (isset($this->container[$name]) || isset($this->singleton[$name]));
	}
	
	public function getService($name){

		if(isset($this->container[$name])){
			
			if(is_array($this->container[$name]))
				return $this->OTronic($this->container[$name],$name,'container');
			else
				return $this->resolve($this->container[$name]);
			
		} else if(isset($this->singleton[$name])){
			/*
			* If already have an instance
			*/
			if(isset($this->resolved[$name])){
				return $this->resolved[$name];
			} else {
				
				if(is_array($this->singleton[$name])){ 
					$this->resolved[$name] = $this->OTronic($this->singleton[$name],$name); 
				} else {
					$this->resolved[$name] = $this->resolve($this->singleton[$name]);
				}
				
				return $this->resolved[$name];
			}
		} else {
			throw new NotFoundException($name);
		}
	}
	
	public function addSingleton($name,$service){
		$this->singleton[$name] = $service;
	}
	
	public function addSingletonResolved($name,$object){
		$this->resolved[$name] = $object;
		$this->addSingleton($name,$name);
	}
	
	public function addService($name,$service){
		$this->container[$name] = $service;
	}
	
	/*
	* Adding predefined parameters (e.g. from the Router)
	* @param $params array
	*/
	public function addOptParams($params = []){
		/*
		* Normalise param array for exact classes!
		*/
		foreach($params as $key => $val){
			if(stripos($key,'interface') !== false){
				//Get the binded class name
				$realName = (isset($this->singleton[$key])) ? $this->singleton[$key] : $this->container[$key];
				$params[$realName] = $val;
				unset($params[$key]);
			}
		}
		$this->optParams = array_merge($this->optParams,$params);
	}
	
	/*
	* note:
	* If It's a Closure,the class gonna be just a "placeholder",and
	* the method will be the id
	*/
	protected function resolveParameters($parameters,$class,$method = null,$closure = null){
		$params = [];
		foreach($parameters as $k => $v){
			if($v->isDefaultValueAvailable()){
				//If we have default value,BUT check if have "fresher" arg in the predefined params
				if(isset($this->optParams[$class][$method][$v->name]) && !is_null($method)){
					$params[] = $this->optParams[$class][$method][$v->name];
				} else {
					$params[] = $v->getDefaultValue();
				}
			} else{
				$ex = ($closure == null) ? $this->checkParamTypeHint($class,$method,$v->name) : $ex = $this->checkParamTypeHint($class,$closure,$v->name);
				
				if($ex){
					$params[] = $this->resolve($ex);
				} else {
					if(!is_null($method)){
						//If it's have predefined parameters
						if(isset($this->optParams[$class][$method][$v->name])){
							$params[] = $this->optParams[$class][$method][$v->name];
						} else {
							throw new \Exception("Can't resolve parameter: [" . $v->name . "] (Predefined param not found!)");
						}
					} else {
						throw new \Exception("Can't resolve parameter: [" . $v->name . "]");
					}
				}
			}
		}
		return $params;
	}
	
	/*
	* Resolve a method's parameters
	* @param $class string
	* @param $method string
	*/
	public function resolveMethod($class,$method){
		//Get method's parameter
		$ref_method = new \ReflectionMethod($class,$method);
		return $this->resolveParameters($ref_method->getParameters(),$class,$method);
	}
	
	/*
	* Resolve a Closure
	* @param $closure callable
	* @param $id string
	*/
	public function resolveClosure($closure,$id = null){
		$ref_closure = new \ReflectionFunction($closure);
		$params = $this->resolveParameters($ref_closure->getParameters(),'__closure',$id,$closure);
		return call_user_func_array($closure,$params);
	}

	/*
	* Resolve a class by name (And method)
	* @param $class string
	* @param $method string
	* @param $onlyParams boolean
	*/
	public function resolve($class,$method = '__construct',$onlyParams = false){
		if($onlyParams === false && $this->checkService($class)){
			return $this->getService($class);
		} else {
			$reflection = new \ReflectionClass($class);
			if($reflection->hasMethod($method)){
				$refParams = $reflection->getMethod($method)->getParameters();
				$params = $this->resolveParameters($refParams,$class,$method);
				
				if(!$onlyParams){
					return $reflection->newInstanceArgs($params);
				} else {
					return $params;
				}
			} else {
				return new $class;
			}
		}	
	}
	
	/*
	* Resolve with additional parameters
	*/
	public function resolveWithParams($class,$params,$method = '__construct'){
		$realName = (stripos($class,'interface') !== false) ? ((isset($this->singleton[$class])) ? $this->singleton[$class] : $this->container[$class]) : $class;
		$opttimalParameters[$class][$method] = $params;
		$this->addOptParams($opttimalParameters);
		$obj = $this->resolve($class,$method);
		unset($this->optParams[$class]);
		return $obj;
	}
	
	/*
	* If we don't want a new instance,just only the parameters
	*/
	public function resolveOnlyParams($class,$method = '__construct'){
		return $this->resolve($class,$method,true);
	}

	/*
	* Checks if a "hint" is a class,not a primitive hint
	* @param $class string
	* @param $method mixed (string or null)
	* @param $param string
	*/
	protected function checkParamTypeHint($class,$method,$param){
		//If $method is null,that means $class is a closure
		$ex = (is_null($method) || $class == '__closure') ? \ReflectionParameter::export($method,$param,true) : \ReflectionParameter::export([$class,$method],$param,true);
		$ex = strip_tags($ex);
		//preg_match('#\[[ ]?(?!.*integer|.*int|.*float|.*bool|.*boolean|.*callable|.*string|.*array|.*&)(.*)[ ]?\$#',$ex,$m);
		preg_match('#\[ *([\w\\\]+?) *\$#',$ex,$m);
		if(isset($m[1])){
			return trim($m[1]);
		} else {
			return false;
		}
	}
	
	public function has($id){
		return $this->checkService($id);
	}
	
	public function get($id){
		return $this->getService($id);
	}
}