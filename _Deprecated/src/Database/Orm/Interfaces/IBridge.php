<?php

namespace Csifo\Database\Orm\Interfaces;

interface IBridge {
	public function init();
	public function get();
}