<?php

namespace Csifo\Database\Orm;

use Csifo\Database\Interfaces\ITransfer as ITransfer;
use Csifo\Cache\Interfaces\ICacheItemPool as ICacheItemPool;

class Builder implements Interfaces\IBuilder {
	
	protected $connection;
	protected $loader;

	/**
	* The built query itself
	*/
	protected $query = '';
	
	protected $tables = [];
	protected $selectFields = [];
	protected $where = [];
	protected $joins = [];
	protected $limit = '';
	protected $attributes = [];
	protected $groupBy = [];
	
	protected $selectTmp;
	
	/*
	* Need for cache [Implementing later...]
	*/
	protected $modelLine = 0;
	
	/*
	* Cache objects
	*/
	protected $cache;
	protected $cacheItem;
	
	public function __construct(ITransfer $connection,ICacheItemPool $cache){
		$this->connection = $connection;
		$this->loader	  = $this->connection->getLoader();
		/**
		* Prepare cache
		**/
		$this->cache	  = $cache;
		$this->cacheItem  = $cache->getItem('ORM_builder');
		if($this->cacheItem->get() === null) $this->cacheItem->set([]);
	}
	
	public function getLoader(){
		return $this->loader;
	}
	
	protected function checkCalledLine(){
		$arr = $this->cacheItem->get();
		return isset($arr[$this->modelLine]);
	}
	
	protected function setCalledLine(){
		if($this->modelLine > 0){
			$arr = $this->cacheItem->get();
			$arr[$this->modelLine] = $this->query;
			$this->cacheItem->set($arr);
		}
	}
	
	protected function getQuery(){
		if($this->checkCalledLine()){
			return $this->cacheItem->get()[$this->modelLine];
		}
	}
	
	public function emptyThis(){
		$this->tables 		= [];
		$this->query 		= '';
		$this->table		= '';
		$this->selectFields = [];
		$this->where 		= [];
		$this->joins 		= [];
		$this->limit 		= '';
		$this->attributes 	= [];
		$this->groupBy		= [];
		$this->selectTmp	= '';
		
		$this->currentQueryFile = '';
		$this->currentQueryLine = '';
	}

	public function getPdo(){
		return $this->connection;
	}
	
	protected function fields($select,$table = null){
		if($this->checkCalledLine()) return;
		if(($select.'') === 'Array'){
			$this->selectFields[$table] = $select;
		} else {
			if($select != '*'){
				$fields = explode(',',$select);
				$this->selectFields[$table] = $fields;
			} else {
				$this->selectFields[$table] = $this->loader->getColumns($table);
			}
		}
		$pk = $this->loader->getPrimaryKey($table);
		if(in_array($pk,$this->selectFields[$table]) === false) {
			$this->selectFields[$table][] = $pk;
		}
	}
	
	/*
	* Declares a mySQL variable
	*/
	public function declareVal($var,$value){
		$this->connection->GiveUpdate('SET @:var=:val;',[':var' => $var,':val' => $value]);
		return $this;
	}
	
	public function findByPk($table,$id){
		$pk = $this->loader->getPrimaryKey($table);
		return $this->select()->from($table)->where($pk,'=',$id)->run();
	}
	
	public function exists($table,$where = []){
		$whereString = '';
		foreach($where as $key => $val){
			$whereString .= $key . ' = ' . (strpos($key,':') === false ? ':' : '') . $key . ' AND ';
		}
		$whereString = rtrim($whereString,' AND ');
		$this->connection->Connect();
		return $this->connection->GiveExists('SELECT * FROM ' . $table . ' WHERE ' . $whereString,$where);
	}
	
	public function reWrite($table,$data,$where = []){
		$this->connection->Connect();
		$where = (empty($where) === true) ? $data : $where;
		$this->connection->UorI($table,$data,$where);
	}
	
	public function insert($table,$params){
		$q = 'INSERT INTO ' . $table . ' ';
		$updateParams = [];
		
		$insertColumns	= '(';
		$insertValues	= '(';
		
		foreach($params as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$insertColumns .= $column . ',';
			$insertValues .= $key . ',';
		}
		
		$insertColumns	= rtrim($insertColumns,',') . ') VALUES ';
		$insertValues	= rtrim($insertValues,',') . ') ';
		
		$q .= $insertColumns . $insertValues;
		
		$this->connection->Connect();
		return $this->connection->GiveUpdate($q,$updateParams);
	}
	
	public function delete($table,$where = [],$limit = 0){
		$q = 'DELETE FROM ' . $table . ' WHERE ';
		$updateParams = [];
		
		foreach($where as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$q .= $column . ' = ' . $key . ' AND ';
		}
		
		$q = rtrim($q,' AND ');
		
		if($limit > 0){
			$q.= ' LIMIT ' . $limit;
		}
		$this->connection->Connect();
		return $this->connection->GiveUpdate($q,$updateParams);
	}
	
	public function update($table,$params = [],$where = [],$limit = 0){
		$q = 'UPDATE ' . $table . ' SET ';
		$updateParams = [];
		
		foreach($params as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$q .= $column . ' = ' . $key . ',';
		}
		
		$q = rtrim($q,',');
		$q .= ' WHERE ';
		
		foreach($where as $column => $param){
			$key = (strpos($column,':') === false ? ':' : '') . $column;
			$updateParams[$key] = $param;
			$q .= $column . ' = ' . $key . ' AND ';
		}
		
		$q = rtrim($q,' AND ');
		
		if($limit > 0){
			$q.= ' LIMIT ' . $limit;
		}
		$this->connection->Connect();
		return $this->connection->GiveUpdate($q,$updateParams);
	}
	
	public function raw($query,$data = null){
		$this->connection->Connect();
		if(preg_match('#^[\S\s]+LIMIT 1$#i',$query) === 1){
			return $this->connection->GiveRow($query,$data);
		} else {
			return $this->connection->GiveAll($query,$data);
		}
	}
	
	public static function insertUpdateRaw($query,$data = null){
		return $this->connection->GiveUpdate($query,$data);
	}

	public function cacheSelect($key,$select = '*',$distinct = false){
		$this->modelLine = $key;
		return $this->select($select,false);
	}

	public function select($select = '*',$distinct = false){
		if($this->checkCalledLine()) return $this;
		
		$this->query = 'SELECT ' . ($distinct === true ? 'DISTINCT ' : '');
		$this->selectTmp = $select;
		return $this;
	}
	
	public function from($table){
		if($this->checkCalledLine()) return $this;
		
		$this->table = $table;
		$this->fields($this->selectTmp,$table);
		return $this;
	}
	
	public function orWhere($key, $operator, $value){
		$this->where($key, $operator, $value, 'OR');
		return $this;
	}
	
	public function where($key, $operator, $value, $type = ''){		
		if($this->checkCalledLine()) return $this;
			
		$hash = str_replace(['.'],'',$key);
		
		$this->where[] = ($type =! '' ? $type : '') . ' ' . $key . ' ' . $operator . ' :' . $hash;
		$this->attributes[':' . $hash] = $value;
		return $this;
	}
	
	public function limit($limit = '1',$offset = null){
		if($this->checkCalledLine()) return $this;
		
		if($offset === null){
			$this->limit = ' LIMIT ' . $limit;
		} else {
			$this->limit = ' LIMIT ' . $limit . ',' . $offset;
		}
		
		return $this;
	}
	
	public function rawJoin($table,$select = '*',$method = 'LEFT',$joinData){
		$this->joins[$table]['method'] = $method;
		$this->joins[$table]['raw'] = $joinData;
		if(!empty($select))
			$this->fields($select,$table);
		return $this;
	}
	
	public function join($table,$select = '*',$method = 'LEFT',$target = null,$fk_key = null,$pri_key = null){
		if($this->checkCalledLine()) return $this;
		
		$target = ($target === null ? $this->table : $target);
		$this->joins[$table]['method'] = $method;
		$fk_keyData = ($fk_key === null) ? $this->loader->getForeignKey($table,$target) : $table . '.' . $fk_key;
		/*
		* We have the commection only in 1 direction,so we need to switch to the
		* primary table, IF it's joins backward...
		*/
		$realTable = (preg_match('#^' . $table . '\.#i',$fk_keyData) == 1) ? $target : $table;
		
		$this->joins[$table]['fk_key'] = $fk_keyData;
		$this->joins[$table]['pr_key'] = $realTable . '.' . ($pri_key === null ? $this->loader->getPrimaryKey($table) : $pri_key);
		if(!empty($select))
			$this->fields($select,$table);
		return $this;
	}
	
	public function groupBy($value){
		$this->groupBy[] = $value;
		return $this;
	}
	
	protected function fetch($isVal){
		
		if($this->limit == ' LIMIT 1'){
			return $this->connection->GiveRow($this->query,$this->attributes);
		} else if($isVal !== ''){
			return $this->connection->GiveRow($this->query,$this->attributes)->$isVal;
		} else {
			return $this->connection->GiveAll($this->query,$this->attributes);
		}
	}
	
	/*
	* Creates a specified alias from a basic alias 
	* (If is not aliased,then return with the given column name)
	*/
	protected function checkAlias($table,$field){
		if(preg_match('#[\'"]*([\w]+?)[\'"]* as [\'"]*([\w]+)[\'"]*#i',$field,$realField) === 1){
			return [true,$realField[1] . ' as "' . $table . '.' . $realField[1] . '_aliased_' . $realField[2] . '"'];
		} else {
			return [false,$field];
		}
	}
	
	protected function buildQuery(){
		/*
		* Ha nincs cahce-elve a query string,akkor �sszerakjuk
		*/
		if(!$this->checkCalledLine()){
			$haveJoins = !empty($this->joins);
		
			foreach($this->selectFields as $table => $array){
				if(empty($array)){
					$columns = $this->loader->getColumns($table);
					foreach($columns as $field){
						$this->query .= ($haveJoins ? $table . '.' . $field . ' AS "' . $table . '.' . $field . '"' : $field ) . ',';
					}
				} else {
					foreach($array as $field){
						$fieldByAlias = $this->checkAlias($table,$field);
						/*
						* Handle Aliases...
						*/
						if($haveJoins){
							$this->query .= ($fieldByAlias[0] == true) ? $table . '.' . $fieldByAlias[1] : $table . '.' . $field . ' AS "' . $table . '.' . $field . '"';
						} else {
							$this->query .= ($fieldByAlias[0] == true) ? $table . '.' . $fieldByAlias[1] : $field;
						}
						$this->query .= ',';
					}
				}
			}
			$this->query = rtrim($this->query,',');
			$this->query .= ' FROM ' . $this->table . ' ';
			
			if($haveJoins){
				foreach($this->joins as $table => $joinData){
					if(isset($joinData['raw']) && !empty($joinData['raw'])){
						$this->query .= $joinData['method'] . ' JOIN ' . $table . ' ON ' . $joinData['raw'];
					} else {
						$this->query .= ' ' . $joinData['method'] . ' JOIN ' . $table . ' ON ' . 
						$joinData['pr_key'] . '=' . 
						$joinData['fk_key'];
					}
				}
			}
			
			$this->query .= (!empty($this->where) ? ' WHERE ' . implode(' ',$this->where) : '');
			$this->query .= (!empty($this->groupBy) ? ' GROUP BY ' . implode(',',$this->groupBy) : '');
			$this->query .= $this->limit;
			$this->setCalledLine();
			
		} else {
			$this->query = $this->getQuery();
		}
	}
	
	public function run($isVal = ''){
		$this->connection->Connect();
		$this->buildQuery();
		$all = $this->fetch($isVal);
		$this->emptyThis();
		return $all;
	}
	
	public function __destruct(){
		if($this->cacheItem->get() === null){
			$this->cache->save($this->cacheItem);
		}
	}
}