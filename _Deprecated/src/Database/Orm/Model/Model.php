<?php

namespace Csifo\Database\Orm\Model;

//use Csifo\Database\Interfaces\ITransfer as ITransfer;
use Csifo\Database\Orm\Interfaces\IBuilder;
use Csifo\Grammar\Plural;

class Model {
	
	protected $builder;
	protected $loader;
	
	protected $primaryTable;
	protected $primaryKey;
	
	protected $modified = [];
	protected $original = [];
	protected $aliases = [];
	
	public function __construct(IBuilder $builder,$table){ 
		$this->primaryTable = $table;
		$this->builder		= $builder;
		$this->loader	    = $this->builder->getLoader();
	}
	
	public function getOriginal(){
		return $this->original;
	}
	
	public function toDate($name,$format){
		if(preg_match('#[\d]{4}-[\d]{2}-[\d]{2}(?: [\d]{2}:[\d]{2}:[\d]{2})?#',$this->get($name)) === 1){
			if($format === 'elapsed'){
				return \Supply\Dater::elapsed($this->get($name));
			} else {
				return \Supply\Dater::format($this->get($name),$format);
			}
		}
	}
	
	public function toJson(){
		return json_encode($this->original);
	}
	
	protected function handleAlias($name){
		/*
		* $pure[1] -> Table name
		* $pure[2] -> Column name
		* $pure[3] -> Alias (if exists)
		*/
		if(preg_match('#([\w]+)\.([\w]+)_aliased_([\w.]+)#i',$name,$pure) == 1){
			$this->aliases[$pure[3]] = ['TABLE' => $pure[1],'COLUMN' => $pure[2]]; //Table name
			return [$pure[1],$pure[2],$pure[3]];
		} else if(preg_match('#([\w]+)\.([\w.]+)#i',$name,$data) == 1){
			return [$data[1],$data[2]];
		} else {
			return $name;
		}
	}
	
	public function get($name){
		if(!isset($this->aliases[$name])){
			$pureName = $this->handleAlias($name);
			if(is_array($pureName)){
				if(isset($this->modified[$pureName[0]][$pureName[1]])){
					return $this->modified[$pureName[0]][$pureName[1]];
				} else {
					//if(isset($this->original[$pureName[0]][$pureName[1]])){
						return $this->original[$pureName[0]][$pureName[1]];
					/*} else {
						/*
						* I know,it's VERY!!! awful...
						*/
						//dd(key($this->original));
						/*$key = key($this->original);
						$name = preg_replace('#^[\w_-]+?\.#i','',$name);
						return $this->original[$key][$name];
					}*/
				}
			} else {
				if(isset($this->modified[$this->primaryTable][$pureName])){
					return $this->modified[$this->primaryTable][$pureName];
				} else {
					return $this->original[$this->primaryTable][$pureName];
				}
			}
		} else {
			$realNameData = $this->aliases[$name];
			if(isset($this->modified[$realNameData['TABLE']][$realNameData['COLUMN']])){
				return $this->modified[$realNameData['TABLE']][$realNameData['COLUMN']];
			} else {
				return $this->original[$realNameData['TABLE']][$realNameData['COLUMN']];
			}
			
		}
	}
	
	public function set($name, $val){
		/*
		* Check for alias 
		*/
		if(!isset($this->aliases[$name])){
			$pureName = $this->handleAlias($name);
			if(is_array($pureName)){
				if(isset($this->original[$pureName[0]][$pureName[1]])){
					$this->modified[$pureName[0]][$pureName[1]] = $val;
				} else {
					$this->original[$pureName[0]][$pureName[1]] = $val;
				}
			} else {
				if(isset($this->original[$this->primaryTable][$pureName])){
					$this->modified[$this->primaryTable][$pureName] = $val;
				} else {
					$this->original[$this->primaryTable][$pureName] = $val;
				}
			}
		} else {
			$realNameData = $this->aliases[$name];
			$this->modified[$realNameData['TABLE']][$realNameData['COLUMN']] = $val;
		}
	}
	
	/*
	* Saves the data into the database
	*/
	public function save(){
		foreach($this->modified as $table => $columns){
			$primary_key = $this->loader->getPrimaryKey($table);
			
			$q = 'UPDATE ' . $table . ' SET';
			$d = [];
			foreach($columns as $name => $value){
				$q .= ' ' . $name . ' = :' . $name;
				$d[':' . $name] = $value;
			}
			
			$q .= ' WHERE ' . $primary_key . ' = :prim_' .  $primary_key . ';';
			
			$d[':prim_' .  $primary_key] = $this->original[$table][$primary_key];

			$this->connection->GiveUpdate($q,$d);
		}
	}
	
	/* ========== MAGIC METHODS ========== */
	
	public function __set($name, $val){
		return $this->set($name, $val);
	}
	
	public function __get($name){
		return $this->get($name);
	}
	
	public function __call($method,$args){
		$isPlural = Plural::isPlural($method);
		
		$table_name = ($isPlural) ? Plural::getNonPluralExtended($method) : $method;
		$foreign_key = $this->loader->getForeignKey($table_name,$this->primaryTable);
		
		//$result = $this->builder->select(implode(',',$args))->where('','=',)->run();
	}
}