<?php

namespace Csifo\Database\Orm;

use Csifo\Database\Orm\Interfaces\IBridge;

class Bridge implements IBridge {
	protected static $builder;
	
	public function init(){
		if(self::$builder === null){
			self::$builder = resolve('Csifo\Database\Orm\Interfaces\IBuilder');
		}
		
		return self::$builder;
	}
	
	public function get(){
		return self::$builder;
	}
}