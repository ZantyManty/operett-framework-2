<?php

namespace Csifo\Database;

use \PDO;
use Csifo\Core\Interfaces\IConfig as IConfig;

class Connection implements Interfaces\IConnection
{    
	protected static $pdo;
	
	protected static $host;
	protected static $db;
	protected static $user;
	protected static $pass;
	
	public function __construct(IConfig $config)
	{
		$db = $config->get('db');
		static::$host = $db['DBHOST'];
		static::$db   = $db['DBNAME'];
		static::$user = $db['DBUSER'];
		static::$pass = $db['DBPASS'];
		unset($db);
	}
	
	public function Connect(){
		if(self::$pdo === null){
			try {
				//sprintf("mysql:host=%s;dbname=%s;charset=utf8",static::$host,static::$db)
				if(static::$host != ''){
					self::$pdo = new \PDO(
						'mysql:host=' . static::$host . ';dbname=' . static::$db . ';charset=utf8',
						static::$user,
						static::$pass,
						[
							PDO::ATTR_ERRMODE 			 => PDO::ERRMODE_EXCEPTION,
							PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
						]
					);
				}
			} catch (PDOException $e) {
				die('Connection failed: ' . $e->getMessage());
			}
			/*
			* Other options...
			*/
			self::$pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, false);
		}
		
		return self::$pdo;
	}
	
	/*
	* Close the connection
	*/
	public function __destruct()
	{
		$pdo = null;
		unset($pdo);
	}
}