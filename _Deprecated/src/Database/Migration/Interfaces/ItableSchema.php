<?php

namespace Csifo\Database\Migration\Interfaces;

interface ItableSchema {
	
	public function getTableName();
}