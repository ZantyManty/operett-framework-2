<?php

namespace Csifo\Database\Migration;

use Csifo\Database\Migration\Interfaces\ItableSchema as ItableSchema;

class tableSchema implements ItableSchema {
	
	protected $schema;
	protected $name = '';
	protected $collation;
	protected $engine = 'MyISAM';
	protected $autoIncrement = 0;
	protected $comment = '';
	
	public function __construct(Schema $schema){
		$this->schema = $schema;
	}
	
	public function name($name){
		$this->name = $name;
		return $this;
	}
	
	public function collation($collation){
		$this->collation = $collation;
		return $this;
	}
	
	public function engine($engine){
		$this->engine = $engine;
		return $this;
	}
	
	public function autoIncrementFrom($int){
		$this->autoIncrement = $int;
		return $this;
	}
	
	public function addField(){
		return $this->schema;
	}
	
	public function getTableName(){
		return $this->name;
	}
	
	public function getComment(){
		return $this->comment;
	}
	
	public function comment($comment){
		$this->comment = $comment;
	}
	
	public function __toString(){
		$output = "CREATE TABLE `{$this->name}` (" . PHP_EOL;
		$output .= (string)$this->schema;
		$output .= ")" . PHP_EOL;
		$output .= "COLLATE='{$this->collation}'" . PHP_EOL;
		$output .= "ENGINE='{$this->engine}'" . PHP_EOL;
		
		if($this->autoIncrement > 0){
			$output .= "AUTO_INCREMENT={$this->autoIncrement}" . PHP_EOL;
		}
		
		if($this->comment != ''){
			$output .= "COMMENT='{$this->comment}'" . PHP_EOL;
		}
		
		$output .= ";";
		
		return $output;
	}
	
}