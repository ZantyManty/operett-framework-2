<?php

namespace Csifo\Database\Iterators;

class NormalIterator implements \Iterator { 
	
	protected $data;
	protected $cursor = 0;
	protected $item;
	
	public function __construct($data){
		$this->data = $data;
	}
	
	public function next(){
		$this->item = $this->data[$this->cursor];
		$this->cursor++;
	}
	
	public function current(){
		return $this->item;
	}
	
	public function rewind() {
		$this->cursor = 0;
	}
	
	public function key() {
		return $this->cursor;
	}
	
	public function valid(){
		return ($this->item === false) ? false : true;
	}
	
	public function toJson(){
		$json = '[';
		while($this->valid() === true){
			$json .= $this->current()->toJson() . ',';
			$this->next();
		}
		$json = rtrim($json,',');
		return $json . ']';
	}
}