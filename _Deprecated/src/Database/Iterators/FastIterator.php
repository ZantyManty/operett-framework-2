<?php

namespace Csifo\Database\Iterators;
use \PDO;

class FastIterator implements \Iterator {
	
	protected $stmt;
	protected $cursor = 0;
	protected $item;
	protected $class;
	protected $table;
	protected $connection;
	
	public function __construct($statement,$class,$connection,$table = null){
		$this->stmt = $statement;
		$this->class = $class;
		$this->table = $table;
		$this->connection = $connection;
		$this->stmt->setFetchMode(\PDO::FETCH_INTO, new $this->class($this->connection,$this->table));
		$this->next();
	}
	
	public function next(){
		$this->cursor++;
		//Minden rekord egy �j objektumba ker�l
		
		$this->item = $this->stmt->fetch(
            \PDO::FETCH_ORI_NEXT,
			$this->cursor
		);
	}
	
	public function current(){
		return $this->item;
	}
	
	public function rewind() {
		$this->cursor = 0;
	}
	
	public function key() {
		return $this->cursor;
	}
	
	public function valid(){
		return ($this->item === false) ? false : true;
	}
	
	public function toJson(){
		$json = '[';
		while($this->valid() === true){
			$json .= $this->current()->toJson() . ',';
			$this->next();
		}
		$json = rtrim($json,',');
		return $json . ']';
	}
}