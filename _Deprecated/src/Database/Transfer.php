<?php

namespace Csifo\Database;

use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Database\Interfaces\IConnection as IConnection;
//use Csifo\Database\Loader\Interfaces\ILoader as ILoader;
use \PDO;

use Csifo\Database\Orm\Interfaces\IBridge;

class Transfer implements Interfaces\ITransfer
{
	protected $connection;
	protected static $pdo = null; //Adatb�zis objektum
	
	protected static $iteratorClass;
	protected static $mode;
	protected static $models;
	protected static $loader;
	
	protected static $bridge;
	
	public function __construct(IConnection $connection,/*ILoader $loader,*/IConfig $config,IBridge $bridge){
		if($this->connection == null) { $this->connection = $connection; }
		$dbData = $config->get('db');
		static::$models = $dbData['MODELS'];
		//static::$loader = $loader;
		
		static::$bridge = $bridge;
		
		$this->setMode($dbData['MODE'],$dbData['ITERATORS']);
	}
	
	public function getLoader(){
		return self::$loader;
	}
	
	public function Connect(){
		self::$pdo = $this->connection->Connect();
	}
	
	protected function setMode($mode,$iterators){
		if(strtolower($mode) == 'auto'){
			/*
			* Fast
			*/
			if(/*$_SERVER['SERVER_ADDR']*/'127.0.0.1' == static::$host || strpos($_SERVER['HTTP_HOST'], static::$host) !== false){
				static::$iteratorClass = $iterators['FAST'];
				static::$mode = 'fast';
			}
			/*
			* Normal
			*/
			else {
				static::$iteratorClass = $iterators['NORMAL'];
				static::$mode = 'normal';
			}
		} else {
			static::$iteratorClass = $iterators[strtoupper($mode)];
			static::$mode = strtolower($mode);
		}
	}
	
	public function GiveExists($query,$executeArray = null) //Igazat,vagy hamisat ad vissza
	{
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		$count = $q->rowCount();
		switch($count)
		{
			case 0:
				return false;
			default:
				return true;
		}
	}
	
	public function GiveCount($query,$executeArray = null){
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		return $q->rowCount();
	}
	
	public function GiveRow($query,$executeArray = null,$modeAssoc = false) //Csak 1 sort ad vissza
	{
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		$count = $q->rowCount();
		
		if($modeAssoc == false){
			//OLD: FROM[\s]*([\w]+?)\b
			if(preg_match('#(?:^SELECT[\s\S]+?FROM[\s]*(?|([\w]+?)\b|\([\S\s]+\) AS ([\w]+)))|FROM[\s]*(?|[\w]+)#i',$query,$m)){
				$primaryTable = (isset($m[1]) && !empty($m[1])) ? $m[1] : '--default--';
				
			} else {
				$primaryTable = '--default--';
			}
			
			switch($count){
					case 0:
						return false;
					default:
						$q->setFetchMode(PDO::FETCH_INTO,new static::$models['BASIC'](static::$bridge->get(),$primaryTable));
						$eredm = $q->fetch();
						return $eredm;
				}
			
		} else {
			switch($count)
			{
				case 0:
					return false;
				default:
					$eredm = $q->fetch(PDO::FETCH_ASSOC);
					return $eredm;
			}
		}
	}
	
	public function GiveVal($query,$executeArray = null){
		$arr = array();
		$q = self::$pdo->prepare($query);
		$q->execute($executeArray);
		$count = $q->rowCount();
		switch($count)
		{
			case 0:
				return false;
			default:
				$eredm = $q->fetch(PDO::FETCH_ASSOC);
				if(count($eredm) == 1 && $eredm != false && reset($eredm) != NULL){
					$flipped = array_flip($eredm);
					$key = end($flipped);
					unset($flipped);
					return $eredm[$key];
				} else {return false; }
		}
	}
	
	public function GiveAll($query,$executeArray = null,$modeAssoc = false) //T�bb sort ad vissza
	{
		if($modeAssoc == false){
			//OLD: FROM[\s]*([\w]+?)\b
			//OLD2: (?:^SELECT[\s\S]+?FROM[\s]*(?|([\w]+?)\b|\([\S\s]+\) AS ([\w]+)))|(?:FROM[\s]*(?|([\w]+?)))
			if(preg_match('#(?:^SELECT[\s\S]+?FROM[\s]*(?|([\w]+?)\b|\([\S\s]+\) AS ([\w]+)))|FROM[\s]*(?|[\w]+)#i',$query,$m)){
				$primaryTable = (isset($m[1]) && !empty($m[1])) ? $m[1] : '--default--';
				$iteratorClass = static::$iteratorClass;
				
				if(static::$mode === 'fast'){
					$q = self::$pdo->prepare($query,[PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
					$q->execute($executeArray);
					return new $iteratorClass($q,static::$models['BASIC'],static::$bridge->get()/*$this*/,$primaryTable);

				} else {
					$q = self::$pdo->prepare($query);
					$q->execute($executeArray);
					$count = $q->rowCount();
					switch($count)
					{
						case 0:
							return false;
						default:
							$data = $q->fetchAll(\PDO::FETCH_CLASS,static::$models['BASIC'],[$this,$primaryTable]);	
							return new $iteratorClass($data);
					}
				}
			} else {
				return false;
			}
			
		} else {
			$q = self::$pdo->prepare($query);
			$q->execute($executeArray);
			$count = $q->rowCount();
			switch($count)
			{
				case 0:
					return false;
				default:
					return $q->fetchAll(PDO::FETCH_ASSOC);	
			}
		}
	}
	
	public function GiveUpdate($query,$executeArray = null) //Beilleszt�s VAGY Friss�t�s VAGY T�rl�s t�bl�ba
	{
		self::$pdo->beginTransaction();
		try
		{
			$q = self::$pdo->prepare($query);
			if(!is_null($executeArray))
				$q->execute($executeArray);
			else
				$q->execute();
			
			$id = (int)self::$pdo->lastInsertId('id');
			self::$pdo->commit();
			return $id;
		}
		catch(Expection $e)
		{
			self::$pdo->rollBack();
			return $e->getMessage();
		}
	}
	
	public function GiveColumns($table)
	{
		$query = "SELECT COLUMN_NAME
					FROM   information_schema.columns
					WHERE  table_name = ':table' AND TABLE_SCHEMA = ':db'
					ORDER  BY ordinal_position ";
		$data = [':table' => $table, ':db' => DBNAME];
		return $this->GiveAll($query,$data);
	}
	
	public function UorI($table,$executeArray,$where) //Eld�nti,hogy van-e m�r ilyen bejegyz�s a t�bl�ban. Ha van,akkor UPDATE,ha nincs,akkor INSERT
	{
		$whereString = 'WHERE ';
		
		foreach($where as $k => $v){
			$whereString .= $k . ' = :' . $k . ' AND ';
		}
		$whereString = rtrim($whereString,' AND ');
		
		$queryA = "SELECT * FROM `".$table."` ".$whereString;
		if(!$this->GiveExists($queryA,$where)) //Beilleszt�s (Ha m�g nem l�tezik)
		{
			$query = "INSERT INTO `".$table."` ";
			$rowNames = "(";
			$values = " VALUES (";
			
			foreach($executeArray as $k => $v){
				$rowNames .= $k . ',';
				$values   .= ':' . $k . ',';
			}
			
			$rowNames .= ")";
			$values .= ")";
			$query .= $rowNames.$values;
			$query = str_replace(",)",")",$query); //Kis korrekt�ra

			$this->GiveUpdate($query,$executeArray);
		}
		else //Friss�t�s
		{ 
			$query = "UPDATE `".$table."` SET ";
			
			foreach($executeArray as $k => $v){
				$query .= $k . '=:' . $k . ',';
			}
			$query = rtrim($query,',');
			$query .= ' ' . $whereString;
			$arr = array_merge($executeArray,$where);

			$this->GiveUpdate($query,$arr);
		}
	}
	
	public function __destruct() //Kapcsolat bez�r�sa
	{
		$pdo = null;
		unset($pdo);
	}
}