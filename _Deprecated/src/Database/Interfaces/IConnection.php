<?php

namespace Csifo\Database\Interfaces;

interface IConnection {
	
	public function Connect();
}