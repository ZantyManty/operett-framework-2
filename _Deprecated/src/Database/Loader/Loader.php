<?php

namespace Csifo\Database\Loader;

use Csifo\Database\Interfaces\IConnection as IConnection;
use Csifo\Database\Loader\Interfaces\IBuilder as IBuilder;
use Csifo\Grammar\Plural as Plural;
use Csifo\Cache\Interfaces\ICacheItemPool as ICacheItemPool;

class Loader implements Interfaces\ILoader {
	
	protected $metaData;
	protected $builder;
	
	protected $cache;
	protected $cacheItem;
	
	public function __construct(IBuilder $builder,ICacheItemPool $cache){
		$this->builder	  = $builder;
		$this->cache	  = $cache;
		
		/*
		* Prepare cache...
		*/
		$this->cacheItem  = $this->cache->getItem('ORM_loader');
		$cacheData = $this->cacheItem->get();

		if($cacheData === null){
			$this->metaData = $this->builder->buildCache();
			$this->cacheItem->set($this->metaData);
		} else {
			$this->metaData = $cacheData;
		}
	}
	
	public function getColumns($table){
		return $this->metaData['TABLES'][$table];
	}
	
	public function getPrimaryKey($table){
		return $this->getColumns($table)['PRIMARY'];
	}
	
	protected function findForeignByPrimary($primary_table,$target_table,$primary_table_prim_key){
		$connections = $this->metaData['CONNECTIONS'];
		/*
		* Direction 1:
		*/
		if(isset($connections[$primary_table][$primary_table_prim_key][$target_table])){
			$nonPlural = Plural::getNonPluralExtended($primary_table);
			$primary_table_foreign_keys = $connections[$primary_table][$primary_table_prim_key][$target_table];
			$returnTable = $target_table;
		} 
		/*
		* Direction 2:
		*/
		else {
			$nonPlural = Plural::getNonPluralExtended($target_table);
			$primary_table_foreign_keys = $connections[$target_table][$primary_table_prim_key][$primary_table];
			$returnTable = $primary_table;
		}
		
		foreach($primary_table_foreign_keys as $foreign_key => $dummy){
			if(preg_match('#' . $nonPlural . '[_-]' . $primary_table_prim_key . '#i',$foreign_key) === 1){
				return $returnTable . '.' . $foreign_key;
			}
		}
	}
	
	public function getForeignKey($table,$target,$key = ''){		
		$prim_key1 = ($key == '') ? $this->getPrimaryKey($table) : $key;
		return $this->findForeignByPrimary($table,$target,$prim_key1);
	}
	
	public function __destruct(){
		if($this->cacheItem->get()){
			$this->cache->save($this->cacheItem);
		}
	}
}