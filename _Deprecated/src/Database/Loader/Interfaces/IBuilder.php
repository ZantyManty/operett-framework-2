<?php

namespace Csifo\Database\Loader\Interfaces;

interface IBuilder {

	public function buildCache();

}