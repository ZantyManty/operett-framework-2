<?php

namespace Csifo\Database\Loader\Interfaces;

interface ILoader {
	
	public function getColumns($table);
	
	public function getPrimaryKey($table);
	
	public function getForeignKey($table,$target);
}