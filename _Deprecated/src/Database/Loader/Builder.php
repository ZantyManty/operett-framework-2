<?php

namespace Csifo\Database\Loader;

use Csifo\Database\Interfaces\ITransfer as ITransfer;
use Csifo\Grammar\Plural as Plural;

class Builder implements Interfaces\IBuilder {
	
	protected static $connection;
	
	public function __construct(ITransfer $connection){
		if(self::$connection == null){
			self::$connection = $connection;
		}

		echo '01<br>';
	}
	
	public function buildCache(){
		$trunk = [];
		
		$tables = $this->getTables();
		
		if($tables){
			foreach($tables as $table){
				//Table meta data (Columns,primary key,etc...)
				$column_name = key($table);
				$trunk['TABLES'][$table[$column_name]] = $this->getTableData($table[$column_name]);
			}
		}
		
		/** Get connections **/
		$connections = $this->getConnections();
		if($connections){
			foreach($connections as $connection){
				$trunk['CONNECTIONS'][$connection['table']][$connection['column']][$connection['t_table']][$connection['t_column']] = true;
			}
		}
		
		/** Build connections by name (If doesn't InnoDB) **/
		$connectionsByName = $this->buildConnectionsByName($trunk);
		
		$trunk['CONNECTIONS'] = isset($trunk['CONNECTIONS']) ? array_merge($trunk['CONNECTIONS'],$connectionsByName) : $connectionsByName;
		
		return $trunk;
	}
	
	protected function buildConnectionsByName($trunk){
		$connections = [];
		/**
		* In the $trunk['TABLES'] array,we have the columns
		*/
		foreach($trunk['TABLES'] as $table => $columns){
			$table_name = Plural::getNonPluralExtended($table);
			$pattern = '#^' . $table_name . '[-_]\S+$#i';

			foreach($trunk['TABLES'] as $tableInnner => $tableMeta){
				/*
				* Let's check the columns
				*/
				foreach($tableMeta['COLUMNS'] as $columnName => $columnData){
					if(preg_match($pattern,$columnName)){
						$column = preg_replace('#' . $table_name . '[-_]#i','',$columnName);
						/*
						* If the generated column is really exists
						*/
						if(isset($trunk['TABLES'][$table]['COLUMNS'][$column])){
							$connections[$table][$column][$tableInnner][$columnName] = true;
						}
					}
				}
			}
		}
		
		return $connections;
	}
	
	protected function getTableData($table){
		$trunk = [];
		
		/** Get primary key **/
		$query = "SHOW INDEX FROM {$table} WHERE Key_name = 'PRIMARY';";
		$key_row = self::$connection->GiveRow($query,null,true);
		$trunk[$key_row['Key_name']] = $key_row['Column_name'];
		
		/** Get columns **/
		$query = "DESCRIBE {$table};";
		$columns = self::$connection->GiveAll($query,null,true);
		if($columns){
			foreach($columns as $column){
				$trunk['COLUMNS'][$column["Field"]] = $column;
				unset($trunk['COLUMNS'][$column["Field"]]["Field"]);
			}
		}
		return $trunk;
	}
	
	protected function getTables(){
		return self::$connection->GiveAll('SHOW TABLES;',null,true);
	}
	
	protected function getConnections(){
		$query = "SELECT
			i.TABLE_NAME AS 'table', 
			k.COLUMN_NAME AS 'column',
			k.REFERENCED_TABLE_NAME AS 't_table',
			k.REFERENCED_COLUMN_NAME AS 't_column' 
		FROM information_schema.TABLE_CONSTRAINTS i 
			LEFT JOIN information_schema.KEY_COLUMN_USAGE k 
			ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME 
		WHERE i.CONSTRAINT_TYPE = 'FOREIGN KEY' AND i.TABLE_SCHEMA = DATABASE();";
		return self::$connection->GiveAll($query,null,true);
	}
}