<?php

namespace Csifo\Mvc\View\Template;

use Csifo\Mvc\View\Template\Interfaces\ITemplate;
use Csifo\Http\Router\Path\Interfaces\IGenerator;
use Csifo\Mvc\View\Form\Interfaces\IBuilder;

class Template implements ITemplate {
	
	protected $frame = '\|';
	
	protected $urlGenerator	= null;
	protected $formBuilder	= null;
	
	public function __construct(IGenerator $urlGenerator,IBuilder $formBuilder){
		$this->urlGenerator = $urlGenerator;
		$this->formBuilder = $formBuilder;
	}
	
	public function parse($content){
		
		$content = $this->parseFors($content);
		$content = $this->parseForeaches($content);
		$content = $this->parseIf($content);
		$content = $this->parseFunctions($content);
		$content = $this->parseForms($content);
		$content = $this->parseRoutes($content);
		$content = $this->parseBlocks($content);
		$content = $this->parseObjectCalls($content);
		return $this->parseVariables($content);
	}
	
	protected function parseForms($content){
		if(preg_match_all('#' . $this->frame . ' *form *: *([\w_-]+?) *' . $this->frame . '#i',$content,$match)){
			foreach($match[0] as $key => $syntax){
				$formHtml = $this->formBuilder->build($match[1][$key]);
				$content = str_replace($syntax,$formHtml,$content);
			}
		}
		return $content;
	}
	
	protected function parseFunctions($content){
		if(preg_match_all('#' . $this->frame . ' *([\w-]+?\([\S\s]*?\)) *' . $this->frame . '#i',$content,$match)){
			foreach($match[0] as $key => $m){
				$content = str_replace($m,'<?php echo ' . trim($match[1][$key]) . '; ?>',$content);
			}
		}
		
		return $content;
	}
	
	protected function parseObjectCalls($content){
		if(preg_match_all('#' . $this->frame . ' *(\$[\w_-]+?->[\w_-]+? *\([\S\s]*?\)) *' . $this->frame . '#i',$content,$match)){
			$i = 0;
			foreach($match[0] as $syntax){
				$content = str_replace($syntax,'<?php echo ' . $match[1][$i] . '; ?>',$content);
				++$i;
			}
		} return $content;
	}
	
	protected function parseRoutes($content){

		if(preg_match_all('#' . $this->frame . '\/ *([\w]+?) *: *\[ *([\S\s]*?) *\] *' . $this->frame . '#i',$content,$match)){
			$i = 0;
			foreach($match[1] as $routeName){
				if(!empty($match[2][$i])){
					$routeParams = trim($match[2][$i]);
					if(preg_match_all('#([\w_-]+?) *=*> *([\S\s]+?)(?:,|$)#i',$routeParams,$paramMatch)){
						$normalisedPathParams = [];
						$j = 0;
						foreach($paramMatch[1] as $paramName){
							$paramVariable = trim($paramMatch[2][$j]);
							$normalisedPathParams[$paramName] = (substr($paramVariable,0,1) == "'" || substr($paramVariable,0,1) == '"') ? trim($paramVariable,'"\'') : "' . " . $paramVariable . " . '";
							++$j;
						}
					}
				} else {
					$normalisedPathParams = [];
				}
				$generatedUrl = $this->urlGenerator->generatePath($routeName,$normalisedPathParams);
				$content = str_replace($match[0][$i],"<?php echo '" . $generatedUrl . "'; ?>",$content);
				++$i;
			}
		}
		
		return $content;
	}

	protected function parseVariables($content){
		
		if(preg_match_all('#' . $this->frame . ' *((?<!for|fe|else|if|/if)(?:\$|[\w_-]+?)[\S\s]*?(?:[\'"]}|\)|[\w]+|\]|;)) *' . $this->frame . '#ui',$content,$vars)){
			foreach($vars[1] as $full_var_key => $var){
				/*
				* Explode the vars with regex
				*
				* E.g. we have: |$ds->{'sertertdf'} - $ds->{"sertertdf"}|
				*
				* Then we have: 
				*				1: $ds->{'sertertdf'}
				*				2: - $ds->{"sertertdf"}
				*/
				if(preg_match_all('#(([=*+\/% -]*)(?:\d+|\$[\w]+(?:\[ *([\'"])[\S\s]+?\3 *\]|->(?:{([\'"]{1})[\S\s]*?\4}|[\w_-]+?\((?:([\'"])[\S\s]*?\5|[\d.]+)?\)))?))| *(=? *[\w_-]+?\((?:[\S\s]+,?)\))#ui',$var,$pieces)){
					$imploded = '';
					/*
					* If ith has an = sign in it,then is a declaration,so the short echo is useless!
					*/
					$hasEqualSign = false;
					
					foreach($pieces[1] as $key => $piece){
							/*
							* If we have a function call
							*/
							if(!empty($pieces[6][$key])){
								/*
								* If the first char is =,then we have a declaration
								*/
								if(substr($pieces[6][$key],0,1) == '='){
									$hasEqualSign = true;
								}
								
							} else if (!empty($piece)){
								$hasEqualSign = (trim($pieces[2][$key]) == '=') ? true : false;
							}
							
							/*
							* If there's no variable,then we have a function call,in the 6. capture group
							*/
							$imploded .= (!empty($piece)) ? trim($piece) : trim($pieces[6][$key]);
					}
					
					if($hasEqualSign === false){
						$imploded = '<?php echo ' . $imploded . '; ?>';
					} else {
						$imploded = '<?php ' . $imploded . (substr($imploded,-1) != ';' ? ';' : '') . ' ?>';
					}
				}
				
				$content =  str_replace($vars[0][$full_var_key],$imploded,$content);
			}
		}
		
		return $content;
	}

	protected function parseForeaches($content){
		$content = preg_replace('#' . $this->frame . '\s*fe\(\s*(\$[\w\[\]\'"\(\)>-]+) +as +(\$\w+(?:\s*=>\s*\$[\w]+)*)\s*\)\s*' . $this->frame . '#i','<?php foreach($1 as $2): ?>',$content);
		return preg_replace('#' . $this->frame . ' */fe *' . $this->frame . '#','<?php endforeach; ?>',$content);
	}

	protected function parseIf($content){
		if(preg_match_all('#' . $this->frame . ' *(else)?if\(([\S\s]*?)\)' . $this->frame . '(?:[\s]+?|[\w]|$|\r*\n)#ui',$content,$ifMatch)){
			
			foreach($ifMatch[2] as $key => $ifContent){
				$replaceString = '<?php if(';
				/*
				* If we have "elseif"
				*/
				if(isset($ifMatch[1][$key]) && !empty($ifMatch[1][$key])){
					$replaceString = str_replace('if','elseif',$replaceString);
				}
				$replaceString .= trim($ifContent) . '): ?>';
				$content = str_replace($ifMatch[0][$key],$replaceString,$content);
			}

			/*
			* Replace "else" and "eif"(End if)
			*/
			$content = preg_replace('#' . $this->frame . ' *else *' . $this->frame . '#ui','<?php else: ?>',$content);
			$content = preg_replace('#' . $this->frame . ' */if *' . $this->frame . '#ui','<?php endif; ?>',$content);
			
			return $content;
			
		} 
		/*
		* One line ifs...
		*/
		else if(preg_match_all('#(' . $this->frame . ' *(else)?if\(([\S\s]*?)\)' . $this->frame . ') *([^\n\r]+?)' . $this->frame . '\/ *if *' . $this->frame . '#i',$content,$ifOneLineMatch)){

			foreach($ifOneLineMatch[3] as $key => $ifContent){
				$replaceString = '<?php if(';
				/*
				* If we have "elseif"
				*/
				if(isset($ifOneLineMatch[2][$key]) && !empty($ifOneLineMatch[2][$key])){
					$replaceString = str_replace('if','elseif',$replaceString);
				}
				$replaceString .= trim($ifContent) . '){ ?>';
				$content = str_replace($ifOneLineMatch[1][$key],$replaceString,$content);
			}
			
			/*
			* Replace "else" and "eif"(End if)
			*/
			$content = preg_replace('#' . $this->frame . ' *else *' . $this->frame . '#ui','<?php } else { ?>',$content);
			$content = preg_replace('#' . $this->frame . ' */if *' . $this->frame . '#ui','<?php } ?>',$content);
			
			return $content;
			
		} else {
			return $content;
		}
	}
	
	protected function parseBlocks($content){
		if(preg_match_all('#' . $this->frame . ' *php *' . $this->frame . '([\S\s]+?)(?:' . $this->frame . ' *\/php *' . $this->frame . ')#ui',$content,$blocks)){
			foreach($blocks[1] as $key => $block){
				$content = str_replace($blocks[0][$key],'<?php ' . $block . ' ?>',$content);
			}
		}
		
		return $content;
	}

	/*
	* For loops
	*/
	protected function parseFors($content){
		/*
		* If it's a "step for" e.g.: |for(10)|
		*/
		if(preg_match_all('#' . $this->frame . ' *for *\( *(\d+?) *\) *' . $this->frame . '#i',$content,$match_step)){
			$used_vars = [];
			foreach($match_step[1] as $key => $number){
				/*
				* Because the variable name is not given,we use the calssic "$i"
				* BUT if we have nested shit,we must generate uniqe $i vars...
				*/
				do {
					$generatedVar = rand(0,5000);
				} while(in_array($generatedVar,$used_vars));
				
				$used_vars[] = $generatedVar;
				$generatedVar = '$i_' . $generatedVar;
				$content = str_replace($match_step[0][$key],'<?php for(' . $generatedVar . ' = 0; ' . $generatedVar . ' <= ' . $number . '; ++' . $generatedVar . '): ?>',$content);
			}
			
		}
		/*
		* Casual for
		*/
		if(preg_match_all('#' . $this->frame . ' *(for\([\S\s]+?)(?:\)' . $this->frame . ')+#ui',$content,$match_for)){

			foreach($match_for[0] as $key => $full_match){
				$temp = preg_replace('#^ *' . $this->frame . '#','<?php ',$full_match);
				$temp = preg_replace('# *' . $this->frame . ' *$#',': ?>',$temp);
				 
				 $content = str_replace($full_match,$temp,$content);
			}
		}
		
		$content = preg_replace('#' . $this->frame . ' *\/for *' . $this->frame . '#i','<?php endfor; ?>',$content);

		return $content;
	}
}