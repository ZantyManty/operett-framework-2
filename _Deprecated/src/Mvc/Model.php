<?php

namespace Csifo\Mvc;

//use Csifo\Database\Orm\Interfaces\IBuilder as IBuilder;

use Csifo\Database\Orm\Interfaces\IQuery;

class Model implements Interfaces\IModel {
	
	protected $sql;
	
	public function setBuilder(IQuery $builder){
		$this->sql = $builder;
	}
	
}