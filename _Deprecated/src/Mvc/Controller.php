<?php

namespace Csifo\Mvc;

use Csifo\Mvc\Interfaces\IView as IView;
use Csifo\Database\Orm\Interfaces\IQuery as IQuery;

class Controller implements Interfaces\IController {
	
	protected $view;
	protected $model;
	
	protected $module;
	protected $action;
	
	public function init($model,IView $view,$module,$action,IQuery $builder){
		$this->view		= $view;
		$this->model	= $model;
		$this->module	= $module;
		$this->action	= $action;
		$this->setBuilder($builder);
	}
	
	protected function setBuilder($builder){
		$this->model->setBuilder($builder);
		unset($builder);
	}
	
}