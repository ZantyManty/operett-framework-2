<?php

namespace Csifo\Mvc\Interfaces;

//use Csifo\Database\Orm\Interfaces\IBuilder as IBuilder;
use Csifo\Database\Orm\Interfaces\IQuery;

interface IController {
	
	public function init($model,IView $view,$module,$action,IQuery $builder);
	
}