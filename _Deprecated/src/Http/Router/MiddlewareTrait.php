<?php

namespace Csifo\Http\Router;

trait MiddlewareTrait {
	
	protected $middlewares = [];
	
	public function addMiddlewareArray($midlewareArray){
		if(is_array($midlewareArray)){
			/*
			* Megfordítjuk a tömböt,így az UTOLSÓ kerül ELŐRE,ami után ugye NINCS más.
			* A "$midlewareArray" üres,tehát nincs elötte (Mármint utána,de most fordítva van a tömb!!) 
			* elem,így a controller fog oda kerülni a $next-be.
			*/
			$mids = array_reverse($midlewareArray);
			/*
			* Végigmegyünk a már megfordított tömbön
			*/
			foreach($mids as $mid){
				$this->addMiddleware($mid);
			}
		} else if (is_string($midlewareArray)) {
			$this->addMiddleware($midlewareArray);
		}
	}
	
	/*
	* Add a middleware (Create $next() callable)
	*/
	protected function addMiddleware($className){
		/*
		* Ha üres a tömb,akkor ez az utolsó middleware
		*    (---!!!Ne feledjük,array_revers-elt tömb első elemét kapjuk,ami az utolsó middleware!!!---),
		* akkor ide már a Controller feloldása jön.
		*/
		$next = (empty($this->middlewares)) ? $this->createControllerCallable() : end($this->middlewares);
		
		$this->middlewares[] = function($request,$response) use ($className,$next){
			$class = 'App\Middleware\\' . ucfirst($className);
			$midlewareObject = new $class();
			return $midlewareObject->handle($request,$response,$next);
		};
	}
	
	protected function runMiddlewares($isGroup = false){

		if(!empty($this->middlewares)){
			/*
			* Megfordítjuk (Vagy inkább vissza) a tömböt,hogy sorban legyenek a middleware-ek (Az igazi első kerüljön előre)
			*/
			$middlewareArray = array_reverse($this->middlewares);

			/*
			* Vesszük az első Middleware-t
			*/
			$start = $middlewareArray[0];
			/*
			* Az elsőnek bedobjuk a Request,és a Response onjektumot
			*/
			$result = $start($this->request,$this->response);
		} else {
			$result = $this->createControllerCallable();
		}
		
		$this->middlewares = [];
		
		return (!$isGroup) ? $this->createResponse($result) : true;
	}	
}