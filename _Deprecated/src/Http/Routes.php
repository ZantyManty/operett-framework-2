<?php

namespace Csifo\Http;
use Csifo\Http\Interfaces\IRouter as IRouter;
use Csifo\Core\Interfaces\IConfig as IConfig;

class Routes implements Interfaces\IRoutes {
	
	protected $router = null;
	protected $routesFile = '';
	
	public function __construct(IRouter $router,IConfig $config){
		$this->router = $router;
		$this->routesFile = $config->app('routesFile');
	}
	
	public function handle(){
		$router = $this->router;
		include $this->routesFile;
		return $router->dispatch();
	}
	
}