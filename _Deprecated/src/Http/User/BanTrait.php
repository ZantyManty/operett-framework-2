<?php

namespace Csifo\Http\User;

use Csifo\Core\Interfaces\IConfig as IConfig;

trait BanTrait {
	
	protected $banAfter = 0;
	
	protected function setBan(IConfig $config){
		$banCount = $config->app('BanLoginAttempt');
		if($banCount){
			$this->banAfter = $banCount;
			if(!$this->has('ip')){
				$this->set('ip',$this->ip());
			}
			
			/**
			* Get information from cookie,if it exists
			*/
			$cookieData = $this->cookie->get('banCount');
			if($cookieData){
				$this->set('bannedCount',$cookieData);
			}
		}
	}
	
	public function isBannable(){
		if($this->has('bannedCount')){
			$count = $this->get('bannedCount');
			return ($count == $this->banAfter || $count > $this->banAfter) ? true : false;
		} else {
			return false;
		}
	}
	
	public function incraseBanCount(){
		if($this->has('bannedCount')){
			$count = $this->get('bannedCount');
			$this->set('bannedCount',$count + 1);
		} else {
			$this->set('bannedCount',1);
		}
	}
}