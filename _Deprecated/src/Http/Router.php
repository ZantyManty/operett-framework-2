<?php
namespace Csifo\Http;

use Csifo\Http\Interfaces\IRequest as IRequest;
use Csifo\Http\Interfaces\IResponse as IResponse;
use Csifo\Core\Resolver as Resolver;
use Csifo\Core\Interfaces\IConfig as IConfig;
use Csifo\Factory\Controller as ControllerFactory;
use Csifo\Cache\Interfaces\IDumper as IDumper;
use \Closure;

class Router implements Interfaces\IRouter {

	/*
	* Request object
	*/
	protected $request;
	
	/*
	* Response object
	*/
	protected $response;
	
	/**
	* Routes contaienr
	*/
	protected $routes = [];
	
	/*
	* Groups
	*/
	protected $groups = [];
	
	/*
	* Group counter
	*/
	protected $groupCounter = 0;
	
	/**
	* Methods
	*/
	protected $methods = [];
	
	/*
	* Cache object
	*/
	protected $cache;
	
	/*
	* Cache path
	*/
	protected $cachePath;
	
	/*
	* Types
	*/
	protected $types = ['any' => '[^/]+?','num' => '[\d]+?', 'str' => '[a-zA-Z_-]+?'];
	
	public function __construct(IRequest $request,IResponse $response,IDumper $cache,IConfig $config){
		$this->request = $request;
		$this->response = $response;
		$this->cache	= $cache;
		$this->cachePath = $config->app('CACHE_DIR');
	}
	
	protected function checkCache(){
		return is_file($this->cachePath . '/routes.php');
	}
	
	protected function createCache(){
		if(!$this->checkCache()){
			$arr = [$this->methods,$this->routes];
			$this->cache->cacheArray($this->cachePath . '/routes.php',$arr); 
		} else {
			$arr = include $this->cachePath . '/routes.php';
			$this->methods	= $arr[0];
			$this->routes	= $arr[1];
		}
	}
	
	/**
	* Add routes by types
	*/
	public function get($route,$call = '',$middleware = []){
		$this->createPattern('GET',$call,$route,$middleware);
	}
	
	public function post($route,$call = '',$middleware = []){
		$this->createPattern('POST',$call,$route,$middleware);
	}
	
	public function patch($route,$call = '',$middleware = []){
		$this->createPattern('PATCH',$call,$route,$middleware);
	}
	
	public function delete($route,$call = '',$middleware = []){
		$this->createPattern('DELETE',$call,$route,$middleware);
	}
	
	public function all($route,$call = '',$middleware = []){
		$this->createPattern('ALL',$call,$route,$middleware);
	}
	
	/**
	* Creates a regex pattern,and make the routes array with the parameters (Controller,method,etc..)
	*/
	private function createPattern($method,$call,$route,$middlewares){
		//if($this->checkCache()) return;
		
		
		
		//Let's create a pattern
		$route = trim($route,'/');
		$routes = explode('/',$route);
		
		$pattern = '#^';
		$num = 1;
		$params = [];
		
		foreach($routes as $rout){
			if(preg_match('#({.*})#i',$rout) == false){
				$pattern .= '\/(' . $rout . ')';
			} else {
				preg_match('#{(.*)}(?:\[:(any|num|str)\])?#i',$rout,$m);
				$paramName = $m[1];
				$pattern .= (isset($m[2])) ? '\/(' . $this->types[$m[2]] . ')' : '\/(' . $this->types['any'] . ')';
				$params[$paramName] = intval($num);
			}
			$num++;
		}
		$pattern .= '#i';
		//End of pattern creation
		
		if(isset($this->routes[$pattern])) return;
		
		$this->routes[$pattern]['params'] = (isset($params)) ? $params : [];
		/*
		* If the method is "ALL",we set it for all methods
		*/
		if($method == 'ALL'){
			$this->methods['GET'][] = $pattern;
			$this->methods['POST'][] = $pattern;
			$this->methods['PATCH'][] = $pattern;
			$this->methods['DELETE'][] = $pattern;
		} else {
			$this->methods[$method][] = $pattern;
		}
		
		if(!empty($call)){
			if(!is_callable($call)){
				$pos = strpos($call,'@');
				if($pos !== false){
					$this->routes[$pattern]['cont'] = ucfirst(strtolower(substr($call,0,$pos)));
					$this->routes[$pattern]['meth'] = substr($call,$pos + 1);
				} else {
					$this->routes[$pattern]['cont'] = ucfirst(strtolower($call));
					$this->routes[$pattern]['meth'] = 'index';
				}
			} else {
				//If this is a closure
				$this->routes[$pattern]['call'] = $call;
			}
		} else {
			$this->routes[$pattern]['cont'] = (isset($routes[0]) ? ucfirst(strtolower($routes[0])) : 'AlapContr');
			$this->routes[$pattern]['meth'] = (isset($routes[1]) ? $routes[1] : 'index');
		}
		
		/*
		* Middlewares
		*/
		if(!empty($middlewares)){
			/*
			* Megfordítjuk a tömböt,így az UTOLSÓ kerül ELŐRE,ami után ugye NINCS más.
			* A "$this->middlewares" üres,tehát nincs elötte (Mármint utána,de most fordítva van a tömb!!) 
			* elem,így a controller fog oda kerülni a $next-be.
			*/
			$mids = array_reverse($middlewares);
			/*
			* Végigmegyünk a már megfordított tömbön
			*/
			foreach($mids as $mid){
				$this->addMiddleware($mid,$pattern);
			}
		}
	}
	
	/*
	* Create the controller's resolving callabe for the last middleware
	*
	* @return callable
	*/
	protected function getController($pattern){
		$routeData = $this->routes[$pattern];
		/*
		* Return with the callable
		*/
		return function() use($routeData){
			if(is_callable($routeData['cont'])){
				//Coming soon
			} else {
				/*
				* Controller factory returns Controller object ($controller[0]) and Controller class string ($controller[1])
				*/
				$controller = ControllerFactory::make($routeData['cont'],$routeData['meth'],$routeData['params']);
				$parameters = resolver()->resolveOnlyParams($controller[1],$routeData['meth']);
				return call_user_func_array(array($controller[0],$routeData['meth']),$parameters);
			}
		};
	}
	
	/*
	* Groups
	*/
	public function group($middleware,$callable){
		$this->groupCounter++;
		$pattern = 'Group_' . (string)$this->groupCounter;
		$this->groups[$pattern] = $callable;
		
		if(is_array($middleware)){
			$middleware = array_reverse($middlewares);
			foreach($middleware as $mid)
				$this->addMiddleware($mid,$pattern,true);
		} else {
			$this->addMiddleware($middleware,$pattern,true);
		}
		
		$this->runMiddlewares($pattern);
	}
	
	/*
	* Run group's callable
	*/
	protected function runGroup($pattern){
		$callable = $this->groups[$pattern];
		$router = $this;
		return function() use ($callable,$router) {
			$callable($router);
		};
	}
	
	/*
	* Add a middleware (Create $next() callable)
	*/
	protected function addMiddleware($className,$pattern,$isGroup = false){
		/*
		* Ha üres a tömb,akkor ez az utolsó middleware
		*    (---!!!Ne feledjük,array_revers-elt tömb első elemét kapjuk,ami az utolsó middleware!!!---),
		* akkor ide már a Controller feloldása jön.
		*/
		$next = (empty($this->middlewares[$pattern])) ? ($isGroup == true) ? $this->runGroup($pattern) : $this->getController($pattern) : end($this->middlewares[$pattern]);
		
		$this->middlewares[$pattern][] = function($request,$response) use ($className,$next){
			$class = 'App\Middleware\\' . $className;
			$midlewareObject = new $class();
			return $midlewareObject->handle($request,$response,$next);
		};
	}
	
	/*
	* $paramNumbers	->	index numbers of the params in the url (Come from the pattern array)
	* $fetchedParams -> Fetched from the request URL
	* 
	*/
	public function normalizeParams($paramNumbers,$fetchedParams){
		$arguments = [];
		//We collect params,that we need from the exploded url array(In the "dispatch()" method,it's called $match)
		foreach($paramNumbers as $k => $v){
			$arguments[$k] = $fetchedParams[$v];
		}
		return $arguments;
	}
	
	/*
	* Run the middlewares
	*
	* @param $pattern		 array	|	The route's regex pattern. We need this,because is the route's key
	* @param $controllerData array  |	Controllers name,method,and the params for it
	*/
	protected function runMiddlewares($pattern){
		if(isset($this->middlewares[$pattern]) && !empty($this->middlewares[$pattern])){
			/*
			* Megfordítjuk (Vagy inkább vissza) a tömböt,hogy sorban legyenek a middleware-ek (Az igazi első kerüljön előre)
			*/
			$this->middlewares[$pattern] = array_reverse($this->middlewares[$pattern]);

			/*
			* Vesszük az első Middleware-t
			*/
			$start = $this->middlewares[$pattern][0];
			/*
			* Az elsőnek bedobjuk a Request,és a Response onjektumot
			*/
			$result = $start($this->request,$this->response);
		} else {
			$result = $this->getController($pattern);
		}
		
		//Aftermarket
		if($result instanceof IResponse){
			return $result;
		} else{
			$result = (is_callable($result)) ? $result() : $result;
			$stream = resolve('Csifo\Psr\Interfaces\IStream')->changeStream('php://memory','w+')->write($result);
			return $this->response->withBody($stream);
		}
	}
	
	public function dispatch(){
		$this->createCache();
		$url 	= $this->request->getRequestTarget();
		$method = $this->request->getMethod();

		if(!empty($this->methods[$method])){
			$patterns = $this->methods[$method];
			$count = count($patterns);
			for($i = 0; $i < $count; ++$i){
				if(preg_match($patterns[$i],$url,$match) === 1){
					$match[0] = $patterns[$i];
					$controllerData = $this->routes[$match[0]];
					$this->routes[$match[0]]['params'] = $this->normalizeParams($controllerData['params'],$match);
					return $this->runMiddlewares($match[0]);
				}
			}
		} else {
			$stream = resolve('Csifo\Psr\Interfaces\IStream')->changeStream('php://memory','w+')->write('Not Found!');
			return $this->response->withStatus(404)->withBody($stream);
		}
	}
}