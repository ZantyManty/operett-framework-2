<?php

namespace Csifo\Factory;

use Csifo\Factory\Interfaces\IController as IController;
use Csifo\Core\Interfaces\IConfig 		 as IConfig;
use Csifo\Core\App						 as WebApp;

class Controller implements IController {
	
	protected $triggers = [];
	protected $resolver;
	protected $cliTriggers;
	
	public function __construct(IConfig $config){
		$this->triggers = $config->app('ControllerTriggers');
		$this->cliTriggers = $config->app('ControllerTriggersCli');
		$this->resolver = resolver();
	}
	
	public function make($module,$method = 'index',$args = null){
		$module = ucfirst(strtolower($module));
		$controllerClass = 'App\\Controllers\\' . $module;
		$modelClass 	 = 'App\\Models\\' . $module;
		
		$preParams = [
			$controllerClass => [
					'init' => [
						'model' => new $modelClass(),
						'module' =>	$module,
						'action' => $method,
						'builder' => 'Csifo\Database\Orm\Interfaces\IQuery'
					],
			],
			'Csifo\Mvc\Interfaces\IView' => [
					'__construct' => [
						'module' =>	$module,
						'action' => $method
					]
				]
		];
		
		if(!is_null($args) === true){
			$preParams[$controllerClass][$method] = $args;
		}
		
		
		$this->resolver->addOptParams($preParams);
		
		/*
		* Prepare the controller
		*/
		$c = new $controllerClass();
		$parameters = $this->resolveParameters($controllerClass,'init');
		call_user_func_array(array($c,'init'),$parameters);
		/*
		* If we have triggers from the GLOBAL controller,run them
		*/
		$isCLI = WebApp::isCli();
		if(($isCLI == true && $this->cliTriggers == true) || $isCLI == false){
			foreach($this->triggers as $method){
				$parameters = $this->resolveParameters($controllerClass,$method);
				call_user_func_array(array($c,$method),$parameters);
			}
		}
		
		return [$c,$controllerClass];
	}
	
	protected function resolveParameters($class,$method){
		return $this->resolver->resolveOnlyParams($class,$method);
	}
}