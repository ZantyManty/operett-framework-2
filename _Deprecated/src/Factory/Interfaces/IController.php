<?php

namespace Csifo\Factory\Interfaces;

interface IController {
	
	public function make($module,$method = 'index',$args = null);
	
}