<?php

namespace Tests\Csifo\Grammar\Hun;

use PHPUnit\Framework\TestCase as TestCase;
use Csifo\Grammar\Hun\Plural as Plural;

final class PluralTest extends TestCase 
{
	public function testIt_can_make_non_plural_words(){
		$grammar = new Plural;
		
		$words = [
			'érmék'		=> 'érme',
			'halak'		=> 'hal',
			'ökrök'		=> 'ökör',
			'farkak'	=> 'farok',
			'szobrok'	=> 'szobor',
			'események'	=> 'esemény',
			'táblák'	=> 'tábla',
			'vadak'		=> 'vad',
			'kutyák'	=> 'kutya',
			'utak'		=> 'út',
			'urak'		=> 'úr',
			'nyulak'	=> 'nyúl',
			'kutak'		=> 'kút',
			'kezek'		=> 'kéz',
			'barmok'	=> 'barom',
			'izmok'		=> 'izom',
			'felhasználók'	=>	'felhasználó',
			'méhek'		=> 'méh',
			'férgek'	=> 'féreg',
			'mérgek'	=> 'méreg',
			'kérgek'	=> 'kéreg',
			'étkek'		=> 'étek',
			'vétkek'	=> 'vétek',
			'nevek'		=> 'név',
			'almák'		=> 'alma',
			'autók'		=> 'autó'
		];

		foreach($words as $plural => $singular){
			$result = $grammar->getNonPlural($plural);
			$excepted = $singular;
			
			$this->assertEquals($excepted,$result);
		}
		
		
	}
}