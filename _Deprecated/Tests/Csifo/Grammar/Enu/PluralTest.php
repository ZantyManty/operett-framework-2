<?php

namespace Tests\Csifo\Grammar\Enu;

use PHPUnit\Framework\TestCase as TestCase;
use Csifo\Grammar\Enu\Plural as Plural;

final class PluralTest extends TestCase 
{
	public function testIt_can_make_non_plural_words(){
		$plural = new Plural;
		$result = $plural->getNonPlural('balls');
		$excepted = 'ball';
		
		$this->assertEquals($excepted,$result);
	}
}