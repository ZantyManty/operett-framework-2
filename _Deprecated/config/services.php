<?php

/*
* Modules
*/

return [

	'container'	=>	[
		/*--- Websocket ---*/
		'Csifo\Http\Websocket\Interfaces\ISession'	=> Csifo\Http\Websocket\Session::class,
		'Csifo\Http\Websocket\Interfaces\IUser'		=> Csifo\Http\Websocket\User::class,
	
		/* --- Route --- */
		'Csifo\Http\Router\Interfaces\IRoute' => Csifo\Http\Router\Route::class,
		
		/* --- Migartion --- -*/
		'Csifo\Database\Migration\Interfaces\ISchema'	=> Csifo\Database\Migration\Schema::class,
		'Csifo\Database\Migration\Interfaces\ItableSchema'	=> Csifo\Database\Migration\tableSchema::class,
		
		/* --- Curl --- */
		'Csifo\Http\Curl\Interfaces\ICurl'	=>	Csifo\Http\Curl\Curl::class,
	],
	
	'singleton'	=>	[
		'Csifo\Http\Interfaces\ICookie'		=>	Csifo\Http\Cookie::class,
		'Csifo\Core\Interfaces\IConfig'		=>	Csifo\Core\Config::class,
		
		/* --- Websocket --- */
		'Csifo\Http\Websocket\Interfaces\IServer'	=> Csifo\Http\Websocket\BasicWsServer::class,
		
		/* --- Router --- */
		'Csifo\Http\Interfaces\IRouter'		=>	Csifo\Http\Router\Router::class,
		'Csifo\Http\Interfaces\IRoutes'		=>	Csifo\Http\Router\Facade\RouterShell::class,
		
		/* --- PSR-7 (Request objects) --- */
		'Csifo\Psr\Http\Message\IRequest'		=>	Csifo\Http\Message\Request::class,
		'Csifo\Psr\Http\Message\IServerRequest'	=>	Csifo\Http\Message\ServerRequest::class,
		'Csifo\Psr\Http\Message\IResponse'		=>	Csifo\Http\Message\Response::class,
		'Csifo\Psr\Http\Message\IUri'			=>	Csifo\Http\Message\Uri::class,
		'Csifo\Psr\Http\Message\IStream'		=>	Csifo\Http\Message\Stream::class,
		
		/* --- PSR-15 (Middlewares) --- */
		'Csifo\Psr\Http\Server\IRequestHandler'		=> Csifo\Http\Server\RequestHandler::class,
		
		/* --- Frontend --- */
		'Csifo\Client\Minify\Interfaces\ICss'		 =>	Csifo\Client\Minify\Css::class,
		'Csifo\Client\Minify\Interfaces\IJavascript' => Csifo\Client\Minify\Javascript::class,
		
		/* --- MVC --- */
		'Csifo\Mvc\Interfaces\ILang'		=>	Csifo\Mvc\Lang::class,
		'Csifo\Mvc\Interfaces\IView'		=>	Csifo\Mvc\View\View::class,
		
		/* --- Template --- */
		'Csifo\Mvc\View\Template\Interfaces\ITemplate'	=> Csifo\Mvc\View\Template\Template::class,
		
		/* --- Helpers --- */
		'Csifo\Helpers\Interfaces\IFileHelper'	=>	Csifo\Helpers\File::class,
		'Csifo\Helpers\Interfaces\IValidator'	=>	Csifo\Helpers\Validator::class,
		'Csifo\Helpers\Interfaces\IStringHelper'=>	Csifo\Helpers\StringHelper::class,
		
		/* --- Database --- */
		'Csifo\Database\Base\Interfaces\IConnection'	=> Csifo\Database\Base\Connection::class,
		'Csifo\Database\Base\Interfaces\IBasic'			=> Csifo\Database\Base\Basic::class,
		'Csifo\Database\Orm\Interfaces\IQuery'			=> Csifo\Database\Orm\Query::class,
		'Csifo\Database\Orm\Interfaces\IRelations'		=> Csifo\Database\Orm\Model\Relations::class,
		'Csifo\Database\Interfaces\IBridge'				=> Csifo\Database\Bridge::class,
		'Csifo\Database\Resolver\Interfaces\IResolver'	=> Csifo\Database\Resolver\Resolver::class,
		
		/* --- Console --- */
		'Csifo\Console\Interfaces\IConsole'			=> Csifo\Console\Console::class,
		
		/* --- APIs --- */
		'Csifo\Api\Flickr\Interfaces\IFlickr'		=> Csifo\Api\Flickr\Flickr::class,
		
		/*--- User --*/
		'Csifo\Http\User\Interfaces\IUserAgent' => Csifo\Http\User\UserAgent::class,
		'Csifo\Http\User\Interfaces\IUser'		=> Csifo\Http\User\User::class,
		'Csifo\Http\GeoIp\Interfaces\IGeoIp'	=> Csifo\Http\GeoIp\IpApi::class,
		
		/*
		* Simple cache (PSR-16)
		*/
		'Csifo\Cache\SimpleCache\Interfaces\ICache'	=> [
			Csifo\Cache\SimpleCache\Memcache::class,	//Memcache
			Csifo\Cache\SimpleCache\File::class,		//File
		],
		
		/*--- Security ---*/
		'Csifo\Http\Security\Interfaces\IXss' => Csifo\Http\Security\Xss::class,
	]
];