<?php

/**
* Hungarian language pack
* Class: Http\Message;
*/

return [
	'namespace' => ['Http\\Interfaces' => 'Mui\\Hu\\Http\\Interfészek'],
	'class'		=> ['IMessage' => 'Üzenet'],
	'methods'	=> [
		'getProtocol'	=>	'protokol',
		'getHeaders'	=> 'fejlécek',
		'getHeader'		=> 'fejléc',
		'getHeaderLine'	=> 'fejlécSor',
		'getBody'		=> 'törzs',
		'getProtocolVersion'	=> 'protokolVerzió',
		'hasHeader'				=> 'venFejléce',
		'withAddedHeader'		=> 'adottFejléccel',
		'withHeader'			=> 'fejléccel',
		'withProtocolVersion'	=> 'protokolVerzióval',
		'withBody'				=> 'törzzsel',
	],
];