<?php

/**
* Hungarian language pack
* Class: Http\Request;
*/

return [
	'namespace' => ['Http\\Interfaces' => 'Mui\\Hu\\Http\\Interfészek'],
	'class'		=> ['IRequest' => 'Kérés'],
	'extends'	=> [ 'Http\\Interfaces\\IMessage' => 'Http\\Message'],
	'methods'	=> [
		'user'			=>	'felhasználó',
		'getMethod'		=> 'típus',
		'withMethod'	=> 'típussal',
		'getRequestTarget'	=> 'kérésCélja',
		'withRequestTarget'	=> 'kérésCéllal',
		'getUri'			=> 'Url',
		'withUri'			=> 'Urllel'
	],
];