<?php

//Mui\Hu\Http\Felhasználó\Interfészek\Felhasználó

return [
	'namespace' => ['Http\\User\\Interfaces' => 'Mui\Hu\Http\Felhasználó\Interfészek'],
	'class'		=> ['IUser' => 'Felhasználó'],
	'methods'	=> [
		'ip'			=> 'ip',
		'rawAgent'		=> 'böngészőAdatok',
		'info'			=> 'információ',
		'ipInfo'		=> 'ipInformáció',
		'isAuthed'		=> 'bejelentkezve',
		'auth'			=> 'beléptetés',
		'has'			=> 'van-e'
	],
];